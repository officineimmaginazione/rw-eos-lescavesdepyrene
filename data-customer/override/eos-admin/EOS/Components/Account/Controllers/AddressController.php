<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class AddressController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $m = new \EOS\Components\Account\Models\AddressModel($this->container);
        $v->pageTitle = $v->trans('account.address.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('address/default');
    }

    public function indexb2c($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $m = new \EOS\Components\Account\Models\AddressModel($this->container);
        $v->pageTitle = $v->trans('account.address.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('address/defaultb2c');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('account.address.insert.title') : $v->trans('account.address.edit.title');
        $v->addBreadcrumb($this->path->urlFor('account', ['address', 'index']), $v->trans('account.address.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\Account\Models\AddressModel($this->container);
        $ma = new \EOS\Components\Account\Models\UserModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->agent = $ma->getUsersByGroup(2);
        $v->account = $ma->getUsersByGroup(0);
        $v->payment = $m->getPayment();
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('address/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Account\Models\AddressModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(1), $m->db);
        $dts->addColumn('id', 'a.id');
        $dts->addColumn('reference', 'a.reference', true);
        $dts->addColumn('alias', 'a.alias', true);
        $dts->addColumn('name', 'a.name', true);
        //$dts->addColumn('surname', 'a.surname', true);
        $dts->addColumn('company', 'a.company', true);
        $dts->addColumn('city', 'lc.name', true);
        $dts->addColumn('status', 'a.status', false);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxListB2C($request, $response, $args)
    {
        $m = new \EOS\Components\Account\Models\AddressModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(42), $m->db);
        $dts->addColumn('id', 'a.id');
        $dts->addColumn('reference', 'a.reference', true);
        $dts->addColumn('alias', 'a.alias', true);
        $dts->addColumn('name', 'a.name', true);
        //$dts->addColumn('surname', 'a.surname', true);
        $dts->addColumn('company', 'a.company', true);
        $dts->addColumn('city', 'lc.name', true);
        $dts->addColumn('status', 'a.status', false);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\AddressModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error)) {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'address/index'));
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\AddressModel($this->container);
            if ($m->deleteData($fv->getInputData())) {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('account', 'address/index'));
            } else {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }

    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue'))) {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Account\Models\AddressModel($this->container);
            switch ($datasource) {
                case 'user':
                    if ($format === 'select2') {
                        $filter = ArrayHelper::getStr($params, 'q');
                        $res['results'] = $m->searchList($filter);
                    }
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            $data = $fv->getInputData();
            switch (ArrayHelper::getStr($data, 'command')) {
                case 'city':
                    $m = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                    $fv->setResult(true);
                    $fv->setOutputValue('city', $m->getCity(ArrayHelper::getInt($data, 'id')));
                    break;
                default:
                    $fv->setMessage($this->lang->transEncode('system.common.error.invalidcommand'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }
}
