<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class AddressModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery($idgroup = 1)
    {
        if ($idgroup != 42) {
            $f = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
                ->select(null)
                ->select('a.id, a.alias, concat(a.name, " " ,a.surname) as name, a.company, lc.name, a.status, a.reference')
                ->leftJoin($this->db->tableFix('#__localization_city lc ON lc.id = a.id_city'))
                ->leftJoin($this->db->tableFix('#__localization_city lc ON lc.id = a.id_city'))
                ->leftJoin($this->db->tableFix('#__account_user_group aug ON a.id_customer = aug.id_user'))
                ->where('aug.id_group in (1, 2)');
        } else {
            $f = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
                ->select(null)
                ->select('a.id, a.alias, concat(a.name, " " ,a.surname) as name, a.company, lc.name, a.status, a.reference')
                ->leftJoin($this->db->tableFix('#__localization_city lc ON lc.id = a.id_city'))
                ->leftJoin($this->db->tableFix('#__localization_city lc ON lc.id = a.id_city'))
                ->leftJoin($this->db->tableFix('#__account_user_group aug ON a.id_customer = aug.id_user'))
                ->where('aug.id_group in (42)');
        }
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->where('a.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc)) {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else {
            $res['id'] = $rc['id'];
            $res['id-customer'] = $rc['id_customer'];
            $res['alias'] = $rc['alias'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['company'] = $rc['company'];
            $res['address1'] = $rc['address1'];
            $res['address2'] = $rc['address2'];
            $res['id-city'] = $rc['id_city'];
            $res['phone'] = $rc['phone'];
            $res['phone-mobile'] = $rc['phone_mobile'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['status'] = $rc['status'] == 1;
            $res['deleted'] = $rc['deleted'];
            $res['reference'] = $rc['reference'];
            $options = json_decode($rc['options'], true);
            $res['id-agent'] = $options['id_agent'];
            $res['payment'] = $options['payment'];
            $res['pec'] = $options['pec'];
            $res['unicode'] = $options['unicode'];
            $res['note'] = $options['note'];
            $res['coddes'] = $options['coddes'];
            $res['advisor'] = (isset($options['advisor'])) ? $options['advisor'] : 0;
            $res['lat'] = (isset($options['lat'])) ? $options['lat'] : '';
            $res['long'] = (isset($options['long'])) ? $options['long'] : '';
            $res['cscommondescription'] = (isset($options['cscommondescription'])) ? $options['cscommondescription'] : '';
            $res['cscreditlimit'] = (isset($options['cscreditlimit'])) ? $options['cscreditlimit'] : '';
            $res['cscommonvalue'] = (isset($options['cscommonvalue'])) ? $options['cscommonvalue'] : '';
            $res['csid'] = (isset($options['csid'])) ? $options['csid'] : '';
            $ml = new LocalizationModel($this->container);
            $current_city = $ml->getCity(ArrayHelper::getInt($rc, 'id_city'));
            $formatFunc = $ml->getFuncFormatCityStateText();
            $res['current-city'] = empty($current_city) ? [] : [$formatFunc($current_city)];
            //$res['status'] = (int) $rc['status'];
        }
        return $res;
    }

    public function getDataDiscount($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->where('a.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc)) {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else {
            $res['id'] = $rc['id'];
            $res['id-customer'] = $rc['id_customer'];
            $res['alias'] = $rc['alias'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['company'] = $rc['company'];
            $res['address1'] = $rc['address1'];
            $res['address2'] = $rc['address2'];
            $res['id-city'] = $rc['id_city'];
            $res['phone'] = $rc['phone'];
            $res['phone-mobile'] = $rc['phone_mobile'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['status'] = $rc['status'] == 1;
            $res['deleted'] = $rc['deleted'] == 1;
            $ml = new LocalizationModel($this->container);
            $current_city = $ml->getCity(ArrayHelper::getInt($rc, 'id_city'));
            $formatFunc = $ml->getFuncFormatCityStateText();
            $res['current-city'] = empty($current_city) ? [] : [$formatFunc($current_city)];
            $res['status'] = (int) $rc['status'];
            $tblAttribute = $this->db->tableFix('#__account_user_group aug');
            $query = $this->db->newFluent()->from($tblAttribute)
                ->select('cr.name as discount')
                ->innerJoin($this->db->tableFix('#__catalog_rules cr ON cr.id_group = aug.id_group'))
                ->where('aug.id_group', $rc['id']);
            $rca = $query->fetchAll();
            $i = 0;
            foreach ($rca as $ra) {
                $res['discount-' . $ra['id']] = $ra['discount'];
                $i++;
            }
        }
        return $res;
    }

    private function prepareData(&$single, &$error)
    {
        $res = true;


        return $res;
    }

    public function saveData(&$single, &$error)
    {
        if (!$this->prepareData($single, $error)) {
            return false;
        } else {
            $this->db->beginTransaction();
            $optionsvalues = [
                'id_agent' => $single['id-agent'],
                'payment' => $single['payment'],
                'pec' => $single['pec'],
                'unicode' => $single['unicode'],
                'note' => $single['note'],
                'coddes' => $single['coddes'],
                'advisor' => $single['advisor'],
                'lat' => $single['lat'],
                'long' => $single['long'],
                'cscommondescription' => $single['cscommondescription'],
                'cscreditlimit' => $single['cscreditlimit'],
                'cscommonvalue' => $single['cscommonvalue'],
                'csid' => $single['csid']
            ];
            $options = json_encode($optionsvalues);
            $values = [
                'id_customer' => $single['id-customer'],
                'alias' => $single['alias'],
                'name' => $single['name'],
                'surname' => $single['surname'],
                'address1' => $single['address1'],
                'address2' => $single['address2'],
                'id_city' => $single['id-city'],
                'id_country' => 1,
                'phone' => $single['phone'],
                'phone_mobile' => $single['phone-mobile'],
                'vat' => $single['vat'],
                'company' => $single['company'],
                'cif' => $single['cif'],
                'options' => $options,
                'reference' => $single['reference'] != "" ? $single['reference'] : null,
                'status' => ArrayHelper::getStr($single, 'status') === 'on' ? 1 : 0,
                'deleted' => $single['deleted'],
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__address');
            if ($single['id'] == 0) {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else {
                $values['id'] = $single['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $single['id']);
            }
            $query->execute();
            if ($single['id'] == 0) {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($single)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__account_user');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $single['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function searchList($filter, $limit = 50)
    {
        $res = [];
        if ((mb_strlen($filter) >= 1) && (mb_strlen($filter) < 200)) {
            $q = $this->getListQuery();
            $fValue = $this->db->quote('%' . $this->db->util->prepareLikeValue($filter) . '%');
            $autType = $this->getAuthType();
            $fList[] = 'u.id like ' . $fValue;
            $fList[] = 'u.name like ' . $fValue;
            switch ($autType) {
                case 1: // Email
                    $fList[] = 'u.email like ' . $fValue;
                    break;
                case 2: // Utente
                    $fList[] = 'u.username like ' . $fValue;
                    break;
                default: // 0 - 3 Utente o Email
                    $fList[] = 'u.email like ' . $fValue;
                    $fList[] = 'u.username like ' . $fValue;
                    break;
            }
            $q->where(implode("\n OR ", $fList));
            if ($limit > 0) {
                $q->limit($limit);
            }
            $res = $this->getTextList($q);
        }
        return $res;
    }


    public function getPayment()
    {
        $sql = 'select epa.id, epa.PADESCRI as name from #__ext_pag_amen epa ';
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
}
