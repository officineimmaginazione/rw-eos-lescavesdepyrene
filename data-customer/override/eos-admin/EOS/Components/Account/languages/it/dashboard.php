<?php

return
    [
        'account.dashboard.menu' => 'Account',
        'account.dashboard.user' => 'Account B2B',
        'account.dashboard.userb2c' => 'Account B2C',
        'account.dashboard.log' => 'Log',
        'account.dashboard.setting' => 'Impostazioni',
        'account.dashboard.widget' => 'Widget',
        'account.dashboard.address' => 'Indirizzi B2B',
        'account.dashboard.addressb2c' => 'Indirizzi B2C',
        'account.dashboard.group' => 'Gruppi account',
        'account.dashboard.invitation' => 'Inviti',
    ];
