<?php

if ($this->componentIsActive('account')) {
    $this->addParent('account', $this->view->trans('account.dashboard.menu'), '', 'fa fa-users', 30);
    $this->addItem('account', 'user', $this->view->trans('account.dashboard.user'), $this->view->path->urlFor('account', ['user', 'index']), 'fa fa-circle-o', null, 'account.user');
    $this->addItem('account', 'user', $this->view->trans('account.dashboard.userb2c'), $this->view->path->urlFor('account', ['user', 'indexb2c']), 'fa fa-circle-o', null, 'account.user');
    $this->addItem('account', 'address', $this->view->trans('account.dashboard.address'), $this->view->path->urlFor('account', ['address', 'index']), 'fa fa-circle-o', null, 'account.address');
    $this->addItem('account', 'address', $this->view->trans('account.dashboard.addressb2c'), $this->view->path->urlFor('account', ['address', 'indexb2c']), 'fa fa-circle-o', null, 'account.address');
    $this->addItem('account', 'group', $this->view->trans('account.dashboard.group'), $this->view->path->urlFor('account', ['group', 'index']), 'fa fa-circle-o', null, 'account.group');
    $this->addItem('account', 'invitation', $this->view->trans('account.dashboard.invitation'), $this->view->path->urlFor('account', ['user', 'invitation']), 'fa fa-circle-o', null, 'account.user');
    $this->addItem('account', 'log', $this->view->trans('account.dashboard.log'), $this->view->path->urlFor('account', ['log', 'index']), 'fa fa-circle-o', null, 'account.log');
    $this->addItem('account', 'section', $this->view->trans('account.dashboard.setting'), $this->view->path->urlFor('account', ['setting', 'index']), 'fa fa-circle-o', null, 'account.setting');
}
