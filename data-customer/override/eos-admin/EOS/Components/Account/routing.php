<?php

use \EOS\Components\System\Classes\AuthRole;

$this->mapComponent(['GET'], 'Account', ['user/index' => 'UserController:index'], ['auth' => ['account.user' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Account', ['user/indexb2c' => 'UserController:indexb2c'], ['auth' => ['account.user' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Account', ['user/edit/{id}' => 'UserController:edit'], ['auth' => ['account.user' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Account', ['user/ajaxlist' => 'UserController:ajaxList']);
$this->mapComponent(['POST'], 'Account', ['user/ajaxlistb2c' => 'UserController:ajaxListB2C']);
$this->mapComponent(['POST'], 'Account', ['user/ajaxsave' => 'UserController:ajaxSave'], ['auth' => ['account.user' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Account', ['user/ajaxdelete' => 'UserController:ajaxDelete'], ['auth' => ['account.user' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Account', ['user/ajaxrequestactive' => 'UserController:ajaxRequestActive'], ['auth' => ['account.user' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Account', ['user/ajaxrequestresetpass' => 'UserController:ajaxRequestResetPass'], ['auth' => ['account.user' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['GET'], 'Account', ['user/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'UserController:getDataSource'], ['auth' => ['account.user' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['GET'], 'Account', ['user/invitation' => 'UserController:invitation'], ['auth' => ['account.user' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Account', ['user/ajaxlistinvitation' => 'UserController:ajaxListInvitation']);

$this->mapComponent(['GET'], 'Account', ['log/index' => 'LogController:index'], ['auth' => ['account.user' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Account', ['log/ajaxlist' => 'LogController:ajaxList']);
$this->mapComponent(['POST'], 'Account', ['log/ajaxdelete' => 'LogController:ajaxDelete'], ['auth' => ['account.user' => [AuthRole::DELETE]]]);

$this->mapComponent(['GET'], 'Account', ['setting/index' => 'SettingController:index'], ['auth' => ['account.user' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Account', ['setting/ajaxsavedata' => 'SettingController:ajaxSaveData'], ['auth' => ['account.user' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);

$this->mapComponent(['GET'], 'Account', ['address/index' => 'AddressController:index'], ['auth' => ['account.address' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Account', ['address/indexb2c' => 'AddressController:indexb2c'], ['auth' => ['account.address' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Account', ['address/edit/{id}' => 'AddressController:edit'], ['auth' => ['account.address' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Account', ['address/ajaxlist' => 'AddressController:ajaxList'], ['auth' => ['account.address' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Account', ['address/ajaxlistb2c' => 'AddressController:ajaxListB2C'], ['auth' => ['account.address' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Account', ['address/ajaxsave' => 'AddressController:ajaxSave'], ['auth' => ['account.address' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Account', ['address/ajaxdelete' => 'AddressController:ajaxDelete'], ['auth' => ['account.address' => [AuthRole::DELETE]]]);
$this->mapComponent(['GET'], 'Account', ['address/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'LocalizationController:getDataSource'], ['auth' => ['account.address' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Account', ['address/ajaxcommand' => 'AddressController:ajaxCommand'], ['auth' => ['account.address' => [AuthRole::READ]]]);

$this->mapComponent(['GET'], 'Account', ['group/index' => 'GroupController:index'], ['auth' => ['account.group' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'Account', ['group/edit/{id}' => 'GroupController:edit'], ['auth' => ['account.group' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Account', ['group/ajaxlist' => 'GroupController:ajaxList'], ['auth' => ['account.group' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'Account', ['group/ajaxsave' => 'GroupController:ajaxSave'], ['auth' => ['account.group' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'Account', ['group/ajaxdelete' => 'GroupController:ajaxDelete'], ['auth' => ['account.group' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'Account', ['group/ajaxcommand' => 'GroupController:ajaxCommand'], ['auth' => ['account.group' => [AuthRole::READ]]]);
