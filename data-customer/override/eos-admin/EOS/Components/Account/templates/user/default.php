<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline main-content-header')->addContent(function () {
    (new \EOS\UI\Bootstrap\Button('btn-new'))
        ->attr('class', ' btn-success ml-auto mr-1')
        ->click('function (e) { addNew();}')
        ->content('Crea')
        ->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->getUrlFor('Account', 'user/ajaxlist'), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('account.user.field.id'));
$tbl->addColumn('reference', $this->transE('account.user.field.reference'));
$tbl->addColumn('username', $this->transE('account.user.field.username'));
$tbl->addColumn('email', $this->transE('account.user.field.email'));
$tbl->addColumn('name', $this->transE('account.user.field.name'));
$tbl->addColumn('last_access', $this->transE('account.user.field.last_access'));
$tbl->addColumn('lang', $this->transE('account.user.field.lang'));
//$tbl->addColumn('grouplist', $this->transE('account.user.field.group'), true, false);
$tbl->addColumn('status', $this->transE('account.user.field.status'));
$tbl->addColumn('advisor', $this->transE('account.user.field.advisor'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->addColumnOrder('username', true);
$tbl->onAjaxSend('function (data) {}');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->onColumnRender('advisor', 'function (data, type, full, meta) {return renderAdvisor(data);}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    function addNew() {
        window.location.href = "<?php echo $this->path->getUrlFor('account', 'user/edit/0') ?>";
    }

    function setLang(item) {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang) {
            id_lang = n_id_lang;
            $('#datatable').DataTable().ajax.reload();
        }
    }

    function editRow(id) {
        window.location.href = "<?php echo $this->path->getUrlFor('account', 'user/edit') ?>" + id + "/";
    }

    function renderStatus(data) {
        switch (parseInt(data)) {
            case 1:
                return '<span class="label label-success"><?php $this->transP('account.user.field.status.1'); ?></span>';
                break;
            case 2:
                return '<span class="label label-warning"><?php $this->transP('account.user.field.status.2'); ?></span>';
                break;
            default:
                return '<span class="label label-danger"><?php $this->transP('account.user.field.status.0'); ?></span>';
        }
    }

    function renderAdvisor(data) {
        var obj = JSON.parse(data.replace(/&quot;/g, '"'));
        switch (parseInt(obj.advisor)) {
            case 2:
                return '<span class="label label-warning">Rifiutato</span>';
                break;
            case 1:
                return '<span class="label label-success">Accettato</span>';
                break;
            case 2:
                return '<span class="label label-warning">Da verificare</span>';
                break;
            default:
                return '<span class="label label-danger">Non attivato</span>';
        }
    }
</script>
<?php
$this->endCaptureScript();
