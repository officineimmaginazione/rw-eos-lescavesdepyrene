<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class FreeRulesController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('lcdpcomp.freerules.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        return $v->render('freerules/default', true);
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\LCDPComp\Models\FreeRulesModel($this->container);
        $v = $this->newView($request, $response);
        $v->loadCKEditor();
        $v->loadiCheck();
        $v->loadSelect2();
        $v->addBreadcrumb($this->path->urlFor('lcdpcomp', ['free-rules', 'index']), $v->trans('lcdpcomp.freerules.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('lcdpcomp.freerules.insert.title') : $v->trans('lcdpcomp.freerules.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        return $v->render('freerules/edit', true);
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\LCDPComp\Models\FreeRulesModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(ArrayHelper::getInt($data, 'id_lang')), $m->db);
        $dts->addColumn('id', 'cr.id');
        $dts->addColumn('name', 'cr.name', true);
        $dts->addColumn('priority', 'cr.priority');
        $dts->addColumn('qty_calc', 'cr.qty_calc');
        $dts->addColumn('qty_free', 'cr.qty_free');
        $dts->addColumn('qty_free_mode', 'cr.qty_free_mode');
        $dts->addColumn('free_calc_type', 'cr.free_calc_type');
        $dts->addColumn('product_include', 'cr.product_include');
        $dts->addColumn('product_exclude', 'cr.product_exclude');
        $dts->addColumn('status', 'cr.status');
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\LCDPComp\Models\FreeRulesModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->urlFor('LCDPComp', ['free-rules', 'index']));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\LCDPComp\Models\FreeRulesModel($this->container);
            $error = '';
            if ($m->deleteData($fv->getInputData(), $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->urlFor('LCDPComp', ['free-rules', 'index']));
            } else
            {
                $fv->setMessage($error);
            }
        }

        return $fv->toJsonResponse();
    }

    public function ajaxTableList($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $params = $fv->getInputData();
            $m = new \EOS\Components\LCDPComp\Models\FreeRulesModel($this->container);
            $fv->setResult(true);
            $data = $m->getTableList(
                ArrayHelper::getStr($params, 'type'),
                ArrayHelper::getStr($params, 'search'),
                ArrayHelper::getArray($params, 'ids'));
            $fv->setOutputValue('count', count($data));
            $fv->setOutputValue('data', $data);
        }

        return $fv->toJsonResponse();
    }

}
