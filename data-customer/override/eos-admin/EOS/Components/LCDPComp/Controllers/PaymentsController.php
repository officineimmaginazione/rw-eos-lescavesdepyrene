<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class PaymentsController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('lcdpcomp.payments.index.title');
        $v->pageSubtitle = '';
        $v->addBreadcrumb('', $v->pageTitle, '');
        return $v->render('payments/default');
    }

    public function edit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadiCheck();
        $v->loadSelect2();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('lcdpcomp.payments.insert.title') : $v->trans('lcdpcomp.payments.edit.title');
        $v->addBreadcrumb($this->path->urlFor('lcdpcomp', ['payments', 'index']), $v->trans('lcdpcomp.payments.index.title'), '');
        $v->addBreadcrumb('', $v->pageTitle, '');
        $m = new \EOS\Components\LCDPComp\Models\PaymentsModel($this->container);
        $v->data = $m->getData($args['id']);
        $v->languages = $this->lang->getArrayFromDB(true);
        return $v->render('payments/edit');
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\LCDPComp\Models\PaymentsModel($this->container);
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery(), $m->db);
        $dts->addColumn('id', 'epa.id');
        $dts->addColumn('PADESCRI', 'epa.PADESCRI', true);
        $dts->addColumn('status', 'epa.status', false);
        $lang = $this->lang;
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\LCDPComp\Models\PaymentsModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveData($data, $error))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('lcdpcomp', 'payments/index'));
            } else
            {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxDelete($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $m = new \EOS\Components\LCDPComp\Models\PaymentsModel($this->container);
            if ($m->deleteData($fv->getInputData()))
            {
                $fv->setResult(true);
                $fv->setRedirect($this->path->getUrlFor('lcdpcomp', 'payments/index'));
            } else
            {
                $fv->setMessage($this->lang->trans('system.content.error'));
            }
        }

        return $fv->toJsonResponse();
    }
    
    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson())
        {
            $data = $fv->getInputData();
            switch (ArrayHelper::getStr($data, 'command'))
            {
                default :
                    $fv->setMessage($this->lang->transEncode('system.error.invalidcommand'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }

}
