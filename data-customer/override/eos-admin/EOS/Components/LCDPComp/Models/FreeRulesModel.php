<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Models;

use EOS\System\Util\ArrayHelper;

class FreeRulesModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery(int $id_lang)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__lcdp_cart_rule_free cr'))
            ->select(null)
            ->select('cr.id, cr.name, cr.priority, cr.free_calc_type, cr.qty_calc, cr.qty_free, cr.qty_free_mode,  cr.status, cr.product_include, cr.product_exclude')
            ->orderBy('cr.name');
        return $f;
    }

    private function getItemsList(int $idRule, int $linkType, string $itemType): array
    {

        $sqlDetail = 'select item_type, id_item_type  
              from #__lcdp_cart_rule_free_item
              where id_rule = :id_rule and link_type = :link_type and item_type = :item_type';
        $qDetail = $this->db->prepare($sqlDetail);
        $qDetail->bindValue(':id_rule', $idRule);
        $qDetail->bindValue(':link_type', $linkType);
        $qDetail->bindValue(':item_type', $itemType);
        $qDetail->execute();

        return $qDetail->fetchAll();
    }

    private function getItemsIDList(int $idRule, int $linkType, string $itemType): array
    {
        return array_column($this->getItemsList($idRule, $linkType, $itemType), 'id_item_type');
    }

    private function getTreeList(array $r, array $hashList, bool $loadName, int $level): array
    {
        if ($level > 99999)
        {
            throw new \Exception('Loop ricorsivo per estrapolare albero in FreeRulesModel ha superto' . $level . ' ricorsioni!');
        }
        $res = $loadName ? [$r['name']] : [(int) $r['id']];
        if ($r['parent'] !== 0)
        {
            if (isset($hashList[$r['parent']]))
            {
                $res = array_merge($this->getTreeList($hashList[$r['parent']], $hashList, $loadName, $level + 1), $res);
            }
        }
        return $res;
    }

    private function getCategoryTree(bool $loadName, bool $onlyActive)
    {
        $sql = 'select c.id, cl.name, c.parent, c.status
                  from #__product_category c
                  left join #__product_category_lang cl on cl.id_category = c.id and cl.id_lang = :id_lang
                  order by cl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $hashList = [];
        while ($r = $q->fetch())
        {
            $r['id'] = $r['id']; // Lascio stringa per Javascript
            $r['parent'] = (int) $r['parent'];
            $hashList[(int) $r['id']] = $r;
        }
        foreach ($hashList as &$r)
        {
            $treeList = $this->getTreeList($r, $hashList, $loadName, 0);
            if ($loadName)
            {
                $r['text'] = implode(' > ', $treeList);
            } else
            {
                $r['list'] = $treeList;
            }
        }
        unset($r);

        $res = array_values($hashList);
        if ($onlyActive)
        {
            $res = array_filter($res, function ($r)
            {
                return (int) $r['status'] === 1;
            });
        }

        if ($loadName)
        {
            usort($res, function ($a, $b)
            {
                $keyA = mb_strtolower($a['text']);
                $keyB = mb_strtolower($b['text']);
                if ($keyA == $keyB)
                {
                    return 0;
                }
                return ($keyA < $keyB) ? -1 : 1;
            });
        }

        return $res;
    }

    private function prepareData(array &$data, string &$error)
    {
        $res = true;


        return $res;
    }

    private function saveItemsList(array $data, int $linkType, string $itemType, string $fName, ?\Closure $filterProc = null)
    {
        $idRule = ArrayHelper::getInt($data, 'id');
        $value = ArrayHelper::getValue($data, $fName);
        $upList = empty($value) ? [] : (is_array($value) ? $value : [$value]);
        if (!is_null($filterProc))
        {
            $upList = $filterProc($upList);
        }
        $oldList = $this->getItemsIDList($idRule, $linkType, $itemType);
        foreach ($oldList as $oldID)
        {
            if (!in_array($oldID, $upList))
            {
                $sql = 'delete from #__lcdp_cart_rule_free_item
                    where id_rule = :id_rule and link_type = :link_type and item_type = :item_type and
                    id_item_type = :id_item_type';
                $qDel = $this->db->prepare($sql);
                $qDel->bindValue(':id_rule', $idRule);
                $qDel->bindValue(':link_type', $linkType);
                $qDel->bindValue(':item_type', $itemType);
                $qDel->bindValue(':id_item_type', $oldID);
                $qDel->execute();
            }
        }


        if (!empty($value))
        {
            $tbl = $this->db->tableFix('#__lcdp_cart_rule_free_item');
            foreach ($upList as $upID)
            {
                $values = [];
                $values['id_rule'] = $idRule;
                $values['link_type'] = $linkType;
                $values['item_type'] = $itemType;
                $values['id_item_type'] = $upID;
                $values['up_id'] = $this->user->getUserID();
                $values['up_date'] = $this->db->util->nowToDBDateTime();
                if (in_array($upID, $oldList))
                {
                    $query = $this->db->newFluent()
                        ->update($tbl)
                        ->set($values)
                        ->where('id_rule', $idRule)
                        ->where('link_type', $linkType)
                        ->where('item_type', $itemType)
                        ->where('id_item_type', $upID);
                } else
                {
                    $values['ins_id'] = $this->user->getUserID();
                    $values['ins_date'] = $this->db->util->nowToDBDateTime();
                    $query = $this->db->newFluent()->insertInto($tbl, $values);
                }
                $query->execute();
            }
        }
    }

    public function getData(int $id): array
    {
        $sql = 'select id, name, priority, free_calc_type, qty_calc, qty_free, qty_free_mode,
              product_include, product_exclude, status, date_start, date_end, change_product, stock_incoming
             from #__lcdp_cart_rule_free  
              where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetch();

        if (empty($res))
        {
            $res = [];
            $res['date_start'] = $this->lang->dateTimeToLangDate(\EOS\System\Util\DateTimeHelper::now());
            $res['date_end'] = $this->lang->dateTimeToLangDate((new \DateTimeImmutable)->setDate(9999, 12, 31));
            $res['priority'] = 1;
            $res['qty_calc'] = 1;
            $res['qty_free'] = 0;
            $res['qty_free_mode'] = 1;
            $res['_i_products'] = [];
            $res['_i_manufacturers'] = [];
            $res['_i_categories'] = [];
            //$res['_i_addresses'] = [];
            $res['_i_account'] = [];
            $res['_e_products'] = [];
            $res['_e_manufacturers'] = [];
            $res['_e_categories'] = [];
            //$res['_e_addresses'] = [];
            $res['_e_account'] = [];
        } else
        {
            $res['status'] = $res['status'] == 1;
            $res['free_calc_type'] = (int) $res['free_calc_type'];
            $res['qty_free_mode'] = (int) $res['qty_free_mode'];
            $res['date_start'] = $this->lang->dbDateToLangDate($res['date_start']);
            $res['date_end'] = $this->lang->dbDateToLangDate($res['date_end']);
            $res['product_include'] = (int) $res['product_include'] === 1;
            $res['product_exclude'] = (int) $res['product_exclude'] === 1;
            $res['change_product'] = (int) $res['change_product'] === 1;
            $res['stock_incoming'] = (int) $res['stock_incoming'] === 1;

            // Leggo sempre le inclusione
            $res['_i_products'] = $this->getItemsIDList($res['id'], 1, 'product');
            $res['_i_manufacturers'] = $this->getItemsIDList($res['id'], 1, 'manufacturer');
            $res['_i_categories'] = $this->getItemsIDList($res['id'], 1, 'category');
            //$res['_i_addresses'] = $this->getItemsIDList($res['id'], 1, 'address');
            $res['_i_account'] = $this->getItemsIDList($res['id'], 1, 'account');
            // Leggo sempre le esclusioni
            $res['_e_products'] = $this->getItemsIDList($res['id'], 2, 'product');
            $res['_e_manufacturers'] = $this->getItemsIDList($res['id'], 2, 'manufacturer');
            $res['_e_categories'] = $this->getItemsIDList($res['id'], 2, 'category');
            //$res['_e_addresses'] = $this->getItemsIDList($res['id'], 2, 'address');
            $res['_e_account'] = $this->getItemsIDList($res['id'], 2, 'account');
        }

        return $res;
    }

    public function saveData(array &$data, string &$error)
    {
        if (!$this->prepareData($data, $error))
        {
            return false;
        } else
        {
            $this->db->beginTransaction();
            $values = [
                'name' => $data['name'],
                'priority' => ArrayHelper::getInt($data, 'priority'),
                'free_calc_type' => ArrayHelper::getInt($data, 'free_calc_type'),
                'qty_calc' => ArrayHelper::getFloat($data, 'qty_calc'),
                'qty_free' => ArrayHelper::getFloat($data, 'qty_free'),
                'qty_free_mode' => ArrayHelper::getInt($data, 'qty_free_mode'),
                'product_include' => ArrayHelper::getBool($data, 'product_include') ? 1 : 0,
                'product_exclude' => ArrayHelper::getBool($data, 'product_exclude') ? 1 : 0,
                'change_product' => ArrayHelper::getBool($data, 'change_product') ? 1 : 0,
                'stock_incoming' => ArrayHelper::getBool($data, 'stock_incoming') ? 1 : 0,
                'date_start' => $this->lang->langDateToDbDate(ArrayHelper::getStr($data, 'date_start')),
                'date_end' => $this->lang->langDateToDbDate(ArrayHelper::getStr($data, 'date_end')),
                'status' => ArrayHelper::getBool($data, 'status') ? 1 : 0,
                'up_id' => $this->user->getUserID(),
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__lcdp_cart_rule_free');
            if ($data['id'] == 0)
            {
                $values['ins_id'] = $this->user->getUserID();
                $values['ins_date'] = $this->db->util->nowToDBDateTime();
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else
            {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0)
            {
                $data['id'] = $this->db->lastInsertId();
            }

            $procAddCatChild = function ($listOrig)
            {
                $res = $listOrig; // Copia
                $catList = $this->getCategoryTree(false, false);
                foreach ($listOrig as $r)
                {
                    $idCat = (int) $r;
                    // Ciclo le categorie
                    foreach ($catList as $ct)
                    {
                        // Prendo le categorie con dei padri
                        if ((count($ct['list']) > 1) && (!in_array($ct['id'], $res)))
                        {
                            // Se il mio record è nella lista è un padre e aggiungo il figlio
                            if (in_array($idCat, $ct['list']))
                            {
                                $res[] = (int) $ct['id'];
                            }
                        }
                    }
                }

                return $res;
            };

            $this->saveItemsList($data, 1, 'product', '_i_products');
            $this->saveItemsList($data, 1, 'manufacturer', '_i_manufacturers');
            $this->saveItemsList($data, 1, 'category', '_i_categories', $procAddCatChild);
            //$this->saveItemsList($data, 1, 'address', '_i_addresses');
            $this->saveItemsList($data, 1, 'account', '_i_account');

            $this->saveItemsList($data, 2, 'product', '_e_products');
            $this->saveItemsList($data, 2, 'manufacturer', '_e_manufacturers');
            $this->saveItemsList($data, 2, 'category', '_e_categories', $procAddCatChild);
            //$this->saveItemsList($data, 2, 'address', '_e_addresses');
            $this->saveItemsList($data, 2, 'account', '_e_account');
            $this->db->commit();
            return true;
        }
    }

    public function deleteData(array $data)
    {
        $this->db->beginTransaction();
        $tblDet = $this->db->tableFix('#__lcdp_cart_rule_free_item');
        $qDet = $this->db->newFluent()->deleteFrom($tblDet)->where('id_rule', (int) $data['id']);
        $qDet->execute();

        $tbl = $this->db->tableFix('#__lcdp_cart_rule_free');
        $query = $this->db->newFluent()->deleteFrom($tbl)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function getTableList(string $type, string $search = '', array $ids = [])
    {
        $res = [];
        // Converto ID in intero per evitare sql injection
        $filterIDs = implode(',', array_map(function ($id)
            {
                return (int) $id;
            }, $ids));
        switch ($type)
        {
            case 'product':
                if ($filterIDs === '' && $search === '')
                {
                    break;
                }

                $sql = 'select p.id, p.reference, pl.name
                  from #__product p
                  left join #__product_lang pl on pl.id_product = p.id and pl.id_lang = :id_lang
                  where 1=1 ';
                if ($filterIDs !== '')
                {
                    $sql .= ' and p.id in (' . $filterIDs . ') ';
                } else
                {
                    $sql .= ' and p.status = 1';
                }
                if ($search !== '')
                {
                    $sql .= ' and (pl.name like :name or p.reference like :reference)';
                }
                $sql .= ' order by pl.name, p.reference';
                $q = $this->db->prepare($sql);
                $q->bindValue(':id_lang', $this->lang->getCurrentID());
                if ($search !== '')
                {
                    $q->bindValue(':name', '%' . $search . '%');
                    $q->bindValue(':reference', '%' . $search . '%');
                }
                $q->execute();
                $res = [];
                while ($r = $q->fetch())
                {
                    $res[] = ['id' => $r['id'], 'text' => $r['name'] . ' [' . $r['reference'] . ']'];
                }
                break;
            case 'manufacturer':
                $sql = 'select m.id, ml.name as text
                  from #__manufacturer m
                  left join #__manufacturer_lang ml on ml.id_manufacturer = m.id and ml.id_lang = :id_lang
                  where m.status = 1
                  order by ml.name';
                $q = $this->db->prepare($sql);
                $q->bindValue(':id_lang', $this->lang->getCurrentID());
                $q->execute();
                $res = $q->fetchAll();
                unset($r);
                break;
            case 'category':
                $tmpList = $this->getCategoryTree(true, true);
                foreach ($tmpList as $r)
                {
                    $res[] = ['id' => $r['id'], 'text' => $r['text']];
                }
                break;
            case 'address':
                if ($filterIDs === '' && $search === '')
                {
                    break;
                }

                $sql = 'select ad.id, au.name, ad.alias, lc.name as city
                    from #__account_user au
                    inner join `#__address` ad on ad.id_customer = au.id
                    left join #__localization_city lc on lc.id = ad.id_city
                    where 1=1 ';
                if ($filterIDs !== '')
                {
                    $sql .= ' and ad.id in (' . $filterIDs . ') ';
                } else
                {
                    $sql .= ' and au.status = 1';
                }
                if ($search !== '')
                {
                    $sql .= ' and (au.name like :name or ad.alias like :alias or lc.name like :city)';
                }
                $sql .= '  order by au.name, ad.alias, lc.name';
                $q = $this->db->prepare($sql);
                if ($search !== '')
                {
                    $q->bindValue(':name', '%' . $search . '%');
                    $q->bindValue(':alias', '%' . $search . '%');
                    $q->bindValue(':city', '%' . $search . '%');
                }
                $q->execute();
                $res = [];
                while ($r = $q->fetch())
                {
                    $res[] = ['id' => $r['id'], 'text' => $r['name'] . ' - ' . $r['alias'] . ' - ' . $r['city']];
                }
                break;
            case 'account':
                $sql = 'select au.id, au.name as text
                    from #__account_user au
                    where au.name like :name
                    order by au.name';
                $q = $this->db->prepare($sql);
                $q->bindValue(':name', '%' . $search . '%');
                $q->execute();
                $res = $q->fetchAll();
                unset($r);
                break;
        }

        return $res;
    }

}
