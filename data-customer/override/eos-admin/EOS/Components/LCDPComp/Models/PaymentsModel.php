<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Models;

use EOS\System\Util\ArrayHelper;

class PaymentsModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__ext_pag_amen epa'))
            ->select(null)
            ->select('epa.id')
            ->select('epa.PADESCRI')
            ->select('epa.status')
            ->orderBy('epa.PADESCRI');
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__ext_pag_amen epa'))
            ->where('epa.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc)) {
            $res = [];
        } else {
            $res['id'] = $rc['id'];
            $res['PACODICE'] = $rc['PACODICE'];
            $res['PADESCRI'] = $rc['PADESCRI'];
            $res['PASCONTO'] = $rc['PASCONTO'];
            $res['status'] = $rc['status'] == 1;
            $res['status_b2b'] = $rc['status_b2b'] == 1;
            $res['status_b2c'] = $rc['status_b2c'] == 1;
        }
        return $res;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;


        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error)) {
            return false;
        } else {
            $this->db->beginTransaction();
            $values = [
                'PADESCRI' => $data['PADESCRI'],
                'PASCONTO' => ArrayHelper::getFloat($data, 'PASCONTO'),
                'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
                'status_b2b' => ArrayHelper::getInt($data, 'status_b2b'),
                'status_b2c' => ArrayHelper::getInt($data, 'status_b2c')
            ];
            $tblContent = $this->db->tableFix('#__ext_pag_amen epa');
            if ($data['id'] == 0) {
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
            } else {
                $values['id'] = $data['id'];
                $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
            }
            $query->execute();
            if ($data['id'] == 0) {
                $values['id'] = $this->db->lastInsertId();
            }
            $this->db->commit();
            return true;
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__ext_pag_amen');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }
}
