<?php

return
    [
        'lcdpcomp.payments.index.title' => 'Tipologie pagamenti',
        'lcdpcomp.payments.insert.title' => 'Inserimento pagamento',
        'lcdpcomp.payments.edit.title' => 'Modifica pagamento',
        'lcdpcomp.payments.tab.include' => 'Includi',
        'lcdpcomp.payments.tab.exclude' => 'Escludi',
        'lcdpcomp.payments.field.id' => 'ID',
        'lcdpcomp.payments.field.PACODICE' => 'Codice',
        'lcdpcomp.payments.field.PADESCRI' => 'Nome',
        'lcdpcomp.payments.field.PASCONTO' => 'Sconto',
        'lcdpcomp.payments.field.status' => 'Attivo',
        'lcdpcomp.payments.field.status.1' => 'Sì',
        'lcdpcomp.payments.field.status.0' => 'No',
        'lcdpcomp.payments.field.statusb2c' => 'Attivo B2C',
        'lcdpcomp.payments.field.statusb2c.1' => 'Sì',
        'lcdpcomp.payments.field.statusb2c.0' => 'No',
        'lcdpcomp.payments.field.statusb2b' => 'Attivo B2B',
        'lcdpcomp.payments.field.statusb2b.1' => 'Sì',
        'lcdpcomp.payments.field.statusb2b.0' => 'No',
    ];
