<?php

if ($this->componentIsActive('product'))
{
    $this->addItem('product', 'lcdpcomp.freerules', $this->view->trans('lcdpcomp.dashboard.freerules'), $this->view->path->urlFor('lcdpcomp', ['free-rules', 'index']), 'fa fa-circle-o', null, 'lcdpcomp.freerules');
}

if ($this->componentIsActive('order'))
{
    $this->addItem('order', 'lcdpcomp.payments', $this->view->trans('lcdpcomp.dashboard.payments'), $this->view->path->urlFor('lcdpcomp', ['payments', 'index']), 'fa fa-circle-o', null, 'lcdpcomp.payments');
}