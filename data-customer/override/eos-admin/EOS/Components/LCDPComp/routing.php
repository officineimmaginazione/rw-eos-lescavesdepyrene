<?php

use \EOS\Components\System\Classes\AuthRole;

$this->mapComponent(['GET'], 'LCDPComp', ['free-rules/index' => 'FreeRulesController:index'], ['auth' => ['lcdpcomp.freerules' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'LCDPComp', ['free-rules/edit/{id}' => 'FreeRulesController:edit'], ['auth' => ['lcdpcomp.freerules' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['free-rules/ajaxlist' => 'FreeRulesController:ajaxlist'], ['auth' => ['lcdpcomp.freerules' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['free-rules/ajaxsave' => 'FreeRulesController:ajaxSave'], ['auth' => ['lcdpcomp.freerules' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['free-rules/ajaxdelete' => 'FreeRulesController:ajaxDelete'], ['auth' => ['lcdpcomp.freerules' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['free-rules/ajaxtablelist' => 'FreeRulesController:ajaxTableList'], ['auth' => ['lcdpcomp.freerules' => [AuthRole::READ]]]);

$this->mapComponent(['GET'], 'LCDPComp', ['payments/index' => 'PaymentsController:index'], ['auth' => ['lcdpcomp.payments' => [AuthRole::READ]]]);
$this->mapComponent(['GET'], 'LCDPComp', ['payments/edit/{id}' => 'PaymentsController:edit'], ['auth' => ['lcdpcomp.payments' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['payments/ajaxlist' => 'PaymentsController:ajaxlist'], ['auth' => ['lcdpcomp.payments' => [AuthRole::READ]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['payments/ajaxsave' => 'PaymentsController:ajaxSave'], ['auth' => ['lcdpcomp.payments' => [AuthRole::READ, AuthRole::UPDATE, AuthRole::CREATE]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['payments/ajaxdelete' => 'PaymentsController:ajaxDelete'], ['auth' => ['lcdpcomp.payments' => [AuthRole::DELETE]]]);
$this->mapComponent(['POST'], 'LCDPComp', ['payments/ajaxcommand' => 'PaymentsController:ajaxCommand'], ['auth' => ['lcdpcomp.payments' => [AuthRole::READ]]]);