<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline main-content-header')->addContent(function () {
    (new \EOS\UI\Bootstrap\Button('btn-new'))
        ->attr('class', ' btn-success ml-auto mr-1')
        ->click('function (e) { addNew();}')
        ->content('Crea')
        ->printRender($this);
    if (isset($this->dataLangDefault)) {
        echo '<div class="dropdown">';
        $sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
        $sel->attr('class', 'btn-secondary text-uppercase');
        $sel->onChange('function(item) {changeLanguage(item);}');
        $sel->bind($this->dataLangDefault, 'id');
        $sel->bindList($this->dataLangList, 'id', 'iso_code', 'iso_code');
        $sel->dropDownMenuClasses(' dropdown-menu-right');
        $sel->printRender($this);
        echo '</div>';
    }
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('LCDPComp', ['free-rules', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('lcdpcomp.freerules.field.id'));
$tbl->addColumn('name', $this->transE('lcdpcomp.freerules.field.name'));
$tbl->addColumn('priority', $this->transE('lcdpcomp.freerules.field.priority'));
$tbl->addColumn('qty_calc', $this->transE('lcdpcomp.freerules.field.qtycalc'));
$tbl->addColumn('qty_free', $this->transE('lcdpcomp.freerules.field.qtyfree'));
$tbl->addColumn('qty_free_mode', '', false, false, false);
$tbl->addColumn('free_calc_type', $this->transE('lcdpcomp.freerules.field.freecalctype'));
$tbl->addColumn('product_include', $this->transE('lcdpcomp.freerules.field.productinclude'));
$tbl->addColumn('product_exclude', $this->transE('lcdpcomp.freerules.field.productexclude'));
$tbl->addColumn('status', $this->transE('lcdpcomp.freerules.field.status'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
//$tbl->onAjaxSend('function (data) {data.id_lang = id_lang}');
$tbl->onColumnRender('product_include', 'function (data, type, full, meta) {return renderFlag(data);}');
$tbl->onColumnRender('product_exclude', 'function (data, type, full, meta) {return renderFlag(data);}');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderFlag(data);}');
$tbl->onColumnRender('free_calc_type', 'function (data, type, full, meta) {return renderCalcType(data);}');
$tbl->onColumnRender('qty_free', 'function (data, type, full, meta) {return renderQtyFree(data, type, full, meta);}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>

    var id_lang = <?php echo $this->dataLangDefault['id']; ?>;
    function addNew()
    {
        window.location.href = "<?php echo $this->path->urlFor('LCDPComp', ['free-rules', 'edit', '0']) ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
            id_lang = n_id_lang;
            $('#datatable').DataTable().ajax.reload();
        }
    }

    function editRow(id)
    {
        window.location.href = "<?php echo $this->path->urlFor('LCDPComp', ['free-rules', 'edit']) ?>" + id + "/";
    }

    function renderFlag(data)
    {
        if (data == 1)
        {
            return '<span class="label label-success"><?php $this->transP('system.common.yes');?></span>';
        } else
        {
            return '<span class="label label-danger"><?php $this->transP('system.common.no');?></span>';
        }
    }

    function renderCalcType(data)
    {
        var list = {
            1: '<?php $this->transPE('lcdpcomp.freerules.field.freecalctype.1');?>',
            2: '<?php $this->transPE('lcdpcomp.freerules.field.freecalctype.2');?>',
            3: '<?php $this->transPE('lcdpcomp.freerules.field.freecalctype.3');?>'
        };
        return list[data];
    }

    function renderQtyFree(data, type, full, meta)
    {
        var res = data;
        switch(parseInt(full.qty_free_mode)) {
            case 2:
                res += ' % sp.' 
                break;
            case 3:
                res += ' %' 
                break;
            } 
        return res;
    }

</script>
<?php
$this->endCaptureScript();
