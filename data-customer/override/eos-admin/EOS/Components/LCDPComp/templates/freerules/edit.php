<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function ()
{
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    if ((isset($this->data['id'])) && ($this->data['id'] != 0))
    {
        (new \EOS\UI\Bootstrap\Button('btn-delete'))->content($this->transE('system.common.delete'))->attr('class', 'btn-danger btn-eos mr-1')->click('function (e) {e.preventDefault(); deleteData();}')->printRender($this);
    }

    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function ()
    {
        (new \EOS\UI\Html\Label)->content($this->transE('content.content.field.status'))->printRender();
        (new \EOS\UI\Bootstrap\Checkbox('status'))
            ->bind($this->data, 'status')
            ->printRender($this);
    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-standard', $this->trans('system.common.standard'));
$tab->addItem('tab-include', $this->trans('lcdpcomp.freerules.tab.include'));
$tab->addItem('tab-exclude', $this->trans('lcdpcomp.freerules.tab.exclude'));

$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.name'))->printRender();
    echo '</div>';
    echo '<div>';
    (new \EOS\UI\Bootstrap\Input('name'))->bind($this->data, 'name')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.priority'))->printRender();
    echo '</div>';
    echo '<div>';
    (new \EOS\UI\Bootstrap\Input('priority'))->type('number')->bind($this->data, 'priority')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.datestart'))->printRender();
    echo '</div>';
    echo '<div>';
    (new \EOS\UI\Bootstrap\Input('date_start'))->bind($this->data, 'date_start')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.dateend'))->printRender();
    echo '</div>';
    echo '<div>';
    (new \EOS\UI\Bootstrap\Input('date_end'))->bind($this->data, 'date_end')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.qtycalc'))->printRender();
    echo '</div>';
    echo '<div>';
    (new \EOS\UI\Bootstrap\Input('qty_calc'))->type('number')->bind($this->data, 'qty_calc')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.qtyfree'))->printRender();
    echo '</div>';
    echo '<div>';
    (new \EOS\UI\Bootstrap\Input('qty_free'))->type('number')->bind($this->data, 'qty_free')->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.qtyfreemode'))->printRender();
    echo '</div>';
    echo '<div>';
    $calcList = [];
    $calcList[] = ['id' => 1, 'name' => $this->trans('lcdpcomp.freerules.field.qtyfreemode.1')];
    $calcList[] = ['id' => 2, 'name' => $this->trans('lcdpcomp.freerules.field.qtyfreemode.2')];
    $calcList[] = ['id' => 3, 'name' => $this->trans('lcdpcomp.freerules.field.qtyfreemode.3')];
    (new \EOS\UI\Bootstrap\Select('qty_free_mode'))
        ->bind($this->data, 'qty_free_mode')
        ->bindList($calcList, 'id', 'name', false)
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div class="form-group-title">';
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.freecalctype'))->printRender();
    echo '</div>';
    echo '<div>';
    $calcList = [];
    $calcList[] = ['id' => 1, 'name' => $this->trans('lcdpcomp.freerules.field.freecalctype.1')];
    $calcList[] = ['id' => 2, 'name' => $this->trans('lcdpcomp.freerules.field.freecalctype.2')];
    $calcList[] = ['id' => 3, 'name' => $this->trans('lcdpcomp.freerules.field.freecalctype.3')];

    (new \EOS\UI\Bootstrap\Select('free_calc_type'))
        ->bind($this->data, 'free_calc_type')
        ->bindList($calcList, 'id', 'name', false)
        ->printRender($this);
    echo '</div>';
}, $this)->printRender();
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div>';
    (new \EOS\UI\Bootstrap\Checkbox('change_product'))
        ->bind($this->data, 'change_product')
        ->printRender($this);
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.changeproduct'))->printRender();
    echo '</div>';
}, $this)->printRender();

(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div>';
    (new \EOS\UI\Bootstrap\Checkbox('stock_incoming'))
        ->bind($this->data, 'stock_incoming')
        ->printRender($this);
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.stockincoming'))->printRender();
    echo '</div>';
}, $this)->printRender();

$tab->endTab('tab-standard');


$drawList = function (array $data, string $name, string $type, string $label, bool $asyncFetch)
{
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () use ($data, $name, $type, $label, $asyncFetch)
    {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE($label))->printRender();
        echo '</div>';
        $select = (new \EOS\UI\Bootstrap\Select2($name))
            ->multiple()
            ->attr('class', 'selection-list')
            ->attr('data-type', $type)
            ->attr('data-async-fetch', $asyncFetch ? 'true' : 'false');
        if ($asyncFetch)
        {
            $select->addRawParams('minimumInputLength', 2);
            $select->addRawParams('ajax', 'getFilterAjax(\''.$type.'\')');
        }
        foreach ($data[$name] as $id)
        {
            $select->addOption($id, $id, true);
        }
        $select->printRender($this);
    }, $this)->printRender($this);
};

$tab->startTab();

//$drawList($this->data, '_i_addresses', 'address', 'lcdpcomp.freerules.field.include_address_list', true);
$drawList($this->data, '_i_account', 'account', 'lcdpcomp.freerules.field.include_account_list', true);
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () use ($drawList)
{
    (new \EOS\UI\Bootstrap\Checkbox('product_include'))
        ->bind($this->data, 'product_include')
        ->printRender($this);
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.productinclude'))->printRender();
}, $this)->printRender();

$drawList($this->data, '_i_products', 'product', 'lcdpcomp.freerules.field.include_product_list', true);
$drawList($this->data, '_i_manufacturers', 'manufacturer', 'lcdpcomp.freerules.field.include_manufacturer_list', false);
$drawList($this->data, '_i_categories', 'category', 'lcdpcomp.freerules.field.include_category_list', false);

$tab->endTab('tab-include');

$tab->startTab();
//$drawList($this->data, '_e_addresses', 'address', 'lcdpcomp.freerules.field.exclude_address_list', true);
$drawList($this->data, '_e_account', 'account', 'lcdpcomp.freerules.field.exclude_account_list', true);
(new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function ()
{
    echo '<div>';
    (new \EOS\UI\Bootstrap\Checkbox('product_exclude'))
        ->bind($this->data, 'product_exclude')
        ->printRender($this);
    (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.freerules.field.productexclude'))->printRender();
    echo '</div>';
}, $this)->printRender();


$drawList($this->data, '_e_products', 'product', 'lcdpcomp.freerules.field.exclude_product_list', true);
$drawList($this->data, '_e_manufacturers', 'manufacturer', 'lcdpcomp.freerules.field.exclude_manufacturer_list', false);
$drawList($this->data, '_e_categories', 'category', 'lcdpcomp.freerules.field.exclude_category_list', false);

$tab->endTab('tab-exclude');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);

$this->startCaptureScript();
?>

<script>
    function saveData()
    {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->urlFor('LCDPComp', ['free-rules', 'ajaxsave']); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    location.href = json.redirect;
                } else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData()
    {
        window.location.href = "<?php echo $this->path->urlFor('LCDPComp', ['free-rules', 'index']); ?>";
    }

    function deleteData()
    {
        bootbox.confirm("<?php $this->transPE('lcdpcomp.freerules.delete.confirm') ?>", function (result)
        {
            if (result)
            {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '<?php echo $this->path->urlFor('LCDPComp', ['free-rules', 'ajaxdelete']); ?>',
                    data: JSON.stringify({"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>", "id": $('#data-id').val()}),
                    contentType: 'application/json'
                })
                    .done(function (json)
                    {
                        if (json.result == true)
                        {
                            location.href = json.redirect;
                        } else
                        {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function (jqxhr, textStatus, error)
                    {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    function getFilterAjax(type)
    {
        var res = {
            url: '<?php echo $this->path->urlFor('LCDPComp', ['free-rules', 'ajaxtablelist']); ?>',
            type: 'POST',
            dataType: 'json',
            delay: 500,
            contentType: 'application/json',
            data: function (params)
            {
                var rData = {
                    type: type,
                    search: params.term,
                    "<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>"
                };
                return JSON.stringify(rData);
            },
            processResults: function (data)
            {
                return {
                    results: data.data
                };
            }
        };
        return res;
    }

    function loadFilter()
    {
        $('.selection-list').each(function ()
        {
            var jSelect = $(this);
            var aData = {"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>",
                "type": jSelect.attr('data-type')};
            if (jSelect.attr('data-async-fetch'))
            {
                var currentList = [];
                jSelect.find("option:selected").each(function ()
                {
                    currentList.push(this.value);
                });
                aData.ids = currentList;
            }
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('LCDPComp', ['free-rules', 'ajaxtablelist']); ?>',
                data: JSON.stringify(aData),
                contentType: "application/json"
            })
                .done(function (json)
                {
                    if (json.result == true)
                    {
                        var currentList = [];
                        jSelect.find("option:selected").each(function ()
                        {
                            currentList.push(this.value);
                        });
                        jSelect.html('');

                        json.data.forEach(function (el)
                        {
                            // Create a DOM Option and pre-select by default
                            var newOption = new Option(el.text, el.id, false, currentList.indexOf(el.id) >= 0);
                            // Append it to the select
                            jSelect.append(newOption);
                        });
                        json.data = [];
                        jSelect.trigger('change');
                    } else
                    {
                        bootbox.alert(json.message);
                    }
                })
                .fail(function (jqxhr, textStatus, error)
                {
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });
    }

    $(function ()
    {
        setTimeout(function ()
        {
            loadFilter();
        }, 100);
    });

</script>
<?php
$this->endCaptureScript();

$this->startCaptureScriptReady();
?>
// Fix select 2
$('.select2-container').css('width', '100%');
<?php
$this->endCaptureScriptReady();
