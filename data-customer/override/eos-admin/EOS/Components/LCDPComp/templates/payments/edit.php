<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function () {
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function () {
        (new \EOS\UI\Html\Label)->content($this->transE('content.content.field.status'))->printRender();
        (new \EOS\UI\Bootstrap\Checkbox('status'))->attr('name', 'status')
            ->bind($this->data, 'status')
            ->printRender($this);
    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cancel'))->content($this->transE('system.common.cancel'))->attr('class', 'btn-default btn-eos ')->click('function (e) {e.preventDefault(); cancelData();}')->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->startTab();

(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.payments.field.PACODICE'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('PACODICE'))->attr('name', 'PACODICE')
            ->bind($this->data, 'PACODICE')
            ->attr('disabled', '')->printRender($this);
        echo '</div>';
?>
    <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.payments.field.PADESCRI'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('PADESCRI'))->attr('name', 'PADESCRI')
            ->bind($this->data, 'PADESCRI')
            ->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.payments.field.PASCONTO'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('PASCONTO'))->attr('name', 'PASCONTO')
            ->bind($this->data, 'PASCONTO')
            ->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
    <?php

}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.payments.field.statusb2c'))->printRender();
        echo '</div>';
        echo '<div>';
        $statusB2b = [];
        $statusB2b[] = ['id' => 1, 'name' => $this->trans('lcdpcomp.payments.field.statusb2c.1')];
        $statusB2b[] = ['id' => 0, 'name' => $this->trans('lcdpcomp.payments.field.statusb2c.0')];

        (new \EOS\UI\Bootstrap\Select('status_b2c'))
            ->bind($this->data, 'status_b2c')
            ->bindList($statusB2b, 'id', 'name', false)
            ->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('lcdpcomp.payments.field.statusb2b'))->printRender();
        echo '</div>';
        echo '<div>';
        $statusB2b = [];
        $statusB2b[] = ['id' => 1, 'name' => $this->trans('lcdpcomp.payments.field.statusb2b.1')];
        $statusB2b[] = ['id' => 0, 'name' => $this->trans('lcdpcomp.payments.field.statusb2b.0')];

        (new \EOS\UI\Bootstrap\Select('status_b2b'))
            ->bind($this->data, 'status_b2b')
            ->bindList($statusB2b, 'id', 'name', false)
            ->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();

    echo '</div>';
    ?>
<?php

}, $this)->printRender();

$tab->endTab('tab-data');

$tab->printRender($this);
$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData(proc) {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('lcdpcomp', 'payments/ajaxsave'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    if (proc === undefined) {

                        location.href = json.redirect;
                    } else {
                        proc();
                    }
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData() {
        window.location.href = "<?php echo $this->path->getUrlFor('lcdpcomp', 'payments/index'); ?>";
    }

    function deleteData() {
        bootbox.confirm("<?php $this->transPE('lcdpcomp.payments.delete.confirm') ?>", function(result) {
            if (result) {
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '<?php echo $this->path->getUrlFor('lcdpcomp', 'payments/ajaxdelete'); ?>',
                        data: JSON.stringify({
                            "<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>",
                            "id": $('#data-id').val()
                        }),
                        contentType: "application/json",
                    })
                    .done(function(json) {
                        if (json.result == true) {
                            location.href = json.redirect;
                        } else {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function(jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }

    $(function() {
        $('#status').on('change', function() {
            if ($('#status').val() == '2') {
                $('#btn-request-active').show();
            } else {
                $('#btn-request-active').hide();
            }
        });
        $('#status').trigger('change');
    });

    function runAjax(urlAjax, data, resultEvent) {
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: urlAjax,
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    if (json.message !== undefined) {
                        bootbox.alert(json.message);
                    }
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                bootbox.alert(err);
            });
    }
</script>
<?php
$this->endCaptureScript();
