<?php

namespace EOS\Components\LCDPImpExp\Classes;

class ErrorUtil extends \EOS\System\Models\Model
{

    private $adminList;

    public function __construct($container)
    {
        parent::__construct($container);
        $str = $this->db->setting->getStr('nwimpexp', 'admin.mail', '');
        $this->adminList = $str == '' ? [] : explode(',', $str);
    }

    public function getSiteName(): string
    {
        $path = $this->container->get('path');
        return str_replace(['https://', 'http://'], '', $path->getDomainUrl());
    }

    public function sendMessage(string $subject, string $body)
    {
        try
        {
            foreach ($this->adminList as $m)
            {
                $msg = new \EOS\System\Mail\Message($this->container);
                $msg->to = $m;
                $msg->subject = $subject;
                $msg->content = $body;
                $msg->send();
            }
        } catch (\Exception $e)
        {
            // Se ho i dati sbagliati delle mail ecc...non blocco il processo principale
            error_log('Errore durante l\'invio della mail:' . $e->getMessage() . ' trace: ' . $e->getTraceAsString());
        }
    }

    public function captureException(\Closure $proc)
    {
        try
        {
            $proc();
        } catch (\Exception $e)
        {
            $this->sendMessage($this->getSiteName() . ' | Errore imprevisto', $e->getMessage());
            throw $e;
        }
    }

}
