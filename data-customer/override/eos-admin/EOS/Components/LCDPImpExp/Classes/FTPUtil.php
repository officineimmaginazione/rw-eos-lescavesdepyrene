<?php

namespace EOS\Components\LCDPImpExp\Classes;

class FTPUtil extends \EOS\System\Models\Model
{

    public $connection = null;
    public $ftpMode = \FTP_BINARY;

    public function open()
    {
        $this->close();
        //$ftpHost = $this->db->setting->getStr('nwimpexp', 'ftp.host', 'ftp.arianna.org');
        //$ftpUser = $this->db->setting->getStr('nwimpexp', 'ftp.username', '8023014300029');
        //$ftpPassword = $this->db->setting->getStr('nwimpexp', 'ftp.password', 'uAqjguJq');
        //$ftpSSL = $this->db->setting->getBool('nwimpexp', 'ftp.ssl', false); 

        $this->connection = $ftpSSL ? ftp_ssl_connect($ftpHost) : ftp_connect($ftpHost);
        if ($this->connection === FALSE)
        {
            $this->connection = null;
            throw new \Exception('Impossibile collegari al server FTP!');
        }
        if (!ftp_login($this->connection, $ftpUser, $ftpPassword))
        {
            throw new \Exception('Credenziali FTP non valide!');
        }
    }

    public function close()
    {
        if (!is_null($this->connection))
        {
            ftp_close($this->connection);  
        }
        $this->connection = null;
    }

    public function __destruct()
    {
        $this->close();
    }

    public function getList(string $directory, bool $sort = true): array
    {
        $list = ftp_nlist($this->connection, $directory);
        if ($sort)
        {
            sort($list);
        }
        return $list;
    }

    public function getSizeStr(string $filename): string
    {
        $size = ftp_size($this->connection, $filename);
        if ($size > 0)
        {
            $size = round($size / 1024, 3);
        }
        return $size . ' KB';
    }

    public function getFile(string $localFile, string $remoteFile): bool
    {
        return ftp_get($this->connection, $localFile, $remoteFile, $this->ftpMode);
    }

}
