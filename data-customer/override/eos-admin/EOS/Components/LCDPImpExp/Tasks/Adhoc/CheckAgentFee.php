<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class CheckAgentFee extends \EOS\System\Tasks\Task
{

    protected function load()
    {
        parent::load();
        $this->debug = true;
        $this->params['name'] = 'Check Agent Fee';
    }

    public function run()
    {
        $this->sendCashbackResume();
    }

    public function sendCashbackResume()
    {
        $diffList = [];
        $agents = $this->getAgent();
        foreach ($agents as $agent) {
            $options = json_decode($agent['options'], true);
            if (isset($options['fee']) && (float) $options['fee'] > 0) {
                $sDiff = sprintf('Agente: %s - Fee: %s €', $agent['name'], $options['fee'], ' - Fee da fatturare');
                $diffList[] = $sDiff;
                $this->historyCashback($agent['id'], $options['fee'], 0, 0, 4);
                $this->updateAgent($agent['id'], $agent['options']);
            }
        }
        $sendMail = $this->db->setting->getStr('lcdp', 'shop.email', '');
        //$sendMail2 = $this->db->setting->getStr('lcdp', 'check.mail2', '');
        //$sendMail3 = $this->db->setting->getStr('lcdp', 'check.mail3', '');
        if (!empty($diffList)) {

            // Invia Email
            $subject = 'Riepilogo fee agent : ' . count($diffList);
            $this->addLog($subject);
            $mail = new \EOS\System\Mail\Message($this->container);
            $mail->subject = $subject;
            $mail->to = $sendMail;
            //$mail->cc = $sendMail2;
            //$mail->bcc = $sendMail3;
            foreach ($diffList as $sDiff) {
                $mail->content .= $sDiff . "<br>\n";
            }
            $mail->send();
        }
    }

    public function getAgent(): array
    {
        $select = 'SELECT au.id, au.username, au.email, au.name, au.options ';
        $from = ' FROM `eos_account_user` as au ';
        $inner = ' Left OUTER join eos_account_user_group as aug on aug.id_user = au.id ';
        $where = ' WHERE aug.id_group = 2 ';
        $order = '  ';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $list = $q->fetchAll();
        return $list;
    }

    public function updateAgent($id, $options)
    {
        $tbl = $this->db->tableFix('#__account_user');
        $dataoptions = json_decode($options, true);
        $dataoptions['fee'] = 0;
        $values['options'] = json_encode($dataoptions);
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', $id);
        $query->execute();
    }

    public function historyCashback($id_user, $cashback, $level, $total, $status = 0)
    {
        $tbl = $this->db->tableFix('#__ext_cashback_history');
        $data['id_cart'] = 0;
        $data['id_user'] = $id_user;
        $data['cashback'] = -1 * (float) $cashback;
        $data['level'] = $level;
        $data['status'] = $status;
        $data['cart_total'] = $total;
        $data['ins_date'] = new \FluentLiteral('NOW()');
        $query = $this->db->newFluent()->insertInto($tbl, $data);
        $query->execute();
    }
}
