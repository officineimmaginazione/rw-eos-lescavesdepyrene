<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class CheckDataProduct extends \EOS\System\Tasks\Task
{

    protected function load()
    {
        parent::load();
        $this->debug = true;
        $this->params['name'] = 'Check Product';
    }
    
    public function run()
    {
        $this->checkProduct();
    }

    public function checkProduct()
    {
        $diffList = [];
        $yearbefore = 0;
        $yearafter = date("y");
        $products = $this->getProduct();
        $tot = 0;
        $totd = 0;
        $category = array(2, 3, 4, 5, 11, 15, 17);
        foreach ($products as $product)
        {
            //$sample = mb_substr($product['reference'], 5, 1);
            if($product['online_only'] == 1) {
                if(strpos($product['reference'], '.') === false && strpos($product['reference'], '#') === false) {
                    if($product['quantity_available'] < 1) {
                        if(in_array($product['id_category'], $category) && $product['type_condition'] == 0) {
                            //$this->addLog('Product Ref (NO QTA): ' . $product['reference']);
                            if(strlen($product['reference']) == 7) {
                                $codprod = mb_substr($product['reference'], 0, 5);
                                $vintageprod = mb_substr($product['reference'], -2);
                            }
                            if(strlen($product['reference']) == 8) {
                                $codprod = mb_substr($product['reference'], 0, 6);
                                $vintageprod = mb_substr($product['reference'], -2);
                            }
                            if(strlen($product['reference']) == 9) {
                                $codprod = mb_substr($product['reference'], 0, 7);
                                $vintageprod = mb_substr($product['reference'], -2);
                            }
                            if(is_numeric($vintageprod)) {
                                $vintagestart = $vintageprod;
                                $vintageprod++;
                                $checknewproduct = true;
                                while ($vintageprod <= $yearafter) {
                                    if($this->checkVintageProduct($codprod.$vintageprod) == true) {
                                        $checknewproduct = false;
                                        break;
                                    } 
                                    $vintageprod++;
                                }
                                $vintageprod = $vintagestart;
                                $vintageprod--;
                                $checkvintage = 0;
                                while ($vintageprod >= $yearbefore) {
                                    if($this->checkVintageProduct($codprod.$vintageprod, false) == true) {
                                        $checkvintage = 1;
                                        break;
                                    } else {
                                        $checkvintage = 0;
                                    }
                                    $vintageprod--;
                                }
                                if($checknewproduct == true and $product['price'] > 0 && $product['status'] != 1 && $checkvintage == 0) {
                                    $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Attivato - Nuovo - No Qta - Sì prezzo - No annate precedenti');
                                    $diffList[] = $sDiff;
                                    $this->disableEnableProduct($product['id'], 1, 1);
                                    $tot++;
                                } 
                                if($checknewproduct == false && $product['status'] != 0) {
                                    $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Disattivato - No Qta - Altre annate');
                                    $diffList[] = $sDiff;
                                    $this->disableEnableProduct($product['id'], 0);
                                    $tot++;
                                }
                                if($product['price'] == 0 && $product['status'] != 0) {
                                    $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Disattivato - No Qta - No prezzo');
                                    $diffList[] = $sDiff;
                                    $this->disableEnableProduct($product['id'], 0);
                                    $tot++;
                                }
                            }   
                        } else {
                            if($product['online_only'] == 1 && $product['status'] != 0) {
                                $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Disattivato - No Qta');
                                $diffList[] = $sDiff;
                                $this->disableEnableProduct($product['id'], 0);
                            }
                        }
                    } else {
                        if($product['price'] > 0 && in_array($product['id_category'], $category) && $product['type_condition'] == 0) {
                            if(strlen($product['reference']) == 7) {
                                $codprod = mb_substr($product['reference'], 0, 5);
                                $vintageprod = mb_substr($product['reference'], -2);
                            }
                            if(strlen($product['reference']) == 8) {
                                $codprod = mb_substr($product['reference'], 0, 6);
                                $vintageprod = mb_substr($product['reference'], -2);
                            }
                            if(strlen($product['reference']) == 9) {
                                $codprod = mb_substr($product['reference'], 0, 7);
                                $vintageprod = mb_substr($product['reference'], -2);
                            }
                            if(is_numeric($vintageprod)) {
                                $vintageprod--;
                                $checkvintage = 0;
                                $checkavarage = 0;
                                //print_r('Prod - '.$product['reference'].'<br>');
                                while ($vintageprod >= $yearbefore) {
                                    //print_r($vintageprod.'<br>');
                                    if($this->checkVintageProduct($codprod.$vintageprod, false) == true) {
                                        //print_r('Prec - '.$codprod.$vintageprod.'<br>');
                                        $checkvintage = 1;
                                        if($this->checkAverageSales($codprod.$vintageprod) == true) {
                                            //print_r('Media - '.$codprod.$vintageprod.'<br>');
                                            $checkavarage = 1;
                                        }
                                        break;
                                    } else {
                                        $checkvintage = 0;
                                    }
                                    $vintageprod--;
                                }
                                if($product['online_only'] == 1) {
                                    //print_r($product['reference']. ' - Online - '.$checkvintage.'<br>');
                                    if($checkvintage == 1) {
                                        //print_r($product['reference']. ' - Vintage ok<br>');
                                        if($product['status'] == 0 && $checkavarage == 1) {
                                            $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Attivato - Qta e prezzo - Media annata precedente minore');
                                            $diffList[] = $sDiff;
                                            $this->disableEnableProduct($product['id'], 1);
                                            $tot++;
                                        }
                                        if($product['status'] == 1 && $checkavarage == 0) {
                                            $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Disattivato - Qta e prezzo - Annate precedenti attive');
                                            $diffList[] = $sDiff;
                                            $this->disableEnableProduct($product['id'], 0);
                                            $tot++;
                                        }
                                    } else {
                                        if($product['status'] == 0) {
                                            $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Attivato - Qta e prezzo - No annate precedenti');
                                            $diffList[] = $sDiff;
                                            $this->disableEnableProduct($product['id'], 1);
                                            $tot++;
                                        }
                                    }
                                }
                            }  
                        } else {
                            if($product['status'] != 1 && $product['price'] > 0) {
                                $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Attivato - Qta e prezzo');
                                $diffList[] = $sDiff;
                                $this->disableEnableProduct($product['id'], 1);
                                $tot++;
                            }
                        }
                        if($product['status'] == 1 && $product['price'] == 0) {
                            $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Disabilitato - No prezzo');
                            $diffList[] = $sDiff;
                            $this->disableEnableProduct($product['id'], 0);
                            $totd++;
                        }
                    }
                } else {
                    if($product['status'] != 0) {
                        $sDiff = sprintf('%s: %s', $product['reference'].' - '.$product['name'], 'Disabilitato - Estero');
                        $diffList[] = $sDiff;
                        $this->disableEnableProduct($product['id'], 0);
                        $totd++;
                    }
                }
            }
        }
        $sendMail = $this->db->setting->getStr('lcdp', 'check.mail', '');
        $sendMail2 = $this->db->setting->getStr('lcdp', 'check.mail2', '');
        $sendMail3 = $this->db->setting->getStr('lcdp', 'check.mail3', '');
        if (!empty($diffList) && (!empty($sendMail)))
        {
            // Invia Email
            $subject = 'Prodotti Disattivati/Attivati : ' . count($diffList);
            $this->addLog($subject);
            $mail = new \EOS\System\Mail\Message($this->container);
            $mail->subject = $subject;
            $mail->to = $sendMail;
            $mail->cc = $sendMail2;
            $mail->bcc = $sendMail3;
            foreach ($diffList as $sDiff)
            {
                $mail->content .= $sDiff . "<br>\n";
            }
            $mail->send();
        }
    }

    public function getProduct(): array
    {
        $select = 'select p.*, pl.name, ps.quantity_available, ppc.id_category ';
        $from = ' from #__product as p ';
        $inner = ' left join #__product_lang as pl on p.id = pl.id_product ';
        $inner .= ' left join #__product_stock as ps on p.id = ps.id_product ';
        $inner .= ' left join #__product_product_category as ppc on p.id = ppc.id_product ';
        $where = ' where 1 ';
        $order = ' ';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $list = $q->fetchAll();
        return $list;
    }
       
    protected function disableEnableProduct($id, $status, $outofstock = 2)
    {
        $sql = 'update #__product '.
            ' set status = '.$status.', out_of_stock = '.$outofstock.
            ' where id = '.$id;
        $q = $this->db->prepare($sql);
        $q->execute();
        //$this->addLog('Product: ' . $id. ' '.$status);
    }

    protected function checkVintageProduct($reference, $after = true)
    {
        $select = 'select p.id, p.status, p.online_only ';
        $from = ' from #__product as p ';
        $inner = ' left join #__product_stock as ps on p.id = ps.id_product ';
        //$where = ' where p.reference = :referece and p.status = 1 and p.online_only = 1 ';
        $where = ' where p.reference = :reference ';
        if($after == false) {
            $where .= ' and p.status = 1 ';
        }
        //$where .= ' and ps.quantity_available > 0 ';
        $order = ' ';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->bindValue(":reference", $reference);
        $q->execute();
        $r = $q->fetch();
        if ($r == null)
        {
            return false;
        }
        else {
            /* if($after == true && $r['status'] == 0 && $r['online_only'] == 1) {
                $this->disableEnableProduct($r['id'], 1);
            } */
            return true;
        }
    }
    
    protected function checkAverageSales($reference) {
        $minqty1 = $this->db->setting->getInt('lcdp', 'average.min.qty1', 1);
        $minqty2 = $this->db->setting->getInt('lcdp', 'average.min.qty2', 1);
        $price = $this->db->setting->getInt('lcdp', 'average.min.price', 1);
        $select = 'select p.id, eas.average_sales, ps.quantity_available, p.price ';
        $from = ' from #__ext_average_sales as eas ';
        $inner = ' left outer join #__product as p on p.id = eas.id_product ';
        $inner .= ' left outer join #__product_stock as ps on ps.id_product = p.id ';
        $where = ' where p.reference = :reference and p.status = 1 ';
        $where .= '  ';
        $order = ' ';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->bindValue(":reference", $reference);
        $q->execute();
        $r = $q->fetch();
        //modifica del 25/01/2020 - Se la quantità media venduta 6 mesi è zero non fare controllo
        //if (isset($r['average_sales']) && $r['average_sales'] == 0) {
        //    return true;
        //} else if(isset($r['quantity_available']) && $r['price'] < $price) {
        $averagesales = 0;
        if(isset($r['average_sales']) && $r['average_sales'] > 0) {
            $averagesales = $r['average_sales'] / 4;
        }
        if(isset($r['quantity_available']) && $r['price'] < $price) {    
            if ($r['quantity_available'] <= $minqty1) {
                return true;
            } else {
                if (isset($r['average_sales']) && isset($r['quantity_available']) && $r['quantity_available'] < $averagesales) {
                    return true;
                } else {
                    return false;
                }
            }
        } else if(isset($r['quantity_available']) && $r['price'] >= $price) {
            if ($r['quantity_available'] <= $minqty2) {
                return true;
            } else {
                if (isset($r['average_sales']) && isset($r['quantity_available']) && $r['quantity_available'] < $averagesales) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        else {
            return false;
        }
    }

}
