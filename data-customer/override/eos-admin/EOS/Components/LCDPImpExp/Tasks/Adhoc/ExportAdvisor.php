<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class ExportAdvisor extends \EOS\System\Tasks\Task
{
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Les Caves - Export User';
        $this->exportFile();
    }

    public function exportFile()
    {
        $res = [];
        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $path = PathHelper::slashToDirSep($this->replacePathTag('{uploads}export/lescaves/'));
        $fname = $path . 'export-advisor.csv';
        $writer->openToFile($fname);

        $sqlSelect = 'SELECT a.`id` `alias`, `company`, a.address1, lc.name, lc.zip_code, ls.iso_code '
            . ' FROM `eos_address` as a  '
            . ' left OUTER join eos_localization_city as lc on lc.id = a.id_city '
            . ' left OUTER join eos_localization_state as ls on lc.id_state = ls.id '
            . ' WHERE json_unquote(json_extract(a.options, "$.advisor")) = 1 ';
        $sqlFrom = '  ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $addresses = $q->fetchAll();

        foreach ($addresses as $address) {
            $writer->addRow($address);
        }
        $writer->close();
    }
}
