<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class ExportB2B extends \EOS\System\Tasks\Task
{
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Les Caves - Export User';
        $this->exportFile();
    }

    public function exportFile()
    {
        $res = [];
        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $path = PathHelper::slashToDirSep($this->replacePathTag('{uploads}export/lescaves/'));
        $fname = $path . 'export-customer-b2b.csv';
        $writer->openToFile($fname);

        $sqlSelect = 'SELECT au.`id`, au.`name`, au.email_alternative '
            . ' FROM eos_account_user as au '
            . ' WHERE au.id in (select id_user from eos_account_user_group where id_group = 1) ';
        $sqlFrom = '  ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $addresses = $q->fetchAll();

        foreach ($addresses as $address) {
            $writer->addRow($address);
        }
        $writer->close();
    }
}
