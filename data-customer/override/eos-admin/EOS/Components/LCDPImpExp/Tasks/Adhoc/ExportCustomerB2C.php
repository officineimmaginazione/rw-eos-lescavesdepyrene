<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class ExportCustomerB2C extends \EOS\System\Tasks\Task
{
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Les Caves - Export User';
        $this->exportFile();
    }

    public function exportFile()
    {
        $res = [];
        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $path = PathHelper::slashToDirSep($this->replacePathTag('{uploads}export/lescaves/'));
        $fname = $path . 'export-customer-b2c.csv';
        $writer->openToFile($fname);

        $sqlSelect = 'SELECT DISTINCT au.id, au.ins_date, au.email, a.name, a.surname, json_unquote(json_extract(au.options, "$.birthday")) '
            . ' FROM `eos_account_user` as au '
            . ' inner join eos_account_user_group as aug on aug.id_user = au.id left outer join eos_address as a on a.id_customer = au.id '
            . ' WHERE aug.id_group = 42 and a.name is not null ORDER BY `au`.`id` DESC ';
        $sqlFrom = '  ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $addresses = $q->fetchAll();

        foreach ($addresses as $address) {
            $writer->addRow($address);
        }
        $writer->close();
    }
}
