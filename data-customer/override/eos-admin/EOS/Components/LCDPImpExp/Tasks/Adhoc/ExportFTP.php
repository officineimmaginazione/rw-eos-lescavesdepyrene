<?php

namespace EOS\Components\NWImpExp\Tasks\Arianna;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;

class ExportFTP extends \EOS\System\Tasks\Task
{
    protected $uploadList = ['order_lescaves_%s.txt'];
    
    private function getExportDir(string $directoryChild)
    {
        $path = PathHelper::slashToDirSep($this->replacePathTag('{uploads}adhoc/'));
        if (!is_dir($path))
        {
            mkdir($path);
        }

        if ($directoryChild !== '')
        {
            $path .= PathHelper::addDirSep($directoryChild);
            if (!is_dir($path))
            {
                mkdir($path);
            }
        }

        return $path;
    }

    public function run()
    {
        $eu = new \EOS\Components\NWImpExp\Classes\ErrorUtil($this->container);
        $eu->captureException(function ()
        {
            $orderPath = $this->getExportDir('order');
            $ftpPath = $this->getExportDir('ftp');
            $updatePath = $this->getExportDir('update');
            $ftp = new \EOS\Components\NWImpExp\Classes\FTPUtil($this->container);
            $ftp->open();
            $this->addLog('Lettura elenco file');
            $ftpRoot = 'inbox';
            $files = scandir($orderPath);
            $lRes = [];
            
            foreach ($files as $key => $value)
            {
                if (!in_array($value,array(".","..",".DS_Store")))
                {
                    if (!is_dir($orderPath . DIRECTORY_SEPARATOR . $value))
                    {    
                        $name = $value;
                        $localName = $orderPath . $name;
                        if (file_exists($localName))
                        {
                            $lRes[] = $localName;
                            $this->addLog('Avvio upload:' . $name . ' (' . $ftp->getSizeStr($localName) . ')');
                            if (!$ftp->putFile($localName, $name))
                            {
                                throw new \Exception('Impossibile leggere il file: ' . $fName);
                            }
                            rename($localName, $ftpPath.$name);
                        }
                    }
                }
            } 
            $ftp->close();
            if (empty($lRes))
            {
                $this->addLog('Nessun file caricato');
            }
        });
    }

}
