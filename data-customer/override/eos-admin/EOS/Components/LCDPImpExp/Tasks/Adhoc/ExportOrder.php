<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class ExportOrder extends \EOS\System\Tasks\Task
{
    protected $totalWithTax = 0;
    protected $totalTax = 0;

    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Adhoc - Export Orders';
        $this->exportOrder();
    }

    public function exportOrder()
    {
        $orders = $this->getOrderHeaded();
        foreach ($orders as $singleorder) {
            $rand = mt_rand(10, 80);

            $group = $this->getGroup($singleorder['id_customer']);

            $customertype = 0;
            $customertypefile = "";
            if (in_array(1, $group[0]) && $singleorder['id_customer'] == $singleorder['id_agent']) {
                $customertype = 1;
                $customertypefile = "";
            } else if (in_array(42, $group[0])) {
                $customertype = 2;
                $customertypefile = "pri_";
            }

            $products = $this->getOrderRow($singleorder, $customertype);
            $dataorderrow = $this->createRow($products, $singleorder, $customertype);
            $dataorder = $this->createOrder($singleorder, $customertype);
            $error = '';

            $dateorder = str_replace(':', '', str_replace('-', '', str_replace(' ', '_', $singleorder['ins_date'])));
            $path = PathHelper::slashToDirSep($this->path->getUploadsPath() . 'adhoc/ordini/');
            $filename = "lescaves-export_ordini_" . $customertypefile . $dateorder . '_' . $singleorder['id'] . "_" . $rand . ".csv";
            $myfile = fopen($path . $filename, "w") or die("Unable to open file!");
            fwrite($myfile, mb_convert_encoding($dataorder, 'Windows-1252'));
            fclose($myfile);

            $path = PathHelper::slashToDirSep($this->path->getUploadsPath() . 'adhoc/ordini/');
            $filename = "lescaves-export_ordini_" . $customertypefile . $dateorder . '_' . $singleorder['id'] . "_" . $rand . "_det.csv";
            $myfile = fopen($path . $filename, "w") or die("Unable to open file!");
            fwrite($myfile, mb_convert_encoding($dataorderrow, 'Windows-1252'));
            fclose($myfile);

            $this->cartToAdhoc($singleorder['id']);
        }
    }

    public function createOrder($data, $customertype = 0)
    {
        //print_r($data['ins_date']); exit();
        $options = json_decode($data['options'], true);
        $optionsaddres = json_decode($data['optionsaddress'], true);
        $optionscart = json_decode($data['optionscart'], true);

        $hedaquaterdata = $this->getHeadquater($data['id_customer']);

        $dateorder = $this->lang->dbDateTimeToLangDateTime($data['ins_date']);
        $shippingdate = '';
        $shippingmethod = '';
        if (isset($options['shipping']['date']) && $options['shipping']['date'] != '') {
            $shippingdate = $this->lang->dbDateToLangDate($options['shipping']['date']);
        }
        $shippingtype = '';
        $shippingtype_cart = isset($options['shipping']['type']) ? $options['shipping']['type'] : 0;
        if ($shippingtype_cart == 0) {
            $shippingtype = 'Spezione standard';
        }
        if ($shippingtype_cart == 1) {
            $shippingtype = 'Spezione express';
        }
        $shippingmethod_cart = isset($options['shipping']['method']) ? $options['shipping']['method'] : 0;
        if ($shippingmethod_cart == 0) {
            //$shippingmethod = 'Consegna il prima possibile';
        }
        if ($shippingmethod_cart == 1) {
            $shippingmethod = 'Consegna l\'ordine all\'arrivo dell\'ultimo prodotto';
        }
        $shippingtotal = 0;
        if (isset($options['shipping']['total']) && $options['shipping']['total'] != 0) {
            $shippingtotal = $options['shipping']['total'];
        }

        $id_payment = (isset($optionscart['head_payment'])) ? $optionscart['head_payment'] : 6;
        $paymentcode = $this->getPayment($id_payment);

        //impostazioni di default
        $listprice = 'VEND';
        $track = 'lescaves';
        $caudoc = 'ORDCL';
        $exttag = 'AGE';

        $checkpayment = array("ANT", "A3", "A6", "A9", "A12", "ANTPR", "CARTA");
        if (in_array($paymentcode, $checkpayment)) {
            $caudoc = 'PROF';
        }

        if (isset($optionscart['allissample']) && $optionscart['allissample'] == true) {
            $caudoc = 'ORCAM';
            $listprice = 'CMP';
        }

        if ($customertype == 2) {
            $caudoc = 'ORB2C';
            $listprice = 'B2C';
            $track = 'lescaves_b2c';
            $exttag = 'B2C';
        }

        if ($customertype == 1) {
            $exttag = 'B2B';
        }

        $orderresid = ArrayHelper::getInt($options, 'order_resid', 0);

        $cartnumber = ArrayHelper::getInt($options, 'cart-number', 1);
        $methodshipping = 0;
        if (isset($options['shipping'])) {
            $methodshipping = ArrayHelper::getInt($options['shipping'], 'method', 0);
        }
        $shippingdiscount = 0;

        if ($cartnumber == 2 || $methodshipping == 1 || $orderresid == 1) {
            $caudoc = 'RESID';
            if (($methodshipping == 1 || $orderresid == 1) && $options['shipping']['count'] == 1) {
                $shippingdiscount = $options['discount']['shipping1perc'];
            } else {
                $shippingdiscount = $options['discount']['shipping2perc'];
            }
        } else {
            if (isset($options['shipping']['date']) && (new \DateTime($options['shipping']['date']) > new \DateTime())) {
                $caudoc = 'ORDTE';
            }
            if (isset($options['discount'])) {
                $shippingdiscount = $options['discount']['shipping1perc'];
            }
        }

        $reference = '';
        $noteaddress = '""';
        $unicode = '';
        $pec = '';
        $newcustomer = 'N';
        if ($data['reference'] != 0 && $data['reference'] != null) {
            $reference = $data['reference'];
        } else {
            $newcustomer = 'S';
            $noteaddress = '"' . substr($optionsaddres['note'], 0, 252) . '"';
            $unicode = $optionsaddres['unicode'];
            $pec = $optionsaddres['pec'];
        }

        $rated = 'Non classificato';
        if (isset($optionsaddres['cscommonvalue']) && $optionsaddres['cscommonvalue'] != '') {
            $rated = $optionsaddres['cscommonvalue'] . ' ' . $optionsaddres['cscommonvalue'];
        }

        $email = '""';
        if ($data['emailcustomer'] != '') {
            $email = '"' . $data['emailcustomer'] . '"';
        } else {
            if (strpos($data['emailcustomer2'], 'cavesuser') === false) {
                $email = '"' . $data['emailcustomer2'] . '"';
            }
        }

        $note = '""';
        if ($options['head_note'] != '') {
            $note = '"' . substr((str_replace('"', '', $options['head_note']) . ' ' . $shippingmethod), 0, 252) . '"';
        } else {
            $note = $shippingmethod;
        }

        $vatcif = '';

        $company = '';
        if (isset($hedaquaterdata['company']) && $hedaquaterdata['company'] != '') {
            $company = $hedaquaterdata['company'];
            $vatcif = $data['vat'];
        } else {
            if ((isset($hedaquaterdata['name']) && $hedaquaterdata['name'] != '') || (isset($hedaquaterdata['surname']) && $hedaquaterdata['surname'] != '')) {
                $name = (isset($hedaquaterdata['name'])) ? $hedaquaterdata['name'] . ' ' : '';
                $surname = (isset($hedaquaterdata['surname'])) ? $hedaquaterdata['surname'] : '';
                $company = $name . $surname;
            }
            $vatcif = $data['cif'];
        }

        $coddes = '';
        $shippihgalias = '';
        $shippihgaddress1 = '';
        $shippihgpostcodes = '';
        $shippihgcity = '';
        $shippihgstate = '';
        if ($optionsaddres['coddes'] != '9999') {
            $coddes = $optionsaddres['coddes'];
            $shippihgalias = $data['alias'];
            $shippihgaddress1 = $data['address1'];
            $shippihgpostcodes = $data['postcode'];
            $shippihgcity = $data['city'];
            $shippihgstate = $data['state'];
        }

        $codagent = '';
        if ($customertype == 2) {
            $codagent = '000';
        } else {
            $codagent = $data['idagent'];
        }

        $res = substr($track, 0, 20) . ';' . //1		Nome Tracciato	CHAR	20	Codice Tracciato di importazione
            substr($data['id'], 0, 20) . ';' . //2    B	Numero Documento	CHAR	20	Numero documento 	Inserire la parte interamente numerica del numero documento. Esempio: 2017-test-00001 Inserire 00001
            substr($dateorder, 0, 10) . ';' . //3    C	Data Documento	CHAR	10	Data Documento	        
            substr($company, 0, 60) . ';' . //4    D	Ragione sociale	CHAR	60	Ragione sociale Cli/For	
            substr('', 0, 60) . ';' . //5    E	Ragione sociale 2	CHAR	60	Ragione sociale secondaria Cli/For	Non obbligatoria
            substr($hedaquaterdata['address1'], 0, 35) . ';' . //6    F	Indirizzo	CHAR	35	Indirizzo di Fatturazione  Cli/For	
            substr($hedaquaterdata['postcode'], 0, 8) . ';' . //7    G	CAP	CHAR	8	Cap di Fatturazione Cli/For	
            substr($hedaquaterdata['city'], 0, 30) . ';' . //8    H	Località	CHAR	30	Località di Fatturazione Cli/For	
            substr($hedaquaterdata['state'], 0, 20) . ';' . //9	I	Provincia	CHAR	20	Provincia di Fatturazione Cli/For	
            substr('Italia', 0, 35) . ';' . //10	J	Nome nazione	CHAR	35	Nome nazione di Fatturazione Cli/For	
            substr('', 0, 2) . ';' . //11	K	Codice ISO 2 Nazione	CHAR	2	Codice ISO 2 Nazione di Fatturazione Cli/For	Non obbligatoria
            substr('', 0, 3) . ';' . //12	L	Codice ISO3 Nazione	CHAR	3	 Codice ISO 3 Nazione di Fatturazione Cli/For	Non obbligatoria
            substr($vatcif, 0, 16) . ';' . //13	M	Partita IVA/Cod fisc	CHAR	16	Partita IVA o Codice fiscale Cli/For	
            substr($data['phone'], 0, 18) . ';' . //14	N	Telefono	CHAR	18	Telefono principale Cli/For	
            substr('', 0, 18) . ';' . //15	O	Telefono 2	CHAR	18	Telefono secondario Cli/For	Non obbligatorio
            substr($email, 0, 254) . ';' . //16	P	Email del cliente	CHAR	254	Email cliente	Inserire eventuale email del cliente
            substr($data['phone_mobile'], 0, 18) . ';' . //17	Q	Cellulare	CHAR	18	Cellulare Cli/For	Non obbligatorio
            substr($shippingdate, 0, 10) . ';' . //18	R	Data Consegna	CHAR	10	Data consegna prevista del documento	Non obbligatorio
            substr($codagent, 0, 254) . ';' . //19	S	Mail Agente/Codice agente	CHAR	254	Email agente assegnato al documento	Inserire il codice agente di AHR (esempio: 00273)
            substr('', 0, 40) . ';' . //20	T	Nome Destinatario	CHAR	40	Nome + Cognome Destinatario	Non obbligatorio
            substr($shippihgalias, 0, 40) . ';' . //21   U	Ragione soc. Destinatario	CHAR	40	Ragione Sociale Destinatario documento	Se vuoto, viene valorizzato con la ragione sociale di fatturazione
            substr($shippihgaddress1, 0, 35) . ';' . //22	V	Indirizzo Destinatario	CHAR	35	Indirizzo Destinatario documento	Se vuoto, viene valorizzato con l’indirizzo di fatturazione
            substr($shippihgpostcodes, 0, 8) . ';' . //23	W	CAP Destinatario	CHAR	8	Cap Destinatario documento	Se vuoto, viene valorizzato con il cap di fatturazione
            substr($shippihgcity, 0, 30) . ';' . //24	X	Località Destinatario	CHAR	30	Località Destinatario documento	Se vuoto, viene valorizzato con la località di fatturazione
            substr($shippihgstate, 0, 20) . ';' . //25	Y	Provincia Destinatario	CHAR	20	Provincia destinatario documento	Se vuoto, viene valorizzato con la provincia di fatturazione
            substr('IT', 0, 2) . ';' . //26	Z	ISO 2 Nazione Spedizione	CHAR	2	Codice ISO 2 Nazione destinatario documento	Non obbligatorio
            substr('', 0, 3) . ';' . //27	AA	ISO3 Nazione Spedizione	CHAR	3	Codice ISO 3 Nazione destinatario documento	Non obbligatorio
            substr($data['phone'], 0, 30) . ';' . //28	AB	Telefono Destinatario	CHAR	30	Telefono destinatario documento	Se vuoto, viene valorizzato con il telefono di fatturazione
            substr($shippingtype, 0, 50) . ';' . //29	AC	Metodo Spedizione	CHAR	50	Metodo di spedizione della merce	Inserite la dicitura VETTORE (sarà ns cura usare transcodifica)
            substr($newcustomer, 0, 1) . ';' . //30	AD	Nuovo Cliente	CHAR	1	Check se nuovo cliente 	Assume i valori S (si) o N(no).';'. Di default valorizzare a N
            substr($paymentcode, 0, 30) . ';' . //31	AE	Codice Pagamento	CHAR	30	Codice del metodo di pagamento utilizzato	Inserite il metodo di pagamento utilizzato (sarà ns cura usare transcodifica)
            substr('', 0, 1) . ';' . //32	AF	Codice Zona	CHAR	1	Codice Zona	Non obbligatorio
            substr('', 0, 1) . ';' . //33	AG	Codice Porto	CHAR	1	Codice porto assegnato	Non obbligatorio
            substr('', 0, 5) . ';' . //34	AH	Categoria Cliente	CHAR	5	Categoria cliente ( uso interno ) 	Da Non valorizzare ( vuoto )
            substr('', 0, 5) . ';' . //35	AI	Attività Cliente	CHAR	5	Attività cliente (uso interno)	Da Non valorizzare ( vuoto )
            substr('0', 0, 0) . ';' . //36	AJ	Sconto Piede	NUM	16,4	Sconto di piede documento 	Non obbligatorio, default 0
            $shippingtotal . ';' . //37	AK	Importo Spedizione	NUM	16,4	Importo spese di spedizione documento	Non obbligatorio, default 0
            $note . ';' . //38	AL	Note	CHAR	254	Note generali documento	Non obbligatorio
            substr($data['id_customer'], 0, 15) . ';' . //39	AM	Codice Cliente Interno	CHAR	15	Identificativo interno cliente	Codice cliente nel sistema fornitrice del file 
            substr('S', 0, 1) . ';' . //40	AN	Fatturazione	CHAR	1	Flag richiesta fatturazione (S/N)	Default N
            substr('', 0, 16) . ';' . //41	AO	Codice fiscale cli/for	CHAR	16	Codice fiscale cli/for 	Non da valorizzare con il campo tax_vat. Non obbligatorio
            substr($reference, 0, 15) . ';' . //42	AP	Codice AHR 	CHAR	15	Codice AdHoc del cliente/fornitore	Non obbligatorio
            substr('', 0, 50) . ';' . //43	AQ	Stato Documento	CHAR	50	Stato documento	
            substr($listprice, 0, 20) . ';' . //44	AR	Codice Listino 	CHAR	20	Codice listino cli/for	Non obbligatorio - VEND per standard CMP per ORCAM
            substr('EUR', 0, 5) . ';' . //45	AS	Valuta 	CHAR	5	Valuta documento	
            substr('N', 0, 1) . ';' . //46	AT	Scorporo 	CHAR	1	Gestione prezzi con tasse incluse/escluse	Se prezzo include tasse valorizzare a S, altrimenti N
            substr('', 0, 15) . ';' . //47	AU	Seriale Esterno 	CHAR	15	Seriale esterno documento	Non obbligatorio
            substr($exttag, 0, 15) . ';' . //48	AV	Tag Esterno 	CHAR	15	Codice applicativo provenienza ordine	Usare un valore per identificare il sistema che fornisce il file
            substr($noteaddress, 0, 254) . ';' . //49	AW	Dettaglio spedizione 	CHAR	254	Xml con dettagliati i corrieri che prenderanno in carico la merce	Inserire note per la spedizione
            substr('', 0, 254) . ';' . //50	AX	Dettaglio spedizione  2	CHAR	254	Parte 2 di un eventuale xml più grosso	Da non valorizzare
            substr('', 0, 20) . ';' . //51	AY	Codice vettore	CHAR	20	Codice vettore 	(sarà ns cura usare transcodifica)
            substr($rated, 0, 20) . ';' . //52	AZ	Rating 	CHAR	20	Priorità gestione documento	Da non valorizzare - NUOVO
            substr('F', 0, 5) . ';' . //53	BA	Tipo Consegna	CHAR	5	Tipologia di consegna ( identificativo consegna usato soprattutto dai corrieri )	non obbligatorio (F => FORZATO)
            substr('', 0, 10) . ';' . //54	BB	Campagna	CHAR	10	Eventuale campagna associata al documento	Non obbligatorio
            substr('O', 0, 1) . ';' . //55	BC	Tipo Documento	CHAR	1	Tipologia documento	O => Ordine, F => Fattura, N=> Nota credito, A => Fattura di Acquisto. Default valorizzato a O
            substr($data['id'], 0, 15) . ';' . //56	BD	Riferimento ordine	CHAR	15	Riferimento ordine	Valorizzare con il codice di origine dell’ordine. 
            substr('', 0, 15) . ';' . //57	BE	Categoria Contabile	CHAR	15	Cat. Contab. Cliente	
            substr('', 0, 15) . ';' . //58	BF	Categoria Commerciale	CHAR	15	Cat. Commerc. Cliente	
            substr('', 0, 5) . ';' . //59	BG	Codice tariffa corriere	CHAR	5	Codice tariffa corriere da applicare alla spedizione 	Non obbligatorio
            substr('', 0, 10) . ';' . //60	BH	Alfa documento	CHAR	10	Parte alfanumerica del numero documento	
            '0' . ';' . //61	 BI	Totale num. Colli	NUM	5	Totale colli del documento	Da valorizzare su spedizioni
            '0' . ';' . //62	BJ	Totale Peso Lordo	NUM	10,3	Totale peso lordo del documento	Da valorizzare su spedizioni
            substr($caudoc, 0, 5) . ';' . //63	BK	Causale Doc	CHAR	5	Causale con cui creare i documenti	Non valorizzare; 'ORDCL' standard 'ORCAM' ordine campionatura
            number_format(($data['total']  + $this->totalTax), 5, '.', '') . ';' . //64	BL	Totale Doc	NUM	18,5	Totale documento (incluse tasse)	Valorizzare per eseguire check di congruità importi
            number_format($data['total'], 5, '.', '') . ';' . //65	BM	Totale Imponibile	NUM	18,5	Totale Imponibile	Valorizzare per eseguire check di congruità importi
            number_format($this->totalTax, 5, '.', '') . ';' . //66	BN	Totale Imposta	NUM	18,5	Totale Imposte	Valorizzare per eseguire check di congruità importi
            substr('', 0, 100) . ';' . //67	BO	Nome sogg. Privato	Non valorizzare; 
            substr('', 0, 100) . ';' . //68	BP	Cognome sogg. Privato	
            substr($unicode, 0, 7) . ';' . //69      BQ	Codice sdi fatt. elettr 
            substr($pec, 0, 254) . ';' . //70	BR	Email PEC Fatt. Elettr. 
            substr($coddes, 0, 5) . ';' . //71	Codice Destinazione
            number_format($shippingdiscount, 2, '.', '') . ';' . //72	Sconto cliente 1 NUM 18,5 % Sconto/magg. 1
            number_format(0, 2, '.', '')  . ';' . //73	Sconto cliente 2 NUM 18,5 % Sconto/magg. 2
            substr('', 0, 8) . ';' . //74	Data Nascita persona fisica  DATA  8  Data nascita persona fisica 
            substr('', 0, 2) . ';' . //75	Provincia persona fisica  DATA  2  Data nascita persona fisica
            substr('', 0, 30); //76	Città persona fisica  DATA  30  Data nascita persona fisica
        return $res;
    }

    public function createRow($dataRows, $dataHead, $customertype = 0)
    {
        $res = '';
        $i = 0;
        $optionshead = ArrayHelper::fromJSON($dataHead['options']);
        $price = 0;
        foreach ($dataRows as $row) {
            $optionsrow = ArrayHelper::getArray(ArrayHelper::fromJSON($row['options']), 'data');
            $freesample = 'N';
            $descadd = '';

            if (isset($row['rate'])) {
                $rate = (int)$row['rate'];
            } else {
                $rate = 22;
            }

            if ($customertype == 2 && $row['nameproduct'] != 'SCONTO' && $row['nameproduct'] != 'CASHBACK') {
                $price = $row['priceb2c'] / ((100 + $row['rate']) / 100);
            } elseif ($customertype == 2 && $row['nameproduct'] == 'CASHBACK') {
                $price = $row['price'] / ((100 + 22) / 100);
            } else {
                $price = $row['price'];
            }
            if ($optionsrow['issample'] == 1) {
                $freesample = 'S';
                $descadd = '* ';
            }

            $fee = 0;

            if (isset($optionsrow['free-row-discount'])) {
                $freerowdiscount = $optionsrow['free-row-discount'];
                $discount1 = ArrayHelper::getFloat($optionshead, 'head_discount1');
                if ($freerowdiscount[0] > 0) {
                    if (isset($optionsrow['additional-discount']) && $optionsrow['additional-discount'] != 0) {
                        $discount1 =  (1 - (1 - ($optionsrow['additional-discount'] + $freerowdiscount[0]) / 100) * (1 - ArrayHelper::getFloat($optionshead, 'head_discount1') / 100)) * 100;
                    } else {
                        $discount1 =  (1 - (1 - ($freerowdiscount[0]) / 100) * (1 - ArrayHelper::getFloat($optionshead, 'head_discount1') / 100)) * 100;
                    }
                } else if ($freerowdiscount[1] > 0) {
                    if (isset($optionsrow['additional-discount']) && $optionsrow['additional-discount'] != 0) {
                        $discount1 =  (1 - (1 - ($optionsrow['additional-discount'] + $freerowdiscount[1]) / 100) * (1 - ArrayHelper::getFloat($optionshead, 'head_discount1') / 100)) * 100;
                    } else {
                        $discount1 =  (1 - (1 - ($freerowdiscount[1]) / 100) * (1 - ArrayHelper::getFloat($optionshead, 'head_discount1') / 100)) * 100;
                    }
                } else {
                    if (isset($optionsrow['additional-discount']) && $optionsrow['additional-discount'] != 0) {
                        $discount1 =  (1 - (1 - $optionsrow['additional-discount'] / 100) * (1 - ArrayHelper::getFloat($optionshead, 'head_discount1') / 100)) * 100;
                    }
                }
            } else {
                $discount1 = 0;
            }

            $discount2 = ArrayHelper::getFloat($optionshead, 'head_discount2');
            $discount3 = ArrayHelper::getFloat($optionshead, 'head_discount3');
            $reference = $row['reference'];
            if ($row['nameproduct'] == 'SCONTO') {
                //$price = $price * ((1 - $discount1 / 100) * (1 - $discount2 / 100) * (1 - $discount3 / 100));
                $reference = 'SCONTO INC';
                $discount1 = 0;
                $discount2 = 0;
                $discount3 = 0;
            }
            if ($row['nameproduct'] == 'SCONTO QTA') {
                //$price = $price * ((1 - $discount1 / 100) * (1 - $discount2 / 100) * (1 - $discount3 / 100));
                $reference = 'SCONTO QTA';
                $discount1 = 0;
                $discount2 = 0;
                $discount3 = 0;
            }
            if ($row['nameproduct'] == 'CASHBACK') {
                //$price = $price * ((1 - $discount1 / 100) * (1 - $discount2 / 100) * (1 - $discount3 / 100));
                $reference = 'CASHBACK';
                $discount1 = 0;
                $discount2 = 0;
                $discount3 = 0;
                $rate = 22;
            }

            $is_free = isset($optionsrow['isfree']) ? $optionsrow['isfree'] : false;
            if ($is_free) {
                $descadd = '* ';
                $discount1 = 0;
                $discount2 = 0;
                $discount3 = 0;
            }

            $isSampleDiscount = '';
            if (isset($row['type_condition']) && $row['type_condition'] == 1) {
                $isSampleDiscount = 'CAMP - ';
            }

            $taxProd = $price * ($rate / 100);
            $subtotalRow = $price * $row['quantity'];
            $totalRowTax = $price * $row['quantity'] * ($rate / 100);
            $totalRow = $subtotalRow + $totalRowTax;
            $pricewithtax = $price * (1 + ($rate / 100));

            $this->totalTax += $totalRowTax;


            $res .= substr($row['id_cart'], 0, 20) . ';' . //1	A	Numero documento	CHAR	20	Numero documento	Obbligatorio, deve essere presente anche in testata
                substr($i, 0, 3) . ';' . //2	B	Riga	NUME	3	Identificativo riga	Obbligatorio
                substr($reference, 0, 20) . ';' . //3	C	Articolo	CHAR	20	Codice articolo	
                $descadd . substr($isSampleDiscount . $row['nameproduct'], 0, 40) . ';' . //4	D	Descrizione articolo	CHAR	40	Descrizione articolo	
                0 . ';' . //5	E	Pezzi	NUM	4	Totale pezzi per collo	Per ora lasciamo vuoto
                0 . ';' . //6	F	Colli	NUM	4	Totale colli riga	Valorizzare in SPEDIZIONI ove presente indicazione del collo di riga
                $row['quantity'] . ';' . //7	G	Quantità	NUM	12,2	Quantità 	Obbligatorio
                number_format($price, 5, '.', '') . ';' . //8	H	Prezzo base	NUM	18,5	Pezzo base prodotto	Valorizzare con l’attributo BASE_PRICE o BASE_ORIGINAL_PRICE
                number_format($price, 5, '.', '') . ';' . //9	I	Prezzo	NUM	18,5	Prezzo prodotto	Valorizzare con l’attributo PRICE
                number_format($pricewithtax, 5, '.', '') . ';' . //10	J	Prezzo iva inclusa	NUM	18,5	Prezzo iva inclusa prodotto	
                $discount1 . ';' . //11	K	Sconto 	NUM	12,2	Sconto di riga	
                $discount2 . ';' . //12	L	Sconto 2	NUM	12,2	Sconto 2 di riga	
                (int) $rate . ';' . //13	M	IVA	CHAR	5	Percentuale tassa iva	
                (int) $rate . ';' . //14	N	Classe iva	CHAR	50	Nome classe iva applicata	Inserite il codice IVA utilizzato (sarà ns cura usare transcodifica)
                number_format($taxProd, 5, '.', '') . ';' . //15	O	Importo iva	NUM	18,5	Iva applicata all’articolo	
                number_format($subtotalRow, 5, '.', '') . ';' . //16	P	Subtotale	NUM	18,5	Subtotale di riga	Utilizzare l’attributo BASE_ROW_SUBTOTAL
                number_format($totalRowTax, 5, '.', '') . ';' . //17	Q	Totale tasse	NUM	18,5	Totale tasse applicate	Utilizzare l’attributo base_tax_amount
                number_format($totalRow, 5, '.', '')  . ';' . //18	R	Totale	NUM	18,5	Totale complessivo di riga	
                $freesample . ';' . //19	    S	Omaggio	CHAR	1	Flag per identificare se l’articolo è in omaggio	S => SI , N => NO. Default N
                '' . ';' . //20	T	Note	CHAR	254	Note di riga	Informazioni aggiuntive di riga
                '' . ';' . //21	    U	Commessa	CHAR	15	Codice commessa prodotto	Vedere gestione commesse
                '' . ';' . //22	V	Attività	CHAR	15	Codice attività prodotto	Vedere gestione commesse
                '' . ';' . //23	W	Importo Storno Commessa	NUM	18,5	Importo di storno da applicare alla commessa - attività	Vedere gestione commesse
                '' . ';' . //24	X	Commessa2	CHAR	15	Codice seconda commessa  prodotto	Vedere gestione commesse
                '' . ';' . //25	Y	Attività 2	CHAR	15	Codice seconda attività prodotto	Vedere gestione commesse
                '' . ';' . //26	Z	Importo Storno 2 Commessa	NUM	18,5	Importo del secondo storno da applicare alla commessa 2 – attivita2 	Vedere gestione commesse
                '' . ';' . //27	AA	Rating	CHAR	20		
                'O' . ';' . //28	AB	TipoDocumento	CHAR	2	Tipologia Documento (Speculare al campo di testata)	O => Ordine, F => Fattura, N=> Nota credito, A => Fattura di Acquisto. Default valorizzato a O
                substr('', 0, 10) . //29	AC	Alfa Documento	CHAR	10	Parte alfanumerica del numero documento	Valorizzare con lo stesso alfa del doc. di testata
                substr('', 0, 10) . ';' . //29	AC	Alfa Documento	CHAR	10	Parte alfanumerica del numero documento	Valorizzare con lo stesso alfa del doc. di testata
                $discount3 . ';' . //30	L	Sconto 3	NUM	12,2	Sconto 2 di riga
                $fee . ';'; //31 % Provvigione NUM 6,2 % Provvigione agente
            $res .= "\n";
            $i++;
        }
        return $res;
    }

    public function getOrderHeaded(): array
    {
        $select = 'select c.*, a.id as id_customer, a.id_country, a.id_state, a.id_customer, a.id_manufacturer, a.alias, a.company, ';
        $select .= 'a.name, a.surname, a.address1, a.address2, lc.zip_code as postcode, a.id_city, a.other, a.phone, a.phone_mobile, a.vat, ';
        $select .= 'a.cif, a.options as optionsaddress, a.reference, lc.name as city, ls.name as state, au.reference as idagent, ';
        $select .= 'au.email, c.options as optionscart, au2.email_alternative as emailcustomer, au2.email as emailcustomer2 ';
        $from = 'from #__cart as c ';
        $inner = 'inner join #__address as a on a.id = c.id_address_delivery ';
        $inner .= 'left join #__account_user as au on au.id = c.id_agent ';
        $inner .= 'left join #__account_user as au2 on au2.id = a.id_customer ';
        $inner .= 'left join #__localization_city as lc on lc.id = a.id_city ';
        $inner .= 'left join #__localization_state as ls on ls.id = lc.id_state ';
        $where = ' where c.status = 2 ';
        $order = 'order by c.id';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $list = $q->fetchAll();
        return $list;
    }

    public function getPayment(int $id): string
    {
        $select = 'select epa.PACODICE ';
        $from = 'from #__ext_pag_amen as epa ';
        $inner = ' ';
        $where = ' where id = ' . $id . ' ';
        $order = ' ';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? '' : $r['PACODICE'];
    }

    public function getGroup(int $id): array
    {
        $select = 'select id_group ';
        $from = 'from #__account_user_group as aug ';
        $inner = ' ';
        $where = ' where id_user = ' . $id . ' ';
        $order = ' and id_group in (1, 2, 42) ';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $r = $q->fetchAll();
        return empty($r) ? '' : $r;
    }

    public function getHeadquater(int $id)
    {
        $select = 'select a.*, lc.name as city, lc.zip_code as postcode, ls.name as state ';
        $from = 'from #__address as a ';
        $inner = 'left join #__localization_city as lc on lc.id = a.id_city ';
        $inner .= 'left join #__localization_state as ls on ls.id = lc.id_state ';
        $where = ' where id_customer = ' . $id . ' ';
        $order = ' and options like \'%coddes":"9999%\'';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            $select = 'select a.*, lc.name as city, lc.zip_code as postcode, ls.name as state ';
            $from = 'from #__address as a ';
            $inner = 'left join #__localization_city as lc on lc.id = a.id_city ';
            $inner .= 'left join #__localization_state as ls on ls.id = lc.id_state ';
            $where = ' where id_customer = ' . $id . ' ';
            $order = ' order by a.id limit 1';
            $q = $this->db->prepare($select . $from . $inner . $where . $order);
            $q->execute();
            $r = $q->fetch();
        }
        return empty($r) ? '' : $r;
    }

    public function getOrderRow($data, $customertype = 0): array
    {
        $siteLang = new \EOS\System\Language\Manager($this->container);
        $siteLang->loadFromDB(true);
        $idLang = $this->lang->getCurrentID();
        if ($customertype == 2) {
            $select = 'select p.*, cr.*,  pl.name as nameproduct, t.rate, cru.price as priceb2c ';
        } else {
            $select = 'select p.*, cr.*, pl.name as nameproduct, t.rate ';
        }
        $from = 'from #__cart_row as cr ';
        $inner = 'inner join #__product as p on p.id = cr.id_product ';
        $inner .= 'inner join #__tax as t on t.id = p.id_tax ';
        $inner .= 'left join #__product_lang as pl on p.id = pl.id_product and pl.id_lang = ' . $idLang;
        if ($customertype == 2) {
            $inner .= ' inner join #__catalog_rules cru on p.id = cru.id_product and id_group = 42 ';
        }
        $where = ' where cr.id_cart = ' . $data['id'] . ' ';
        $order = '';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $list = $q->fetchAll();
        // Calcolo omaggi
        $newRow = [];
        $newRow['id'] = 0;
        $newRow['id_cart'] = 0;
        $newRow['reference'] = '';
        $newRow['nameproduct'] = 'SCONTO';
        $newRow['quantity'] = 1;
        $newRow['price'] = 0;
        $newRow['options'] = ArrayHelper::toJSON(['type' => 'product', 'data' => ['issample' => 0]]);

        foreach ($list as &$row) {
            $optionsrow = ArrayHelper::getArray(ArrayHelper::fromJSON($row['options']), 'data');
            $isFree = ArrayHelper::getBool($optionsrow, 'isfree', false);
            $additionalDiscout = ArrayHelper::getFloat($optionsrow, 'additiona-discount', 0);
            if ($additionalDiscout > 0) {
                // Calcolo lo sconto riga
                $row['price'] = (float) $row['price'] * (1 - $additionalDiscout / 100);
            }
            if ($isFree) {
                $newRow['id_cart'] = $row['id_cart'];
                $newRow['price'] -= $row['price'] * $row['quantity'];
            }
        }

        /* Controllo se ci sono degli sconti di quantità e genero una riga - 2020/04/26 */
        /* Eliminato sconto quantità come valore - 2020/07/10 */
        /*
        $optionshead = ArrayHelper::fromJSON($data['options']);
        $cartnumber = ArrayHelper::getInt($optionshead, 'cart-number', 1);
        $discount = $optionshead['discount'];
        if(ArrayHelper::getFloat($discount, 'shipping1', 0) > 0 && $cartnumber == 1) {
            $newRow['id_cart'] = $row['id_cart'];
            $newRow['price'] -= ArrayHelper::getFloat($discount, 'shipping1', 0);
            $newRow['nameproduct'] = 'SCONTO QTA';
        }
        if(ArrayHelper::getFloat($discount, 'shipping2', 0) > 0 && $cartnumber == 2) {
            $newRow['id_cart'] = $row['id_cart'];
            $newRow['price'] -= ArrayHelper::getFloat($discount, 'shipping2', 0);
            $newRow['nameproduct'] = 'SCONTO QTA';
        }
        */
        $optionshead = ArrayHelper::fromJSON($data['options']);
        if (isset($optionshead['cashback'])) {
            $cashback = (float)$optionshead['cashback'];
            $newRow['id_cart'] = $row['id_cart'];
            $newRow['price'] = $cashback;
            $newRow['nameproduct'] = 'CASHBACK';
        }

        unset($row);
        if ($newRow['id_cart'] !== 0) {
            $list[] = $newRow;
        }
        return $list;
    }

    public function cartToAdhoc($idorder)
    {
        $tbl = $this->db->tableFix('#__cart');
        $values['status'] = 3;
        $query = $this->db->newFluent()
            ->update($tbl)->set($values)
            ->where('id', (int) $idorder);
        $query->execute();
    }
}
