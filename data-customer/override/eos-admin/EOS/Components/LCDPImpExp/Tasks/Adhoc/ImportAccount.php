<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\ArrayHelper;

class ImportAccount extends ImportTextData
{

    protected $model;
    protected $otherFields = [];
    protected $importStatus = true;

    protected function getAddress(string $reference): array
    {
        //$this->addLog($this->RE('Reference: ' . $reference));
        $q = $this->db->prepare('select id, id_customer, reference 
              from #__address 
              where reference  = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(',', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getAddressByVAT(string $vat, string $cif): array
    {
        //$this->addLog($this->RE('VAT: ' . $vat. 'CIF: ' . $cif));
        $q = $this->db->prepare('select distinct id, id_customer, reference 
              from #__address 
              where vat  = :vat and cif = :cif');
        $q->bindValue(':vat', $vat);
        $q->bindValue(':cif', $cif);
        $q->execute();
        $list = $q->fetchAll();
        return empty($list) ? [] : $list[0];
    }

    protected function getCoddesAddress(string $reference, string $coddes): array
    {
        //$this->addLog($this->RE('Reference: ' . $reference.' - CodDes: '.$coddes));
        $q = $this->db->prepare('select id, id_customer, reference
              from #__address 
              where reference  = :reference
              and options like ' . $this->db->quote('%coddes":"' . $coddes . '%'));
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(',', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Codice Destinazione: ' . $coddes));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getCoddesAddressByVAT(string $vat, string $coddes): array
    {
        //$this->addLog($this->RE('VAT: ' . $vat.' - CodDes: '.$coddes));
        $q = $this->db->prepare('select id, id_customer, reference
              from #__address 
              where vat  = :vat
              and  json_unquote(JSON_EXTRACT(options, "$.coddes")) = ' . $coddes);
        $q->bindValue(':vat', $vat);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(',', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $vat . ' Codice Destinazione: ' . $coddes));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getAccount(string $reference): array
    {
        //$this->addLog($this->RE('Reference: ' . $reference));
        $q = $this->db->prepare('select id
            from #__account_user 
            where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(', ', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getCity(string $city, string $zipcode, string $prov): array
    {
        //$this->addLog($this->RE('Ricerca città: ' . $city . ' CAP: ' . $zipcode));
        $q = $this->db->prepare('select ct.id, ct.name, ct.zip_code
              from #__localization_city ct
              inner join #__localization_state st on st.id = ct.id_state 
              where ((ct.name = :name) or (ct.name = :namens)) and ct.zip_code = :zipcode and st.iso_code = :prov');
        $q->bindValue(':name', $city);
        // Montecatini Terme == Montecatini-Terme [Alcuni protebbero essere senza spazi]
        $q->bindValue(':namens', str_replace(' ', '-', $city));
        $q->bindValue(':zipcode', $zipcode);
        $q->bindValue(':prov', $prov);
        $q->execute();
        $r = $q->fetch();
        if (!empty($r)) {
            $res['id'] = $r['id'];
            $res['find'] = true;
        } else {
            $q2 = $this->db->prepare('select ct.id, ct.name, ct.zip_code
              from #__localization_city ct
              inner join #__localization_state st on st.id = ct.id_state 
              where ct.zip_code = :zipcode');
            $q2->bindValue(':zipcode', $zipcode);
            $q2->execute();
            $r2 = $q2->fetch();
            $res['id'] = empty($r2) ? 1 : (int) $r2['id'];
            $res['find'] = false;
        }
        return $res;
    }

    protected function getPaymentID(string $codpag): int
    {
        $sql = 'select epa.id, epa.PADESCRI as name from #__ext_pag_amen epa where epa.PACODICE = :codpag  ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':codpag', $codpag);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : (int)$r['id'];
    }

    protected function getDiscountByRef(string $coddiscount): int
    {
        $sql = 'select ag.id from #__account_group ag where ag.name = :coddiscount  ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':coddiscount', $coddiscount);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : (int)$r['id'];
    }

    protected function getNewAccount(): string
    {
        // Il max non è garantito che sia sempre uguale l'autoincrementale!
        $q = $this->db->prepare('select max(id) as MaxID 
            from #__account_user');
        $q->execute();
        $r = $q->fetch();
        $num = empty($r) ? 0 : (int) $r['MaxID'];
        $num++;
        return 'cavesuser' . $num;
    }

    protected function updateAddresses(string $reference, int $payment)
    {
        $sql = 'select id, options from #__address where reference = :reference  ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $address) {
            $this->db->beginTransaction();
            $options = json_decode($address['options'], true);
            $optionsvalues = [
                'id_agent' => $options['id_agent'],
                'payment' => $payment,
                'pec' => $options['pec'],
                'unicode' => $options['unicode'],
                'note' => $options['note'],
                'coddes' => $options['coddes'],
                'advisor' => (isset($options['advisor'])) ? $options['advisor'] : 0,
                'lat' => (isset($options['lat'])) ? $options['lat'] : '',
                'long' => (isset($options['long'])) ? $options['long'] : '',
            ];
            $tblAddress = $this->db->tableFix('#__address');
            $values['options'] = json_encode($optionsvalues);
            $query = $this->db->newFluent()->update($tblAddress)->set($values)->where('id', $address['id']);
            $query->execute();
            $this->db->commit();
        }
    }

    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Import Account';
        if (!isset($this->params['filename'])) {
            $this->params['filename'] = '{uploads}import/test_account.txt';
        }

        $this->params['mapping'] = [
            'codice' => ['pos' => 0], //Codice cliente
            'tipocliente' => ['pos' => 1], // Tipo cliente
            'datainserimento' => ['pos' => 2], //Data inserimento
            'datamodifica' => ['pos' => 3], //Data modifica
            'piva' => ['pos' => 4], //PIVA
            'codfic' => ['pos' => 5], //Codice fiscale
            'address1' => ['pos' => 6], //Indirizzo
            'descri' => ['pos' => 7], //Descrizione
            'city' => ['pos' => 8], //Città
            'isostate' => ['pos' => 9], //Codice Stato
            'prov' => ['pos' => 10], //Provincia
            'zipcode' => ['pos' => 11], //Codice postale
            'phone' => ['pos' => 12], // Numero di telefono
            'fax' => ['pos' => 13], // Fax
            'numlis' => ['pos' => 14], // Numlis
            'catsconto' => ['pos' => 15], // Categoria sconto
            'codicezona' => ['pos' => 16], // Codice zona
            'agente' => ['pos' => 17], // Codice Agente
            'codpag' => ['pos' => 18], // Codice pag
            'consup' => ['pos' => 19], // Consup
            'obsolescenza' => ['pos' => 20], // Obsolescenza
            'email-alternative' => ['pos' => 21], // Email
            'note' => ['pos' => 22] // Email
        ];
        // Creo il model per velocizzare ma su PHP 7.2 è molto poca la differenza
        $this->model = new \EOS\Components\Account\Models\UserModel($this->container);
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);
    }

    protected function readRow(array $data)
    {
        if ($this->readIdx === 0 || $data['tipocliente'] != "C") {
            return;
        }

        $datecheck = strtotime("-2 days");
        if ($data['datamodifica'] == "/  /") {
            $dateins = strtotime(str_replace('/', '-', $data['datainserimento']));
            if ($dateins < $datecheck) {
                return;
            }
        } else {
            $dateup = strtotime(str_replace('/', '-', $data['datamodifica']));
            if ($dateup < $datecheck) {
                return;
            }
        }

        $r = $this->getAccount($data['codice']);
        $new = false;
        if (empty($r)) {
            $rv = $this->getAddressByVAT($data['piva'], $data['codfic']);
            if (empty($rv)) {
                $data['id'] = 0;
                $data['email'] = $this->getNewAccount() . '@lescaves.it';
                $data['username'] = $this->getNewAccount();
                $data['name'] = $data['descri'];
                $data['status'] = 1;
                $data['password'] = '0000000000';
                $data['budget'] = '0';
                $data['account-type'] = '0';
                $data['reference'] = $data['codice'];
                $data['id_lang'] = 0;
                if ($data['numlis'] == 'B2C') {
                    $data['group'] = 42;
                } else {
                    if ($data['catsconto'] != '') {
                        $group = $this->getDiscountByRef($data['catsconto']);
                        $data['group'] = [1, $group];
                    } else {
                        $data['group'] = 1;
                    }
                }
                $new = true;
            } else {
                //$this->addLog('Update by VAT ID: ' . $rv['id'] . ' - Reference: ' . $data['codice']);
                $oldData = $this->model->getData($rv['id_customer']);
                $this->assignExtraData($data, $oldData);
                $data['status'] = $oldData['status'] == 1 ? 1 : 0;
                if ($data['obsolescenza'] != '/  /') {
                    $data['status'] = 0;
                }
                $data['name'] = $data['descri'];
                $data['reference'] = $data['codice'];
                if ($data['numlis'] == 'B2C') {
                    $data['group'] = 42;
                } else {
                    if ($data['catsconto'] != '') {
                        $group = $this->getDiscountByRef($data['catsconto']);
                        $data['group'] = [1, $group];
                    } else {
                        $data['group'] = 1;
                    }
                }
            }
        } else {
            //$this->addLog('Update ID: ' . $r['id'] . ' - Reference: ' . $data['codice']);
            $oldData = $this->model->getData($r['id']);
            $this->assignExtraData($data, $oldData);
            $data['status'] = $oldData['status'] == 1 ? 1 : 0;
            if ($data['obsolescenza'] != '/  /') {
                $data['status'] = 0;
            }
            $data['name'] = $data['descri'];
            if ($data['numlis'] == 'B2C') {
                $data['group'] = 42;
            } else {
                if ($data['catsconto'] != '') {
                    $group = $this->getDiscountByRef($data['catsconto']);
                    $data['group'] = [1, $group];
                } else {
                    $data['group'] = 1;
                }
            }
        }

        $error = '';
        if ($this->debug) {
            $this->addLog('save data:' . json_encode($data));
        }
        if (!$this->model->saveData($data, $error)) {
            throw new \Exception($this->RE('Errore sui dati: ' . $error));
        }

        $codpag = isset($data['codpag']) ? $this->getPaymentID($data['codpag']) : 0;

        if ($new == 1) {
            //echo "<br/>sono in aggiunta indirizzo<br/>";
            $r = $this->getAccount($data['codice']);
            $data['id'] = 0;
            $data['id_country'] = 0;
            $data['id_state'] = 0;
            //$data['id-customer'] = intval($data['codice']);
            $data['id-customer'] = $r['id'];
            $data['id_manufacturer'] = 0;
            $data['alias'] = $data['descri'];
            $data['company'] = $data['descri'];
            $data['name'] = '';
            $data['surname'] = '';
            $data['address1'] = $data['address1'];
            $data['postcode'] = $data['zipcode'];
            $city = $this->getCity($data['city'], $data['zipcode'], $data['prov']);
            if ($city['find'] == true) {
                $data['id-city'] = $city['id'];
                $data['address2'] = '';
            } else {
                $data['id-city'] = $city['id'];
                $data['address2'] = $data['city'];
            }
            $data['other'] = '';
            $data['phone'] = $data['phone'];
            $data['phone-mobile'] = '';
            $data['vat'] = $data['piva'];
            $data['cif'] = $data['codfic'];
            $data['reference'] = $data['codice'];
            $data['status'] = 'on';
            $data['deleted'] = 0;
            $agent = $this->getAccount($data['agente']);
            $data['id-agent'] = (!empty($agent)) ? $agent['id'] : 0;
            $data['payment'] = $codpag;
            $data['pec'] = '';
            $data['unicode'] = '';
            $data['advisor'] = 0;
            $data['lat'] = '';
            $data['long'] = '';
            $data['coddes'] = '9999';
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            $ma->saveData($data, $error);
        } else {
            //echo "<br/>sono in aggiornamento indirizzo<br/>";
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            $ra = $this->getCoddesAddress($data['codice'], 9999);
            unset($data['advisor']);
            if (!empty($ra)) {
                //$this->addLog('Update ID: ' . $ra['id'] . ' - Reference: ' . $data['codice']);
                $data['id'] = $ra['id'];
                $oldData = $ma->getData($ra['id']);
                $codpag = $codpag;
                $company = $data['descri'];
                $this->assignExtraData($data, $oldData);
                $agent = $this->getAccount($data['agente']);
                $data['id-agent'] = (!empty($agent)) ? $agent['id'] : 0;
                $data['id-customer'] = $this->getAccount($data['codice'])['id'];
                $data['status'] = $oldData['status'] == 1 ? 'on' : '';
                $data['deleted'] = $oldData['deleted'] == 1 ? 1 : 0;
                if ($data['obsolescenza'] != '/  /') {
                    $data['deleted'] = 1;
                }
                $data['payment'] = $codpag;
                $data['coddes'] = '9999';
                if (!isset($data['alias']) || $data['alias'] == '') {
                    if ($company != '') {
                        $data['alias'] = $company;
                    }
                }
                //
                $data['company'] = $company;
                $data['name'] = '';
                $ma->saveData($data, $error);
            } else {
                $rav = $this->getCoddesAddressByVAT($data['piva'], 9999);
                if (!empty($rv)) {
                    $this->addLog('Update ID: ' . $rav['id'] . ' - Reference: ' . $data['codice']);
                    $data['id'] = $rav['id'];
                    $oldData = $ma->getData($rav['id']);
                    $agent = $this->getAccount($data['agente']);
                    $data['id-agent'] = (!empty($agent)) ? $agent['id'] : 0;
                    $codpag = $codpag;
                    $company = $data['descri'];
                    $this->assignExtraData($data, $oldData);
                    $data['id-customer'] = $this->getAccount($data['codice'])['id'];
                    $data['status'] = $oldData['status'] == 1 ? 'on' : '';
                    $data['deleted'] = $oldData['deleted'] == 1 ? 1 : 0;
                    if ($data['obsolescenza'] != '/  /') {
                        $data['deleted'] = 1;
                    }
                    $data['payment'] = $codpag;
                    $data['coddes'] = '9999';
                    if (!isset($data['alias']) || $data['alias'] == '') {
                        if ($company != '') {
                            $data['alias'] = $company;
                        }
                    }
                    $data['company'] = $company;
                    $data['name'] = '';
                    $data['reference'] = $data['codice'];
                    $ma->saveData($data, $error);
                } else {
                    $r = $this->getAccount($data['codice']);
                    $data['id'] = 0;
                    $data['id_country'] = 0;
                    $data['id_state'] = 0;
                    //$data['id-customer'] = intval($data['codice']);
                    $data['id-customer'] = $r['id'];
                    $data['id_manufacturer'] = 0;
                    if (!isset($data['alias']) || $data['alias'] == '') {
                        if ($data['descri'] != '') {
                            $data['alias'] = $data['descri'];
                        }
                    }
                    $data['company'] = $data['descri'];
                    $data['name'] = '';
                    $data['surname'] = '';
                    $data['address1'] = $data['address1'];
                    $data['postcode'] = $data['zipcode'];
                    $city = $this->getCity($data['city'], $data['zipcode'], $data['prov']);
                    if ($city['find'] == true) {
                        $data['id-city'] = $city['id'];
                        $data['address2'] = '';
                    } else {
                        $data['id-city'] = $city['id'];
                        $data['address2'] = $data['city'];
                    }
                    $data['other'] = '';
                    $data['phone'] = $data['phone'];
                    $data['phone-mobile'] = '';
                    $data['vat'] = $data['piva'];
                    $data['cif'] = $data['codfic'];
                    $data['reference'] = $data['codice'];
                    $data['status'] = 'on';
                    $data['deleted'] = 0;
                    $agent = $this->getAccount($data['agente']);
                    $data['id-agent'] = (!empty($agent)) ? $agent['id'] : 0;
                    $data['payment'] = $this->getPaymentID($data['codpag']);
                    $data['pec'] = '';
                    $data['unicode'] = '';
                    $data['lat'] = '';
                    $data['long'] = '';
                    $data['advisor'] = 0;
                    $data['coddes'] = '9999';
                    $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
                    $ma->saveData($data, $error);
                }
            }
        }
        //Aggiorno tutti gli indirizzi collegati al principale
        $this->updateAddresses($data['codice'], $codpag);
    }
}
