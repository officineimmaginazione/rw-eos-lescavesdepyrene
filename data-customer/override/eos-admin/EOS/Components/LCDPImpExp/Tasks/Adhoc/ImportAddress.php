<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\ArrayHelper;

class ImportAddress extends ImportTextData
{

    protected $model;
    protected $otherFields = [];
    protected $importStatus = true;

    protected function getAccount(string $reference): array
    {
        //$this->addLog($this->RE('Reference: ' . $reference));
        $q = $this->db->prepare('select id
            from #__account_user 
            where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(', ', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getAddress(string $reference): array
    {
        //$this->addLog($this->RE('Reference: ' . $reference));
        $q = $this->db->prepare('select id, id_customer, reference 
              from #__address 
              where reference  = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(',', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getAddressByVAT(string $vat, string $cif): array
    {
        //$this->addLog($this->RE('VAT: ' . $vat. 'CIF: ' . $cif));
        $q = $this->db->prepare('select distinct id, id_customer, reference 
              from #__address 
              where vat  = :vat and cif = :cif');
        $q->bindValue(':vat', $vat);
        $q->bindValue(':cif', $cif);
        $q->execute();
        $list = $q->fetchAll();
        return empty($list) ? [] : $list[0];
    }

    protected function getCoddesAddress(string $reference, string $coddes): array
    {
        //$this->addLog($this->RE('Reference: ' . $reference.' - CodDes: '.$coddes));
        $q = $this->db->prepare('select id, id_customer, reference
              from #__address 
              where reference  = :reference
              and options like ' . $this->db->quote('%coddes":"' . $coddes . '%'));
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(',', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getCity(string $city, string $zipcode, string $prov): array
    {
        //$this->addLog($this->RE('Ricerca città: ' . $city . ' CAP: ' . $zipcode));
        $q = $this->db->prepare('select ct.id, ct.name, ct.zip_code
              from #__localization_city ct
              inner join #__localization_state st on st.id = ct.id_state 
              where ((ct.name = :name) or (ct.name = :namens)) and ct.zip_code = :zipcode and st.iso_code = :prov');
        $q->bindValue(':name', $city);
        // Montecatini Terme == Montecatini-Terme [Alcuni protebbero essere senza spazi]
        $q->bindValue(':namens', str_replace(' ', '-', $city));
        $q->bindValue(':zipcode', $zipcode);
        $q->bindValue(':prov', $prov);
        $q->execute();
        $r = $q->fetch();
        if (!empty($r)) {
            $res['id'] = $r['id'];
            $res['find'] = true;
        } else {
            $q2 = $this->db->prepare('select ct.id, ct.name, ct.zip_code
              from #__localization_city ct
              where ct.zip_code = :zipcode');
            $q2->bindValue(':zipcode', $zipcode);
            $q2->execute();
            $r2 = $q2->fetch();
            $res['id'] = empty($r2) ? 1 : (int) $r2['id'];
            $res['find'] = false;
        }
        return $res;
    }

    protected function getPayment(): int
    {
        $sql = 'select epa.id, epa.PADESCRI as name from #__ext_pag_amen epa ';
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : (int)$r['id'];
    }


    protected function checkAddress(string $reference, string $coddes): bool
    {

        $q = $this->db->prepare('select reference
            from #__address 
            where reference =' . $this->db->quote($reference) . '
            and options like ' . $this->db->quote('%coddes":"' . $coddes . '%'));
        $q->execute();
        $list = $q->fetchAll();
        return empty($list) ? false : true;
    }

    protected function getAddressByRefandAddress(string $reference, string $address): bool
    {
        $q = $this->db->prepare('select id
            from #__account_user 
            where reference = "' . $this->db->quote($reference) . '"');
        $q->execute();
        $customer = $q->fetch();
        $q2 = $this->db->prepare('select reference
            from #__address 
            where address = "' . $this->db->quote($reference) . '"
            and id = ' . $customer['id']);
        $q2->execute();
        $r = $q2->fetch();
        return empty($r) ? 0 : (int)$r['id'];
    }

    protected function getCompany(string $reference): string
    {
        $q = $this->db->prepare('select name
            from #__account_user 
            where reference = :reference
        ');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(',', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? "" : $list[0]['name'];
    }


    protected function checkParentIdAgent(string $reference): string
    {
        $q = $this->db->prepare('select options
            from #__address 
            where reference = ' . $this->db->quote($reference) . '
            and options like \'%coddes":"9999%\'
        ');
        $q->execute();
        $list = $q->fetchAll();
        $data = explode(',', $list[0]['options']);
        $options = [];
        foreach ($data as $key => $split) {
            $split = explode(':', $split);
            $options[$key]['name'] = str_replace("{", "", $split[0]);
            $options[$key]['value'] = $split[1];
        }
        $id_agent = str_replace('"', '', $options[0]['value']);
        return $id_agent;
    }
    protected function getParentCodPag(string $reference): string
    {
        $q = $this->db->prepare('select options
            from #__address 
            where reference = :reference
            and options like \'%coddes":"9999%\'
        ');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        $id_payment = 0;
        if (!empty($list)) {
            $data = explode(',', $list[0]['options']);
            $options = [];
            foreach ($data as $key => $split) {
                $split = explode(':', $split);
                $options[$key]['name'] = str_replace("{", "", $split[0]);
                $options[$key]['value'] = $split[1];
            }
            $id_payment = str_replace('"', '', $options[1]['value']);
        }
        return (is_null($id_payment)) ? 0 : $id_payment;
    }


    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Import Indirizzi';
        if (!isset($this->params['filename'])) {
            $this->params['filename'] = '{uploads}import/test_address.txt';
        }

        $this->params['mapping'] = [
            'coddes' => ['pos' => 0], //Coddes
            'codice' => ['pos' => 1], //Codice
            'predef' => ['pos' => 2], //Predef
            'address1' => ['pos' => 3], //Indirizzo
            'descri' => ['pos' => 4], //Descrizione
            'city' => ['pos' => 5], //Città
            'prov' => ['pos' => 6], //Provincia
            'isostate' => ['pos' => 7], //Codice Stato
            'zipcode' => ['pos' => 8], //Codice postale
            'phone' => ['pos' => 9], //Numero di telefono
            'fax' => ['pos' => 10], //Fax
            'tiprif' => ['pos' => 11], //Tiprif
            'datamodifica' => ['pos' => 12], //Data modifica
            'codage' => ['pos' => 13], //Codice Agente
            'notecli' => ['pos' => 14], //Note
            'obsolescence' => ['pos' => 15] //Note
        ];
        // Creo il model per velocizzare ma su PHP 7.2 è molto poca la differenza
        $this->model = new \EOS\Components\Account\Models\AddressModel($this->container);
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);
    }

    protected function readRow(array $data)
    {
        if ($this->readIdx === 0) {
            return;
        }

        if ($data['datamodifica'] != "/  /") {
            $dateup = strtotime(str_replace('/', '-', $data['datamodifica']));
            $datecheck = strtotime("-1 days");

            if ($dateup < $datecheck) {
                return;
            }
        }

        $exists = $this->checkAddress($data['codice'], $data['coddes']);
        $save = true;

        if ($data['address1'] != "") {
            $ra = $this->getAccount($data['codice']);
            if (!$exists && isset($ra['id'])) {
                $id = $this->getAddressByRefandAddress($data['codice'], $data['address1']);
                if ($id != 0) {
                    $this->addLog('Update ID: ' . $id . ' - Reference: ' . $data['codice']);
                    $data['id'] = $id;
                    $oldData = $this->model->getData($id);
                    $codpag = $this->getParentCodPag($data['codice']);
                    if ($data['codage'] == "") {
                        $parentIdAgent = $this->checkParentIdAgent($data['codice']);
                        $data['id-agent'] = $parentIdAgent;
                    } else {
                        $agentId = $this->getAccount($data['codage']);
                        $data['id-agent'] = $agentId['id'];
                    }
                    $address = $data['address1'];
                    $alias = $data['descri'];
                    $company = $this->getCompany($data['codice']);
                    $data['note'] = $data['notecli'];
                    $this->assignExtraData($data, $oldData);
                    $data['status'] = $oldData['status'] == 1 ? 'on' : '';
                    $data['deleted'] = 0;
                    if ($data['obsolescence'] != "/  /") {
                        $data['deleted'] = 1;
                        $data['status'] = '';
                    }
                    $data['id-customer'] = $this->getAccount($data['codice'])['id'];
                    if ($data['id-customer'] == '') {
                        return;
                    }
                    $data['reference'] = $data['codice'];
                    $data['address1'] = ($address != $oldData['address1']) ? $address : $oldData['address1'];
                    $city = $this->getCity($data['city'], $data['zipcode'], $data['prov']);
                    if ($city['find'] == true) {
                        $data['id-city'] = $city['id'];
                        $data['address2'] = '';
                    } else {
                        $data['id-city'] = $city['id'];
                        $data['address2'] = $data['city'];
                    }
                    $data['company'] = ($company != $oldData['company']) ? $company : $oldData['company'];
                } else {
                    $data['id'] = 0;
                    $data['id_country'] = 0;
                    $data['id_state'] = 0;
                    $data['id-customer'] = $ra['id'];
                    $data['id_manufacturer'] = 0;
                    $data['alias'] = $data['descri'];
                    $company = $this->getCompany($data['codice']);
                    $data['company'] = ($company != "") ? $company : $data['descri'];
                    $data['name'] = '';
                    $data['surname'] = '';
                    $data['address1'] = $data['address1'];
                    $data['postcode'] = $data['zipcode'];
                    $city = $this->getCity($data['city'], $data['zipcode'], $data['prov']);
                    if ($city['find'] == true) {
                        $data['id-city'] = $city['id'];
                        $data['address2'] = '';
                    } else {
                        $data['id-city'] = $city['id'];
                        $data['address2'] = $data['city'];
                    }
                    $data['other'] = '';
                    $data['phone'] = $data['phone'];
                    $data['phone-mobile'] = '';
                    $data['vat'] = '';
                    $data['cif'] = '';
                    $data['reference'] = $data['codice'];
                    $data['status'] = 'on';
                    $data['deleted'] = 0;
                    if ($data['obsolescence'] != "/  /") {
                        $data['deleted'] = 1;
                        $data['status'] = '';
                    }
                    $data['id-agent'] = '9999';
                    $codpag = $this->getParentCodPag($data['codice']);
                    $data['payment'] = $codpag;
                    $data['pec'] = '';
                    $data['unicode'] = '';
                    $data['note'] = $data['notecli'];
                    $data['coddes'] = $data['coddes'];
                    if ($data['codage'] == "") {
                        $parentIdAgent = $this->checkParentIdAgent($data['codice']);
                        $data['id-agent'] = $parentIdAgent;
                    } else {
                        $agentId = $this->getAccount($data['codage']);
                        $data['id-agent'] = $agentId['id'];
                    }
                }
            } elseif ($exists) {
                $r = $this->getCoddesAddress($data['codice'], $data['coddes']);
                $this->addLog('Update ID: ' . $r['id'] . ' - Reference: ' . $data['codice']);
                $data['id'] = $r['id'];
                $oldData = $this->model->getData($r['id']);
                $codpag = $this->getParentCodPag($data['codice']);
                if ($data['codage'] == "") {
                    $parentIdAgent = $this->checkParentIdAgent($data['codice']);
                    $data['id-agent'] = $parentIdAgent;
                } else {
                    $agentId = $this->getAccount($data['codage']);
                    $data['id-agent'] = $agentId['id'];
                }
                $address = $data['address1'];
                $alias = $data['descri'];
                $company = $this->getCompany($data['codice']);
                $data['note'] = $data['notecli'];
                $this->assignExtraData($data, $oldData);
                $data['status'] = $oldData['status'] == 1 ? 'on' : '';
                $data['deleted'] = 0;
                if ($data['obsolescence'] != "/  /") {
                    $data['deleted'] = 1;
                    $data['status'] = '';
                }
                $data['id-customer'] = $this->getAccount($data['codice'])['id'];
                if ($data['id-customer'] == '') {
                    return;
                }
                $data['reference'] = $data['codice'];
                $data['address1'] = ($address != $oldData['address1']) ? $address : $oldData['address1'];
                $city = $this->getCity($data['city'], $data['zipcode'], $data['prov']);
                if ($city['find'] == true) {
                    $data['id-city'] = $city['id'];
                    $data['address2'] = '';
                } else {
                    $data['id-city'] = $city['id'];
                    $data['address2'] = $data['city'];
                }
                $data['company'] = ($company != $oldData['company']) ? $company : $oldData['company'];
                //$data['alias'] = ($alias != "") ? $alias : $data['descri'];
                //echo ("<br />");
                //$data['payment'] = $codpag;
                //exit();
            } else {
                $save = false;
            }

            $error = '';
            if ($this->debug) {
                $this->addLog('save data:' . json_encode($data));
            }
            if ($save) {
                if (!$this->model->saveData($data, $error)) {
                    throw new \Exception($this->RE('Errore sui dati: ' . $error));
                }
            }
        }
    }
}
