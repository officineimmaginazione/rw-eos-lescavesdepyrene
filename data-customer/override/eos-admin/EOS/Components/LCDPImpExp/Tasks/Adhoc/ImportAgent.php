<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;
use EOS\System\Util\ArrayHelper;

class ImportAgent extends ImportTextData
{

    protected $model;
    protected $otherFields = [];
    protected $importStatus = true;

    protected function getAccount(string $reference): array
    {
        //$this->addLog($this->RE('Reference: ' . $reference));
        $q = $this->db->prepare('select id
            from #__account_user 
            where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1)
        {
            $idList = implode(', ', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "reference" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getNewAccount(): string
    {
        // Il max non è garantito che sia sempre uguale l'autoincrementale!
        $q = $this->db->prepare('select max(id) as MaxID from #__account_user');
        $q->execute();
        $r = $q->fetch();
        $num = empty($r) ? 0 : (int) $r['MaxID'];
        $num++;
        return 'agelescaves' . $num;
    }

    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Import Agent';
        if (!isset($this->params['filename']))
        {
            $this->params['filename'] = '{uploads}import/test_account.txt';
        }

        $this->params['mapping'] = [
            'codice' => ['pos' => 0], //Codice cliente
            'name' => ['pos' => 1], // Nome
            'email' => ['pos' => 2], // Email
            'pec' => ['pos' => 3], // PEC
            'obsolescenza' => ['pos' => 4], // PIVA
            'tipage' => ['pos' => 5] // PIVA
        ];
        // Creo il model per velocizzare ma su PHP 7.2 è molto poca la differenza
        $this->model = new \EOS\Components\Account\Models\UserModel($this->container);
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);
    }

    protected function readRow(array $data)
    {
        if ($this->readIdx === 0 || $data['tipage'] == 'C' || $data['codice'] == '84' || $data['codice'] == '85')
        {
            return;
        }
        
        $r = $this->getAccount($data['codice']);
        $new = false;
        if (empty($r))
        {
            //$this->addLog('Insert Reference: ' . $data['codice']);
            //echo "<br/>sono in aggiunta cliente<br/>";
            $data['id'] = 0;
            $data['email_alternative'] = $data['email'];
            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email'] = $this->getNewAccount() . '@lescaves.it';
            } 
            $data['username'] = $this->getNewAccount();
            $data['name'] = $data['name'];
            $data['status'] = 1;
            $data['password'] = '0000000000';
            $data['budget'] = '0';
            $data['reference'] = $data['codice'];
            $data['id_lang'] = 0;
            $data['group'] = 2;
            $new = true;
        } else
        {
            //$this->addLog('Update ID: ' . $r['id'] . ' - Reference: ' . $data['codice']);
            //echo "<br/>sono in aggiornamento cliente<br/>";
            $oldData = $this->model->getData($r['id']);
            $this->assignExtraData($data, $oldData);
            if ($data['obsolescenza'] != '/  /')
            {
                $data['status'] = 0;
            } else
            {
                $data['status'] = $oldData['status'] == 1 ? 1 : 0;
            }
            //$data['email'] = $oldData['email'];
            //$data['name'] = $data['descri'];
        }

        $error = '';
        if ($this->debug)
        {
            $this->addLog('save data:' . json_encode($data));
        }
        if (!$this->model->saveData($data, $error))
        {
            throw new \Exception($this->RE('Errore sui dati: ' . $error));
        }
                        
        
    }    
}
