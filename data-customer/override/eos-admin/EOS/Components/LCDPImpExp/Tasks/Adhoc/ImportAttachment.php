<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class ImportAttachment extends ImportTextData
{
    
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->captureException = true;
        $this->params['name'] = 'AdHoc - Attachment';

        $this->params['mapping'] = [
            'invoice' => ['pos' => 0],
            'order' => ['pos' => 3],
            'attachment' => ['pos' => 5]
        ];
    }

    protected function readRow(array $data)
    {
        if($this->readIdx === 0) {
            return;
        }

        if($data['order'] != '') {
            $this->updateOrder($data);
        }
        
        
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);

        if ($this->debug)
        {

        }
    }
    
    protected function updateOrder($data) {
        $tbl = $this->db->tableFix('#__order');
        $values['notes_transaction'] = $data['attachment'];
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('reference', $data['order']);
        $query->execute();
    }

}
