<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;


class ImportFTPAccount extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_conti*.csv';
        $this->modelClass = ImportAccount::class;
        $this->debug = true;
    }
}
