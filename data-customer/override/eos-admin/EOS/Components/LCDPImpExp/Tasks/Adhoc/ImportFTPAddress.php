<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;


class ImportFTPAddress extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_des_dive*.csv';
        $this->modelClass = ImportAddress::class;
        $this->debug = true;
    }
}
