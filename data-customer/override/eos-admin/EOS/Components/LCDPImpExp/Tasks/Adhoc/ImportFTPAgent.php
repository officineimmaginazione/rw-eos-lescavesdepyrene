<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;


class ImportFTPAgent extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_agenti*.csv';
        $this->modelClass = ImportAgent::class;
        $this->debug = true;
    }
}
