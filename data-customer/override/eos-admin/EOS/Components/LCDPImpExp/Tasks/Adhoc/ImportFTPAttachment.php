<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;
use EOS\System\Routing\PathHelper;


class ImportFTPAttachment extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_allegatiordine.csv';
        $this->modelClass = ImportAttachment::class;
        $this->debug = true;
    }
}
