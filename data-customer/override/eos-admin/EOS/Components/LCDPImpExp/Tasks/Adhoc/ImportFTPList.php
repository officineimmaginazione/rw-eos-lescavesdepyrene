<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;

class ImportFTPList extends \EOS\System\Tasks\Task
{

    protected $fileFilter = '';
    protected $modelClass = '';

    protected function load()
    {
        parent::load();
    }

    public function run()
    {
        $eu = new \EOS\Components\LCDPImpExp\Classes\ErrorUtil($this->container);
        $eu->captureException(function ()
        {
            parent::run();
            if ($this->fileFilter == '')
            {
                throw new \Exception('Impostare Filter file!');
            }

            if ($this->modelClass == '')
            {
                throw new \Exception('Impostare la classe del model!');
            }

            $ftpPath = PathHelper::slashToDirSep($this->replacePathTag('{uploads}adhoc/'));
            $updatePath = PathHelper::slashToDirSep($this->replacePathTag('{uploads}adhoc/imported/'));
            $list = glob($ftpPath . $this->fileFilter);
            // La glob non è chiaro se ordina per nome o data e quindi ordino per sicurezza
            sort($list);
            if (empty($list))
            {
                $this->addLog('Nessun file da importare');
            } else
            {
                foreach ($list as $f)
                {
                    $this->addLog('Importazione : ' . basename($f));
                    $tImport = new $this->modelClass($this->container, ['filename' => $f]);
                    $tImport->logProc = $this->logProc;
                    $tImport->run();
                    if (!empty($tImport->getExceptionList()))
                    {
                        // In caso di errori non blocco ma mando una notifica con la lista di problemi   
                    }
                    // Sposto il file alla fine del processo
                    if (!$this->debug)
                    {
                        rename($f, $updatePath . basename($f));
                    }
                }
            }
        });
    }

}
