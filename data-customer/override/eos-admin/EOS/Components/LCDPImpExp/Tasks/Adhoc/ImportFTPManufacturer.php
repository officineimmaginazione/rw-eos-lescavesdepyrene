<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;


class ImportFTPManufacturer extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_marchi*.csv';
        $this->modelClass = ImportManufacturer::class;
        $this->debug = false;
    }
}
