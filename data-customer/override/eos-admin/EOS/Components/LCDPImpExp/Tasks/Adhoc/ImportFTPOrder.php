<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;
use EOS\System\Routing\PathHelper;


class ImportFTPOrder extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_dati_ordini.csv';
        $this->modelClass = ImportOrder::class;
        $this->debug = true;
    }
}
