<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;
use EOS\System\Routing\PathHelper;


class ImportFTPPrice extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_listini*.csv';
        $this->modelClass = ImportPrice::class;
        $this->debug = true;
    }
}
