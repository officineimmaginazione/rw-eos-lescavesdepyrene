<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;


class ImportFTPProduct extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_art_icol*.csv';
        $this->modelClass = ImportProduct::class;
        $this->debug = true;
    }
}
