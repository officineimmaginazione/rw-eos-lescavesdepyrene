<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;


class ImportFTPStock extends ImportFTPList
{

    protected function load()
    {
        parent::load();
        $this->fileFilter = 'exp_saldiart*.csv';
        $this->modelClass = ImportStock::class;
        $this->debug = true;
    }
}
