<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;

class ImportManufacturer extends ImportTextData
{

    protected function getManufacturerID(string $reference): int
    {
        $q = $this->db->prepare('select id
              from #__manufacturer
              where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : $r['id'];
    }
    
    protected function getLastID(): int
    {
        $q = $this->db->prepare('SELECT id FROM #__manufacturer ORDER BY id DESC LIMIT');
        $q->execute();
        $r = $q->fetch();
        print_r($q);
        return empty($r) ? 0 : $r['id'];
    }

    protected function updateReference(int $id, string $reference)
    {
        $q = $this->db->prepare('
              update #__manufacturer
              set reference = :reference
              where id = :id');
        $q->bindValue(':reference', $reference);
        $q->bindValue(':id', $id);
        $q->execute();
    }

    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->captureException = true;
        $this->params['name'] = 'AdHoc - Import manufacturer';

        $this->params['mapping'] = [
            'reference' => ['pos' => 0], // MACODICE	Codice Marchio
            'name' => ['pos' => 1], // AMADESCRI	Descrizione Marchio
        ];
        // Creo il model per velocizzare ma su PHP 7.2 è molto poca la differenza
        $this->model = new \EOS\Components\Product\Models\ManufacturerModel($this->container);
    }

    protected function readRow(array $data)
    {
        if ($this->readIdx === 0)
        {
            return;
        }
        
        $r = $this->getManufacturerID($data['reference']);
        if (empty($r))
        { 
            if($data['name'] == 'MADESCRI') {
                return;
            }
            $data['name-' . $this->idLang] = $data['name'];
            $this->fixListFields($data, 'name-', $this->langList, '');
            $this->fixListFields($data, 'descr-', $this->langList, '');
            $data['id'] = 0;
            $data['status'] = 'on';
        } else {
            $oldData = $this->model->getData($r);
            $this->assignExtraData($data, $oldData);
            if ($this->readIdx === 0)
            {
                return;
            }
            $data['id'] = $r;
            $data['status'] = $oldData['status'] ? 'on' : '';
            
            
        }
        //aramadani: problema - ogni importazione defleggava lo status.
        //$data['status'] = 'on';
        
        
        $error = '';
        if (!$this->model->saveData($data, $error))
        {
            throw new \Exception($this->RE('Errore sui dati: ' . $error));
        }
        if (empty($r))
        { 
            $data['id'] = $this->getLastID();
            $this->updateReference($data['id'], $data['reference']);
        }
        
    }

}
