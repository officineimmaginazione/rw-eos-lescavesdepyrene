<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;
use EOS\System\Util\ArrayHelper;

class ImportOpenDebit extends ImportTextData
{

    protected $model;
    protected $otherFields = [];
    protected $importStatus = true;

    

    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Import Partite';
        if (!isset($this->params['filename']))
        {
            $this->params['filename'] = '{uploads}import/test_account.txt';
        }

        $this->params['mapping'] = [
            'PTNUMPAR' => ['pos' => 0], 
            'PTDATSCA' => ['pos' => 1], 
            'PTTIPCON' => ['pos' => 2], 
            'PTCODCON' => ['pos' => 3], 
            'PT_SEGNO' => ['pos' => 4], 
            'PTTOTIMP' => ['pos' => 5], 
            'PTNUMDOC' => ['pos' => 6], 
            'PTDATDOC' => ['pos' => 7], 
            'PTFLSOSP' => ['pos' => 8], 
            'PTFLIMPE' => ['pos' => 9], 
            'PTDATREG' => ['pos' => 10]
        ];
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);
    }

    protected function readRow(array $data)
    {
        if ($this->readIdx === 0)
        {
            $this->truncateTable();
            return;
        }
        $this->insertRow($data);
    }
    
    
    protected function truncateTable(): bool
    {
        $q = $this->db->prepare('TRUNCATE TABLE eos_ext_open_debit;');
        $q->execute();
        return true;
    }
    
    protected function insertRow(array $data): bool
    {
        /* $q = $this->db->prepare('
            INSERT INTO `eos_ext_open_debit`(`Capo_Area`, `Agente`, `Codice_Agente`, `Cliente`, `Codice_Cliente`, `Documento`, `Tipo_Documento`, `Numero_Documento`, `Data_Documento`, `Scadenza_Documento`, `Ultima_Registrazione`, `Fatturato`, `IVA`, `Importo`, `Dare`, `Avere`, `Esposizione`, `A_Scadere`, `Scaduto`)
            VALUES (:capoarea,:agente,:codagente,:cliente,:codcliente,:documento,:tipodocumento,:numerodocumento,:datadocumento,:scadenzadocumento,:ultimaregistrazione,:fatturato,:iva,:importo,:dare,:avere,:esposizione,:ascadere,:scaduto) 
            ');
        $q->bindValue(':capoarea', $data['capoarea']);
        $q->bindValue(':agente', $data['agente']);
        $q->bindValue(':codagente', $data['codagente']);
        $q->bindValue(':cliente', $data['cliente']);
        $q->bindValue(':codcliente', $data['codcliente']);
        $q->bindValue(':documento', $data['documento']);
        $q->bindValue(':tipodocumento', $data['tipodocumento']);
        $q->bindValue(':numerodocumento', $data['numerodocumento']);
        $q->bindValue(':datadocumento', $data['datadocumento']);
        $q->bindValue(':scadenzadocumento', $data['scadenzadocumento']);
        $q->bindValue(':ultimaregistrazione', $data['ultimaregistrazione']);
        $q->bindValue(':fatturato', $data['fatturato']);
        $q->bindValue(':iva', $data['iva']);
        $q->bindValue(':importo', $data['importo']);
        $q->bindValue(':dare', $data['dare']);
        $q->bindValue(':avere', $data['avere']);
        $q->bindValue(':esposizione', $data['esposizione']);
        $q->bindValue(':ascadere', $data['ascadere']);
        $q->bindValue(':scaduto', $data['scaduto']);
        $q->execute(); */
        $data['PTTOTIMP'] = str_replace(',','.',$data['PTTOTIMP']);
        $data['PTDATSCA'] = $this->lang->langDateTimeToDbDateTime($data['PTDATSCA']);
        if($data['PTDATDOC'] != '') {
            $data['PTDATDOC'] = $this->lang->langDateTimeToDbDateTime($data['PTDATDOC']);
        } else {
            $data['PTDATDOC'] = null;
        }
        $data['PTDATREG'] = $this->lang->langDateTimeToDbDateTime($data['PTDATREG']);
        $values = $data;
        $tblContent = $this->db->tableFix('#__ext_open_debit');
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();
        return true;
    }
    
}
