<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class ImportOrder extends ImportTextData
{

    private $tot = 0;
    private $totimp = 0;
    private $tottax = 0;
    private $idorder = 1;
    private $checkorder = 0;
    private $reference = '0000000000';
    private $diffList = [];

    protected function getProductIDActive(string $reference): int
    {
        $q = $this->db->prepare('select id
              from #__product
              where reference = :reference and status = 1');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : $r['id'];
    }

    protected function getOrder(string $reference)
    {
        $q = $this->db->prepare('select id
              from #__order
              where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? null : $r['id'];
    }

    protected function getCustomer(string $reference, $id): array
    {
        if ($id != 'NULL' && $id != '') {
            $q = $this->db->prepare('select *
              from #__address
              where reference = :reference and json_unquote(JSON_EXTRACT(options, "$.coddes")) = ' . rtrim($id) . ' ');
            $q->bindValue(':reference', $reference);
        } else {
            $q = $this->db->prepare('select *
              from #__address
              where reference = :reference and json_unquote(JSON_EXTRACT(options, "$.coddes")) = 9999 ');
            $q->bindValue(':reference', $reference);
        }
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            $q = $this->db->prepare('select *
              from #__address
              where reference = :reference order by id ASC limit 1 ');
            $q->bindValue(':reference', $reference);
        }
        return empty($r) ? [] : $r;
    }

    protected function getLastID(): int
    {
        $q = $this->db->prepare('select max(id) as max from #__order');
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 1 : $r['max'] + 1;
    }

    public function changeStatusCartById($id)
    {
        $this->db->beginTransaction();
        $tblCart = $this->db->tableFix('#__cart');
        $values['status'] = 5;
        $query = $this->db->newFluent()->update($tblCart)->set($values)->where('id', $id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function changeStatusOrderByReference($reference)
    {
        $this->db->beginTransaction();
        $tblOrder = $this->db->tableFix('#__order');
        $values['id_transaction'] = 'D';
        $query = $this->db->newFluent()->update($tblOrder)->set($values)->where('reference', $reference);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function deleteRowById($id)
    {
        $this->db->beginTransaction();
        $tblOrderProduct = $this->db->tableFix('#__order_row');
        $query = $this->db->newFluent()->deleteFrom($tblOrderProduct)->where('id_order', (int) $id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    protected function load()
    {
        parent::load();
        $this->debug = true;
        $this->captureException = true;
        $this->params['name'] = 'AdHoc - Order';

        $this->params['mapping'] = [
            'reference' => ['pos' => 0],
            'row' => ['pos' => 1],
            'row2' => ['pos' => 2],
            'idcart' => ['pos' => 3],
            'type' => ['pos' => 4],
            'type2' => ['pos' => 5],
            'date' => ['pos' => 8],
            'refcustomer' => ['pos' => 10],
            'refaddress' => ['pos' => 11],
            'idpayment' => ['pos' => 12],
            'refpayment' => ['pos' => 13],
            'totdiscount' => ['pos' => 14],
            'note' => ['pos' => 17],
            'totimp' => ['pos' => 20],
            'tottax' => ['pos' => 21],
            'refprod' => ['pos' => 22],
            'nameprod' => ['pos' => 24],
            'quantity' => ['pos' => 26],
            'price' => ['pos' => 28],
            'totrow' => ['pos' => 32],
            'refhistory' => ['pos' => 34],
            'invoice' => ['pos' => 35],
            'evaso' => ['pos' => 36]
        ];
    }

    protected function readRow(array $data)
    {
        $tot = 0;

        if ($this->readIdx === 0) {
            return;
        }

        if ($this->readIdx === 0 || $data['type'] === 'ORCAM' || $data['type'] === 'ORCAT' || $data['type'] === 'EX-TU' || $data['type'] === 'EXP') {
            return;
        }

        if ($data['type'] === 'ORDTE') {
            $data['status'] = 1;
        } elseif ($data['type'] === 'PROF') {
            $data['status'] = 2;
        } elseif ($data['type'] === 'PREFV') {
            $data['status'] = 3;
        } elseif ($data['type'] === 'ORDCL') {
            $data['status'] = 4;
        } elseif ($data['type'] === 'RESID') {
            $data['status'] = 5;
        } elseif ($data['type'] === 'ORSOS') {
            $data['status'] = 6;
        } elseif ($data['type'] === 'PROSC') {
            $data['status'] = 7;
        } elseif ($data['type'] === 'ORRIF') {
            $data['status'] = 8;
        } elseif ($data['type'] === 'PRECO') {
            $data['status'] = 9;
        } elseif ($data['type'] === 'ORDTR') {
            $data['status'] = 10;
        } elseif ($data['type'] === 'ORDEG') {
            $data['status'] = 11;
        } elseif ($data['type'] === 'PREVE') {
            $data['status'] = 12;
        } else {
            $data['status'] = 13;
        }

        if (!empty($data['totrow']) && $data['totrow'] > 0) {
            $tot = $data['totrow'];
        }

        if (!empty($data['invoice']) && $data['invoice'] == 'S') {
            $data['id_transaction'] = 'I';
            $sDiff = sprintf('%s: %s', $data['reference'], ' Inserito allegato ');
            $this->diffList[] = $sDiff;
        } else {
            if (!empty($data['evaso']) && $data['evaso'] == 'S') {
                $sDiff = sprintf('%s: %s', $data['reference'], ' Documento successivo presente ');
                $this->diffList[] = $sDiff;
                $data['id_transaction'] = 'D';
            } else {
                $data['id_transaction'] = null;
            }
        }

        $orderid = $this->getOrder(rtrim($data['reference']));
        if ($this->reference !== rtrim($data['reference']) && $orderid == null) {
            $this->idorder = $this->getLastID();
            $this->tot = $tot;
            $this->reference = rtrim($data['reference']);
            if ($data['idcart'] != '' && is_numeric($data['idcart'])) {
                $sDiff = sprintf('%s: %s', $data['reference'], ' Disabilitato Carrello n° ' . $data['idcart']);
                $this->diffList[] = $sDiff;
                $this->changeStatusCartById($data['idcart']);
            }
            if ($data['refhistory'] != '' && is_numeric($data['refhistory'])) {
                //$this->changeStatusOrderByReference($data['refhistory']);
            }
            $this->insertOrder($data);
            if ($data['totdiscount'] > 0) {
                $this->insertRowDiscount($this->idorder, $data['totdiscount'], $data['date']);
            }
        } else if ($this->reference !== rtrim($data['reference']) && $orderid > 0) {
            $this->idorder = $orderid;
            $this->reference = $data['reference'];
            $this->deleteRowById($orderid);
            $this->tot = number_format(((float) str_replace(',', '.', ($data['tottax'])) + (float) str_replace(',', '.', ($data['totimp']))), 5, '.', '');
            $this->totimp = number_format((float) str_replace(',', '.', ($data['totimp'])), 5, '.', '');
            $this->tottax = number_format((float) str_replace(',', '.', ($data['tottax'])), 5, '.', '');
            $this->updateOrder($data);
            if ($data['totdiscount'] !== 0) {
                $this->insertRowDiscount($this->idorder, $data['totdiscount'], $data['date']);
            }
        } else {
            $this->tot = number_format(((float) str_replace(',', '.', ($data['tottax'])) + (float) str_replace(',', '.', ($data['totimp']))), 5, '.', '');
            $this->totimp = number_format((float) str_replace(',', '.', ($data['totimp'])), 5, '.', '');
            $this->tottax = number_format((float) str_replace(',', '.', ($data['tottax'])), 5, '.', '');
            $this->updateOrder($data);
        }
        $data['id_product'] = $this->getProductIDActive($refprod = rtrim($data['refprod']));
        $this->insertRow($data);
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);

        if ($this->debug) {
            foreach ($this->diffList as $sDiff) {
                $this->addLog($sDiff);
            }
        }

        $sendMail = $this->db->setting->getStr('lcdp', 'check.mail', '');
        $sendMail2 = $this->db->setting->getStr('lcdp', 'check.mail2', '');
        $sendMail3 = $this->db->setting->getStr('lcdp', 'check.mail3', '');
        if (!empty($this->diffList) && (!empty($sendMail))) {

            // Invia Email
            $subject = 'Inserimento/Aggiornamento Ordini : ' . count($this->diffList);
            $this->addLog($subject);
            $mail = new \EOS\System\Mail\Message($this->container);
            $mail->subject = $subject;
            $mail->to = $sendMail;
            $mail->cc = $sendMail2;
            $mail->bcc = $sendMail3;
            foreach ($this->diffList as $sDiff) {
                $mail->content .= $sDiff . "<br>\n";
            }
            $mail->send();
        }
    }

    protected function updateOrder($data)
    {
        $tbl = $this->db->tableFix('#__order');
        $customer = $this->getCustomer($data['refcustomer'], $data['refaddress']);
        $options = json_decode($customer['options'], true);
        $idcustomer = (isset($customer['id_customer'])) ? $customer['id_customer'] : 0;
        $idaddress = (isset($customer['id'])) ? $customer['id'] : 0;
        $idagent = (isset($options['id_agent'])) ? $options['id_agent'] : 0;
        $values = [
            'company' => ArrayHelper::getStr($customer, 'company'),
            'cif' => ArrayHelper::getStr($customer, 'cif'),
            'vat' => ArrayHelper::getStr($customer, 'vat'),
            'phone' => (isset($data['phone'])) ? $customer['phone'] : '',
            'address' => $customer['address1'],
            'city' => $customer['id_city'],
            'amount' => number_format((float) str_replace(',', '.', ($data['totimp'])) + (float) str_replace(',', '.', ($data['tottax'])), 5, '.', ''),
            'id_transaction' => $data['id_transaction'],
            'privacy' => 1,
            'id_customer' => $idcustomer,
            'id_address_delivery' => (int) $idaddress,
            'id_address_invoice' => (int) $idaddress,
            'id_agent' => (int) $idagent,
            'total_paid' => number_format((float) str_replace(',', '.', ($data['totimp'])) + (float) str_replace(',', '.', ($data['tottax'])), 5, '.', ''),
            'total_paid_tax_incl' => number_format((float) str_replace(',', '.', ($data['totimp'])) + (float) str_replace(',', '.', ($data['tottax'])), 5, '.', ''),
            'total_paid_tax_excl' => number_format((float) str_replace(',', '.', $data['totimp']), 5, '.', ''),
            'ins_id' => 0,
            'ins_date' => preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $data['date']),
            'upd_id' => 0,
            'upd_date' => preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $data['date']),
            'reference' => $this->reference
        ];
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', $this->idorder);
        $query->execute();
        $sDiff = sprintf('%s: %s', $data['reference'], ' Aggiornamento ordine ');
        $this->diffList[] = $sDiff;
    }

    protected function insertOrder($data)
    {
        $customer = $this->getCustomer($data['refcustomer'], $data['refaddress']);
        $tbl = $this->db->tableFix('#__order');
        $options = json_decode($customer['options'], true);
        $idcustomer = (isset($customer['id_customer'])) ? $customer['id_customer'] : 0;
        $idaddress = (isset($customer['id'])) ? $customer['id'] : 0;
        $idagent = (isset($options['id_agent'])) ? $options['id_agent'] : 0;
        $values = [
            'id' => $this->idorder,
            'company' => ArrayHelper::getStr($customer, 'company'),
            'cif' => ArrayHelper::getStr($customer, 'cif'),
            'vat' => ArrayHelper::getStr($customer, 'vat'),
            'phone' => (isset($data['phone'])) ? $customer['phone'] : '',
            'address' => $customer['address1'],
            'city' => $customer['id_city'],
            'amount' => number_format((float) str_replace(',', '.', ($data['totimp'])) + (float) str_replace(',', '.', ($data['tottax'])), 5, '.', ''),
            'status' => $data['status'],
            'id_transaction' => $data['id_transaction'],
            'privacy' => 1,
            'id_customer' => $idcustomer,
            'id_address_delivery' => (int) $idaddress,
            'id_address_invoice' => (int) $idaddress,
            'id_agent' => (int) $idagent,
            'total_paid' => number_format((float) str_replace(',', '.', ($data['totimp'])) + (float) str_replace(',', '.', ($data['tottax'])), 5, '.', ''),
            'total_paid_tax_incl' => number_format((float) str_replace(',', '.', ($data['totimp'])) + (float) str_replace(',', '.', ($data['tottax'])), 5, '.', ''),
            'total_paid_tax_excl' => number_format((float) str_replace(',', '.', $data['totimp']), 5, '.', ''),
            'ins_id' => 0,
            'ins_date' => preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $data['date']),
            'upd_id' => 0,
            'upd_date' => preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $data['date']),
            'reference' => $this->reference
        ];
        $query = $this->db->newFluent()->insertInto($tbl, $values);
        $query->execute();
        $sDiff = sprintf('%s: %s', $data['reference'], ' Inserito ordine ');
        $this->diffList[] = $sDiff;
    }

    protected function insertRowDiscount($idorder, $discount, $date)
    {
        $tbl = $this->db->tableFix('#__order_row');
        $values = [
            'id_order' => $idorder,
            'id_object' => 0,
            'type_object' => 'product',
            'product_name' => 'Sconto',
            'price' => number_format((float) str_replace(',', '.', $discount), 5, '.', ''),
            'quantity' => 1,
            'options' => ''
        ];
        $values['ins_id'] = 0;
        $values['ins_date'] = preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $date);
        $values['upd_id'] = 0;
        $values['upd_date'] = preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $date);
        $query = $this->db->newFluent()->insertInto($tbl, $values);
        $query->execute();
    }


    protected function insertRow($data)
    {
        $tbl = $this->db->tableFix('#__order_row');
        /*
        if(!empty($data['price'])) {
            $values = [
                'id_order' => $this->idorder,
                'id_object' => $data['id_product'],
                'type_object' => 'product',
                'product_name' => strip_tags($data['nameprod']),
                'price' => number_format((float) str_replace(',','.',$data['price']), 5, '.', ''),
                'quantity' => number_format((int) $data['quantity'], 0),
                'options' => ''
            ];
        } else {
            $values = [
                'id_order' => $this->idorder,
                'id_object' => $data['id_product'],
                'type_object' => 'product',
                'product_name' => $data['nameprod'],
                'price' => 0,
                'quantity' => 0,
                'options' => ''
            ];
        }*/
        $values = [
            'id_order' => $this->idorder,
            'id_object' => $data['id_product'],
            'type_object' => 'product',
            'product_name' => strip_tags($data['nameprod']),
            'price' => number_format((float) str_replace(',', '.', $data['price']), 5, '.', ''),
            'quantity' => number_format((int) $data['quantity'], 0),
            'options' => ''
        ];
        $values['ins_id'] = 0;
        $values['ins_date'] = preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $data['date']);
        $values['upd_id'] = 0;
        $values['upd_date'] = preg_replace('#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1 00:00:00', $data['date']);
        $query = $this->db->newFluent()->insertInto($tbl, $values);
        $query->execute();
    }
}
