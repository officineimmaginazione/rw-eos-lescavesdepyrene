<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;

class ImportPrice extends ImportTextData
{

    private $diffList = [];

    protected function getProductIDActive(string $reference): int
    {
        $q = $this->db->prepare('select id
              from #__product
              where reference = :reference ');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : $r['id'];
    }

    protected function getCartRule(int $id, int $group): int
    {
        $q = $this->db->prepare('select id
              from #__catalog_rules
              where id_product = :id and id_group = :group ');
        $q->bindValue(':id', $id);
        $q->bindValue(':group', $group);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : $r['id'];
    }


    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->captureException = true;
        $this->params['name'] = 'AdHoc - Price';

        $this->params['mapping'] = [
            'pricelist' => ['pos' => 2],
            'reference' => ['pos' => 3],
            'price' => ['pos' => 5]
        ];
    }

    protected function readRow(array $data)
    {
        // Salto sempre la prima riga
        if ($this->readIdx === 0 || ($data['pricelist'] != 'VEND'  && $data['pricelist'] != 'B2C' && $data['pricelist'] != 'CMP')) {
            return;
        }

        $reference = rtrim($data['reference']);
        $idProduct = $this->getProductIDActive($reference);
        if ($idProduct !== 0) {
            if ($data['pricelist'] == 'VEND') {
                $values = [
                    'price' => $data['price']
                ];
                //print_r($values);
                $tblContent = 'eos_product';
                $q = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $idProduct);
                $q->execute();
            } else if ($data['pricelist'] == 'CMP') {
                //ALTER TABLE `eos_catalog_rules` CHANGE `from` `dt_from` DATETIME NOT NULL, CHANGE `to` `dt_to` DATETIME NOT NULL;
                $tbl = 'eos_catalog_rules';
                $idCatalogRule = 0;
                $idCatalogRule = $this->getCartRule($idProduct, 2);
                if ($idCatalogRule == 0) {
                    $values = [
                        'name' => $data['pricelist'] . ' - ' . $data['reference'],
                        'id_product' => $idProduct,
                        'id_currency' => 1,
                        'id_group' => 2,
                        'price' => $data['price'],
                        'reduction_type' => 'price',
                        'dt_from' => date("Y-m-d H:i:s"),
                        'dt_to' => '2099-01-31 00:00:00',
                        'price' => $data['price']
                    ];
                } else {
                    $values = [
                        'price' => $data['price']
                    ];
                }
                if ($idCatalogRule != 0) {
                    $q = $this->db->newFluent()->update($tbl)->set($values)->where('id', $idCatalogRule);
                    $q->execute();
                } else {
                    $q = $this->db->newFluent()->insertInto($tbl, $values);
                    $q->execute();
                }
            } else if ($data['pricelist'] == 'B2C') {
                //ALTER TABLE `eos_catalog_rules` CHANGE `from` `dt_from` DATETIME NOT NULL, CHANGE `to` `dt_to` DATETIME NOT NULL;
                $tbl = 'eos_catalog_rules';
                $idCatalogRule = 0;
                $idCatalogRule = $this->getCartRule($idProduct, 42);
                if ($idCatalogRule == 0) {
                    $values = [
                        'name' => $data['pricelist'] . ' - ' . $data['reference'],
                        'id_product' => $idProduct,
                        'id_currency' => 1,
                        'id_group' => 42,
                        'price' => $data['price'],
                        'reduction_type' => 'price',
                        'dt_from' => date("Y-m-d H:i:s"),
                        'dt_to' => '2099-01-31 00:00:00',
                        'price' => $data['price']
                    ];
                } else {
                    $values = [
                        'price' => $data['price']
                    ];
                }
                if ($idCatalogRule != 0) {
                    $q = $this->db->newFluent()->update($tbl)->set($values)->where('id', $idCatalogRule);
                    $q->execute();
                } else {
                    $q = $this->db->newFluent()->insertInto($tbl, $values);
                    $q->execute();
                }
            }
        }
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);

        if ($this->debug) {
        }
    }
}
