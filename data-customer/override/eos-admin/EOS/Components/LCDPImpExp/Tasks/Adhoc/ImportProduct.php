<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;

class ImportProduct extends ImportTextData
{

    protected $model;
    protected $langList = [];
    protected $idLang = 0;
    protected $forceUpdateImage = false;

    protected function getProduct(string $reference): array
    {
        $this->addLog($this->RE('Reference: ' . $reference));
        $q = $this->db->prepare('select id, reference 
              from #__product 
              where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $list = $q->fetchAll();
        if (count($list) > 1) {
            $idList = implode(',', array_column($list, 'id'));
            throw new \Exception($this->RE('Attenzione ci sono 2 "codici" uguali: ' . $reference . ' Lista ID: ' . $idList));
        }
        return empty($list) ? [] : $list[0];
    }

    protected function getAttributes(): array
    {
        $q = $this->db->prepare('select id
              from #__product_attribute_group
              order by id');
        $q->execute();
        $list = $q->fetchAll();
        $res = [];
        foreach ($list as $r) {
            $res[] = (int) $r['id'];
        }
        return $res;
    }

    protected function getManufacturerID(string $reference): int
    {
        $q = $this->db->prepare('select id
              from #__manufacturer
              where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            throw new \Exception('Non trovo il brand "' . $reference . '"');
        }
        return $r['id'];
    }

    protected function getCategoryID(string $name): int
    {
        $q = $this->db->prepare('select id_category 
              from #__product_category_lang 
              where name = :name and id_lang = :id_lang ');
        $q->bindValue(':name', $name);
        $q->bindValue(':id_lang', $this->idLang);
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            throw new \Exception('Non trovo la categoria "' . $name);
        }
        return $r['id_category'];
    }

    protected function getAttributeID(string $name): int
    {
        $q = $this->db->prepare('select id_attribute_group
              from #__product_attribute_group_lang
              where name = :name and id_lang = :id_lang');
        $q->bindValue(':name', $name);
        $q->bindValue(':id_lang', $this->idLang);
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            throw new \Exception('Non trovo l\'attributo "' . $name . '"');
        }
        return $r['id_attribute_group'];
    }

    protected function getTaxID(string $reference): int
    {
        $q = $this->db->prepare('select id
              from #__tax
              where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            throw new \Exception('Non trovo l\'aliquota "' . $reference . '"');
        }
        return $r['id'];
    }

    protected function setDataAttribute(array &$data, string $groupname, string $name)
    {
        $idAttributeGroup = $this->getAttributeID($groupname);
        if ($name !== '') {
            $q = $this->db->prepare('select pa.id, pa.id_attribute_group, pal.name
                from #__product_attribute pa
                inner join #__product_attribute_lang pal on pa.id = pal.id_attribute
                where pal.name = :name and pal.id_lang = :id_lang and pa.id_attribute_group = :id_attribute_group ');
            $q->bindValue(':name', $name);
            $q->bindValue(':id_lang', $this->idLang);
            $q->bindValue(':id_attribute_group', $idAttributeGroup);
            $q->execute();
            $r = $q->fetch();
            if (empty($r)) {
                throw new \Exception('Non trovo l\'attributo "' . $name . '" gruppo "' . $groupname . '"');
            }
            $data['attribute-' . $idAttributeGroup] = $r['id'];
        } else {
            $data['attribute-' . $idAttributeGroup] = null;
        }
    }

    protected function load()
    {
        parent::load();
        $this->debug = true;
        $this->captureException = false;
        $this->params['name'] = 'AdHoc - Import product';

        $this->params['mapping'] = [
            'reference' => ['pos' => 0], // ARCODART - Codice Articolo AdHoc, CHIAVE
            'name' => ['pos' => 1], // ARDESART	Descrizione articolo 
            'descr' => ['pos' => 2], // ARDESSUP	Descrizione supplementare articolo
            // '' => ['pos' => 3],  // ARUNMIS1 - Unità misura Articolo
            // '' => ['pos' => 4], //AROPERAT - Operatore di confronto tra 1 e 2 unità di misura            
            'quantity-per-pack' => ['pos' => 5], //ARMOLTIP - Coefficiente di rapporto tra la 1 e la 2 unità di misura
            // '' => ['pos' => 6], //ARUNMIS2 - Seconda unità di misura Articolo
            'groupmerc' => ['pos' => 7], //ARGRUMER - Gruppo merceologico Articolo
            'codfamily' => ['pos' => 8], //ARCODFAM - Codice Famiglia Articolo
            // '' => ['pos' => 9], //ARGRUPPO - Codice Categoria Provvigioni
            // '' => ['pos' => 10], //ARCATSCM - Codice Sconto/Maggiorazione Articolo 
            // '' => ['pos' => 11], //ARCODALT - Articolo Alternatico
            'taxname' => ['pos' => 12], //ARCODIVA - Codice iva associato all'articolo
            'vintage' => ['pos' => 13], //ARCATOMO - Categoria omogenea articolo
            // '' => ['pos' => 14], //ARTPCONF - Tipo Confezione
            // '' => ['pos' => 15], //ARPZCONF - Pezzi confezione
            'manufacturer_reference' => ['pos' => 16], //ARCODMAR - Codice Marchio
            'weight' => ['pos' => 17], //ARPESLOR - Peso lordo
            // '' => ['pos' => 18], //ARPESNET - Peso Netto
            // '' => ['pos' => 19], //UTDC - Data creazione
            // '' => ['pos' => 20], //UTDV - Data modifica
            // '' => ['pos' => 21], //ARDTOBSO - Data obsolescenza
            'price' => ['pos' => 22], //PREZZO - Prezzo articolo
            // '' => ['pos' => 23], //AR__NOTE - Note articolo
            // '' => ['pos' => 24], //AMPUBWEB - Flag pubblica su web
            // '' => ['pos' => 25], //AMDTINIZ - Data inizio pubblicazione su listinonovità/offerte
            // '' => ['pos' => 26], //AMDTFINE - Data fine pubblicazione su listino novità/offerte
            // '' => ['pos' => 27], //AMIDSITO - Codice store magento di pubblicazione
            // '' => ['pos' => 28], //PRCODFOR - Codice fornitore articolo
            // '' => ['pos' => 29], //FLGPROCESS- Check se da processare in base a timestamp ultima esportazione
            'min-quantity' => ['pos' => 30], //ARMINVEN - Quantità minima vendibile/ordinabile
            // '' => ['pos' => 31], //PRCODPRO - Codice produttore articolo
        ];
        // Creo il model per velocizzare ma su PHP 7.2 è molto poca la differenza
        $this->model = new \EOS\Components\Product\Models\ProductModel($this->container);
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);
    }

    protected function readRow(array $data)
    {
        // Salto sempre la prima riga
        if ($this->readIdx === 0) {
            return;
        }

        // Salto i prodotti senza produttore
        if ($data['manufacturer_reference'] === '') {
            return;
        }

        //$updateImage = false;
        $r = $this->getProduct($data['reference']);
        if (empty($r)) {
            $this->addLog('Insert - REFERENCE: ' . $data['reference']);
            //$oldData = $this->model->getData(0);
            // Copio eventuali defaults
            //$this->assignExtraData($data, $oldData);
            $data['id'] = 0;
            $data['status'] = 'on';
            $data['id-category'] = $this->getCategoryID('Tutti');
            $data['id-tax'] = $this->getTaxID($data['taxname']);
            $data['price'] = 0;
            $otherFields = ['width', 'height', 'depth', 'online-only', 'extra-display-info', 'featured'];
            foreach ($otherFields as $f) {
                if (!isset($data[$f])) {
                    $data[$f] = null;
                }
            }
            if ($data['price'] <= 0) {
                $data['status'] = 0;
            }
            $data['online-only'] = 1;
            $coderemove = ["000", "001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012", "P00", "P01", "P02", "P03", "P04", "P05", "P06", "P07", "P08", "P09", "P10", "P11", "P12", "P99", "CCC", "CCP", "CCD", "DDD", "DNU", "DNP", "DNC", "DNG", "NWW", "NWT", "NWP", "TTT", "TTP", "EXP", "EXX", "UUU", "UUP", "RRR", "DNG"];
            $name = trim(str_replace($coderemove, '', $data['name']));
            $data['name-' . $this->idLang] = $name;
            $data['descr-' . $this->idLang] = StringHelper::encodeHtml($data['descr']);
            $data['descr-short-' . $this->idLang] = '';
            $data['id-manufacturer'] = $this->getManufacturerID($data['manufacturer_reference']);
            //$updateImage = true;
        } else {
            $this->addLog('Update ID: ' . $r['id'] . ' - REFERENCE: ' . $data['reference']);
            $oldData = $this->model->getData($r['id']);
            // Ricopio i vecchi valori se non esistono
            $this->assignExtraData($data, $oldData);
            // Fix status
            $data['status'] = $oldData['status'] ? 'on' : '';
            $data['price'] = $oldData['price'];
            if (empty($data['id-category'])) {
                $data['id-category'] = $this->getCategoryID('Tutti');
            }
            //$updateImage = $this->forceUpdateImage; // Disattivato aggiornamtno immagini
            //$data['descr-' . $this->idLang] = StringHelper::encodeHtml($data['descr']);
            //$data['descr-short-' . $this->idLang] = StringHelper::encodeHtml($data['descr-short']);
        }

        if ($data['status'] == 1) {
            $data['status'] = 'on';
        }

        if (strpos($data['name'], 'CCC') !== false || strpos($data['name'], 'CCD') !== false || strpos($data['name'], 'CCP') !== false) {
            $data['type-condition'] = 1;
        } elseif (strpos($data['name'], 'TTT') !== false || strpos($data['name'], 'DDD') !== false) {
            $data['type-condition'] = 2;
            $data['out-of-stock'] = 2;
        } elseif (strpos($data['name'], 'DNU') !== false) {
            $data['status'] = 'off';
            $data['type-condition'] = 2;
            $data['out-of-stock'] = 2;
        } else {
            $data['type-condition'] = 0;
        }

        // Rimuovo i media così mantiene i precedenti
        unset($data['media-image-product']);
        unset($data['media-attachment-product']);
        // Fix categorie trovate su alcuni articoli
        $data['quantity-per-pack'] = (int) $data['quantity-per-pack'];
        $data['min-quantity'] = (int) $data['min-quantity'];
        $data['price'] = (float) str_replace(',', '.', $data['price']); // Fix virgola
        $data['weight'] = (float) str_replace(',', '.', $data['weight']); // Fix virgola 
        $this->fixListFields($data, 'attribute-', $this->getAttributes(), '');
        if ((int) $data['vintage'] > 1800) {
            $this->setDataAttribute($data, 'Annata', $data['vintage']);
        } else {
            $data['vintage'] = null;
        }

        if ($this->debug) {
            // $this->addLog('save data:' . json_encode($data));
        }
        $error = '';

        if (!$this->model->saveData($data, $error)) {
            throw new \Exception($this->RE('Errore sui dati: ' . $error));
        }
        // if ($updateImage)
        // {
        //    $this->updateImage($data);
        // }
    }
}
