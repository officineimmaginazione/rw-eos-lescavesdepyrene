<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;

class ImportQtyCheck extends ImportTextData
{

    private $diffList = [];

    protected function getProductIDActive(string $reference): int
    {
        $q = $this->db->prepare('select id
              from #__product
              where reference = :reference and status = 1');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : $r['id'];
    }

    protected function getDBStock(int $idProduct, int $idProductChild): array
    {
        $q = $this->db->prepare('select * from #__product_stock ' .
            ' where id_product = :id_product and id_product_child = :id_product_child');
        $q->bindValue(':id_product', $idProduct, \EOS\System\Database\Database::PARAM_INT);
        $q->bindValue(':id_product_child', $idProductChild, \EOS\System\Database\Database::PARAM_INT);
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            $r = [];
        }
        return $r;
    }
    
    protected function getProductDesc(string $idproduct): string
    {
        $q = $this->db->prepare('select name
              from #__product_lang
              where id_product = :idproduct');
        $q->bindValue(':idproduct', $idproduct);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? '' : $r['name'];
    }

    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->captureException = true;
        $this->params['name'] = 'AdHoc - Import qty check';

        $this->params['mapping'] = [
            'reference' => ['pos' => 0],
            'quantity' => ['pos' => 1]
        ];
    }

    protected function readRow(array $data)
    {
        // Salto sempre la prima riga
        if ($this->readIdx === 0)
        {
            /* $q = $this->db->prepare('DELETE FROM #__ext_average_sales WHERE 1 ');
            $q->execute();
            return; */
        }

        $reference = rtrim($data['reference']);
        $idProduct = $this->getProductIDActive($reference);
        if ($idProduct !== 0)
        {
            $idProductChild = 0;
            $prodname = $this->getProductDesc($idProduct);
            $stock = $this->getDBStock($idProduct, $idProductChild);
            $qty = (float) str_replace(',', '.', $data['quantity']);
            $qtyAvailable = (float) $stock['quantity_available'];
            if ($qty > $qtyAvailable)
            {
                $sDiff = sprintf('%s: %s > %s', $reference.' - '.$prodname, $qty, $qtyAvailable);
                $this->diffList[] = $sDiff;
            }
            /* $values = [
                'id_product' => $idProduct,
                'average_sales' => $qty
            ];
            $q = $this->db->newFluent()->insertInto($tblContent, $values); */
            $tblContent = $this->db->tableFix('#__ext_average_sales');
            $values = [
                'average_sales' => $qty
            ];
            $q = $this->db->newFluent()->update($tblContent)->set($values)->where('id_product', $idProduct);
            $q->execute();
        }
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);

        if ($this->debug)
        {
            foreach ($this->diffList as $sDiff)
            {
                $this->addLog($sDiff);
            }
        }
        $sendMail = $this->db->setting->getStr('lcdp', 'check.mail', '');
        $sendMail2 = $this->db->setting->getStr('lcdp', 'check.mail2', '');
        $sendMail3 = $this->db->setting->getStr('lcdp', 'check.mail3', '');
        if (!empty($this->diffList) && (!empty($sendMail)))
        {

            // Invia Email
            $subject = 'Differenza stock : ' . count($this->diffList);
            $this->addLog($subject);
            $mail = new \EOS\System\Mail\Message($this->container);
            $mail->subject = $subject;
            $mail->to = $sendMail;
            $mail->cc = $sendMail2;
            $mail->bcc = $sendMail3;
            foreach ($this->diffList as $sDiff)
            {
                $mail->content .= $sDiff . "<br>\n";
            }
            $mail->send();
        }
    }

}
