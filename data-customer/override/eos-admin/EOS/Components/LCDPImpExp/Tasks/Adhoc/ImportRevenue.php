<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\ArrayHelper;

class ImportRevenue extends ImportTextData
{

    protected $model;
    protected $otherFields = [];
    protected $importStatus = true;

    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Import Revenue';
        if (!isset($this->params['filename'])) {
            $this->params['filename'] = '{uploads}import/test_account.txt';
        }

        $this->params['mapping'] = [
            'reference' => ['pos' => 0],
            'company' => ['pos' => 1],
            'revenue' => ['pos' => 2]
        ];
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);
    }

    protected function readRow(array $data)
    {
        if ($this->readIdx === 0) {
            return;
        }
        $this->checkCustomer($data);
    }

    protected function checkCustomer(array $data): bool
    {
        $q = $this->db->prepare('select au.id, au.options, a.company
            from #__account_user as au
            left outer join #__address as a on a.id_customer = au.id
            where au.reference = :reference and au.ins_date > \'2020-01-01 00:00:00\' and (json_unquote(JSON_EXTRACT(a.options, "$.coddes"))) = 9999');
        $q->bindValue(':reference', $data['reference']);
        $q->execute();
        $r = $q->fetch();
        if (isset($r['au.id'])) {
            $arr = array("srls", "Srls", "s.r.l.s", "S.r.l.s");
            $srlscheck = false;
            foreach ($arr as $a) {
                if (stripos($r['a.company'], $a) !== false) {
                    $srlscheck = true;
                }
            }
            if ($srlscheck == false) {
                $options = json_decode($r['au.options'], true);
                //print_r($options); 
                if ($data['revenue'] > 3000) {
                    $options['account_type'] = "1";
                } else {
                    $options['account_type'] = "0";
                }
                $values['options'] = json_encode($options);
                //print_r($values['options']); 
                //exit();
                $tblAccount = $this->db->tableFix('#__account_user');
                $query = $this->db->newFluent()->update($tblAccount)->set($values)->where('id', $r['au.id']);
                $query->execute();
            }
        }
        return true;
    }
}
