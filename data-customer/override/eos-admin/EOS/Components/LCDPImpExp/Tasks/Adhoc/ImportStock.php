<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;

class ImportStock extends ImportTextData
{

    protected function getProductID(string $reference): int
    {
        $q = $this->db->prepare('select id
              from #__product
              where reference = :reference');
        $q->bindValue(':reference', $reference);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : $r['id'];
    }

    protected function getDBStock(int $idProduct, int $idProductChild): array
    {
        $q = $this->db->prepare('select * from #__product_stock ' .
            ' where id_product = :id_product and id_product_child = :id_product_child');
        $q->bindValue(':id_product', $idProduct, \EOS\System\Database\Database::PARAM_INT);
        $q->bindValue(':id_product_child', $idProductChild, \EOS\System\Database\Database::PARAM_INT);
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            $r = [];
        }
        return $r;
    }

    protected function deleteStockWithoutProduct()
    {
        $sql = 'delete from #__product_stock where id_product not in (select id from #__product)';
        $q = $this->db->prepare($sql);
        $q->execute();
    }
    
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->captureException = true;
        $this->params['name'] = 'AdHoc - Import stock';

        $this->params['mapping'] = [
            'reference' => ['pos' => 0], // SLCODICE - Codice Articolo
            //'' => ['pos' => 1], // QTA	Quantità Magazzino
            //'' => ['pos' => 2], //DISPCONT - Disponibilità contabile  | SLQTAPER+SLQTOPER-SLQTIPER-SLQTRPER
            'quantity_physical' => ['pos' => 3], //SLQTAPER - Esistenza
            'quantity_reserved' => ['pos' => 4], //SLQTRPER - Quantità Riservata
            'quantity_ordered1' => ['pos' => 5], //SLQTOPER - Quantità Ordinata
            'quantity_engaged' => ['pos' => 6], //LQTIPER - Quantità Impiegata
            //'' => ['pos' => 7], //PRSCOMIN - Scorta minima Articolo
            //'' => ['pos' => 8], //PRDISMIN - Disponibilità minima articolo
            //'' => ['pos' => 9], //ARFLDISP - Check diponibilità ( N => No, S => Si, C => Si con conferma)
            //'' => ['pos' => 10], //PWCREART - Flag Creazione Articolo S/N
            //'' => ['pos' => 11], //FLGPROCESS - Flag da processare S/N secondo timestamp ultima modifica
            'date_ordered1' => ['pos' => 12], //SLDTRIOR - Data prossimo arrivo merce articolo
            //'' => ['pos' => 13], //NEXTORDI - Ordinato a +5 giorni rispetto esportazione
            //'' => ['pos' => 14], //NEXTIMPEGN - Impegnato a +5 giorni rispetto esportazione
            //'' => ['pos' => 15], //NEXTRISER - Riservato a +5 giorni rispetto esportazione
            //'' => ['pos' => 16], //UTDV - Data ultima variazione saldo
        ];
    }

    protected function readRow(array $data)
    {
        // Salto sempre la prima riga
        if ($this->readIdx === 0)
        {
            // Cancello sempre gli stock orfani
            $this->deleteStockWithoutProduct();
            return;
        }

        $idProduct = $this->getProductID($data['reference']);
        if ($idProduct !== 0)
        {
            $idProductChild = 0;
            $stock = $this->getDBStock($idProduct, $idProductChild);
            if (empty($stock))
            {
                $sql = 'insert into #__product_stock 
                      ( id_product,
                        id_product_child,
                        quantity_available, 
                        quantity_physical, 
                        quantity_engaged,
                        quantity_reserved,
                        quantity_ordered1,
                        quantity_ordered2,
                        date_ordered1,
                        date_ordered2) values
                      ( :id_product,
                        :id_product_child,
                        :quantity_available, 
                        :quantity_physical, 
                        :quantity_engaged,
                        :quantity_reserved,
                        :quantity_ordered1,
                        :quantity_ordered2,
                        :date_ordered1,
                        :date_ordered2) 
                     ';
            } else
            {
                $sql = 'update #__product_stock set 
                    quantity_available = :quantity_available, 
                    quantity_physical = :quantity_physical,
                    quantity_engaged = :quantity_engaged,
                    quantity_reserved = :quantity_reserved,
                    quantity_ordered1 = :quantity_ordered1,
                    quantity_ordered2 = :quantity_ordered2,
                    date_ordered1 = :date_ordered1,
                    date_ordered2 = :date_ordered2
                    where id_product = :id_product and id_product_child = :id_product_child';
            }

            $stock['quantity_physical'] = $data['quantity_physical'];
            $stock['quantity_engaged'] = $data['quantity_engaged'];
            $stock['quantity_reserved'] = $data['quantity_reserved'];
            $stock['quantity_ordered1'] = $data['quantity_ordered1'];
            $stock['quantity_ordered2'] = 0;

            $stock['date_ordered1'] = null;
            $stock['date_ordered2'] = null;
            if (mb_strlen($data['date_ordered1']) >= 10)
            {
                $dt = \DateTimeImmutable::createFromFormat('d/m/Y', mb_substr($data['date_ordered1'], 0, 10));
                $stock['date_ordered1'] = $this->db->util->dateTimeToDBDateTime($dt);
            }


            $stock['quantity_available'] = round(
                $stock['quantity_physical'] - $stock['quantity_engaged'] - $stock['quantity_reserved'] +
                $stock['quantity_ordered1'] + $stock['quantity_ordered2'], 4);

            $q = $this->db->prepare($sql);
            $q->bindValue(':id_product', $idProduct, \EOS\System\Database\Database::PARAM_INT);
            $q->bindValue(':id_product_child', $idProductChild, \EOS\System\Database\Database::PARAM_INT);
            $q->bindValue(':quantity_available', $stock['quantity_available']);
            $q->bindValue(':quantity_physical', $stock['quantity_physical']);
            $q->bindValue(':quantity_engaged', $stock['quantity_engaged']);
            $q->bindValue(':quantity_reserved', $stock['quantity_reserved']);
            $q->bindValue(':quantity_ordered1', $stock['quantity_ordered1']);
            $q->bindValue(':quantity_ordered2', $stock['quantity_ordered2']);
            $q->bindValue(':date_ordered1', $stock['date_ordered1']);
            $q->bindValue(':date_ordered2', $stock['date_ordered2']);
            $q->execute();
        }
    }

}
