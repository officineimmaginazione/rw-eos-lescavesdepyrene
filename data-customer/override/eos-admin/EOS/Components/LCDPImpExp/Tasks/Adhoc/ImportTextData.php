<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

class ImportTextData extends \EOS\System\Tasks\TextReader
{

    protected $langList = [];
    protected $idLang = 0;

    protected function RE(string $error)
    {
        return 'Riga: ' . ($this->readIdx + 1) . ' | ' . $error;
    }

    protected function assignExtraData(array &$data, array $extra)
    {
        foreach ($extra as $k => $v)
        {
            if (!isset($data[$k]))
            {
                $data[$k] = $v;
            }
        }
    }

    protected function fixListFields(array &$data, string $prefix, array $list, string $suffix)
    {
        foreach ($list as $v)
        {
            $k = $prefix . $v . $suffix;
            $data[$k] = isset($data[$k]) ? $data[$k] : null;
        }
    }

    protected function load()
    {
        parent::load();
        // Parametri per i loro file di testo
        $this->params['format'] = 'delimited';
        $this->params['settings'] = ["forceautodetectlineendings" => true,
            "delimiter" => ';',
            "utf8" => false,
            "csv" => true,
            "escape" => "\\",
            "enclosure" => '"'];
        $this->captureException = true;
        $this->captureExceptionToLog = true;

        // Carico la listra delle lingue utile dopo
        $siteLang = new \EOS\System\Language\Manager($this->container);
        $siteLang->loadFromDB(true);
        $this->langList = $siteLang->getLangListID();
        $this->idLang = $this->lang->getCurrentID();
    }

    protected function read(\Iterator $reader)
    {
        parent::read($reader);
        if (!empty($this->exceptionList))
        {
            $error = basename($this->params['filename']) . ' | Importazione con ' . count($this->exceptionList) .
                ' errori: ' . implode("\n", $this->exceptionList) . ' su ' . ($this->readIdx + 1);
            $this->addLog($error);
            $eu = new \EOS\Components\LCDPImpExp\Classes\ErrorUtil($this->container);
            $eu->sendMessage('Importazione con errori', $error);
        }
    }

}
