<?php

namespace EOS\Components\LCDPImpExp\Tasks\Adhoc;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class SendCashbackResumeTotal extends \EOS\System\Tasks\Task
{

    protected function load()
    {
        parent::load();
        $this->debug = true;
        $this->params['name'] = 'Send Cashback Resume';
    }

    public function run()
    {
        $this->sendCashbackResume();
    }

    public function sendCashbackResume()
    {
        $diffList = [];
        $chr = $this->getCashbackHistory();
        foreach ($chr as $ch) {
            $sender = new \EOS\System\Mail\Mail($this->container);
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = 'shop@lescaves.it';
            $sender->subject = 'Cashback EnoteCaves';
            $sender->message = $this->createMail($ch);
            if (isset($ch['cashbacktotal']) && $ch['cashbacktotal'] > 0) {
                $total = $ch['cashbacktotal'];
            } else {
                $total = $ch['feetotal'];
            }
            if ($sender->sendEmail($error)) {
                $sDiff = sprintf('%s - %s : Cashback: %s €', $ch['email'], $ch['name'], number_format($total, 2, '.', ','), ' - Riepilogo cashback');
                $diffList[] = $sDiff;
            } else {
                $sDiff = sprintf('%s - %s : Cashback: %s €', $ch['email'], $ch['name'], number_format($total, 2, '.', ','), ' - Riepilogo cashback non inviato per problemi');
                $diffList[] = $sDiff;
            }
        }
        $sendMail = $this->db->setting->getStr('lcdp', 'shop.email', '');
        //$sendMail2 = $this->db->setting->getStr('lcdp', 'check.mail2', '');
        //$sendMail3 = $this->db->setting->getStr('lcdp', 'check.mail3', '');
        if (!empty($diffList) && (!empty($sendMail))) {
            // Invia Email
            $subject = 'Cashback Inviati : ' . count($diffList);
            $this->addLog($subject);
            $mail = new \EOS\System\Mail\Message($this->container);
            $mail->subject = $subject;
            $mail->to = 'shop@lescaves.it';
            //$mail->cc = $sendMail2;
            //$mail->bcc = $sendMail3;
            foreach ($diffList as $sDiff) {
                $this->addLog($sDiff);
                $mail->content .= $sDiff . "<br>\n";
            }
            $mail->send();
        }
    }

    public function getCashbackHistory(): array
    {
        $select = 'SELECT sum(ech.cashback) as cashback, au.email, au.name, json_unquote(json_extract(au.options, "$.cashback"))  as cashbacktotal, json_unquote(json_extract(au.options, "$.fee")) as feetotal ';
        $from = ' from #__ext_cashback_history as ech ';
        $inner = ' left outer join eos_account_user as au on au.id = ech.id_user ';
        $where = ' WHERE ech.ins_date >= now() - INTERVAL 30 DAY and ech.cashback > 0 ';
        $order = ' group by ech.id_user ';
        $q = $this->db->prepare($select . $from . $inner . $where . $order);
        $q->execute();
        $list = $q->fetchAll();
        return $list;
    }

    public function createMail($data)
    {
        if (isset($data['cashbacktotal']) && $data['cashbacktotal'] > 0) {
            $daytotal = $data['cashback'];
            $total = $data['cashbacktotal'];
            $phrase = "<p>in questo mese hai ricevuto da EnoTecaves un cashback pari a " . number_format($total, 2, '.', ',') . ' €.<br>Per controllare il tuo riepilogo clicca <a href="https://www.lescaves.it/it/account/user/index/" title=""><img src="https://www.lescaves.it/data-customer/uploads/img/qui-xs.jpg" title="qui" /></a><br><br>Buona serata! Rock the cash!</p>';
        } else {
            $daytotal = $data['cashback'];
            $total = $data['feetotal'];
            $phrase = "<p>in questo mese hai ricevuto delle provvigioni da EnoTecaves pari a " . number_format($total, 2, '.', ',') . ' €.<br>Ricordati che quelle dei mesi precedenti sono state scaricate.<br>Per controllare il tuo riepilogo clicca <a href="https://www.lescaves.it/it/account/user/index/" title=""><img src="https://www.lescaves.it/data-customer/uploads/img/qui-xs.jpg" title="qui" /></a><br><br>Buona serata!</p>';
        }
        $content = '<!DOCTYPE html>';
        $content .= '<html lang="it" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">';
        $content .= '<head>';
        $content .= '<meta charset="utf-8">';
        $content .= '<meta name="viewport" content="width=device-width">';
        $content .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $content .= '<meta name="x-apple-disable-message-reformatting">';
        $content .= '<title>Cashback EnoteCaves</title>';
        $content .= "<style>body,html{margin:0 auto!important;padding:0!important;height:100%!important;width:100%!important;background:#f1f1f1}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*='margin: 16px 0']{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}img{-ms-interpolation-mode:bicubic}a{text-decoration:none}.aBn,.unstyle-auto-detected-links *,[x-apple-data-detectors]{border-bottom:0!important;cursor:default!important;color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}.a6S{display:none!important;opacity:.01!important}.im{color:inherit!important}img.g-img+div{display:none!important}@media only screen and (min-device-width:320px) and (max-device-width:374px){u~div .email-container{min-width:320px!important}}@media only screen and (min-device-width:375px) and (max-device-width:413px){u~div .email-container{min-width:375px!important}}@media only screen and (min-device-width:414px){u~div .email-container{min-width:414px!important}}.bg_white{background:#fff}.bg_light{background:#fafafa}.bg_black{background:#000}.bg_dark{background:rgba(0,0,0,.8)}.email-section{padding:2.5em}.btn{padding:10px 15px;display:inline-block}.btn.btn-primary{border-radius:5px;background:#8b2329;color:#fff}.btn.btn-white{border-radius:5px;background:#fff;color:#000}.btn.btn-white-outline{border-radius:5px;background:0 0;border:1px solid #fff;color:#fff}.btn.btn-black-outline{border-radius:0;background:0 0;border:2px solid #000;color:#000;font-weight:700}.btn-custom{color:rgba(0,0,0,.3);text-decoration:underline}h1,h2,h3,h4,h5,h6{font-family:'Work Sans',sans-serif;color:#000;margin-top:0;font-weight:400}p{color:rgba(0,0,0,.7)}body{font-family:'Work Sans',sans-serif;font-weight:400;font-size:15px;line-height:1.8;color:rgba(0,0,0,.7)}a{color:#8b2329}.logo h1{margin:0}.logo h1 a{color:#8b2329;font-size:24px;font-weight:700;font-family:'Work Sans',sans-serif}.hero{position:relative;z-index:0}.hero .text{color:rgba(0,0,0,.3)}.hero .text h2{color:#000;font-size:24px;margin-bottom:15px;font-weight:300;line-height:1.2}.hero .text h3{font-size:21px;font-weight:200}.hero .text h2 span{font-weight:600;color:#000}.product-entry{display:block;position:relative;float:left;padding-top:20px}.product-entry .text{width:calc(100% - 0px);padding-left:20px}.product-entry .text h3{margin-bottom:0;padding-bottom:0}.product-entry .text p{margin-top:0}.product-entry .text,.product-entry img{float:left}ul.social{padding:0}ul.social li{display:inline-block;margin-right:10px}.footer{border-top:1px solid rgba(0,0,0,.05);color:rgba(0,0,0,.5)}.footer .heading{color:#000;font-size:20px}.footer ul{margin:0;padding:0}.footer ul li{list-style:none;margin-bottom:10px}.footer ul li a{color:#000}</style>";
        $content .= '</head>';
        $content .= '<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f1f1f1;">';
        $content .= '<center style="width: 100%; background-color: #f1f1f1;">';
        $content .= '<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">';
        $content .= '</div>';
        $content .= '<div style="max-width: 600px; margin: 0 auto;" class="email-container">';
        /* Testata */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="top" class="bg_white" style="padding: 1em 2.5em 0 2.5em;">';
        $content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td class="logo" style="text-align: left;">';
        $content .= '<h1><a href="https://www.lescaves.it"><img src="https://www.lescaves.it/data-customer/themes/default/img/logo-enotecaves.png" alt="Enotecaves" /></a></h1>';
        $content .= '<p>Ciao ' . $data['name'] . '</p>';
        $content .= $phrase;
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td valign="middle" class="hero bg_white" style="padding: 2em 0 2em 0;">';
        $content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="padding: 0 2.5em; text-align: left;">';
        $content .= '<div class="text">';
        //$content .= '<h3>' . $this->lang->transEncode('account.access.register.invitation.email.msg') . '</h3>';
        $content .= '<p>';
        $content .= '</p>';
        //$privacy = (isset($data['consent1'])) ? $this->lang->transEncode('order.order.yes') : $this->lang->transEncode('order.order.no');
        //$content .= '<p>' . $this->lang->transEncode('order.order.email.privacy.consent1') . ' ' . $privacy . '</p>';
        $content .= '</div>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        /* Footer */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="middle" class="bg_light footer email-section">';
        $content .= '<table>';
        $content .= '<tr>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 5px; padding-right: 5px;">';
        $content .= '<h3 class="heading">Les Caves de Pyrene</h3>';
        $content .= '<ul>';
        $content .= '<li><span class="text">Strada Cagnassi, 8 - 12050 Rodello</span></a></li>';
        $content .= '<li><span class="text">Corso Susa 22/A - 10098 Rivoli</span></a></li>';
        $content .= '<li><span class="text">Tel. +39 0173 617072</span></a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 10px;">';
        $content .= '<h3 class="heading">Link Utili</h3>';
        $content .= '<ul>';
        $content .= '<li><a href="https://www.lescaves.it">Chi siamo</a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td class="bg_white" style="text-align: center;">';
        $content .= '<p>&nbsp;</p>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</center>';
        $content .= '</body>';
        $content .= '</html>';
        return $content;
    }
}
