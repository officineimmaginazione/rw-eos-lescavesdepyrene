<?php

namespace EOS\Components\LCDPImpExp\Tasks\Roambi;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class ExportAccount extends \EOS\System\Tasks\Task
{
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Les Caves - Export User';
        $this->exportFile();
    }
    
    public function exportFile()
    {
        $res = [];
        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $path = PathHelper::slashToDirSep($this->replacePathTag('{uploads}export/roambi/'));
        /* $fname = $path.'export-account-group.csv';
        $writer->openToFile($fname);
        //$columntitle = array("ID", "ISBN", "Titolo", "Prezzo", "Data Uscita", "Descrizione", "Editore", "Autore", "Collana", "Traduttore");
        //$writer->addRow($columntitle);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__account_group ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $accountgroups = $q->fetchAll();
        
        foreach ($accountgroups as $accountgroup)
        {
            $writer->addRow($accountgroup);
        }
        $writer->close(); 
        
        $fname = $path.'export-account-user.csv';
        $writer->openToFile($fname);
        //$columntitle = array("ID", "ISBN", "Titolo", "Prezzo", "Data Uscita", "Descrizione", "Editore", "Autore", "Collana", "Traduttore");
        //$writer->addRow($columntitle);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__account_user ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $accountusers = $q->fetchAll();
        
        foreach ($accountusers as $accountuser)
        {
            $writer->addRow($accountuser);
        }
        $writer->close();
        
        $fname = $path.'export-account-user-group.csv';
        $writer->openToFile($fname);
        //$columntitle = array("ID", "ISBN", "Titolo", "Prezzo", "Data Uscita", "Descrizione", "Editore", "Autore", "Collana", "Traduttore");
        //$writer->addRow($columntitle);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__account_user_group ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $accountusergroups = $q->fetchAll();
        
        foreach ($accountusergroups as $accountusergroup)
        {
            $writer->addRow($accountusergroup);
        }
        $writer->close(); */
        
        $fname = $path.'export-address.csv';
        $writer->openToFile($fname);
        //$columntitle = array("ID", "ISBN", "Titolo", "Prezzo", "Data Uscita", "Descrizione", "Editore", "Autore", "Collana", "Traduttore");
        //$writer->addRow($columntitle);
        
        $sqlSelect = 'SELECT reference as CodiceCliente, company as NomeCliente, alias as Destinazione, ls.iso_code as Provincia, '
            . ' lco.iso_code as Nazione FROM #__address as a LEFT OUTER JOIN #__localization_city as lc on lc.id = a.id_city '
            . ' LEFT OUTER join #__localization_state as ls on ls.id = lc.id_state left OUTER join #__localization_country as lco on lco.id = ls.id_country WHERE 1 ';
        $sqlFrom = '  ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $addresses = $q->fetchAll();
        
        foreach ($addresses as $address)
        {
            $writer->addRow($address);
        }
        $writer->close();
        
    }
    
}
