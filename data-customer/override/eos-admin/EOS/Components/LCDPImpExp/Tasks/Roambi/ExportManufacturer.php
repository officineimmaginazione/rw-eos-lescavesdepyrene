<?php

namespace EOS\Components\LCDPImpExp\Tasks\Roambi;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class ExportManufacturer extends \EOS\System\Tasks\Task
{
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Les Caves - Export Manufacturer';
        $this->exportFile();
    }
    
    public function exportFile()
    {
        $res = [];
        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $path = PathHelper::slashToDirSep($this->replacePathTag('{uploads}export/roambi/'));
        $fname = $path.'export-manufacturer.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__manufacturer ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $manufacturers = $q->fetchAll();
        
        foreach ($manufacturers as $manufacturer)
        {
            $writer->addRow($manufacturer);
        }
        $writer->close(); 
        
        $fname = $path.'export-manufacturer-lang.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__manufacturer_lang ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $manufacturerslang = $q->fetchAll();
        
        foreach ($manufacturerslang as $manufacturer)
        {
            $writer->addRow($manufacturer);
        }
        $writer->close(); 
        
        
    }
    
}
