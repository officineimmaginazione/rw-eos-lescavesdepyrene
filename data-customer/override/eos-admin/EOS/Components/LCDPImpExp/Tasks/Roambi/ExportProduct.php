<?php

namespace EOS\Components\LCDPImpExp\Tasks\Roambi;

use EOS\System\Routing\PathHelper;
use EOS\System\Util\DateTimeHelper;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class ExportProduct extends \EOS\System\Tasks\Task
{
    protected function load()
    {
        parent::load();
        $this->debug = false;
        $this->params['name'] = 'Les Caves - Export Product';
        $this->exportFile();
    }
    
    public function exportFile()
    {
        $res = [];
        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $path = PathHelper::slashToDirSep($this->replacePathTag('{uploads}export/roambi/'));
        /*$fname = $path.'export-product.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__product ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $products = $q->fetchAll();
        
        foreach ($products as $product)
        {
            $writer->addRow($product);
        }
        $writer->close(); 
        
        $fname = $path.'export-product-lang.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__product_lang ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $productslang = $q->fetchAll();
        
        foreach ($productslang as $productlang)
        {
            $writer->addRow($productlang);
        }
        $writer->close();
        
        $fname = $path.'export-product-category.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__product_category ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $productcategories = $q->fetchAll();
        
        foreach ($productcategories as $category)
        {
            $writer->addRow($category);
        }
        $writer->close(); 
        
        $fname = $path.'export-product-lang.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__product_category_lang ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $productcategorieslang = $q->fetchAll();
        
        foreach ($productcategorieslang as $category)
        {
            $writer->addRow($category);
        }
        $writer->close();
        
        
        $fname = $path.'export-product-attribute.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__product_attribute ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $productattributes = $q->fetchAll();
        
        foreach ($productattributes as $attribute)
        {
            $writer->addRow($attribute);
        }
        $writer->close(); 
        
        $fname = $path.'export-product-attribute-lang.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__product_attribute_lang ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $productattributeslang = $q->fetchAll();
        
        foreach ($productattributeslang as $attribute)
        {
            $writer->addRow($attribute);
        }
        $writer->close();
        
        $fname = $path.'export-product-attribute-group.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'select * ';
        $sqlFrom = ' from #__product_attribute ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $productattributegroups = $q->fetchAll();
        
        foreach ($productattributegroups as $group)
        {
            $writer->addRow($group);
        }
        $writer->close();  */
        
        $fname = $path.'export-product.csv';
        $writer->openToFile($fname);
        
        $sqlSelect = 'SELECT pl.id_product, pl.name as Articolo, p.`reference` as CodiceArticolo, m.reference  as CodiceProduttore, ml.name as Produttore, pcl.name as Categoria FROM `#__product` as p
            left OUTER JOIN #__product_lang as pl on pl.id_product = p.id and pl.id_lang = 1
            left OUTER JOIN #__manufacturer as m on m.id = p.`id_manufacturer`
            left OUTER JOIN #__manufacturer_lang as ml on ml.id_manufacturer = m.id and ml.id_lang = 1
            left OUTER JOIN #__product_product_category as ppc on p.id = ppc.id_product
            left OUTER JOIN #__product_category as pc on ppc.id_category = pc.id
            left OUTER JOIN #__product_category_lang as pcl on pcl.id_category = pc.id and ml.id_lang = 1
            WHERE 1';
        $sqlFrom = ' ';
        $sqlWhere = ' ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlWhere);
        $q->execute();
        $products = $q->fetchAll();
        
        $res = [];
        $i = 0;
        foreach ($products as $product)
        {
            $sqlSelect2 = 'SELECT pal.name from
                #__product_product_attribute_combination as ppac
                left OUTER JOIN #__product_attribute as pa on pa.id = ppac.id_attribute
                left OUTER JOIN #__product_attribute_lang as pal on pal.id_attribute = pa.id
                WHERE ppac.id_product = '.$product['id_product']. ' and pa.id_attribute_group = 1 ';
            $sqlFrom2 = ' ';
            $sqlWhere2 = ' ';
            $q2 = $this->db->prepare($sqlSelect2 . $sqlFrom2 . $sqlWhere2);
            $q2->execute();
            $vintage = $q2->fetch();
            $res[$i]['Articolo'] = $product['Articolo'];
            $res[$i]['CodiceArticolo'] = $product['CodiceArticolo'];
            $res[$i]['CodiceProduttore'] = $product['CodiceProduttore'];
            $res[$i]['Produttore'] = $product['Produttore'];
            $res[$i]['Categoria'] = $product['Categoria'];
            $res[$i]['Annata'] = $vintage['name'];
            $i++;
        }
        
        foreach ($res as $product)
        {
            $writer->addRow($product);
        }
        $writer->close();
        
        
    }
    
}
