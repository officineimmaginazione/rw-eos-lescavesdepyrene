<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Controllers;

class CartController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('order.cart.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        return $v->render('cart/default', true);
    }

    public function indexb2c($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $v->pageTitle = $v->trans('order.cart.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        return $v->render('cart/defaultb2c', true);
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\Order\Models\CartModel($this->container);
        $os = new \EOS\Components\Order\Models\StateModel($this->container);
        $v = $this->newView($request, $response);
        $v->addBreadcrumb($this->path->urlFor('Order', ['cart', 'index']), $v->trans('order.cart.index.title'));
        $v->loadDataTables();
        $v->loadiCheck();
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('order.cart.insert.title') : $v->trans('order.cart.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        $idgroup = $v->data['idgroup'];
        $v->datarow = $m->getDataRow($args['id'], $idgroup);
        $v->state = $os->getList();
        return $v->render('cart/edit', true);
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Order\Models\CartModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery($data['id_lang'], 1), $m->db);
        $dts->addColumn('id', 'c.id');
        $dts->addColumn('date', 'c.up_date', true);
        $dts->addColumn('company', 'a.company', true);
        $dts->addColumn('alias', 'a.alias', true);
        $dts->addColumn('total', 'c.total', true);
        $dts->addColumn('status', 'c.status', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxListB2C($request, $response, $args)
    {
        $m = new \EOS\Components\Order\Models\CartModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery($data['id_lang'], 42), $m->db);
        $dts->addColumn('id', 'c.id');
        $dts->addColumn('date', 'c.up_date', true);
        $dts->addColumn('company', 'a.company', true);
        $dts->addColumn('alias', 'a.alias', true);
        $dts->addColumn('total', 'c.total', true);
        $dts->addColumn('status', 'c.status', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxRowList($request, $response, $args)
    {
        $m = new \EOS\Components\Order\Models\CartModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getRowListQuery($args['id']), $m->db);
        $dts->addColumn('id', 'oro.id');
        $dts->addColumn('product_name', 'oro.product_name', true);
        $dts->addColumn('price', 'oro.price', true);
        //     $dts->addColumn('subtitle', 'cl.subtitle', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request)) {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody)) {
                $m = new \EOS\Components\Order\Models\CartModel($this->container);
                if ($m->saveData($parsedBody)) {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Order', ['cart', 'index']);
                } else {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxCreditCashback($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        $error = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request)) {
            $data = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($data)) {
                $m = new \EOS\Components\Order\Models\CartModel($this->container);
                if ($m->realignCashback($data['id'], $data['total'], $data['typealign'], $error)) {
                    $result['result'] = true;
                    $result['message'] = 'Riallineamento Cashback avvenuto con successo';
                    //$result['redirect'] = $this->path->urlFor('Order', ['cart', 'index']);
                } else {
                    $result['message'] = $error;
                }
            } else {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDelete($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request)) {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody)) {
                $m = new \EOS\Components\Order\Models\CartModel($this->container);
                if ($m->deleteData($parsedBody)) {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Order', ['cart', 'index']);
                } else {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }
}
