<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

use EOS\System\Util\ArrayHelper;

class CartModel extends \EOS\Components\System\Classes\AuthModel
{

    public function getListQuery($id_lang, $idgroup = 1)
    {
        if ($idgroup != 42) {
            $f = $this->db->newFluent()->from($this->db->tableFix('#__cart c'))
                ->select(null)->select('c.id, a.company, a.alias, c.total, c.status, c.up_date')
                ->leftJoin($this->db->tableFix('#__address as a ON c.id_address_invoice = a.id'))
                ->leftJoin($this->db->tableFix('#__account_user_group aug ON a.id_customer = aug.id_user'))
                ->where('aug.id_group in (1, 2)')
                ->orderBy('c.id', 'DESC');
        } else {
            $f = $this->db->newFluent()->from($this->db->tableFix('#__cart c'))
                ->select(null)->select('c.id, concact(a.name, " ", a.surname) as company, a.alias, c.total, c.status, c.up_date')
                ->leftJoin($this->db->tableFix('#__address as a ON c.id_address_invoice = a.id'))
                ->leftJoin($this->db->tableFix('#__account_user_group aug ON a.id_customer = aug.id_user'))
                ->where('aug.id_group in (42) and c.status in (2, 3, 5) and id_address_delivery > 0')
                ->orderBy('c.id', 'DESC');
        }
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__cart c'))
            ->select(null)
            ->select('c.id')
            ->select('c.status')
            ->select('c.total')
            ->select('c.id_customer')
            ->select('c.id_address_delivery')
            ->select('c.id_address_invoice')
            ->select('c.id_agent')
            ->select('c.ins_date')
            ->select('c.options')
            ->where('c.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null) {
            $res = [];
        } else {
            $res['id'] = $rc['id'];
            $res['status'] = $rc['status'];
            $res['total'] = str_replace('.', ',', $rc['total']);
            $res['ins_date'] = $rc['ins_date'];
            $options = json_decode($rc['options'], true);
            $query2 = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
                ->where('a.id = ?', (int) $rc['id_address_delivery']);
            $rc2 = $query2->fetch();
            if (!empty($rc2)) {
                $optionsaddress = json_decode($rc2['options'], true);
                $res['name'] = $rc2['name'];
                $res['surname'] = $rc2['surname'];
                $res['phone'] = $rc2['phone'];
                $res['address'] = $rc2['address1'];
                $res['company'] = $rc2['company'];
                $res['vat'] = $rc2['vat'];
                $res['cif'] = $rc2['cif'];
                $res['address-notes'] = $optionsaddress['note'];
                $options = json_decode($rc['options'], true);
                /* {"head_additional_discount":"","head_note":"","shipping":{"count":1,"total":0,"prices":[0,0]},"allissample":false,"head_discount1":"0","head_discount2":"0","head_discount3":"0","head_payment":"","head_payment_discount":""} */
                $res['head-note'] = $options['head_note'];
                $res['head-discount1'] = $options['head_discount1'];
                $res['head-discount2'] = $options['head_discount2'];
                $res['head-payment-discount'] = $options['head_payment_discount'];
                $res['shipping-total'] = $options['shipping']['total'];
                $res['cashback'] = (isset($options['cashback'])) ? $options['cashback'] : 0;
                $res['shipping1perc'] = (isset($options['discount']['shipping1perc'])) ? $options['discount']['shipping1perc'] : 0;
                $res['shipping2perc'] = (isset($options['discount']['shipping2perc'])) ? $options['discount']['shipping2perc'] : 0;
                $res['orderresid'] = $options['order_resid'];
                $payment = $this->getPayment($options['head_payment']);
                $res['payment'] = $payment['name'];
                $ml = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                $au = new \EOS\Components\Account\Models\UserModel($this->container);
                $current_city = $ml->getCity(ArrayHelper::getInt($rc2, 'id_city'));
                $res['city'] = (isset($current_city['name']) && $current_city['name'] != '')  ? $current_city['name'] : '';
                $res['state'] = (isset($current_city['statecode']) && $current_city['statecode'] != '') ? $current_city['statecode'] : '';
                $res['country'] = (isset($current_city['country']) && $current_city['name'] != '') ? $current_city['country'] : '';
            } else {
                $res['name'] = '';
                $res['surname'] = '';
                $res['phone'] = '';
                $res['email'] = '';
                $res['address'] = '';
                $res['city'] = '';
                $res['state'] = '';
                $res['country'] = '';
                $res['company'] = '';
                $res['vat'] = '';
                $res['cif'] = '';
                $res['address-notes'] = '';
                $res['head-note'] = '';
                $res['head-discount1'] = '';
                $res['head-discount2'] = '';
                $res['head-payment-discount'] = '';
                $res['payment'] = '';
            }
            $query3 = $this->db->newFluent()->from($this->db->tableFix('#__account_user au'))
                ->select('au.name')
                ->where('au.id = ?', (int) $rc['id_agent']);
            $rc3 = $query3->fetch();
            if (!empty($rc3)) {
                $res['agent-name'] = $rc3['name'];
            }
            $query4 = $this->db->newFluent()->from($this->db->tableFix('#__account_user au'))
                ->select('au.name')
                ->select('au.email_alternative')
                ->select('au.email')
                ->where('au.id = ?', (int) $rc['id_customer']);
            $rc4 = $query4->fetch();
            if (!empty($rc4)) {
                if ($rc4['email_alternative'] != '') {
                    $res['email'] = $rc4['email_alternative'];
                } else {
                    $res['email'] = $rc4['email'];
                }
            }
            $query5 = $this->db->newFluent()->from($this->db->tableFix('#__account_user_group aug'))
                ->select('aug.id_group')
                ->where('aug.id_group in (1, 2, 42) and aug.id_user = ?', (int) $rc['id_customer']);
            $rc5 = $query5->fetch();
            $res['idgroup'] = $rc5['id_group'];
        }
        return $res;
    }

    protected function getPayment($id)
    {
        $sql = 'select epa.PADESCRI as name from #__ext_pag_amen epa where epa.id = :id  ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? null : $r;
    }

    public function getDataRow($id, $idgroup = 2)
    {
        if ($idgroup != 42) {
            $sql = 'select cr.id, cr.id_product, pl.name, pl.description, ml.name as manufacturer_name, cr.quantity, cr.options, p.price, p.type_condition, t.rate ' .
                ' from #__cart_row cr ' .
                ' left outer join #__product p on cr.id_product = p.id ' .
                ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
                ' left outer join #__tax t on t.id = p.id_tax ' .
                ' left outer join #__manufacturer_lang ml on p.id_manufacturer = ml.id_manufacturer ' .
                ' where cr.id_cart = :id_cart ';
        } else {
            $sql = 'select cr.id, cr.id_product, pl.name, pl.description, ml.name as manufacturer_name, cr.quantity, cr.options, cru.price, p.type_condition, t.rate ' .
                ' from #__cart_row cr ' .
                ' left outer join #__product p on cr.id_product = p.id ' .
                ' left outer join #__catalog_rules cru on p.id = cru.id_product and cru.id_group = 42 ' .
                ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
                ' left outer join #__tax t on t.id = p.id_tax ' .
                ' left outer join #__manufacturer_lang ml on p.id_manufacturer = ml.id_manufacturer ' .
                ' where cr.id_cart = :id_cart ';
        }
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        //print_r($sql); exit();
        $r = $q->fetchAll();
        $i = 0;
        $res = [];
        foreach ($r as $item) {
            $sql = 'select pal.name' .
                ' from #__product_product_attribute_combination ppac ' .
                ' inner join #__product_attribute pa on pa.id = ppac.id_attribute ' .
                ' inner join #__product_attribute_lang pal on pal.id_attribute = pa.id' .
                ' where ppac.id_product = :id_product and pa.id_attribute_group  = 1';
            $q = $this->db->prepare($sql);
            $q->bindValue(':id_product', $item['id_product']);
            $q->execute();
            //$res = $q->fetchAll();
            $attribute = $q->fetch();
            $res[$i]['id'] = $item['id'];
            $res[$i]['id_product'] = $item['id_product'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['description'] = $item['description'];
            $res[$i]['manufacturer'] = $item['manufacturer_name'];
            $res[$i]['quantity'] = $item['quantity'];
            $res[$i]['price'] = $item['price'];
            $res[$i]['rate'] = $item['rate'];
            $res[$i]['type_condition'] = $item['type_condition'];
            $res[$i]['options'] = $item['options'];
            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($item['id_product']);
            foreach ($listimage as $single) {
                $img = $image->getObject($item['id_product'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
                if (!empty($img)) {
                    $res[$i]['url'] = $img->url;
                }
            }
            $i++;
        }
        //print_r($res); exit();
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $values = [
            'status' => $data['status'],
            'total' => (float) str_replace(',', '.', str_replace('.', '', $data['total'])),
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        $tblCart = $this->db->tableFix('#__cart');
        if ($data['id'] == 0) {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblCart, $values);
        } else {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblCart)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0) {
            $data['id'] = $this->db->lastInsertId();
        }
        $this->db->commit();

        return true;
    }

    public function realignCashback($id, $total, $typerealign = null, &$error)
    {
        if (is_numeric($typerealign)) {
            /* accredito */
            if ($typerealign == 0) {
                $cashbacks = $this->getCashbackByCart($id);
                foreach ($cashbacks as $row) {
                    if ($row['status'] == 0) {
                        $sql = 'select au.options, aug.id_group from #__account_user as au ';
                        $sql .= ' inner join eos_account_user_group as aug on aug.id_user = au.id ';
                        $sql .= ' where au.id = :id and aug.id_group in (1, 2, 42) ';
                        $q = $this->db->prepare($sql);
                        $q->bindValue('id', $row['id_user']);
                        $q->execute();
                        $r = $q->fetch();
                        $options = json_decode($r['options'], true);
                        $this->updateCashbackHistory($row['id'], null, 0, 1);
                        if ($r['id_group'] != 2) {
                            $this->updateCashback($row['id_user'], $row['cashback'], false, $options);
                        } else {
                            $this->updateCashback($row['id_user'], $row['cashback'], true, $options);
                        }
                    } else {
                        $error = 'Cashback già accreditato';
                        return false;
                    }
                }
            }
            /* storno parz */
            if ($typerealign == 1) {
                $cashbacks = $this->getCashbackByCart($id);
                foreach ($cashbacks as $row) {
                    if ($row['status'] == 1) {
                        $sql = 'select au.options, aug.id_group from #__account_user as au ';
                        $sql .= ' inner join eos_account_user_group as aug on aug.id_user = au.id ';
                        $sql .= ' where au.id = :id and aug.id_group in (1, 2, 42) ';
                        $q = $this->db->prepare($sql);
                        $q->bindValue('id', $row['id_user']);
                        $q->execute();
                        $r = $q->fetch();
                        $options = json_decode($r['options'], true);
                        $cart = $this->getData($row['id_cart']);
                        $totalnoship = $total - $cart['shipping-total'];
                        $totaltaxexcl = $totalnoship / ((100 + (float) $this->db->setting->getStr('cart', 'tax', 22)) / 100);
                        if ($r['id_group'] != 2) {
                            $diff = ($totalnoship * $row['cashback']) / $row['cart_total'];
                            $this->updateCashbackHistory($row['id'], $diff, $totalnoship, 2);
                            $this->updateCashback($row['id_user'], $diff, false, $options);
                        } else {
                            $diff = ($totaltaxexcl * $row['cashback']) / $row['cart_total'];
                            $this->updateCashbackHistory($row['id'], $diff, $totaltaxexcl, 2);
                            $this->updateCashback($row['id_user'], $diff, true, $options);
                        }
                    } else if ($row['status'] == 0) {
                        $error = 'Cashbasck non accreditato. Impossibile effetuare lo storno';
                        return false;
                    } else {
                        $error = 'Storno già effettuato';
                        return false;
                    }
                }
            }
            /* storno totale */
            if ($typerealign == 2) {
                $cashbacks = $this->getCashbackByCart($id);
                foreach ($cashbacks as $row) {
                    if ($row['status'] == 1) {
                        $sql = 'select au.options, aug.id_group from #__account_user as au ';
                        $sql .= ' inner join eos_account_user_group as aug on aug.id_user = au.id ';
                        $sql .= ' where au.id = :id and aug.id_group in (1, 2, 42) ';
                        $q = $this->db->prepare($sql);
                        $q->bindValue('id', $row['id_user']);
                        $q->execute();
                        $r = $q->fetch();
                        $options = json_decode($r['options'], true);
                        $total = str_replace(',', '.', $total);
                        $totaltaxexcl = $total / ((100 + (float) $this->db->setting->getStr('cart', 'tax', 22)) / 100);
                        if ($r['id_group'] != 2) {
                            $diff = $row['cashback'];
                            $this->updateCashbackHistory($row['id'], $total, 0, 3);
                            $this->updateCashback($row['id_user'], $diff, false, $options);
                        } else {
                            $diff = $row['cashback'];
                            $this->updateCashbackHistory($row['id'], $totaltaxexcl, 0, 3);
                            $this->updateCashback($row['id_user'], $diff, true, $options);
                        }
                    } else if ($row['status'] == 0) {
                        $error = 'Cashbasck non accreditato. Impossibile effetuare lo storno';
                        return false;
                    } else {
                        $error = 'Storno già effettuato';
                        return false;
                    }
                }
            }
        } else {
            $error = 'Impossibile gestire cashback';
            return false;
        }
        return true;
    }

    public function getCashbackByCart($id)
    {
        $sql = 'select * from #__ext_cashback_history where id_cart = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', $id);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function updateCashbackHistory($id, $cashback = null, $totalremained = 0, $status = null)
    {
        $tbl = $this->db->tableFix('#__ext_cashback_history');
        if ($cashback != null) {
            $values['cashback'] = $cashback;
        }
        if ($totalremained != 0) {
            $values['cart_total_remained'] = $totalremained;
        }
        if ($status != null) {
            $values['status'] = $status;
        }
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', $id);
        $query->execute();
    }

    public function updateCashback($id_user, $cashback, $agent = false, $options)
    {
        $tbl = $this->db->tableFix('#__account_user');
        $data['id'] = $id_user;
        if ($agent == true) {
            if (isset($options['fee'])) {
                $options['fee'] = $cashback + $options['fee'];
            } else {
                $options['fee'] = $cashback;
            }
        } else {
            $options['cashback'] = $cashback + $options['cashback'];
        }
        $data['options'] = json_encode($options);
        $query = $this->db->newFluent()->update($tbl)->set($data)->where('id', (int) $id_user);
        $query->execute();
    }
}
