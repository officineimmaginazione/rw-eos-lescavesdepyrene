<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

use EOS\System\Util\ArrayHelper;

class CashbackModel extends \EOS\Components\System\Classes\AuthModel
{

    /* Status
    0 - Non accreditato
    1 - Accreditato
    2 - Stornato parzialmente
    3 - Stornato totalmente
    4 - Fee scaricate
    */
    public function getListQuery($id_lang)
    {
        /*
        create view `eos_view_cashback_admin_list`  AS  select `ech`.`id` AS `id`,`ech`.`id_cart` AS `id_cart`,`au`.`name` AS `beneficiary`,`au2`.`name` AS `customer`,`ech`.`status` AS `status`,`ech`.`cashback` AS `cashback`,`ech`.`ins_date` AS `ins_date` from (((`eos_ext_cashback_history` `ech` left join `eos_account_user` `au` on((`ech`.`id_user` = `au`.`id`))) left join `eos_cart` `c` on((`ech`.`id_cart` = `c`.`id`))) left join `eos_account_user` `au2` on((`c`.`id_customer` = `au2`.`id`))) order by `ech`.`id` desc ;
        */
        $f = $this->db->newFluent()->from($this->db->tableFix('#__view_cashback_admin_list'))
            ->select(null)->select('id, id_cart, beneficiary, customer, status, ins_date');
        return $f;
    }

    public function getCashbackByID($id)
    {
        $sql = 'select ech.*, au.name ' .
            ' from #__ext_cashback_history ech ' .
            ' left outer join #__cart c on ech.id_cart = c.id ' .
            ' left outer join #__account_user au on au.id = c.id_customer ';
        $sql .= ' where ech.id_user = ' . $id;
        $sql .= '  ' .
            ' order by id desc ';
        $q = $this->db->prepare($sql);
        //$q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }
}
