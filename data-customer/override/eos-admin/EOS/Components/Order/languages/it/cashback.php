<?php

return
    [
        'order.cashback.index.title' => 'Cashback',
        'order.cashback.edit.title' => 'Visualizza cashback',
        'order.cashback.insert.title' => 'Inserimento cashback',
        'order.cashback.title' => 'Cashback #',
        'order.cashback.field.beneficiary' => 'Beneficiario',
        'order.cashback.field.id' => 'Numero Cashback',
        'order.cashback.field.cart' => 'Numero Carello',
        'order.cashback.field.cashback' => 'Valore',
        'order.cashback.field.date' => 'Data',
        'order.cashback.field.status' => 'Stato',
        'order.cashback.field.customer' => 'Cliente',
        'order.cashback.field.level' => 'Livello',
    ];
