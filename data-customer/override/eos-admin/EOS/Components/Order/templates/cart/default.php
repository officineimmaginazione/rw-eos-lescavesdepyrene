<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline')->addContent(function ()
{
    (new \EOS\UI\Bootstrap\Button('btn-new'))
        ->attr('class', ' btn-info btn-sm pull-right')
        ->click('function (e) { addNew();}')
        ->content('<i class="fa fa-plus"></i>')
        ->printRender($this);
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('Order', ['cart', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('order.cart.field.id'));
$tbl->addColumn('date', $this->transE('order.cart.field.date'));
$tbl->addColumn('company', $this->transE('order.cart.field.company'));
$tbl->addColumn('alias', $this->transE('order.cart.field.alias'));
$tbl->addColumn('total', $this->transE('order.cart.field.amount'));
$tbl->addColumn('status', $this->transE('order.cart.field.status'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->onAjaxSend('function (data) {data.id_lang = id_lang}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    var id_lang = <?php echo $this->dataLangDefault['id'];?>;
    function addNew()
    {
        window.location.href = "<?php echo $this->path->urlFor('Order', ['cart', 'edit', '0']) ?>";
    }

    function renderStatus(data)
    {
      if (data == 0)
      {
        return '<span class="label label-success"><?php $this->transP('order.cart.field.status.0'); ?></span>';
      } else if(data == 1)
      {
        return '<span class="label label-danger"><?php $this->transP('order.cart.field.status.1'); ?></span>';
      } else if(data == 2)
      {
        return '<span class="label label-danger"><?php $this->transP('order.cart.field.status.2'); ?></span>';
      } else if(data == 3)
      {
        return '<span class="label label-danger"><?php $this->transP('order.cart.field.status.3'); ?></span>';
      } else if(data == 4)
      {
        return '<span class="label label-danger"><?php $this->transP('order.cart.field.status.4'); ?></span>';
      } else if(data == 5)
      {
        return '<span class="label label-danger"><?php $this->transP('order.cart.field.status.5'); ?></span>';
      } else
      {
        return '<span class="label label-danger"><?php $this->transP('order.cart.field.status.6'); ?></span>';
      }
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
          id_lang = n_id_lang;
          $('#datatable').DataTable().ajax.reload();
        }
    }
    
    function editRow(id)
    {
      window.location.href = "<?php echo $this->path->urlFor('Order', ['cart', 'edit']) ?>" + id + "/";     
    }
</script>
<?php
$this->endCaptureScript();
