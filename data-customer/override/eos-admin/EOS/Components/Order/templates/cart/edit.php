<?php
$form = new EOS\UI\Html\Form();
$form->id('edit');
$form->attr('method', 'POST');
$form->attr('class', 'form-horizontal');
$form->startContent();
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\Row)->attr('class', 'mx-0 form-inline main-content-header')->addContent(function () {
    $title = '<span>' . $this->pageTitle . '</span>' . '<span>' . $this->pageSubtitle . '</span>';
    (new \EOS\UI\Bootstrap\Heading1('content-header-title'))
        ->content($title)
        ->printRender($this);
    (new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'ml-auto mr-3')->addContent(function () {
    }, $this)->printRender();
    (new \EOS\UI\Bootstrap\Button('btn-save'))->content($this->transE('system.common.save'))->attr('class', 'btn-success btn-eos mr-1')->click('function (e) {e.preventDefault(); saveData();}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-tot'))->content('Storno CB Totale')->attr('class', 'btn-warning btn-eos ')->click('function (e) {e.preventDefault(); realingCashback(2);}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-par'))->content('Storno CB Parz.')->attr('class', 'btn-warning btn-eos ')->click('function (e) {e.preventDefault(); realingCashback(1);}')->printRender($this);
    (new \EOS\UI\Bootstrap\Button('btn-cred'))->content('Accredito CB')->attr('class', 'btn-primary btn-eos ')->click('function (e) {e.preventDefault(); realingCashback(0);}')->printRender($this);
}, $this)->printRender($this);
$box->endHeader();
$box->startContent();

$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-data', $this->trans('system.common.data'));
$tab->addItem('tab-extra', $this->trans('system.common.extra'));
$tab->addItem('tab-products', 'Riepilogo prodotti');

$tab->startTab();
(new \EOS\UI\Html\Input())->name('id')->id('data-id')->type('hidden')->bind($this->data, 'id')->printRender();
(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.company'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('companyname'))->attr('disabled', '')->attr('name', 'company_name')->bind($this->data, 'company')->printRender($this);
        echo '</div>';
?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.vat'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('vat'))->attr('disabled', '')->attr('name', 'vat')->bind($this->data, 'vat')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.cif'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('cif'))->attr('disabled', '')->attr('name', 'cif')->bind($this->data, 'cif')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.name'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('name'))->attr('disabled', '')->attr('name', 'name')->bind($this->data, 'name')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.surname'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('surname'))->attr('disabled', '')->attr('name', 'surname')->bind($this->data, 'surname')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.phone'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('phone'))->attr('disabled', '')->attr('name', 'phone')->bind($this->data, 'phone')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.email'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('email'))->attr('disabled', '')->attr('name', 'email')->bind($this->data, 'email')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.address'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('address'))->attr('disabled', '')->attr('name', 'address')->bind($this->data, 'address')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.city'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('city'))->attr('disabled', '')->attr('name', 'city')->bind($this->data, 'city')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.state'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('state'))->attr('disabled', '')->attr('name', 'state')->bind($this->data, 'state')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.country'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('country'))->attr('disabled', '')->attr('name', 'country')->bind($this->data, 'country')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();


(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        $status = [];
        $status[0] = ['id' => 0, 'name' => $this->transE('order.cart.field.status.0')];
        $status[1] = ['id' => 1, 'name' => $this->transE('order.cart.field.status.1')];
        $status[2] = ['id' => 2, 'name' => $this->transE('order.cart.field.status.2')];
        $status[3] = ['id' => 3, 'name' => $this->transE('order.cart.field.status.3')];
        $status[4] = ['id' => 4, 'name' => $this->transE('order.cart.field.status.4')];
        $status[5] = ['id' => 5, 'name' => $this->transE('order.cart.field.status.5')];
        $status[6] = ['id' => 6, 'name' => $this->transE('order.cart.field.status.6')];
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.status'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('status'))
            ->bind($this->data, 'status')
            ->bindList($status, 'id', 'name', false)
            ->printRender($this);
        echo "</div>";
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content($this->transE('order.cart.field.amount'))->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('total'))->bind($this->data, 'total')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();
$tab->endTab('tab-data');


$tab->startTab();
(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Agente')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('agent-name'))->attr('disabled', '')->attr('name', 'agent-name')->bind($this->data, 'agent-name')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Note carrello')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Textarea('head-note'))->attr('disabled', '')->attr('name', 'head-note')->bind($this->data, 'head-note')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Sconto 1')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('head-discount1'))->attr('disabled', '')->attr('name', 'head-discount1')->bind($this->data, 'head-discount1')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Sconto 2')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('head-discount2'))->attr('disabled', '')->attr('name', 'head-discount2')->bind($this->data, 'head-discount2')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Sconto pagamento')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('head-payment-discount'))->attr('disabled', '')->attr('name', 'head-payment-discount')->bind($this->data, 'head-payment-discount')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Cashback')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('cashback'))->attr('disabled', '')->attr('name', 'cashback')->bind($this->data, 'cashback')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Sconto Q.tà - 1° Spedizione')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('shipping1perc'))->attr('disabled', '')->attr('name', 'shipping1perc')->bind($this->data, 'shipping1perc')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Sconto Q.tà - 2° Spedizione')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Input('shipping2perc'))->attr('disabled', '')->attr('name', 'shipping2perc')->bind($this->data, 'shipping2perc')->printRender($this);
        echo '</div>';
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';
    ?>
    <?php
}, $this)->printRender();

(new \EOS\UI\Bootstrap\Row())->addContent(function () {
    echo '<div class="col-12 col-md-6">';
    (new \EOS\UI\Bootstrap\FormGroup())->attr("class", " form-group-custom")->addContent(function () {
        $orderresid = [];
        $orderresid[0] = ['id' => 0, 'name' => $this->transE('system.common.no')];
        $orderresid[1] = ['id' => 1, 'name' => 'Sì/Ordine diviso'];
        echo '<div class="form-group-title">';
        (new \EOS\UI\Html\Label)->content('Ordine Q.tà non disponibili')->printRender();
        echo '</div>';
        echo '<div>';
        (new \EOS\UI\Bootstrap\Select('orderresid'))
            ->bind($this->data, 'orderresid')
            ->bindList($orderresid, 'id', 'name', false)
            ->attr('disabled', '')
            ->printRender($this);
        echo "</div>";
    ?>
    <?php
    }, $this)->printRender();
    echo '</div>';

    ?>
<?php
}, $this)->printRender();

$tab->endTab('tab-extra');

$tab->startTab();
echo '<div class="row">';
echo '<div class="col-12">';
echo '<div class="box-body no-padding">';
echo '<table class="table table-striped">';
echo '<tr>';
echo '<th>' . $this->transE('order.cart.product.id') . '</th>';
echo '<th>' . $this->transE('order.cart.product.name') . '</th>';
echo '<th>' . $this->transE('order.cart.product.manufacturer') . '</th>';
echo '<th>' . $this->transE('order.cart.product.quantity') . '</th>';
echo '<th>' . $this->transE('order.cart.product.additionaldiscount') . '</th>';
echo '<th>' . $this->transE('order.cart.product.freeextradiscount1') . '</th>';
echo '<th>' . $this->transE('order.cart.product.freeextradiscount2') . '</th>';
echo '<th>' . $this->transE('order.cart.product.freerowdiscount1') . '</th>';
echo '<th>' . $this->transE('order.cart.product.freerowdiscount2') . '</th>';
echo '<th>' . $this->transE('order.cart.product.price') . '</th>';
echo '</tr>';
if (count($this->datarow)) {
    foreach ($this->datarow as $row) {
        $options = json_decode($row['options'], true);
        echo "<tr>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['name'] . "</td>";
        echo "<td>" . $row['manufacturer'] . "</td>";
        echo "<td>" . $row['quantity'] . "</td>";
        echo (isset($options['data']['additional-discount'])) ? "<td>" . $options['data']['additional-discount'] . "</td>" : "<td>0</td>";
        echo (isset($options['data']['free-extra-discount'][0])) ? "<td>" . $options['data']['free-extra-discount'][0] . "</td>" : "<td>0</td>";
        echo (isset($options['data']['free-extra-discount'][1])) ? "<td>" . $options['data']['free-extra-discount'][1] . "</td>" : "<td>0</td>";
        echo (isset($options['data']['free-row-discount'][0])) ? "<td>" . $options['data']['free-row-discount'][0] . "</td>" : "<td>0</td>";
        echo (isset($options['data']['free-row-discount'][1])) ? "<td>" . $options['data']['free-row-discount'][1] . "</td>" : "<td>0</td>";
        echo "<td>" . $row['price'] . "</td>";
        echo "<tr>";
    }
}
echo '</table>';
echo '</div>';
echo '</div>';
echo '</div>';

$tab->endTab('tab-products');
$tab->printRender($this);

$box->endContent();
$box->startFooter();

$box->endFooter();
$box->printRender($this);
$this->writeTokenHtml();
$form->endContent();
$form->printRender($this);
$this->startCaptureScript();
?>
<script>
    function saveData() {
        var data = $('#edit').serializeFormJSON();
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('Order', ['cart', 'ajaxsave']); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    location.href = json.redirect;
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function realingCashback(typealign) {
        var data = $('#edit').serializeFormJSON();
        data['typealign'] = typealign;
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('Order', ['cart', 'ajaxcreditcashback']); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    //location.href = json.redirect;
                    bootbox.alert(json.message);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }

    function cancelData() {
        window.location.href = "<?php echo $this->path->urlFor('Order', ['cart', 'index']); ?>";
    }

    function deleteData() {
        bootbox.confirm("<?php $this->transPE('order.cart.delete.confirm') ?>", function(result) {
            if (result) {
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '<?php echo $this->path->urlFor('Order', ['cart', 'ajaxdelete']); ?>',
                        data: JSON.stringify({
                            "<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>",
                            "id": $('#data-id').val()
                        }),
                        contentType: "application/json",
                    })
                    .done(function(json) {
                        if (json.result == true) {
                            location.href = json.redirect;
                        } else {
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function(jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        alert(err);
                    });
            }
        });
    }
</script>
<?php
$this->endCaptureScript();
