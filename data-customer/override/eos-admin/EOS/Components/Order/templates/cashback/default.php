<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline')->addContent(function () {
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();

$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('Order', ['cashback', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('order.cashback.field.id'));
$tbl->addColumn('cart', $this->transE('order.cashback.field.cart'));
$tbl->addColumn('beneficiary', $this->transE('order.cashback.field.beneficiary'));
$tbl->addColumn('customer', $this->transE('order.cashback.field.customer'));
$tbl->addColumn('cashback', $this->transE('order.cashback.field.cashback'));
$tbl->addColumn('date', $this->transE('order.cashback.field.date'));
$tbl->addColumn('status', $this->transE('order.cashback.field.status'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fas fa-shopping-cart"></i></button>');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->onAjaxSend('function (data) {data.id_lang = id_lang}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().cart);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
  var id_lang = <?php echo $this->dataLangDefault['id']; ?>;

  function setLang(item) {
    var n_id_lang = item.getAttribute('data-key1');
    if (n_id_lang != id_lang) {
      id_lang = n_id_lang;
      $('#datatable').DataTable().ajax.reload();
    }
  }

  function editRow(id) {
    window.location.href = "<?php echo $this->path->urlFor('Order', ['cart', 'edit']) ?>" + id + "/";
  }

  function renderStatus(data) {
    switch (parseInt(data)) {
      case 0:
        return '<span class="label label-warning">Non accreditato</span>';
        break;
      case 1:
        return '<span class="label label-warning">Accreditato</span>';
        break;
      case 2:
        return '<span class="label label-success">Parz. Stornato</span>';
        break;
      case 3:
        return '<span class="label label-warning">Stornato</span>';
        break;
      case 4:
        return '<span class="label label-warning">Fee scaricate</span>';
        break;
      case 5:
        return '<span class="label label-warning">Cashback utilizzato</span>';
        break;
      default:
        return '<span class="label label-warning">Operazione da verificare</span>';
    }
  }
</script>
<?php
$this->endCaptureScript();
