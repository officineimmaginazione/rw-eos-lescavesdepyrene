<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline')->addContent(function ()
{
    (new \EOS\UI\Bootstrap\Button('btn-new'))
        ->attr('class', ' btn-info btn-sm pull-right')
        ->click('function (e) { addNew();}')
        ->content('<i class="fa fa-plus"></i>')
        ->printRender($this);
}, $this)->printRender($this);

$box->endHeader();
$box->startContent();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('Order', ['order', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('order.order.field.id'));
$tbl->addColumn('reference', $this->transE('order.order.field.reference'));
$tbl->addColumn('name', $this->transE('order.order.field.name'));
$tbl->addColumn('surname', $this->transE('order.order.field.surname'));
$tbl->addColumn('company', $this->transE('order.order.field.company'));
$tbl->addColumn('status', $this->transE('order.order.field.status'));
$tbl->addColumn('amount', $this->transE('order.order.field.amount'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->onAjaxSend('function (data) {data.id_lang = id_lang}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    var id_lang = <?php echo $this->dataLangDefault['id'];?>;
    function addNew()
    {
        window.location.href = "<?php echo $this->path->urlFor('Order', ['order', 'edit', '0']) ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
          id_lang = n_id_lang;
          $('#datatable').DataTable().ajax.reload();
        }
    }
    
    function editRow(id)
    {
      window.location.href = "<?php echo $this->path->urlFor('Order', ['order', 'edit']) ?>" + id + "/";     
    }
</script>
<?php
$this->endCaptureScript();
