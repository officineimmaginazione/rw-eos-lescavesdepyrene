<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Controllers;

class ProductController extends \EOS\Components\System\Classes\AuthController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->loadDataTables();
        $m = new \EOS\Components\Product\Models\ProductModel($this->container);
        $v->pageTitle = $v->trans('product.product.index.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        return $v->render('product/default', true);
    }

    public function edit($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\ProductModel($this->container);
        $manufacturer = new \EOS\Components\Product\Models\ManufacturerModel($this->container);
        $categories = new \EOS\Components\Product\Models\CategoryModel($this->container);
        $attribute = new \EOS\Components\Product\Models\AttributeModel($this->container);
        $tax = new \EOS\Components\Localization\Models\TaxModel($this->container);
        $v = $this->newView($request, $response); 
        $v->loadCKEditor();
        $v->loadiCheck();
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        $v->addBreadcrumb($this->path->urlFor('Product', ['product', 'index']), $v->trans('product.product.index.title'));
        $v->pageTitle = ($args['id'] == 0) ? $v->trans('product.product.insert.title') : $v->trans('product.product.edit.title');
        $v->addBreadcrumb('', $v->pageTitle);
        $v->dataLangDefault = $this->lang->getDefaultFromDB(true);
        $v->dataLangList = $this->lang->getArrayFromDB(true);
        $v->data = $m->getData($args['id']);
        $v->manufacturer = $manufacturer->getList($v->dataLangDefault['id']);
        $v->categories = $categories->getList($v->dataLangDefault['id']);
        $v->attributes = $attribute->getListComplete($v->dataLangDefault['id']);
        $v->tax = $tax->getList($v->dataLangDefault['id']);
        \EOS\UI\Loader\Library::load($v, 'MediaImage');
        $v->mediaImage = $m->newMediaServer()->getMedia();
        return $v->render('product/edit', true);
    }

    public function ajaxList($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\ProductModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery($data['id_lang'], 1), $m->db);
        $dts->addColumn('id', 'p.id');
        $dts->addColumn('product', 'pl.name', true);
        $dts->addColumn('reference', 'p.reference', true);
        $dts->addColumn('status', 'status', true);
        $dts->addColumn('manufacturer', 'ml.name', true);
        $dts->addColumn('quantity', 'ps.quantity_available', true);
        
//     $dts->addColumn('subtitle', 'cl.subtitle', true);
        return $response->withJson($dts->toArray());
    }
    
    public function ajaxList2($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\ProductModel($this->container);
        $data = $request->getParsedBody();
        $dts = new \EOS\UI\Bootstrap\DataTableServer($this->session, $request, $m->getListQuery($data['id_lang'], 0), $m->db);
        $dts->addColumn('id', 'p.id');
        $dts->addColumn('product', 'pl.name', true);
        $dts->addColumn('reference', 'p.reference', true);
        $dts->addColumn('status', 'status', true);
        $dts->addColumn('manufacturer', 'ml.name', true);
        $dts->addColumn('quantity', 'ps.quantity_available', true);
        
//     $dts->addColumn('subtitle', 'cl.subtitle', true);
        return $response->withJson($dts->toArray());
    }

    public function ajaxSave($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\ProductModel($this->container);
                if ($m->saveData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['product', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxDelete($request, $response, $args)
    {
        $result['result'] = false;
        $result['message'] = '';
        if (\EOS\System\Util\MessageHelper::isContentTypeJson($request))
        {
            $parsedBody = $request->getParsedBody();
            if ($this->session->isValidTokenArrayKey($parsedBody))
            {
                $m = new \EOS\Components\Product\Models\ProductModel($this->container);
                if ($m->deleteData($parsedBody))
                {
                    $result['result'] = true;
                    $result['redirect'] = $this->path->urlFor('Product', ['product', 'index']);
                } else
                {
                    $result['message'] = $this->lang->trans('system.content.error');
                }
            } else
            {
                $result['message'] = 'Invalid token';
            }
        }
        return $response->withJson($result);
    }

    public function ajaxImageUpload($request, $response, $args)
    {
        $m = new \EOS\Components\Product\Models\ProductModel($this->container);
        $mediaServer = $m->newMediaServer();
        return $mediaServer->upload($this, $request, $response);
    }

}
