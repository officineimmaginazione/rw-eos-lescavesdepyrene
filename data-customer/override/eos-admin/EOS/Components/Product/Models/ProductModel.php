<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Models;

use EOS\System\Util\ArrayHelper;

class ProductModel extends \EOS\System\Models\Model
{

    private function getCompName()
    {
        return 'product';
    }

    public function getListQuery($id_lang, $status = null)
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__product p'))
            ->select(null)
            ->select('p.id')
            ->select('p.reference')
            ->select('p.status')
            ->select('pl.name')
            ->select('ml.name')
            ->select('ps.quantity_available')
            ->where('pl.id_lang = ' . (int) $id_lang)
            ->innerJoin($this->db->tableFix('#__product_lang pl ON pl.id_product = p.id'))
            ->leftJoin($this->db->tableFix('#__manufacturer_lang ml ON ml.id_manufacturer = p.id_manufacturer'))
            ->leftJoin($this->db->tableFix('#__product_stock ps ON ps.id_product = p.id'))
            ->orderBy('pl.name');
        if ($status !== null) {
            $f->where('p.status = ' . $status);
        }
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product p'))
            ->select(null)
            ->select('p.id')
            ->select('p.status')
            ->select('p.price')
            ->select('p.ean13')
            ->select('p.isbn')
            ->select('p.upc')
            ->select('p.width')
            ->select('p.height')
            ->select('p.depth')
            ->select('p.weight')
            ->select('p.type_condition')
            ->select('p.featured')
            ->select('p.reference')
            ->select('p.on_sale')
            ->select('p.online_only')
            ->select('p.pack')
            ->select('p.extra_display_info')
            ->select('p.id_manufacturer')
            ->select('p.expected_date')
            ->select('p.out_of_stock')
            ->select('p.id_tax')
            ->select('p.quantity_per_pack')
            ->select('p.min_quantity')
            ->select('p.out_of_stock')
            ->where('p.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null) {
            $res = [];
            $res['price'] = 0;
            $res['out-stock'] = 2;
        } else {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['price'] = $rc['price'];
            $res['ean13'] = $rc['ean13'];
            $res['isbn'] = $rc['isbn'];
            $res['upc'] = $rc['upc'];
            $res['reference'] = $rc['reference'];
            $res['online-only'] = $rc['online_only'];
            $res['extra-display-info'] = $rc['extra_display_info'];
            $res['width'] = $rc['width'];
            $res['height'] = $rc['height'];
            $res['depth'] = $rc['depth'];
            $res['weight'] = $rc['weight'];
            $res['pack'] = $rc['pack'];
            $res['type-condition'] = $rc['type_condition'];
            $res['featured'] = $rc['featured'];
            $res['expected-date'] = empty($rc['expected_date']) ? null : $this->lang->dbDateToLangDate($rc['expected_date']);
            $res['status'] = $rc['status'] == 1;
            $res['id-manufacturer'] = $rc['id_manufacturer'];
            $res['out-of-stock'] = $rc['out_of_stock'];
            $res['quantity-per-pack'] = $rc['quantity_per_pack'];
            $res['min-quantity'] = $rc['min_quantity'];
            $res['id-tax'] = $rc['id_tax'];
            foreach ($ll as $l) {
                $idlang = $l['id'];
                $tblContent = $this->db->tableFix('#__product_lang');
                $query = $this->db->newFluent()->from($tblContent)->select('id')
                    ->where('id_product', $rc['id'])
                    ->where('id_lang', $idlang);
                $rcl = $query->fetch();
                if ($rcl != null) {
                    $res['name-' . $idlang] = $rcl['name'];
                    $res['descr-' . $idlang] = $rcl['description'];
                    $res['descr-short-' . $idlang] = $rcl['description_short'];
                }


                $seo = new \EOS\System\Seo\Manager($this->container);
                $seo->getSeoArray($this->getCompName(), $id, $idlang, $res);
            }


            $tblContent = $this->db->tableFix('#__product_product_category');
            $query = $this->db->newFluent()->from($tblContent)->select('id_category')
                ->where('id_product', $rc['id'])
                ->orderBy('id_category DESC');
            $rcc = $query->fetch();
            $res['id-category'] = $rcc['id_category'];
            //print_r($res); exit();

            $tblContent = $this->db->tableFix('#__product_image');
            $query = $this->db->newFluent()->from($tblContent)->select('url')->select('pos')
                ->where('id_product', $rc['id']);
            $rci = $query->fetchAll();
            $i = 0;
            foreach ($rci as $r) {
                $res['url-' . $i] = $r['url'];
                $res['pos-' . $i] = $r['pos'];
                $i++;
            }

            $tblAttribute = $this->db->tableFix('#__product_product_attribute_combination ppac');
            $query = $this->db->newFluent()->from($tblAttribute)->select('ppac.id_attribute as attribute')->select('pagl.id as id')
                ->innerJoin($this->db->tableFix('#__product_attribute pa ON pa.id = ppac.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pa.id = pal.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group pag ON pa.id_attribute_group = pag.id'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pag.id = pagl.id_attribute_group'))
                ->where('id_product', $rc['id']);
            $rca = $query->fetchAll();

            foreach ($rca as $ra) {
                $res['attribute-' . $ra['id']] = $ra['attribute'];
            }
            $ms = $this->newMediaServer();
            $ms->load($id, $res, 'media-image-product');
        }
        return $res;
    }

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $expectedDate = \EOS\System\Util\ArrayHelper::getStr($data, 'expected-date');
        //print_r($data); exit();
        $values = [
            'up_id' => 0,
            'price' => $data['price'],
            'id_tax' => $data['id-tax'],
            'quantity_per_pack' => $data['quantity-per-pack'],
            'min_quantity' => $data['min-quantity'],
            'reference' => $data['reference'],
            'width' => (float) $data['width'],
            'height' => (float) $data['height'],
            'weight' => (float) $data['weight'],
            'depth' => (float) $data['depth'],
            'type_condition' => (int) $data['type-condition'],
            'online_only' => (int) $data['online-only'],
            'pack' => (int) $data['pack'],
            'extra_display_info' => (int) $data['extra-display-info'],
            'featured' => (int) $data['featured'],
            'id_manufacturer' => (int) $data['id-manufacturer'],
            'expected_date' => empty($expectedDate) ? null : $this->lang->langDateToDbDate($expectedDate),
            'out_of_stock' => ArrayHelper::getInt($data, 'out-of-stock', 2),
            'status' => ArrayHelper::getStr($data, 'status') === 'on' ? 1 : 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];

        $tblContent = $this->db->tableFix('#__product');
        if ($data['id'] == 0) {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
        } else {
            $values['id'] = $data['id'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        }
        $query->execute();
        if ($data['id'] == 0) {
            $data['id'] = $this->db->lastInsertId();
            $this->saveStockAverage($data['id']);
        }
        $ll = $this->lang->getArrayFromDB(true);
        foreach ($ll as $l) {
            $this->saveDataLang($l['id'], $l['iso_code'], $data);
        }
        $this->saveAttribute($data);
        $this->saveDataimg($data);
        $this->saveCategory($data);
        $this->db->commit();

        return true;
    }

    private function saveDataLang($id_lang, $langiso, &$data)
    {
        $tblContent = $this->db->tableFix('#__product_lang');
        $query = $this->db->newFluent()->from($tblContent)->select(null)->select('id')
            ->where('id_product', $data['id'])
            ->where('id_lang', $id_lang);
        $item = $query->fetch();
        $dataop = [];
        $options = json_encode($dataop);
        $values = [
            'id_product' => $data['id'],
            'id_lang' => $id_lang,
            'name' => $data['name-' . $id_lang],
            'description_short' => $data['descr-short-' . $id_lang],
            'description' => $data['descr-' . $id_lang],
            'options' => $options,
            'up_id' => 0,
            'up_date' => new \FluentLiteral('NOW()')
        ];
        if ($item == null) {
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $item['id'] = $this->db->lastInsertId();
        } else {
            $query = $this->db->newFluent()->update($tblContent)->set($values)
                ->where('id', $item['id']);
        }
        $query->execute();
        $seo = new \EOS\System\Seo\Manager($this->container);
        $seo->setSeoArray($this->getCompName(), $data['id'], $id_lang, $data);
    }

    private function saveDataimg(&$data)
    {
        $ms = $this->newMediaServer();
        $error = '';
        if (!$ms->save($data['id'], $data, 'media-image-product', '', true, $error)) {
            throw new \Exception($error);
        }
    }

    private function saveAttribute(&$data)
    {
        $tblContent = $this->db->tableFix('#__product_product_attribute_combination');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id_product', (int) $data['id']);
        $query->execute();
        $mag = new AttributeModel($this->container);
        $attributegroups = $mag->getGroupList(1);
        foreach ($attributegroups as $attributegroup) {
            if (isset($data['attribute-' . $attributegroup['id'] . '-custom']) && $data['attribute-' . $attributegroup['id'] . '-custom'] != '') {
                $attribute = [];
                $attribute = [
                    'id_attribute_group' => $data['attribute-' . $attributegroup['id']],
                    'color' => 0,
                    'position' => 0
                ];
                $tblpa = $this->db->tableFix('#__product_attribute');
                $query = $this->db->newFluent()->insertInto($tblpa, $attribute);
                $query->execute();
                $attribute['id'] = $this->db->lastInsertId($tblpa);
                $attribute['id_attribute_group'] = $data['attribute-' . $attributegroup['id']];
                $ll = $this->lang->getArrayFromDB(true);
                foreach ($ll as $l) {
                    $attribute['name-' . $l['id']] = $data['attribute-' . $attributegroup['id'] . '-custom'];
                    $mag->saveDataLang($l['id'], $l['iso_code'], $attribute);
                }
                $idattribute = $attribute['id'];
            } else {
                $idattribute = $data['attribute-' . $attributegroup['id']];
            }
            // Salvo i dati solo se l'ID attributo <> 0 o null o vuoto
            if (!empty($idattribute)) {
                $values = [
                    'id_product' => $data['id'],
                    'id_attribute' => $idattribute
                ];
                $query = $this->db->newFluent()->insertInto($tblContent, $values);
                $query->execute();
            }
        }
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        //
        $mediaImage = $this->newMediaServer()->getMedia();
        $mediaImage->deleteAll((int) $data['id']);
        //
        $tblContentLang = $this->db->tableFix('#__product_lang');
        $query = $this->db->newFluent()->deleteFrom($tblContentLang)->where('id_product', (int) $data['id']);
        $query->execute();
        $tblContent = $this->db->tableFix('#__product');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $ll = $this->lang->getArrayFromDB(true);
        $this->db->commit();
        return true;
    }

    public function newMediaServer(): \EOS\UI\System\Media\ImageServer
    {
        $mediaImage = new \EOS\System\Media\Image($this->container, 'product');
        return new \EOS\UI\System\Media\ImageServer($this->container, $mediaImage);
    }

    public function saveCategory($data)
    {
        $tblContent = $this->db->tableFix('#__product_product_category');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id_product', (int) $data['id']);
        $query->execute();
        $values = [
            'id_product' => $data['id'],
            'id_category' => $data['id-category']
        ];
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();
    }

    public function saveStockAverage($id)
    {
        $tblContent = $this->db->tableFix('#__product_stock');
        $values = [
            'id_product' => $id,
            'id_product_child' => 0,
            'quantity_available' => -1,
            'quantity_physical' => 0,
            'quantity_engaged' => 0,
            'quantity_reserved' => 0,
            'quantity_ordered1' => 0,
            'quantity_ordered2' => 0,
            'date_ordered1' => null,
            'date_ordered2' => null
        ];
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();
        $tblContent = $this->db->tableFix('#__ext_average_sales');
        $values = [
            'id_product' => $id,
            'average_sales' => 0
        ];
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();
        return true;
    }
}
