<?php

return
    [
        'product.dashboard.menu' => 'Prodotti',
        'product.dashboard.product' => 'Catalogo prodotti',
        'product.dashboard.manufacturer' => 'Brand',
        'product.dashboard.category' => 'Categorie',
        'product.dashboard.attribute' => 'Attributi',
        'product.dashboard.catalogrules' => 'Regole Carrello'
];
