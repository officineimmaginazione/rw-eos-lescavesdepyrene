<?php
$box = new EOS\UI\Bootstrap\Box();
$box->startHeader();
(new \EOS\UI\Bootstrap\FormGroup)->attr('class', 'form-inline main-content-header')->addContent(function ()
{
	(new \EOS\UI\Bootstrap\Button('btn-new'))
	->attr('class', ' btn-success ml-auto mr-1')
	->click('function (e) { addNew();}')
	->content('Crea')
	->printRender($this);
	if(isset($this->dataLangDefault)) {
		echo '<div class="dropdown">';
		$sel = new EOS\UI\Bootstrap\DropDownMenu('lng-title');
		$sel->attr('class', 'btn-secondary text-uppercase');
		$sel->onChange('function(item) {changeLanguage(item);}');
		$sel->bind($this->dataLangDefault, 'id');
		$sel->bindList($this->dataLangList, 'id', 'iso_code', 'iso_code');
		$sel->dropDownMenuClasses(' dropdown-menu-right');
		$sel->printRender($this);
		echo '</div>';
	}
}, $this)->printRender($this);
$box->endHeader();

$box->startContent();
$tab = new \EOS\UI\Bootstrap\Tab();
$tab->addItem('tab-standard', 'Prodotti attivi');
$tab->addItem('tab-data', 'Prodotti disattivi');

$tab->startTab();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable', $this->path->urlFor('Product', ['product', 'ajaxlist']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('product.product.field.id'));
$tbl->addColumn('reference', $this->transE('product.product.field.reference'));
$tbl->addColumn('status', $this->transE('product.product.field.status'));
$tbl->addColumn('product', $this->transE('product.product.field.name'));
$tbl->addColumn('manufacturer', $this->transE('product.product.field.manufacturer'));
$tbl->addColumn('quantity', $this->transE('product.product.field.quantity'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->onColumnRender('quantity', 'function (data, type, full, meta) {return renderQuantity(data);}');
$tbl->onAjaxSend('function (data) {data.id_lang = id_lang}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$tab->endTab('tab-standard');

$tab->startTab();
$tbl = new \EOS\UI\Bootstrap\DataTable('datatable2', $this->path->urlFor('Product', ['product', 'ajaxlist2']), $this->session->getTokenName(), $this->session->getTokenValue());
$tbl->addColumn('id', $this->transE('product.product.field.id'));
$tbl->addColumn('reference', $this->transE('product.product.field.reference'));
$tbl->addColumn('status', $this->transE('product.product.field.status'));
$tbl->addColumn('product', $this->transE('product.product.field.name'));
$tbl->addColumn('manufacturer', $this->transE('product.product.field.manufacturer'));
$tbl->addColumn('quantity', $this->transE('product.product.field.quantity'));
$tbl->addColumnCustom('', 'edit-row', '<button class="btn  btn-info btn-sm" id="btn-new"><i class="fa fa-edit"></i></button>');
$tbl->onColumnRender('status', 'function (data, type, full, meta) {return renderStatus(data);}');
$tbl->onColumnRender('quantity', 'function (data, type, full, meta) {return renderQuantity(data);}');
$tbl->onAjaxSend('function (data) {data.id_lang = id_lang}');
$tbl->clickRow('function (e, r) {e.stopPropagation(); editRow(r.data().id);}', 'edit-row');
$tbl->printRender($this);
$tab->endTab('tab-data');
$tab->printRender($this);

$box->endContent();
$box->printRender($this);

$this->startCaptureScript();
?>
<script>
    var id_lang = <?php echo $this->dataLangDefault['id'];?>;
    function addNew()
    {
        window.location.href = "<?php echo $this->path->urlFor('Product', ['product', 'edit', '0']) ?>";
    }

    function setLang(item)
    {
        var n_id_lang = item.getAttribute('data-key1');
        if (n_id_lang != id_lang)
        {
          id_lang = n_id_lang;
          $('#datatable').DataTable().ajax.reload();
        }
    }
    
    function editRow(id)
    {
      window.location.href = "<?php echo $this->path->urlFor('Product', ['product', 'edit']) ?>" + id + "/";     
    }
    
    function renderStatus(data)
    {
        if (data == 1)
        {
            return '<span class="label label-success"><?php $this->transP('system.common.yes');?></span>'; 
        }
        else
        {
            return '<span class="label label-danger"><?php $this->transP('system.common.no');?></span>';     
        }
    }
    
    function renderQuantity(data)
    {
        return Number(data).toFixed(0); 
    }
   
</script>
<?php
$this->endCaptureScript();
