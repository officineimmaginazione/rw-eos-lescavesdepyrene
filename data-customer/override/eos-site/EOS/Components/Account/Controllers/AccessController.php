<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class AccessController extends \EOS\Components\System\Classes\SiteController
{

    public function login($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\AccessModel($this->container);
        if (!$m->getAuthLogin()) {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $v->authType = $m->getAuthType();
        $v->authForgot = $m->getAuthForgot();
        $v->authRegister = $m->getAuthRegister();
        return $v->render('access/login');
    }

    public function forgot($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\AccessModel($this->container);
        // Blocco la pagina se non attivo
        if (!$m->getAuthForgot()) {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $v->authType = $m->getAuthType();
        return $v->render('access/forgot');
    }

    public function register($request, $response)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Account\Models\AccessModel($this->container);
        $mr = new \EOS\Components\Account\Models\RegistrationModel($this->container);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Select2');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $v->chain = $request->getQueryParam('chain', 0);
        $v->dateins = $request->getQueryParam('di', 0);
        $v->id = $request->getQueryParam('idin', 0);
        if ($mr->checkInvitationValid($v->id)) {
            $v->valid = true;
        } else {
            $v->valid = false;
        }
        // Blocco la pagina se non attivo
        if (!$m->getAuthRegister()) {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $v->authType = $m->getAuthType();
        return $v->render('access/register');
    }

    public function active($request, $response)
    {
        $v = $this->newView($request, $response);
        $v->addMeta('robots', 'noindex, nofollow'); // Blocco indicizzazione pagina
        return $v->render('access/active');
    }

    public function resetPass($request, $response)
    {
        $v = $this->newView($request, $response);
        $v->addMeta('robots', 'noindex, nofollow'); // Blocco indicizzazione pagina
        return $v->render('access/reset-pass');
    }

    public function ajaxLogin($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\AccessModel($this->container);
            if (!$m->getAuthLogin()) {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }
            $data = $fv->getInputData();
            $error = '';
            if ($m->login($data, $error)) {
                $fv->setResult(true);
                $fv->setRedirect($m->getAuthHome());
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxForgot($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\AccessModel($this->container);

            // Blocco la pagina se non attivo
            if (!$m->getAuthForgot()) {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }

            $data = $fv->getInputData();
            $error = '';
            if ($m->forgot($data, $error)) {
                $fv->setResult(true);
                $fv->setMessage($this->lang->trans('account.access.forgot.send'));
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxRegister($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\AccessModel($this->container);

            // Blocco la pagina se non attivo
            if (!$m->getAuthRegister()) {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }

            $data = $fv->getInputData();
            $error = '';
            if ($m->register($data, $error)) {
                $fv->setResult(true);
                $fv->setMessage($this->lang->trans('account.access.register.send2'));
                // Resetto i token di default della sessione
                $this->session->resetToken();
                if (ArrayHelper::getBool($data, 'reg-consent2') == 1) {
                    $sendinblue = new \Mailin("https://api.sendinblue.com/v2.0", "yrvajS7BGbTZH6d0");
                    $data = array(
                        "email" => ArrayHelper::getStr($data, 'email'),
                        "attributes" => array("NOME" => ArrayHelper::getStr($data, 'name'), "COGNOME" => ArrayHelper::getStr($data, 'surname'), "DATA_DI_NASCITA" => str_replace("/", "-", ArrayHelper::getStr($data, 'birthday'))),
                        "listid" => array(5)
                    );
                    $sendinblue->create_update_user($data);
                }
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue'))) {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
            switch ($datasource) {
                case 'city':
                    if ($format === 'select2') {
                        $filter = ArrayHelper::getStr($params, 'q');
                        $res['results'] = $m->searchCity($filter, $m->getFuncFormatCityStateText());
                    } else if ($format === 'typeahead') {
                        $res = $m->getCityList($m->getFuncFormatCityStateName());
                    }
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }

    public function ajaxRequest($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            // Blocco la pagina se non attivo
            $data = $fv->getInputData();
            $error = '';

            $mu = new \EOS\Components\Account\Models\UserModel($this->container);
            $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);

            //$customer = $mu->getInfoUser($data['idcustomer']);
            if ($m->saveInviation($data, $error)) {
                $fv->setResult(true);
                $fv->setMessage('Grazie! A breve riceverai una mail per completare l’iscrizione');
                // Resetto i token di default della sessione
                $this->session->resetToken();
            } else {
                $fv->setMessage($error);
            }
            // Resetto i token di default della sessione
            $this->session->resetToken();
        }
        return $fv->toJsonResponse();
    }
}
