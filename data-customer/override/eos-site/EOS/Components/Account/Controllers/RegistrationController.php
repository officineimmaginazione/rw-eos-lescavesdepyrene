<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class RegistrationController extends \EOS\Components\Account\Classes\AuthController
{

    public function register($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Select2');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $v->payment = $m->getMethodPayment();
        return $v->render('registration/customer');
    }

    public function ajaxRegister($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        //$this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
            $mcs = new \EOS\Components\LCDPComp\Models\CreditSafeModel($this->container);
            // Blocco la pagina se non attivo
            if (!$m->getAuthRegister()) {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }
            $data = $fv->getInputData();
            $error = '';
            if ($m->register($data, $error)) {
                if (isset($data['csid']) && $data['csid'] != '') {
                    $mcs->addCompanyToPortfolioCreditSafe($data['csid'], $error);
                }
                $fv->setResult(true);
                $fv->setMessage($this->lang->trans('account.access.register.send'));
                // Resetto i token di default della sessione
                $this->session->resetToken();
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            $data = $fv->getInputData();
            $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
            $mcs = new \EOS\Components\LCDPComp\Models\CreditSafeModel($this->container);
            $error = '';
            if (isset($data['command'])) {
                switch ($data['command']) {
                    case 'retrieveCSdata':
                        $res = $mcs->companyReportCreditSafe($data['csid'], $error);
                        $csdata = json_decode($res, true);
                        $fv->setResult(true);
                        $fv->setMessage('Dati inseriti correttamente');
                        $commonValue = (isset($csdata['report']['companySummary']['creditRating']['providerValue']['value'])) ? $csdata['report']['companySummary']['creditRating']['providerValue']['value'] : '';
                        if ($commonValue < 40 || $commonValue == 'Not rated') {
                            $fv->setOutputValue("commonDescription", 'Rischio Alto - Solo anticipato');
                            $commonDescription = 'Rischio Alto - Solo anticipato';
                        } else if (isset($csdata['report']['alternateSummary']['legalForm']) && $csdata['report']['alternateSummary']['legalForm'] == "SOCIETA' A RESPONSABILITA' LIMITATA SEMPLIFICATA") {
                            $fv->setOutputValue("commonDescription", 'Rischio Alto - S.r.l.s - Solo anticipato');
                            $commonDescription = 'Rischio Alto - S.r.l.s - Solo anticipato';
                        } else if ($commonValue >= 40 && $commonValue < 60) {
                            $fv->setOutputValue("commonDescription", 'Rischio Alto - Solo anticipato');
                            $commonDescription = 'Rischio Medio - Possibile modificare pagamento';
                        } else {
                            $fv->setOutputValue("commonDescription", 'Rischio Alto - Solo anticipato');
                            $commonDescription = 'Rischio Basso - Possibile modificare pagamento';
                        }
                        $creditLimit = (isset($csdata['report']['companySummary']['creditRating']['creditLimit']['value'])) ? number_format((int)$csdata['report']['companySummary']['creditRating']['creditLimit']['value'], 0, "", ".") : 0;
                        $pec = (isset($csdata['report']['alternateSummary']['emailAddresses'])) ? $csdata['report']['alternateSummary']['emailAddresses'] : "";
                        $phone = (isset($csdata['report']['alternateSummary']['telephone'])) ? $csdata['report']['alternateSummary']['telephone'] : "";
                        $fv->setOutputValue("pec", $pec);
                        $fv->setOutputValue("phone", $phone);
                        $fv->setOutputValue("commonValue", $commonValue);
                        $fv->setOutputValue("commonDescription", $commonDescription);
                        $fv->setOutputValue("creditLimit", $creditLimit);
                        if (isset($data['update']) && $data['update'] == 1) {
                            $mcs->addCompanyToPortfolioCreditSafe($data['csid'], $error);
                            $m->updateCSInfo($data['idaddress'], $data['csid'], $commonValue, $creditLimit, $commonDescription);
                        }
                        break;
                    case 'checkcreditsafe':
                        if (isset($data['update']) && $data['update'] == 1) {
                            $res = $mcs->companyCreditSafe($data['vatnumber'], $error);
                            $data = json_decode($res, true);
                            if (isset($data['totalSize']) && $data['totalSize'] == 1) {
                                $fv->setResult(false);
                                $fv->setMessage('È stata trovata la seguente azienda');
                                $fv->setOutputValue("name", $data['companies'][0]['name']);
                                $fv->setOutputValue("cif", $data['companies'][0]['vatNo'][1]);
                                $fv->setOutputValue("addresscomplete", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("address", $data['companies'][0]['address']['street'] . " " . $data['companies'][0]['address']['houseNo']);
                                $fv->setOutputValue("city", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("province", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("postcode", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("id", $data['companies'][0]['id']);
                            } else {
                                $fv->setResult(true);
                                $fv->setMessage('Ricontrolla la Partita Iva');
                            }
                        } else if ($m->checkVatNumber($data['vatnumber']) == false) {
                            $res = $mcs->companyCreditSafe($data['vatnumber'], $error);
                            $data = json_decode($res, true);
                            if (isset($data['totalSize']) && $data['totalSize'] == 1) {
                                $fv->setResult(false);
                                $fv->setMessage('È stata trovata la seguente azienda');
                                $fv->setOutputValue("name", $data['companies'][0]['name']);
                                $fv->setOutputValue("cif", $data['companies'][0]['vatNo'][1]);
                                $fv->setOutputValue("addresscomplete", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("address", $data['companies'][0]['address']['street'] . " " . $data['companies'][0]['address']['houseNo']);
                                $fv->setOutputValue("city", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("province", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("postcode", $data['companies'][0]['address']['simpleValue']);
                                $fv->setOutputValue("id", $data['companies'][0]['id']);
                            } else {
                                $fv->setResult(true);
                                $fv->setMessage('Ricontrolla la Partita Iva');
                            }
                        } else {
                            $fv->setResult((true));
                            $fv->setMessage('Partita Iva già inserita');
                        }
                        break;
                    case 'checkvatnumber':
                        if ($m->checkVatNumber($data['vatnumber']) == false) {
                            $fv->setResult(false);
                        } else {
                            $fv->setResult((true));
                        }
                        break;
                    default:
                        $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
                        break;
                }
            } else {
                $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }

    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue'))) {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
            switch ($datasource) {
                case 'city':
                    if ($format === 'select2') {
                        $filter = ArrayHelper::getStr($params, 'q');
                        $res['results'] = $m->searchCity($filter, $m->getFuncFormatCityStateText());
                    } else if ($format === 'typeahead') {
                        $res = $m->getCityList($m->getFuncFormatCityStateName());
                    }
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }


    public function invitation($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Select2');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        return $v->render('invitation/send');
    }

    public function ajaxInvitation($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        //$this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
            // Blocco la pagina se non attivo
            if (!$m->getAuthRegister()) {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }
            $data = $fv->getInputData();
            $error = '';
            if ($m->saveInviation($data, $error)) {
                $fv->setResult(true);
                $fv->setMessage($this->lang->trans('account.access.register.invitation.send'));

                // Resetto i token di default della sessione
                $this->session->resetToken();
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }
}
