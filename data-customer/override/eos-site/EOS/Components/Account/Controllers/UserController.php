<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class UserController extends \EOS\Components\Account\Classes\AuthController
{

    public function index($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $mo = new \EOS\Components\Order\Models\OrderModel($this->container);
        $m->userInfo($v);
        $error = '';
        $v->agentid = $v->user->id;
        $v->enablecheckcondition = true;
        $v->customer = (int)$request->getQueryParam('customer', 0);
        $v->filterBy = (int)$request->getQueryParam('filterby', 0);
        $advisor = (int)$request->getQueryParam('advisor', 0);
        $user = (int)$request->getQueryParam('user', 0);
        if ($user > 0) {
            if ($advisor != 2) {
                $v->enablecheckcondition = false;
                if ($v->advisor ==  2 || $v->advisor == 0) {
                    $m->setAdvisor($user, $advisor);
                    $data['name'] = $v->accountinfo['name'];
                    $data['acceptcontract'] = $advisor;
                    $sender = new \EOS\System\Mail\Mail($this->container);
                    $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
                    $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
                    $sender->to = $v->accountinfo['email'];
                    $sender->bcc = $this->container->get('database')->setting->getValue('lcdp', 'shop.email');
                    $sender->subject = 'LesCavesDePyrene - Accettazione contratto presentatore';
                    $sender->message = $this->createMail($data);
                    $sender->sendEmail($error);
                }
                if ($advisor == 1) {
                    $v->advisor = 1;
                }
            } else {
                $v->advisor = 2;
            }
        }
        $openDebit = [];
        $v->debit = 0;
        if (!in_array(42, $v->user->groups)) {
            if (!$v->viewagent) {
                $openDebit = $mo->getOpenDebit($v->user->id, 200);
            } else {
                if ($v->customer !== 0) {
                    $openDebit = $mo->getOpenDebit($v->customer, 200);
                }
            }
            if (!empty($openDebit)) {
                foreach ($openDebit as $row) :

                    if ($row["Avere"] == 0.0000 || $row["Dare"] == 0.0000) {

                        if (new \DateTime() > new \DateTime($row["ScadenzaDocumento"])) {
                            $v->debit = 1;
                            break;
                        }
                    }
                endforeach;
            }
        }
        $mc = new \EOS\Components\Cart\Models\CartModel($this->container);
        if (!$v->viewagent) {
            $v->customer = $v->user->id;
            $mc->setAccountInCart($v->customer, 0);
        }
        if ($v->customer != 0) {
            $v->customerinfo = $m->getCustomerInfo($v->customer);
            $mc->setAccountInCart($v->customerinfo['idaddress'], $v->agentid);
            $v->listaddresses = $ma->getAddresses($v->customer, 1);
        } else {
            $v->listaddresses = null;
        }
        return $v->render('user/default');
    }

    public function logout($request, $response)
    {
        // Distruggo la sessione per sicurezza e fa il logout così
        $this->session->destroy();
        $newUrl = $this->path->urlFor('account', ['access', 'login']);
        return $response->withRedirect($newUrl, 302);
    }

    public function edit($request, $response)
    {
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $v = $this->newView($request, $response);
        $v->authType = $m->getAuthType();
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        return $v->render('user/edit');
    }

    public function ajaxEdit($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\AccessModel($this->container);

            // Blocco la pagina se non attivo
            if (!$m->getAuthRegister()) {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }

            $data = $fv->getInputData();
            $error = '';
            if ($m->register($data, $error)) {
                $fv->setResult(true);
                $fv->setMessage($this->lang->trans('account.access.register.send'));
                // Resetto i token di default della sessione
                $this->session->resetToken();
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            $data = $fv->getInputData();
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            if (isset($data['command'])) {
                switch ($data['command']) {
                    case 'customerdata':
                        $data = $m->getCustomerInfo(ArrayHelper::getInt($data, 'idcustomer'), $data['status']);
                        $fv->setOutputValue('address', $data);
                        $fv->setResult(true);
                        break;
                }
            } else {
                $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }

    public function getDataSource($request, $response, $args)
    {
        $res = [];
        if ($this->session->isValidToken(ArrayHelper::getStr($args, 'tokenname'), ArrayHelper::getStr($args, 'tokenvalue'))) {
            $datasource = ArrayHelper::getStr($args, 'datasource');
            $format = ArrayHelper::getStr($args, 'format');
            $params = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
            $m = new \EOS\Components\Account\Models\UserModel($this->container);
            switch ($datasource) {
                case 'user':
                    if ($format === 'typeahead1') {
                        if (isset($this->user)) {
                            $user = $this->user->getInfo();
                            $id = $user->id;
                            $res = $m->getUserList($m->getFuncFormatAddressVAT(), $id, 1);
                        }
                    } elseif ($format === 'typeahead2') {
                        if (isset($this->user)) {
                            $user = $this->user->getInfo();
                            $id = $user->id;
                            $res = $m->getUserList($m->getFuncFormatAddressOffices(), $id, 2);
                        }
                    } else {
                        if (isset($this->user)) {
                            $user = $this->user->getInfo();
                            $id = $user->id;
                            $res = $m->getUserList($m->getFuncFormatAddressName(), $id, 0);
                        }
                    }
                    break;
            }
        }
        return $response->withHeader('X-Robots-Tag', 'noindex, nofollow')->withJson($res);
    }

    public function listInvitation($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $m->userInfo($v);
        $v->listinvitation = $m->getInvitation($v->user->id, false);
        return $v->render('list/invitation', true);
    }

    public function listAdvisor($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $m->userInfo($v);
        $v->listadvisor = $m->getAdvisor($v->user->id);
        return $v->render('list/advisor', true);
    }

    public function changePass($request, $response)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($this->path->getExtraUrlFor('system', 'resource/app.js'));
        $m = new \EOS\Components\Account\Models\UserModel($this->container);
        $m->userInfo($v);
        return $v->render('user/password');
    }

    public function ajaxPassword($request, $response)
    {
        $fv = new FormValidator($this, $request, $response, []);
        $fv->validateRequestUrlField = true;
        // Richiamo un evento per cambiare i parametri dei custom fields
        if ($fv->prepareInputDataFromJson()) {
            $m = new \EOS\Components\Account\Models\RegistrationModel($this->container);
            $mu = new \EOS\Components\Account\Models\UserModel($this->container);
            $data = $fv->getInputData();
            $error = '';
            if ($mu->changePassword($data, $error)) {
                $fv->setResult(true);
                $fv->setMessage('La password è stata cambiata');
                $this->session->resetToken();
            } else {
                $fv->setMessage($error);
            }
        }
        return $fv->toJsonResponse();
    }

    public function createMail($data)
    {
        $m = new \EOS\Components\Reservation\Models\BookingModel($this->container);
        $content = '<!DOCTYPE html>';
        $content .= '<html lang="it" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">';
        $content .= '<head>';
        $content .= '<meta charset="utf-8">';
        $content .= '<meta name="viewport" content="width=device-width">';
        $content .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $content .= '<meta name="x-apple-disable-message-reformatting">';
        $content .= '<link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700" rel="stylesheet">';
        $content .= '<title>LesCavesDePyrene - Accettazione contratto presentatore</title>';
        $content .= "<style>body,html{margin:0 auto!important;padding:0!important;height:100%!important;width:100%!important;background:#f1f1f1}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*='margin: 16px 0']{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}img{-ms-interpolation-mode:bicubic}a{text-decoration:none}.aBn,.unstyle-auto-detected-links *,[x-apple-data-detectors]{border-bottom:0!important;cursor:default!important;color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}.a6S{display:none!important;opacity:.01!important}.im{color:inherit!important}img.g-img+div{display:none!important}@media only screen and (min-device-width:320px) and (max-device-width:374px){u~div .email-container{min-width:320px!important}}@media only screen and (min-device-width:375px) and (max-device-width:413px){u~div .email-container{min-width:375px!important}}@media only screen and (min-device-width:414px){u~div .email-container{min-width:414px!important}}.bg_white{background:#fff}.bg_light{background:#fafafa}.bg_black{background:#000}.bg_dark{background:rgba(0,0,0,.8)}.email-section{padding:2.5em}.btn{padding:10px 15px;display:inline-block}.btn.btn-primary{border-radius:5px;background:#8b2329;color:#fff}.btn.btn-white{border-radius:5px;background:#fff;color:#000}.btn.btn-white-outline{border-radius:5px;background:0 0;border:1px solid #fff;color:#fff}.btn.btn-black-outline{border-radius:0;background:0 0;border:2px solid #000;color:#000;font-weight:700}.btn-custom{color:rgba(0,0,0,.3);text-decoration:underline}h1,h2,h3,h4,h5,h6{font-family:'Work Sans',sans-serif;color:#000;margin-top:0;font-weight:400}p{color:rgba(0,0,0,.7)}body{font-family:'Work Sans',sans-serif;font-weight:400;font-size:15px;line-height:1.8;color:rgba(0,0,0,.7)}a{color:#8b2329}.logo h1{margin:0}.logo h1 a{color:#8b2329;font-size:24px;font-weight:700;font-family:'Work Sans',sans-serif}.hero{position:relative;z-index:0}.hero .text{color:rgba(0,0,0,.3)}.hero .text h2{color:#000;font-size:24px;margin-bottom:15px;font-weight:300;line-height:1.2}.hero .text h3{font-size:21px;font-weight:200}.hero .text h2 span{font-weight:600;color:#000}.product-entry{display:block;position:relative;float:left;padding-top:20px}.product-entry .text{width:calc(100% - 0px);padding-left:20px}.product-entry .text h3{margin-bottom:0;padding-bottom:0}.product-entry .text p{margin-top:0}.product-entry .text,.product-entry img{float:left}ul.social{padding:0}ul.social li{display:inline-block;margin-right:10px}.footer{border-top:1px solid rgba(0,0,0,.05);color:rgba(0,0,0,.5)}.footer .heading{color:#000;font-size:20px}.footer ul{margin:0;padding:0}.footer ul li{list-style:none;margin-bottom:10px}.footer ul li a{color:#000}</style>";
        $content .= '</head>';
        $content .= '<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f1f1f1;">';
        $content .= '<center style="width: 100%; background-color: #f1f1f1;">';
        $content .= '<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">';
        $content .= '</div>';
        $content .= '<div style="max-width: 600px; margin: 0 auto;" class="email-container">';
        /* Testata */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="top" class="bg_white" style="padding: 1em 2.5em 0 2.5em;">';
        $content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td class="logo" style="text-align: left;">';
        $content .= '<h1><a href="https://www.lescaves.it">LesCavesDePyrene</a></h1>';
        if ($data['acceptcontract'] == 0) {
            $content .= '<p>Ciao ' . $data['name'] . ', ci spiace che tu non abbia accettato i termini ma così non potrai presentare nuovi clienti e ricevere cashback.</p>';
        } else {
            $content .= '<p>Ciao ' . $data['name'] . ', siamo molto felici che tu  abbia accettato i termini, così potrai presentare nuovi clienti e ricevere cashback.</p>';
        }
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        /* Footer */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="middle" class="bg_light footer email-section">';
        $content .= '<table>';
        $content .= '<tr>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 5px; padding-right: 5px;">';
        $content .= '<h3 class="heading">Les Caves de Pyrene</h3>';
        $content .= '<ul>';
        $content .= '<li><span class="text">Strada Cagnassi, 8 - 12050 Rodello</span></a></li>';
        $content .= '<li><span class="text">Corso Susa 22/A - 10098 Rivoli</span></a></li>';
        $content .= '<li><span class="text">Tel. +39 0173 617072</span></a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 10px;">';
        $content .= '<h3 class="heading">Link Utili</h3>';
        $content .= '<ul>';
        $content .= '<li><a href="https://www.lescaves.it">Chi siamo</a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td class="bg_white" style="text-align: center;">';
        $content .= '<p>&nbsp;</p>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</center>';
        $content .= '</body>';
        $content .= '</html>';
        return $content;
    }
}
