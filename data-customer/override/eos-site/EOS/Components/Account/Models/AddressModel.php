<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class AddressModel extends \EOS\System\Models\Model
{

    public function getListQuery()
    {
        $f = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->select(null)
            ->select('a.id, a.name, a.surname, a.company, lc.name, a.status')
            ->innerJoin($this->db->tableFix('#__localization_city lc ON lc.id = a.id_city'))
            ->where('a.deleted = 0');
        return $f;
    }

    public function getData($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->where('a.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc)) {
            $res = [];
        } else {
            $res['id'] = $rc['id'];
            $res['id_customer'] = $rc['id_customer'];
            $res['alias'] = $rc['alias'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['surname'] = $rc['surname'];
            $res['company'] = $rc['company'];
            $res['address1'] = $rc['address1'];
            $res['address2'] = $rc['address2'];
            $res['phone'] = $rc['phone'];
            $res['phone_mobile'] = $rc['phone_mobile'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['status'] = $rc['status'] == 1;
            if (!empty($rc['options'])) {
                $ops = json_decode($rc['options'], true);
                $res['id-agent'] = $ops['id_agent'];
                $res['payment'] = $ops['payment'];
                $res['note'] = $ops['note'];
            }
            $ml = new LocalizationModel($this->container);
            $current_city = $ml->getCity(ArrayHelper::getInt($rc, 'id_city'));
            $formatFunc = $ml->getFuncFormatCityStateText();
            $res['city'] = empty($current_city) ? '' : $current_city['name'] . ' (' .  $current_city['statecode'] . ')';
            $res['status'] = (int) $rc['status'];
            $tblAccountUser = $this->db->tableFix('#__account_user_group aug');
            $query = $this->db->newFluent()->from($tblAccountUser)
                ->select('cr.name as discount')
                ->select('cr.id')
                ->select('cr.reduction')
                ->innerJoin($this->db->tableFix('#__catalog_rules cr ON cr.id_group = aug.id_group'))
                ->where('aug.id_user', $rc['id_customer']);
            $rca = $query->fetchAll();
            $i = 0;
            foreach ($rca as $ra) {
                $res['discount-' . $i . '-id'] = $ra['id'];
                $res['discount-' . $i . '-name'] = $ra['discount'];
                $res['discount-' . $i . '-reduction'] = $ra['reduction'];
                $i++;
            }
            $res['discount-count'] = $i;
        }
        return $res;
    }

    public function getDataDiscount($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
            ->where('a.id = ?', (int) $id);
        $rc = $query->fetch();
        if (empty($rc)) {
            $res = [];
            $res['id_lang'] = $this->lang->getCurrentID();
        } else {
            $res['id'] = $rc['id'];
            $res['id-customer'] = $rc['id_customer'];
            $res['alias'] = $rc['alias'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['company'] = $rc['company'];
            $res['address1'] = $rc['address1'];
            $res['address2'] = $rc['address2'];
            $res['id-city'] = $rc['id_city'];
            $res['phone'] = $rc['phone'];
            $res['phone-mobile'] = $rc['phone_mobile'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['status'] = $rc['status'] == 1;
            if (!empty($rc['options'])) {
                $ops = json_decode($rc['options'], true);
                $res['id-agent'] = $ops['id_agent'];
            }
            $options = json_decode($rc['options'], true);
            $qp = $this->db->newFluent()->from($this->db->tableFix('#__ext_pag_amen epa'))
                ->where('epa.id = ?', (int) $options['payment']);
            $rp = $qp->fetch();
            $res['payment'] = $rp['PADESCRI'];
            $res['id-payment'] = $rp['id'];
            $res['payment-discount'] = $rp['PASCONTO'];
            $ml = new LocalizationModel($this->container);
            $current_city = $ml->getCity(ArrayHelper::getInt($rc, 'id_city'));
            //print_r($current_city);
            //$formatFunc = $ml->getFuncFormatCityStateText();
            $res['city'] = empty($current_city['name']) ? '' : $current_city['name'];
            $res['zip_code'] = empty($current_city['zip_code']) ? '' : $current_city['zip_code'];
            $res['statecode'] = empty($current_city['statecode']) ? '' : $current_city['statecode'];
            $res['status'] = (int) $rc['status'];
            $tblAccountUser = $this->db->tableFix('#__account_user_group aug');
            $query = $this->db->newFluent()->from($tblAccountUser)
                ->select('cr.name as discount')
                ->select('cr.id')
                ->select('cr.reduction')
                ->select('cr.reduction_sum2')
                ->select('cr.reduction_sum3')
                ->innerJoin($this->db->tableFix('#__catalog_rules cr ON cr.id_group = aug.id_group'))
                ->where('aug.id_user', $rc['id_customer']);
            $rca = $query->fetchAll();
            $i = 0;
            foreach ($rca as $ra) {
                $res['discount-' . $i . '-id'] = $ra['id'];
                $res['discount-' . $i . '-name'] = $ra['discount'];
                $res['discount-' . $i . '-reduction1'] = $ra['reduction'];
                $res['discount-' . $i . '-reduction2'] = $ra['reduction_sum2'];
                $res['discount-' . $i . '-reduction3'] = $ra['reduction_sum3'];
                $i++;
            }
            $res['discount-count'] = $i;
        }
        return $res;
    }

    public function getDataByIDCustomer($idcustomer, $coddes = 9999)
    {
        $sql1 = "select a.* from #__address a
            where a.id_customer = " . $idcustomer . " and JSON_CONTAINS(a.options, '\"" . $coddes . "\"', '$.coddes')
            order by a.id";
        $q1 = $this->db->prepare($sql1);
        $q1->execute();
        $rc = $q1->fetch();
        if (empty($rc)) {
            $sql2 = "select a.* from #__address a
            where a.id_customer = " . $idcustomer . "
            order by a.id limit 1";
            $q2 = $this->db->prepare($sql2);
            $q2->execute();
            $rc = $q2->fetch();
        }

        if (empty($rc)) {
            $res = [];
        } else {
            $res['id'] = $rc['id'];
            $res['id_customer'] = $rc['id_customer'];
            $res['alias'] = $rc['alias'];
            $res['name'] = $rc['name'];
            $res['surname'] = $rc['surname'];
            $res['company'] = $rc['company'];
            $res['address1'] = $rc['address1'];
            $res['address2'] = $rc['address2'];
            $res['phone'] = $rc['phone'];
            $res['phone_mobile'] = $rc['phone_mobile'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['status'] = $rc['status'] == 1;
            if (!empty($rc['options'])) {
                $ops = json_decode($rc['options'], true);
                $res['id-agent'] = $ops['id_agent'];
                $res['payment'] = $ops['payment'];
                $res['note'] = $ops['note'];
            }
            $ml = new LocalizationModel($this->container);
            $current_city = $ml->getCity(ArrayHelper::getInt($rc, 'id_city'));
            $formatFunc = $ml->getFuncFormatCityStateText();
            $res['city'] = empty($current_city) ? '' : $current_city['name'] . ' (' .  $current_city['statecode'] . ')';
            $res['status'] = (int) $rc['status'];
        }
        return $res;
    }

    public function getAddressList(callable $formatFunc = null, $id)
    {
        if ($id != 8363) {
            $sql = 'select a.id, concat(a.company, " - ", a.alias, " - ", lc.name, ", ", a.address1 ) as name from #__address a
                left outer join #__localization_city lc on lc.id = a.id_city
                where (a.status = 1 and a.options like \'%id_agent":"' . $id . '%\' or a.id = ' . $id . ' and a.deleted = 0)
                order by company';
        } else {
            $sql = 'select a.id, concat(a.company, " - ", a.alias, " - ", lc.name, ", ", a.address1 ) as name from #__address a
                left outer join #__localization_city lc on lc.id = a.id_city
                where a.status = 1 and a.deleted = 0
                order by company';
        }
        //print_r($sql);
        $q = $this->db->prepare($sql);
        $q->execute();
        return $this->formatResult($q, $formatFunc);
    }

    public function getAddresses($id, $status = 0, $idagent = 0)
    {
        if ($idagent == 8363) {
            $idagent = 0;
        }
        if ($status == 0) {
            $sql = 'select a.*, lc.name as cityname, lc.zip_code, ls.iso_code as state_code from #__address a
                left outer join #__localization_city lc on lc.id = a.id_city
                left outer join #__localization_state ls on ls.id = lc.id_state ';
            if ($idagent == 0) {
                $sql .= 'where a.id_customer = ' . $id . ' and a.deleted = 0 
                        order by a.id';
            } else {
                $sql .= 'where a.id_customer = ' . $id . ' and a.deleted = 0 and JSON_CONTAINS(a.options, \'\"' . $idagent . '\"\', \'$.id_agent\')
                        order by a.id';
            }
        } else {
            $sql = 'select a.*, lc.name as cityname, lc.zip_code, ls.iso_code as state_code from #__address a
                left outer join #__localization_city lc on lc.id = a.id_city
                left outer join #__localization_state ls on ls.id = lc.id_state ';
            if ($idagent == 0) {
                $sql .= 'where a.id_customer = ' . $id . ' and status = 1 and a.deleted = 0 
                        order by a.id';
            } else {
                $sql .= 'where a.id_customer = ' . $id . ' and status = 1 and a.deleted = 0 and JSON_CONTAINS(a.options, \'\"' . $idagent . '\"\', \'$.id_agent\')
                        order by a.id';
            }
        }
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function getFuncFormatAddressText()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'text' => sprintf('%s (%s)', $row['name'], $row['id'])];
        };
    }

    public function getFuncFormatAddressName()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'name' => sprintf('%s (%s)', $row['name'], $row['id'])];
        };
    }

    public function getAddressCoords()
    {
        $select = 'select au.id, a.id as idaddress, a.alias, a.company, a.address1, a.phone, au.email, lc.name as city, (json_unquote(JSON_EXTRACT(a.options, "$.lat"))) as latitude, (json_unquote(JSON_EXTRACT(a.options, "$.long"))) as longitude ';
        $from = ' from #__address a ';
        $inner = ' inner join #__localization_city as lc on lc.id = a.id_city ';
        $inner .= ' inner join #__account_user as au on au.id = a.id_customer ';
        $where = " where JSON_CONTAINS(a.options, '\"1\"', '$.advisor') ";
        $q = $this->db->prepare($select . $from . $inner . $where);
        $q->execute();
        return $q->fetchAll();
    }

    public function saveData(&$data, &$error)
    {
        $options = [
            'id_agent' => $data['id_agent'],
            'payment' => $data['payment'],
            'pec' => $data['pec'],
            'unicode' => $data['unicode'],
            'note' => $data['note'],
            'coddes' => (isset($data['coddes'])) ? $data['coddes'] : ''
        ];
        $values = [
            'id_customer' => $data['id_customer'],
            'alias' => $data['alias'],
            'name' => $data['name'],
            'surname' => $data['surname'],
            'address1' => $data['address1'],
            'address2' => '',
            'id_city' => $data['city1'],
            'phone' => $data['phone'],
            'phone_mobile' => $data['phone'],
            'vat' => $data['vat'],
            'company' => $data['company'],
            'id_country' => 1,
            'cif' => $data['cif'],
            'reference' => '0',
            'options' => json_encode($options),
            //'status' => (isset($data['status']) ? $data['status'] == 'on' : false),
            'status' => 1,
            'up_id' => $data['id_agent'],
            'up_date' => $this->db->util->nowToDBDateTime()
        ];
        $tblContent = $this->db->tableFix('#__address');

        $values['ins_id'] = $data['id_agent'];
        $values['ins_date'] = $this->db->util->nowToDBDateTime();
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();

        return true;
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;


        return $res;
    }

    public function updateStatus($id, $status)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__address');
        $values['status'] = $status;
        $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', (int) $id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function deleteData($data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__account_user');
        $query = $this->db->newFluent()->deleteFrom($tblContent)->where('id', (int) $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function searchList($filter, $limit = 50)
    {
        $res = [];
        if ((mb_strlen($filter) >= 1) && (mb_strlen($filter) < 200)) {
            $q = $this->getListQuery();
            $fValue = $this->db->quote('%' . $this->db->util->prepareLikeValue($filter) . '%');
            $autType = $this->getAuthType();
            $fList[] = 'u.id like ' . $fValue;
            $fList[] = 'u.name like ' . $fValue;
            switch ($autType) {
                case 1: // Email
                    $fList[] = 'u.email like ' . $fValue;
                    break;
                case 2: // Utente
                    $fList[] = 'u.username like ' . $fValue;
                    break;
                default: // 0 - 3 Utente o Email
                    $fList[] = 'u.email like ' . $fValue;
                    $fList[] = 'u.username like ' . $fValue;
                    break;
            }
            $q->where(implode("\n OR ", $fList));
            if ($limit > 0) {
                $q->limit($limit);
            }
            $res = $this->getTextList($q);
        }
        return $res;
    }

    protected function formatResult($q, callable $formatFunc = null)
    {
        $res = [];
        if (is_null($formatFunc)) {
            $res = $q->fetchAll();
        } else {
            while ($r = $q->fetch()) {
                $res[] = $formatFunc($r);
            }
        }
        return $res;
    }
}
