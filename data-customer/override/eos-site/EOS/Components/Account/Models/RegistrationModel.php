<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\ArrayHelper;
use EOS\System\Routing\PathHelper;
use EOS\System\Util\StringHelper;
use EOS\System\Util\DateTimeHelper;

class RegistrationModel extends \EOS\System\Models\Model
{

    public $checkRequestLimit = true;
    public $lastRegisterID = null;

    protected function checkUserExists($username, $email)
    {
        $q = $this->db->prepare('select id from #__account_user where ((username = :username) or (email = :email))');
        $q->bindValue(':username', $username);
        $q->bindValue(':email', $email);
        $q->execute();
        $r = $q->fetch();
        return empty($r);
    }

    protected function createUniqueUsername()
    {
        $res = '';
        while (empty($res)) {
            // genero 8 caratteri random tra numeri e lettere
            $letters = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'), ['.', '-', '_']);
            for ($i = 0; $i < 8; $i++) {
                $res .= $letters[rand(0, count($letters) - 1)];
            }
            // genero 4 bytes random (8 caratteri random) 
            $res .= \EOS\System\Security\CryptHelper::randomString(4);
            // Converto il timestamp in coda
            $dt = (new \DateTimeImmutable())->getTimestamp();
            $res .= $res . dechex($dt);
            // Verifico che non sia presente nel DB
            $q = $this->db->prepare('select id from #__account_user where username = :username');
            $q->bindValue(':username', $res);
            $q->execute();
            $r = $q->fetch();
            $res = empty($r) ? $res : '';
        }
        return $res;
    }

    protected function isValidUsername($username)
    {
        $res = true;
        if ((mb_strlen($username) < 6) || (mb_strlen($username) > 150)) {
            $res = false;
        } else {
            $letters = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'), ['.', '-', '_']);
            $ul = str_split($username);
            foreach ($ul as $c) {
                if (!in_array($c, $letters)) {
                    $res = false;
                    break;
                }
            }
        }
        return $res;
    }

    protected function getUserGroups($idUser)
    {
        $res = [];
        $q = $this->db->prepare('select id_group from #__account_user_group where id_user = :id_user');
        $q->bindValue(':id_user', $idUser);
        $q->execute();
        $list = $q->fetchAll();
        if (!empty($list)) {
            foreach ($list as $r) {
                $res[] = (int) $r['id_group'];
            }
        }
        return $res;
    }

    protected function getDefGroupID()
    {
        $q = $this->db->prepare('select id, name, def from #__account_group where status = 1 and def = 1');
        $q->execute();
        $r = $q->fetch();
        return empty($r) ? 0 : (int) $r['id'];
    }

    /*
      0 Bloccato - Non autentica nulla
      1 Solo Email
      2 Utente
      3 Utente o Mail
     */

    public function getAuthType()
    {
        return $this->db->setting->getInt('account', 'auth.type', 0);
    }

    public function getAuthLogin()
    {
        return $this->db->setting->getBool('account', 'auth.login', false);
    }

    public function getAuthForgot()
    {
        return $this->db->setting->getBool('account', 'auth.forgot', false);
    }

    public function getAuthRegister()
    {
        return $this->db->setting->getBool('account', 'auth.register', false);
    }

    // Deve essere pubblico perché viene usato esternamente da registrazioni custom per validare i dati
    public function prepareRegister(&$data, &$error)
    {
        $res = true;
        $registerMinutes = $this->db->setting->getInt('account', 'register.minutes', 1);
        // Quando uso l'autenticazione per EMail genero un nome utente sempre!!
        /* if ($this->getAuthType() == 1)
        {
            $data['reg-username'] = $this->createUniqueUsername();
        } */
        $log = new LogModel($this->container);
        if ((mb_strlen(ArrayHelper::getStr($data, 'vat')) < 11)) {
            $error = $this->lang->trans('account.access.register.error.vat');
            $res = false;
        } else if ((mb_strlen(ArrayHelper::getStr($data, 'company')) < 2) || (mb_strlen(ArrayHelper::getStr($data, 'company')) > 255)) {
            $error = $this->lang->trans('account.access.register.error.company');
            $res = false;
        } else if (ArrayHelper::getInt($data, 'city1', 0) < 1) {
            $error = $this->lang->trans('account.access.register.error.city');
            $res = false;
        } else if (ArrayHelper::getStr($data, 'address1', '') == '') {
            $error = $this->lang->trans('account.access.register.error.address');
            $res = false;
        } else if (filter_var(ArrayHelper::getStr($data, 'email'), FILTER_VALIDATE_EMAIL) === false) {
            $error = $this->lang->trans('account.access.register.error.email');
            $res = false;
        } else if (!$this->checkUserExists(ArrayHelper::getStr($data, 'email'), ArrayHelper::getStr($data, 'email'))) {
            $error = $this->lang->trans('account.access.register.error.userexists');
            $res = false;
        } else if ((mb_strlen(ArrayHelper::getStr($data, 'note')) < 2) || (mb_strlen(ArrayHelper::getStr($data, 'note')) > 255)) {
            $error = $this->lang->trans('account.access.register.error.note');
            $res = false;
        } else if (ArrayHelper::getBool($data, 'reg-consent', false) != true) {
            $error = $this->lang->trans('account.access.register.error.consent');
            $res = false;
        }
        /*
        else if (filter_var(ArrayHelper::getStr($data, 'pec'), FILTER_VALIDATE_EMAIL) === false) {
            $error = $this->lang->trans('account.access.register.error.pec');
            $res = false;
        }
        */

        if ($res) {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            // Rileggo i dati del prepare se modificati
            $data = $ev->getData();
        }
        return $res;
    }

    public function register(&$data, &$error)
    {
        $this->lastRegisterID = null;
        if (!$this->prepareRegister($data, $error)) {
            return false;
        } else {
            $q = $this->db->prepare('select max(id) as last from #__account_user');
            $q->execute();
            $r = $q->fetch();
            $id = $r['last'] + 1;
            $values['username'] = 'cavesuser' . $id;
            if (isset($data['email']) && $data['email'] != '') {
                $values['email'] = $data['email'];
            } else {
                $values['email'] = 'cavesuser' . $id . '@lescaves.it';
            }
            $values['name'] = $data['company'];
            $values['status'] = 1; // In attivazione
            $pass = new \EOS\System\Security\Password(null);
            $values['password'] = $pass->hashPassword(substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10));
            $values['password_date'] = $this->db->util->nowToDBDateTime();
            $values['id_lang'] = $this->lang->getCurrentID();
            $values['reference'] = "0";
            $options = [
                'budget' => '0',
                'account_type' => '0',
                'cashback' => '0',
                'chain' => '0',
                'advisor' => '2'
            ];
            $values['options'] = json_encode($options);
            $values['up_id'] = $data['idagent'];
            $values['up_date'] = $this->db->util->nowToDBDateTime();
            $values['ins_id'] = $data['idagent'];
            $values['ins_date'] = $this->db->util->nowToDBDateTime();
            $tblContent = $this->db->tableFix('#__account_user');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
            $id = $this->db->lastInsertId();
            $this->lastRegisterID = $id;
            // Devo inserire sempre il gruppo di default per gli utenti creati
            $tblUG = $this->db->tableFix('#__account_user_group');
            $vug['id_user'] = $id;
            $vug['id_group'] = $this->getDefGroupID();
            $vug['ins_id'] = 0;
            $vug['ins_date'] = $this->db->util->nowToDBDateTime();
            $vug['up_id'] = 0;
            $vug['up_date'] = $this->db->util->nowToDBDateTime();
            $qug = $this->db->newFluent()->insertInto($tblUG, $vug);
            $qug->execute();
            $data['id_customer'] = $id;
            $log = new LogModel($this->container);
            if ($this->saveAddress($data, $error) != true) {
                return false;
            }
            return true;
        }
    }

    public function saveAddress(&$data, &$error)
    {
        //$this->db->beginTransaction();
        //print_r($data); exit();
        $options = [
            'id_agent' => $data['idagent'],
            'payment' => $data['payment'],
            'pec' => $data['pec'],
            'unicode' => ($data['unicode'] != '') ? $data['unicode'] : '0000000',
            'note' => $data['note'],
            'coddes' => '9999',
            'cscommonvalue' => $data['cscommonvalue'],
            'cscreditlimit' => $data['cscreditlimit'],
            'cscommondescription' => $data['cscommondescription'],
            'csid' => $data['csid']
        ];
        $values = [
            'id_customer' => $data['id_customer'],
            'alias' => $data['company'],
            'name' => $data['name'],
            'surname' => $data['surname'],
            'address1' => $data['address1'],
            'address2' => '',
            'id_city' => $data['city1'],
            'phone' => $data['phone'],
            'phone_mobile' => $data['phone'],
            'vat' => $data['vat'],
            'company' => $data['company'],
            'id_country' => 1,
            'cif' => $data['cif'],
            'reference' => '0',
            'options' => json_encode($options),
            //'status' => (isset($data['status']) ? $data['status'] == 'on' : false),
            'status' => 1,
            'up_id' => $data['idagent'],
            'up_date' => $this->db->util->nowToDBDateTime()
        ];
        $tblContent = $this->db->tableFix('#__address');

        $values['ins_id'] = $data['idagent'];
        $values['ins_date'] = $this->db->util->nowToDBDateTime();
        $query = $this->db->newFluent()->insertInto($tblContent, $values);
        $query->execute();

        if ($data['address2'] != "" && isset($data['city2']) && $data['city2'] != null) {
            $options = [
                'id_agent' => $data['idagent'],
                'payment' => $data['payment'],
                'pec' => $data['pec'],
                'unicode' => $data['unicode'],
                'note' => $data['note'],
                'coddes' => '01'
            ];
            $values = [
                'id_customer' => $data['id_customer'],
                'alias' => $data['company'],
                'name' => $data['name'],
                'surname' => $data['surname'],
                'address1' => $data['address2'],
                'address2' => '',
                'id_city' => (isset($data['city2']) && $data['city2'] != null) ? $data['city2'] : 0,
                'phone' => $data['phone'],
                'phone_mobile' => $data['phone'],
                'vat' => $data['vat'],
                'company' => $data['company'],
                'id_country' => 1,
                'cif' => $data['cif'],
                'reference' => '0',
                'options' => json_encode($options),
                //'status' => (isset($data['status']) ? $data['status'] == 'on' : false),
                'status' => 1,
                'up_id' => $data['idagent'],
                'up_date' => $this->db->util->nowToDBDateTime()
            ];
            $tblContent = $this->db->tableFix('#__address');
            $values['ins_id'] = $data['idagent'];
            $values['ins_date'] = $this->db->util->nowToDBDateTime();
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
        }
        //$this->db->commit();
        return true;
    }

    public function getCity($id)
    {
        $sql = 'select lc.id, lc.name, lc.zip_code, lc.id_state, ls.name as state, ls.iso_code as statecode,
                ls.id_region, lr.name as region, lr.id_country, lcn.name as country, lcn.iso_code as countrycode,
                lc.latitude, lc.longitude
                from #__localization_city lc
                left join #__localization_state ls on ls.id = lc.id_state 
                left join #__localization_region lr on lr.id = ls.id_region
                left join #__localization_country lcn on lcn.id = lr.id_country
                where lc.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', (int) $id);
        $q->execute();
        return $q->fetch();
    }

    public function updateCSInfo($idaddress, $csid, $cscommonvalue, $cscreditlimit, $cscommondescription)
    {
        $q = $this->db->prepare('select options from #__address where id = ' . $idaddress);
        $q->execute();
        $r = $q->fetch();
        if ($r != null) {
            $this->db->beginTransaction();
            $tblContent = $this->db->tableFix('#__address');
            $options = json_decode($r['options'], true);
            $options['csid'] = $csid;
            $options['cscommonvalue'] = $cscommonvalue;
            $options['cscreditlimit'] = $cscreditlimit;
            $options['cscommondescription'] = $cscommondescription;
            $values['options'] = json_encode($options);
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', (int) $idaddress);
            $query->execute();
            $this->db->commit();
        }
    }

    public function searchCity($partialName, callable $formatFunc = null)
    {
        // Città più corta ha 2 cifre
        if (mb_strlen($partialName) < 2) {
            $res = [];
        } else {
            $sql = 'select lc.id, lc.name, lc.zip_code, ls.iso_code as statecode from #__localization_city lc 
                left join #__localization_state ls on ls.id = lc.id_state 
                where lc.name like :name
                order by name';
            $q = $this->db->prepare($sql);
            $q->bindValue(':name', $this->db->util->prepareLikeValue($partialName) . '%');
            $q->execute();
            $res = $this->formatResult($q, $formatFunc);
        }
        return $res;
    }

    public function getFuncFormatCityStateName()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'name' => sprintf('%s (%s) %s', $row['name'], $row['statecode'], $row['zip_code'])];
        };
    }

    public function getFuncFormatCityStateText()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'text' => sprintf('%s (%s) %s', $row['name'], $row['statecode'], $row['zip_code'])];
        };
    }

    protected function formatResult($q, callable $formatFunc = null)
    {
        $res = [];
        if (is_null($formatFunc)) {
            $res = $q->fetchAll();
        } else {
            while ($r = $q->fetch()) {
                $res[] = $formatFunc($r);
            }
        }
        return $res;
    }

    public function getMethodPayment()
    {
        $q = $this->db->prepare('select id, PADESCRI as name from #__ext_pag_amen where status = 1');
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function checkVatNumber($vatnumber)
    {
        $q = $this->db->prepare('select id as name from #__address where vat like "' . $vatnumber . '"');
        $q->execute();
        $r = $q->fetch();
        if ($r != null) {
            $res = true;
        } else {
            $res = false;
        }
        return $res;
    }

    public function saveInviation(&$data, &$error)
    {
        /* ALTER TABLE `eos_ext_invitation` ADD `id_address` BIGINT NOT NULL DEFAULT '0' AFTER `id_user`; */
        $this->lastRegisterID = null;
        if (!$this->prepareInvitationRegister($data, $error)) {
            return false;
        } else {
            $values['email'] = $data['email'];
            $values['id_user'] = $data['iduser'];
            $values['id_address'] = $data['idaddress'];
            $values['name'] = $data['name'];
            $values['ins_date'] = $this->db->util->nowToDBDateTime();
            $tblContent = $this->db->tableFix('#__ext_invitation');
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            $query->execute();
            $data['idinvitation'] = $this->db->lastInsertId();
            return true;
        }
    }

    public function prepareInvitationRegister(&$data, &$error)
    {
        $res = true;
        $registerMinutes = $this->db->setting->getInt('account', 'register.minutes', 1);
        // Quando uso l'autenticazione per EMail genero un nome utente sempre!!
        /* if ($this->getAuthType() == 1)
        {
            $data['reg-username'] = $this->createUniqueUsername();
        } */
        $log = new LogModel($this->container);
        if (filter_var(ArrayHelper::getStr($data, 'email'), FILTER_VALIDATE_EMAIL) === false) {
            $error = $this->lang->trans('account.access.register.error.email');
            $res = false;
        } else if (!$this->checkInvitationExists(ArrayHelper::getStr($data, 'email'), ArrayHelper::getStr($data, 'iduser'))) {
            $error = $this->lang->trans('account.access.register.error.userexists');
            $res = false;
        }

        if ($res) {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Account\Classes\Events::ACCESS_REGISTER_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            // Rileggo i dati del prepare se modificati
            $data = $ev->getData();
        }
        return $res;
    }

    protected function checkInvitationExists($email, $user)
    {
        $q = $this->db->prepare('select id from #__account_user where email = :email');
        $q->bindValue(':email', $email);
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            $q2 = $this->db->prepare('select id from #__ext_invitation where id_user = :user and email = :email and ins_date BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()');
            $q2->bindValue(':email', $email);
            $q2->bindValue(':user', $user);
            $q2->execute();
            $r = $q2->fetch();
        }
        return empty($r);
    }

    public function checkInvitationValid($id)
    {
        $q = $this->db->prepare('select id from #__ext_invitation where id = :id and ins_date BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()');
        $q->bindValue(':id', $id);
        $q->execute();
        $r = $q->fetch();
        if (empty($r)) {
            return false;
        }
        return true;
    }
}
