<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Account\Models;

use EOS\System\Util\DateTimeHelper;
use EOS\System\Util\ArrayHelper;

class UserModel extends \EOS\System\Models\Model
{

    public function getAuthType()
    {
        return $this->db->setting->getInt('account', 'auth.type', 0);
    }

    public function getInfoUser($id)
    {
        $sql = 'select au.username, au.email, au.options, au.last_access, au.name, password_date from #__account_user au
                where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $r = $q->fetch();
        return $r;
    }

    public function getUserList(callable $formatFunc = null, $id, $type)
    {
        $sql = '';
        if ($type == 1) {
            $sql .= 'select distinct au.id, au.name, a.vat from #__account_user au
                left outer join #__address a on a.id_customer = au.id ';
        } elseif ($type == 2) {
            $sql .= 'select distinct au.id, concat(a.alias, \' - \', au.name) as name, a.reference from #__account_user au
                inner join #__address a on a.id_customer = au.id and a.status = 1 ';
        } else {
            $sql .= 'select distinct au.id, au.name, a.reference from #__account_user au
                left outer join #__address a on a.id_customer = au.id and a.status = 1 ';
        }

        if ($id != 8363) {
            $sql .= ' where au.status = 1 and (JSON_CONTAINS(a.options, \'\"' . $id . '\"\', \'$.id_agent\') or JSON_CONTAINS(a.options, \'\"' . $id . '\"\', \'$.id_agent\') or au.id = ' . $id . ') 
                order by name ';
        } else {
            $sql .= ' where au.status = 1
                order by name ';
        }

        $q = $this->db->prepare($sql);
        $q->execute();
        return $this->formatResult($q, $formatFunc);
    }

    public function getCustomerInfo($id, $status = false)
    {
        if ($status) {
            $sql = 'select distinct au.id as idcustomer, au.name as customer, au.email_alternative, au.options as customeroptions, au.email, a.* from #__account_user au
                left outer join #__address a on a.id_customer = au.id 
                where au.id = ' . $id . ' and status = 1';
        } else {
            $sql = 'select distinct au.id as idcustomer, au.name as customer, au.email_alternative, au.options as customeroptions, au.email, a.* from #__account_user au
                left outer join #__address a on a.id_customer = au.id 
                where au.id = ' . $id;
        }
        $query = $this->db->prepare($sql);
        $query->execute();
        $rc = $query->fetch();
        if (empty($rc)) {
            $res = [];
        } else {
            $res['id'] = $rc['idcustomer'];
            $res['idaddress'] = $rc['id'];
            $res['name'] = $rc['customer'];
            $res['vat'] = $rc['vat'];
            $res['cif'] = $rc['cif'];
            $res['email-alternative'] = $rc['email_alternative'];
            $res['email'] = $rc['email'];
            $res['customeroptions'] = $rc['customeroptions'];
            if (!empty($rc['options'])) {
                $ops = json_decode($rc['options'], true);
                $res['id-agent'] = $ops['id_agent'];
                $res['id-agent'] = $ops['id_agent'];
                $res['cscommondescription'] = isset($ops['cscommondescription']) ? $ops['cscommondescription'] : '';
                $res['cscreditlimit'] = isset($ops['cscreditlimit']) ? $ops['cscreditlimit'] : '';
                $res['cscommonvalue'] = isset($ops['cscommonvalue']) ? $ops['cscommonvalue'] : '';
            }
            $options = json_decode($rc['options'], true);
            $qp = $this->db->newFluent()->from($this->db->tableFix('#__ext_pag_amen epa'))
                ->where('epa.id = ?', (int) $options['payment']);
            $rp = $qp->fetch();
            $res['payment'] = $rp['PADESCRI'];
            $res['id-payment'] = $rp['id'];
            $res['payment-discount'] = $rp['PASCONTO'];
            $tblAccountUser = $this->db->tableFix('#__account_user_group aug');
            $query = $this->db->newFluent()->from($tblAccountUser)
                ->select('cr.name as discount')
                ->select('cr.id')
                ->select('cr.reduction')
                ->select('cr.reduction_sum2')
                ->select('cr.reduction_sum3')
                ->innerJoin($this->db->tableFix('#__catalog_rules cr ON cr.id_group = aug.id_group'))
                ->where('aug.id_user', $rc['id_customer'])
                ->where('cr.id < ?', 40);
            $query->execute();
            $rca = $query->fetchAll();
            $i = 0;
            foreach ($rca as $ra) {
                $res['discount-' . $i . '-id'] = $ra['id'];
                $res['discount-' . $i . '-name'] = $ra['discount'];
                $res['discount-' . $i . '-reduction1'] = $ra['reduction'];
                $res['discount-' . $i . '-reduction2'] = $ra['reduction_sum2'];
                $res['discount-' . $i . '-reduction3'] = $ra['reduction_sum3'];
                $i++;
            }
            $res['discount-count'] = $i;
        }
        return $res;
    }

    public function getFuncFormatAddressText()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'text' => sprintf('%s (%s)', $row['name'], $row['id'])];
        };
    }

    public function getFuncFormatAddressName()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'name' => sprintf('%s (%s)', $row['name'], $row['reference'])];
        };
    }

    public function getFuncFormatAddressVAT()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'name' => sprintf('%s (%s)', $row['name'], $row['vat'])];
        };
    }

    public function getFuncFormatAddressOffices()
    {
        return function ($row) {
            return ['id' => (int) $row['id'], 'name' => sprintf('%s (%s)', $row['name'], $row['id'])];
        };
    }

    protected function formatResult($q, callable $formatFunc = null)
    {
        $res = [];
        if (is_null($formatFunc)) {
            $res = $q->fetchAll();
        } else {
            while ($r = $q->fetch()) {
                $res[] = $formatFunc($r);
            }
        }
        return $res;
    }

    public function userInfo($v)
    {
        $m = new \EOS\Components\Product\Models\CatalogModel($this->container);
        $v->budget = 0;
        $v->cashback = 0;
        $v->changepassword = 0;
        $v->viewagent = false;
        $v->userlabel = 'Account non autenticato';
        if (isset($v->controller->user)) {
            $v->user = $v->controller->user->getInfo();
            $v->userlabel = $v->user->username;
            $v->accountinfo = $this->getInfoUser($v->user->id);
            $v->lastaccess = ArrayHelper::getStr($v->accountinfo, 'last_access', 'first');
            $v->passworddate = date($v->accountinfo['password_date']);
            if ($v->passworddate > date('Y-m-d H:m:s', strtotime("-1 days"))) {
                $v->changepassword = 1;
            }
            $uio = json_decode($v->accountinfo['options'], true);
            if (in_array(2, $v->user->groups)) {
                $v->budget = ArrayHelper::getFloat($uio, 'budget', 0);
                if (isset($uio['fee'])) {
                    $v->fee = ArrayHelper::getFloat($uio, 'fee', 0);
                } else {
                    $v->fee = 0;
                }
                $v->viewagent = true;
            }
            if (!in_array(2, $v->user->groups)) {
                if (isset($uio['cashback'])) {
                    $v->cashback = ArrayHelper::getFloat($uio, 'cashback', 0);
                } else {
                    $v->cashback = 0;
                }
                if (isset($uio['advisor'])) {
                    $v->advisor = ArrayHelper::getFloat($uio, 'advisor', 2);
                } else {
                    $v->advisor = 2;
                }
            }
            $v->shop = 1;
        } else {
            $v->shop = 0;
        }
    }

    public function setAdvisor($id, $value)
    {
        $sql = 'select options ' .
            ' from #__account_user au ' .
            ' where au.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', (int)$id);
        $q->execute();
        $res = $q->fetch();
        $tbl = $this->db->tableFix('#__account_user');
        $options = json_decode($res['options'], true);
        $options['advisor'] = $value;
        $values['options'] = json_encode($options);
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', $id);
        $query->execute();
    }

    public function getInvitation($id, $status = 0)
    {
        $sql = 'select ei.*, au.id as idcustomer from #__ext_invitation ei
                left outer join #__account_user au on au.email = ei.email
                where ei.id_user = ' . $id . ' 
                order by ei.id';
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function getAdvisor($id)
    {
        $sql = 'select distinct au.* from #__account_user as au ';
        $sql .= ' left outer join #__address as a on a.id_customer = au.id ';
        if ($id != 8363) {
            $sql .= ' where au.status = 1 and (JSON_CONTAINS(a.options, \'\"' . $id . '\"\', \'$.id_agent\') or JSON_CONTAINS(a.options, \'\"' . $id . '\"\', \'$.id_agent\')) and JSON_EXTRACT(au.options, \'$.advisor\') IS NOT NULL  order by name ';
        } else {
            $sql .= ' where au.status = 1 and JSON_EXTRACT(au.options, \'$.advisor\') IS NOT NULL order by name ';
        }
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function changePassword($data, &$error)
    {
        $password = ArrayHelper::getStr($data, 'pwdold');
        $logStatus = 0;
        $idUser = null;
        if ((mb_strlen(ArrayHelper::getStr($data, 'pwd1')) < 8) || (mb_strlen(ArrayHelper::getStr($data, 'pwd1')) > 100)) {
            $error = $this->lang->trans('account.access.register.error.pwd1');
            $res = false;
        } else if (ArrayHelper::getStr($data, 'pwd1') !== ArrayHelper::getStr($data, 'pwd2')) {
            $error = $this->lang->trans('account.access.register.error.pwd2');
            $res = false;
        } else {
            $sql = 'select id, password from #__account_user where id = :id';
            $q = $this->db->prepare($sql);
            $q->bindValue(':id', (int)$data['id']);
            $q->execute();
            $r = $q->fetch();
            if (!empty($r)) {
                $user = new \EOS\Components\Account\Classes\AuthUser($this->container);
                if ($user->signin($r['password'], $password)) {
                    $tbl = $this->db->tableFix('#__account_user');
                    $pass = new \EOS\System\Security\Password(null);
                    $values['password'] = $pass->hashPassword($data['pwd1']);
                    $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', (int)$data['id']);
                    $query->execute();
                    $res = true;
                } else {
                    $error = 'Password attuale errata';
                    $res = false;
                }
            }
        }
        return $res;
    }
}
