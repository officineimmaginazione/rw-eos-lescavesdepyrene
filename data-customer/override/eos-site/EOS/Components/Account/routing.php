<?php

$this->mapComponent(['GET'], 'Account', ['access/login' => 'AccessController:login']);
$this->mapComponent(['POST'], 'Account', ['access/ajaxlogin' => 'AccessController:ajaxLogin']);
$this->mapComponent(['GET'], 'Account', ['access/forgot' => 'AccessController:forgot']);
$this->mapComponent(['POST'], 'Account', ['access/ajaxforgot' => 'AccessController:ajaxForgot']);
$this->mapComponent(['GET'], 'Account', ['access/register' => 'AccessController:register']);
$this->mapComponent(['POST'], 'Account', ['access/ajaxregister' => 'AccessController:ajaxRegister']);
$this->mapComponent(['GET'], 'Account', ['access/active' => 'AccessController:active']);
$this->mapComponent(['GET'], 'Account', ['access/reset-pass' => 'AccessController:resetPass']);
$this->mapComponent(['GET'], 'Account', ['access/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'AccessController:getDataSource']);
$this->mapComponent(['POST'], 'Account', ['access/ajaxrequest' => 'AccessController:ajaxRequest']);

$this->mapComponent(['GET'], 'Account', ['service/request-active/{token}' => 'ServiceController:requestActive']);
$this->mapComponent(['GET'], 'Account', ['service/active/{token}' => 'ServiceController:active']);
$this->mapComponent(['GET'], 'Account', ['service/request-reset-pass/{token}' => 'ServiceController:requestResetPass']);
$this->mapComponent(['GET'], 'Account', ['service/reset-pass/{token}' => 'ServiceController:resetPass']);

$this->mapComponent(['GET'], 'Account', ['user/index/' => 'UserController:index']);
$this->mapComponent(['GET'], 'Account', ['user/logout' => 'UserController:logout']);
$this->mapComponent(['GET'], 'Account', ['user/edit' => 'UserController:edit']);
$this->mapComponent(['POST'], 'Account', ['user/ajaxedit' => 'UserController:ajaxEdit']);
$this->mapComponent(['POST'], 'Account', ['user/ajaxcommand' => 'UserController:ajaxCommand']);
$this->mapComponent(['GET'], 'Account', ['user/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'UserController:getDataSource']);
$this->mapComponent(['GET'], 'Account', ['user/change-pass' => 'UserController:changePass']);
$this->mapComponent(['POST'], 'Account', ['user/ajaxpassword' => 'UserController:ajaxPassword']);

$this->mapComponent(['GET'], 'Account', ['list/invitation' => 'UserController:listInvitation']);
$this->mapComponent(['GET'], 'Account', ['list/advisor' => 'UserController:listAdvisor']);

$this->mapComponent(['POST'], 'Account', ['address/ajaxcommand' => 'AddressController:ajaxCommand']);
$this->mapComponent(['GET'], 'Account', ['address/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'AddressController:getDataSource']);
$this->mapComponent(['GET'], 'Account', ['address/list/' => 'AddressController:list']);
$this->mapComponent(['GET'], 'Account', ['address/insert/' => 'AddressController:insert']);
$this->mapComponent(['POST'], 'Account', ['address/ajaxregister' => 'AddressController:ajaxRegister']);

$this->mapComponent(['GET'], 'Account', ['registration/customer' => 'RegistrationController:register']);
$this->mapComponent(['POST'], 'Account', ['registration/ajaxregister' => 'RegistrationController:ajaxRegister']);
$this->mapComponent(['POST'], 'Account', ['registration/ajaxcommand' => 'RegistrationController:ajaxCommand']);
$this->mapComponent(['GET'], 'Account', ['registration/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'RegistrationController:getDataSource']);
$this->mapComponent(['GET'], 'Account', ['registration/invitation' => 'RegistrationController:invitation']);
$this->mapComponent(['POST'], 'Account', ['registration/ajaxinvitation' => 'RegistrationController:ajaxInvitation']);
$this->mapComponent(['GET'], 'Account', ['registrationchain/customer' => 'RegistrationController:registerChain']);
$this->mapComponent(['POST'], 'Account', ['registrationchain/ajaxregister' => 'RegistrationController:ajaxRegisterChain']);
$this->mapComponent(['POST'], 'Account', ['registrationchain/ajaxcommand' => 'RegistrationController:ajaxCommand']);
