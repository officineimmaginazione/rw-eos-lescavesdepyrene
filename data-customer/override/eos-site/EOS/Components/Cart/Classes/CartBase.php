<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Classes;

use Curl\ArrayUtil;
use EOS\System\Util\ArrayHelper;

class CartBase extends \EOS\System\Models\Model
{

    public $id = 0;
    public $rows = [];

    public function __construct($container)
    {
        parent::__construct($container);
        $this->session = $container->get('session');
        $this->open(false);
    }

    private function open($autocreate = true)
    {
        $this->checkDataCart();
        if ($this->session->has('cart.id')) {
            $this->id = (int) $this->session->get('cart.id');
            $cart = $this->getCart();
            if (isset($cart['status']) && $cart['status'] == 2) {
                $this->db->beginTransaction();
                $tbl = $this->db->tableFix('#__cart');
                $values = [
                    'status' => 0,
                    'total' => 0,
                    'ins_date' => $this->db->util->nowToDBDateTime()
                ];
                $query = $this->db->newFluent()->insertInto($tbl, $values);
                $query->execute();
                $this->id = $this->db->lastInsertId();
                $this->session->set('cart.id', $this->id);
                $this->session->set('cart.time', time());
                $this->db->commit();
            }
        } else {
            $this->db->beginTransaction();
            $tbl = $this->db->tableFix('#__cart');
            $values = [
                'status' => 0,
                'total' => 0,
                'ins_date' => $this->db->util->nowToDBDateTime()
            ];
            $query = $this->db->newFluent()->insertInto($tbl, $values);
            $query->execute();
            $this->id = $this->db->lastInsertId();
            $this->session->set('cart.id', $this->id);
            $this->session->set('cart.time', time());
            $this->db->commit();
        }
        if (empty($this->id)) {
            throw new Exception('Invalid ID Cart');
        } else {
            //carica riga
        }
    }

    /*** GET ***/

    public function getCart()
    {
        $sql = 'select c.* ' .
            ' from #__cart c' .
            ' where c.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $this->id);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getCartByID($id)
    {
        $sql = 'select c.* ' .
            ' from #__cart c' .
            ' where c.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getCartDetailsByID($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__cart c'))
            ->select(null)
            ->select('c.id')
            ->select('c.status')
            ->select('c.total')
            ->select('c.id_customer')
            ->select('c.id_address_delivery')
            ->select('c.id_address_invoice')
            ->select('c.id_agent')
            ->select('c.ins_date')
            ->select('c.options')
            ->where('c.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null) {
            $res = [];
        } else {
            $res['id'] = $rc['id'];
            $res['status'] = $rc['status'];
            $res['amount'] = $rc['total'];
            $res['ins_date'] = $rc['ins_date'];
            $options = json_decode($rc['options'], true);
            $discount = $options['discount'];
            $res['head_note'] = $options['head_note'];
            $res['head_discount1'] = $options['head_discount1'];
            $res['head_discount2'] = $options['head_discount2'];
            $res['head_discount3'] = $options['head_discount3'];
            $res['head_payment'] = $options['head_payment'];
            $res['cashback'] = (isset($options['cashback'])) ? $options['cashback'] : 0;
            $res['shipping_payment'] = $options['shipping']['total'];
            $res['head_payment_discount'] = $options['head_payment_discount'];
            if (isset($discount['shipping1'])) {
                $res['discount-shipping1'] = ArrayHelper::getFloat($discount, 'shipping1', 0);
            }
            if (isset($discount['shipping2'])) {
                $res['discount-shipping2'] = ArrayHelper::getFloat($discount, 'shipping2', 0);
            }
            $query2 = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
                ->where('a.id = ?', (int) $rc['id_address_delivery']);
            $rc2 = $query2->fetch();
            if (!empty($rc2)) {
                $optionsaddress = json_decode($rc2['options'], true);
                $res['name'] = $rc2['name'];
                $res['surname'] = $rc2['surname'];
                $res['phone'] = $rc2['phone'];
                $res['address'] = $rc2['address1'];
                $res['company'] = $rc2['company'];
                $res['vat'] = $rc2['vat'];
                $res['cif'] = $rc2['cif'];
                $res['note'] = $optionsaddress['note'];
                $ml = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                $current_city = $ml->getCity(ArrayHelper::getInt($rc2, 'id_city'));
                $res['city'] = (isset($current_city['name']) && $current_city['name'] != '')  ? $current_city['name'] : '';
                $res['state'] = (isset($current_city['statecode']) && $current_city['statecode'] != '') ? $current_city['statecode'] : '';
                $res['country'] = (isset($current_city['country']) && $current_city['name'] != '') ? $current_city['country'] : '';
            } else {
                $res['name'] = '';
                $res['surname'] = '';
                $res['phone'] = '';
                $res['email'] = '';
                $res['address'] = '';
                $res['city'] = '';
                $res['state'] = '';
                $res['country'] = '';
                $res['company'] = '';
                $res['vat'] = '';
                $res['cif'] = '';
                $res['note'] = '';
            }
            $query3 = $this->db->newFluent()->from($this->db->tableFix('#__account_user au'))
                ->select('au.name')
                ->where('au.id = ?', (int) $rc['id_agent']);
            $rc3 = $query3->fetch();
            if (!empty($rc3)) {
                $res['agent-name'] = $rc3['name'];
            }
            $query4 = $this->db->newFluent()->from($this->db->tableFix('#__account_user au'))
                ->select('au.name')
                ->where('au.id = ?', (int) $rc['id_customer']);
            $rc4 = $query4->fetch();
            if (!empty($rc4)) {
                $res['email'] = $rc4['email_alternative'];
            }
        }

        return $res;
    }

    public function getCartByIDStatus($id, $status, $maxitem = 500)
    {
        // Se non esiste creare la view
        /* CREATE VIEW eos_view_cart_widget AS
            SELECT c.id, a.alias, a.company, a.name, a.surname, lc.name as city, c.status, c.id_customer, c.id_address_delivery, c.id_agent, sum(cr.quantity) as quantity, GROUP_CONCAT(pl.name) as products, c.ins_date, c.total 
            FROM eos_cart as c 
            left outer join eos_address a on c.id_address_delivery = a.id 
            left outer join eos_localization_city lc on lc.id = a.id_city 
            left outer join eos_cart_row as cr on c.id = cr.id_cart 
            left outer join eos_product as p on p.id = cr.id_product 
            left outer join eos_product_lang as pl on p.id = pl.id_product 
            where (c.id_customer <> 0 || c.id_address_delivery is not null)
            GROUP BY c.id, a.alias, c.ins_date, c.total, c.status, c.id_address_delivery ORDER BY `c`.`id` DESC;    */
        if ($id != 0) {
            $sql = 'select * ' .
                ' from #__view_cart_widget' .
                ' where (id_address_delivery in (select asub.id from #__address asub where JSON_CONTAINS(options, \'"' . $id . '"\', \'$.id_agent\') or asub.id_customer = ' . $id . ') or id_agent = ' . $id . ') ' .
                ' and status in (' . $status . ') ' .
                ' order by id desc limit ' . $maxitem;
        } else {
            $sql = 'select * ' .
                ' from #__view_cart_widget' .
                ' where status in (' . $status . ') and id != 0' .
                ' order by id desc limit ' . $maxitem;
        }
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    private function getDBRow($id)
    {
        $sql = 'select cr.* ' .
            ' from #__cart_row cr ' .
            ' where cr.id_cart = :id_cart and cr.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $this->id);
        $q->bindValue(':id', $id);
        $q->execute();
        return $q->fetch();
    }

    // Uso in reservation per ottenere le righe senza usare gli eventi
    public function getDBRows($idCart = 0)
    {
        $sql = 'select cr.* ' .
            ' from #__cart_row cr ' .
            ' where cr.id_cart = :id_cart ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', (empty($idCart)) ? $this->id : $idCart);
        $q->execute();
        return $q->fetchAll();
    }

    public function getRows(int $typecustomer = 0)
    {
        if ($typecustomer == 0) {
            $sql = 'select cr.id, cr.id_product, pl.name, pl.description, ml.name as manufacturer_name, cr.quantity, cr.options, p.price, p.type_condition, t.rate ' .
                ' from #__cart_row cr ' .
                ' left outer join #__product p on cr.id_product = p.id ' .
                ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
                ' left outer join #__tax t on t.id = p.id_tax ' .
                ' left outer join #__manufacturer_lang ml on p.id_manufacturer = ml.id_manufacturer ' .
                ' where cr.id_cart = :id_cart ';
        } else {
            $sql = 'select cr.id, cr.id_product, pl.name, pl.description, ml.name as manufacturer_name, cr.quantity, cr.options, IF(cru.price, cru.price, (json_unquote(JSON_EXTRACT(cr.options, "$.data.cashback")))) as price, p.type_condition, t.rate ' .
                ' from #__cart_row cr ' .
                ' left outer join #__product p on cr.id_product = p.id ' .
                ' left outer join #__catalog_rules cru on p.id = cru.id_product and cru.id_group = 42 ' .
                ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
                ' left outer join #__tax t on t.id = p.id_tax ' .
                ' left outer join #__manufacturer_lang ml on p.id_manufacturer = ml.id_manufacturer ' .
                ' where cr.id_cart = :id_cart ';
        }

        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $this->id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        //$res = $q->fetchAll();
        $r = $q->fetchAll();
        $i = 0;
        //print_r($r); exit();
        $res = [];
        foreach ($r as $item) {
            $sql = 'select pal.name' .
                ' from #__product_product_attribute_combination ppac ' .
                ' inner join #__product_attribute pa on pa.id = ppac.id_attribute ' .
                ' inner join #__product_attribute_lang pal on pal.id_attribute = pa.id' .
                ' where ppac.id_product = :id_product and pa.id_attribute_group  = 1';
            $q = $this->db->prepare($sql);
            $q->bindValue(':id_product', $item['id_product']);
            $q->execute();
            //$res = $q->fetchAll();
            $attribute = $q->fetch();
            $res[$i]['vintage'] = $attribute['name'];
            $res[$i]['id'] = $item['id'];
            $res[$i]['id_product'] = $item['id_product'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['description'] = $item['description'];
            $res[$i]['manufacturer'] = $item['manufacturer_name'];
            $res[$i]['quantity'] = $item['quantity'];
            $res[$i]['price'] = $item['price'];
            $res[$i]['rate'] = $item['rate'];
            $res[$i]['type_condition'] = $item['type_condition'];
            $res[$i]['options'] = $item['options'];
            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($item['id_product']);
            foreach ($listimage as $single) {
                $img = $image->getObject($item['id_product'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
                if (!empty($img)) {
                    $res[$i]['url'] = $img->url;
                }
            }
            $i++;
        }
        $ev = new \EOS\System\Event\DataEvent($this, $res);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_DATA_PARSE_ROWS, $ev);
        return $ev->getData();
    }

    public function getRowsByCart($id, $viewagent = false)
    {
        $sql = 'select cr.id, cr.id_product, pl.name, pl.description, cr.quantity, cr.options, p.price as priceb2b, cru.price as priceb2c, pi.url, p.reference, t.rate, c.id_customer ' .
            ' from #__cart_row cr ' .
            ' left outer join #__cart c on c.id = cr.id_cart ' .
            ' left outer join #__product p on cr.id_product = p.id ' .
            ' left outer join #__catalog_rules cru on p.id = cru.id_product and cru.id_group = 42 ' .
            ' left outer join #__product_image pi on cr.id_product = pi.id_product and pi.pos = 0' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
            ' left outer join #__account_user_group aug on cr.id_customer = aug.id_user ' .
            ' left outer join #__tax t on p.id_tax = t.id' .
            ' where cr.id_cart = :id_cart ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $r = $q->fetchAll();
        $i = 0;

        $res = [];

        if (!empty($r)) {
            $su = 'select id_group ' .
                ' from #__account_user_group as aug ' .
                ' where aug.id_user = :id_user ';
            $qu = $this->db->prepare($su);
            $qu->bindValue(':id_user', $r[0]['id_customer']);
            $qu->execute();
            $ru = $qu->fetch();
        }

        foreach ($r as $item) {
            $res[$i]['id'] = $item['id'];
            $res[$i]['id_product'] = $item['id_product'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['description'] = $item['description'];
            $res[$i]['quantity'] = $item['quantity'];
            $res[$i]['idgroup'] = $ru['id_group'];
            if ($ru['id_group'] == 42) {
                $res[$i]['price'] = $item['priceb2c'];
            } else {
                $res[$i]['price'] = $item['priceb2b'];
            }
            $res[$i]['rate'] = $item['rate'];
            $res[$i]['reference'] = $item['reference'];
            $res[$i]['options'] = $item['options'];
            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($item['id_product']);
            $tblAttribute = $this->db->tableFix('#__product_product_attribute_combination ppac');
            $query = $this->db->newFluent()->from($tblAttribute)->select('pal.name as attribute')->select('pagl.name as attributegroup')
                ->select('pagl.id as id')
                ->select('pal.id_attribute as id_attribute')
                ->innerJoin($this->db->tableFix('#__product_attribute pa ON pa.id = ppac.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pa.id = pal.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group pag ON pa.id_attribute_group = pag.id'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pag.id = pagl.id_attribute_group'))
                ->where('id_product', $item['id']);
            $rca = $query->fetchAll();

            foreach ($rca as $ra) {
                $res[$i]['attribute-name-' . $ra['id']] = $ra['attribute'];
                $res[$i]['attribute-id-attribute-' . $ra['id']] = $ra['id_attribute'];
                $res[$i]['attribute-group-' . $ra['id']] = $ra['attributegroup'];
            }
            /* foreach ($listimage as $single)
              {
              $img = $image->getObject($item['id_product'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
              $res[$i]['url'] = $img->url;
              } */
            $i++;
        }
        $ev = new \EOS\System\Event\DataEvent($this, $res);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_DATA_PARSE_ROWS, $ev);
        return $ev->getData();
    }

    public function getRowQuantity($id_row)
    {
        $sql = 'select cr.quantity' .
            ' from #__cart_row cr ' .
            ' where cr.id = :id_row ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_row', $id_row);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    private function getDBRowByProduct($id)
    {
        $sql = 'select cr.* ' .
            ' from #__cart_row cr ' .
            ' where cr.id_cart = :id_cart and cr.id_product = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $this->id);
        $q->bindValue(':id', $id);
        $q->execute();
        return $q->fetch();
    }

    public function getProductByID($id)
    {
        $sql = 'select p.price as price, pl.name as name ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' where p.id = :id and pl.id_lang = :id_lang  order by pl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getProduct()
    {
        $sql = 'select p.id, p.price, pl.name, pl.description, ppc.id_category ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' inner join #__product_product_category ppc on p.id = ppc.id_product' .
            ' where pl.id_lang = :id_lang order by p.id ASC';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    public function getProductData($id)
    {
        $sql = 'select p.type_condition as type_condition, p.price as price, pl.name as name ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' where p.id = :id and pl.id_lang = :id_lang  order by pl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getProductPrice($id)
    {
        $sql = 'select p.price as price, pl.name as name ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' where p.id = :id and pl.id_lang = :id_lang  order by pl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getPayments($status = null, $statusb2b = null, $statusb2c = null)
    {
        $sql = 'select * ' .
            ' from #__ext_pag_amen pa ';
        /* if ($status != null) {
            $sql .= ' where status = ' . $status;
        }
        if ($statusb2b != null) {
            $sql .= ' where status_b2b = ' . $statusb2b;
        }
        if ($statusb2c != null) {
            $sql .= ' where status_b2c = ' . $statusb2c;
        } */
        $sql .=  ' order by pa.PADESCRI ASC ';
        $q = $this->db->prepare($sql);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    /*** SAVE ***/

    public function saveData(&$data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__cart');

        $values['up_date'] = $this->db->util->nowToDBDateTime();
        $values['id'] = $data['cartid'];
        $values['status'] = $data['status'];
        $dataop = [
            'head_note' => $data['head-note']
        ];
        $options = json_encode($dataop);

        if ((!isset($data['cartid'])) || ($data['cartid'] == 0)) {
            $values['options'] = $options;
            $values['total'] = number_format($data['total'], 4, '.', '');
            //$values['status'] = $data['status'];
            $query = $this->db->newFluent()->insertInto($tblContent, $values);
            //$this->addCustomRow($data['productid'], $values['total'], "", $data);
        } else {
            $values['options'] = $options;
            $values['total'] = number_format($data['total'], 4, '.', '');
            //$values['status'] = $data['status'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['cartid']);
        }
        //$values['total'] = 0;
        $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['cartid']);
        $query->execute();
        $this->db->commit();

        return true;
    }

    public function saveDataRow($idproduct, $quantity, $options = [], $idrow = null, $datarow = [], $add = false, $optionsduplicated = [], $restore = false, $viewagent = null)
    {
        if (isset($optionsduplicated) && $restore == true) {
            $checkoptions = json_decode($optionsduplicated, true);
            if (isset($checkoptions['data']['isfree']) && $checkoptions['data']['isfree'] == true) {
                return true;
            }
        }
        if (!isset($idrow) && $idrow != null) {
            $data = $this->getDBRow($idrow);
        }
        if (isset($idproduct) && $idproduct != null && $add == true) {
            $data = $this->getDBRowByProduct($idproduct);
            if (!empty($data)) {
                $idrow = $data['id'];
                $quantity = $data['quantity'] + $quantity;
                $dataoptions = ArrayHelper::fromJSON($data['options']);
            }
        }
        if (isset($idproduct) && $idproduct != null && $restore == true) {
            $data = $this->getDBRowByProduct($idproduct);
            if (!empty($data)) {
                $idrow = $data['id'];
                $quantity = $data['quantity'] + $quantity;
                $dataoptions = ArrayHelper::fromJSON($data['options']);
            }
        }
        if (!empty($optionsduplicated)) {
            $data['options'] = $optionsduplicated;
        } elseif (!empty($options)) {
            $datadata = [
                'additional-discount' => $options[0],
                'issample' => $options[1],
            ];
            $dataop = [
                "type" => "product",
                "data" => $datadata
            ];
            $options = json_encode($dataop);
            $data['options'] = $options;
        } elseif (!empty($dataoptions)) {
            if (isset($datarow['issample']) && $datarow['issample'] == 1) {
                $dataoptions['data']['issample'] = 1;
            }
            $data['options'] = ArrayHelper::toJSON($dataoptions);
        } elseif (!empty($datarow)) {
            $issample = '';
            if (isset($datarow['issample'])) {
                $issample = $datarow['issample'];
            }
            $dataprodfromdb = $this->getProductData($datarow['id_product']);
            $adddiscount = 0;
            if ($dataprodfromdb['type_condition'] == 1) {
                $adddiscount = 10;
            }
            if ($viewagent != null && $viewagent == 1) {
                $datadata = [
                    'additional-discount' => $adddiscount,
                    'issample' => $issample,
                ];
                $dataop = [
                    "type" => "product",
                    "data" => $datadata
                ];
            } else {
                $datadata = [
                    'additional-discount' => $adddiscount,
                    'issample' => $issample,
                    "shipping-qty" => [1, 0]
                ];
                $dataop = [
                    "type" => "product",
                    "data" => $datadata
                ];
            }

            $options = json_encode($dataop);
            $data['options'] = $options;
        } else {
            $adddiscount = 0;
            $datadata = [
                'additional-discount' => $adddiscount,
                'issample' => '',
            ];
            $dataop = [
                "type" => "product",
                "data" => $datadata
            ];
            $options = json_encode($dataop);
            $data['options'] = $options;
        }

        $data['quantity'] = abs($quantity);
        $tbl = $this->db->tableFix('#__cart_row');
        $this->open(true);
        $this->db->beginTransaction();
        if (isset($idrow) && $idrow != null) {
            $query = $this->db->newFluent()->update($tbl)->set($data)->where('id', (int) $idrow);
            $query->execute();
            $this->db->commit();
            $this->open(false);
            $this->updateCartTotal($datarow);
            $this->updateCartDate($this->id);
            return true;
        } else {
            $data['id_cart'] = $this->id;
            $data['id_product'] = $idproduct;
            $query = $this->db->newFluent()->insertInto($tbl, $data);
            $query->execute();
            $rowID = $this->db->lastInsertId();
            $this->db->commit();
            $this->open(false);
            $this->updateCartTotal($datarow);
            $this->updateCartDate($this->id);
            return $rowID;
        }
    }

    /*** UPDATE ***/

    private function updateCartDate(int $idCart)
    {
        $tbl = $this->db->tableFix('#__cart');
        $values = [
            'up_date' => $this->db->util->nowToDBDateTime()
        ];
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', (int) $idCart);
        $query->execute();
    }

    public function updateCartTotal(&$data)
    {
        // 2020-15-04 Fix che arriva vuoto 
        if (!empty($data)) {
            $this->db->beginTransaction();
            $tblContent = $this->db->tableFix('#__cart');
            $values['up_date'] = $this->db->util->nowToDBDateTime();
            $values['id'] = $data['cartid'];
            $values['status'] = 1;
            $values['total'] = $data['total'];
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['cartid']);
            $query->execute();
            $this->db->commit();
        }
    }

    public function updateCart($id, $idcustomer, $idagent, $headparameters = [], $idaddress)
    {
        // Recupero le options precedenti
        $cartDB = $this->getCartByID($id);
        if (empty($cartDB)) {
            throw new \Exception('Invalid cart id!');
        }
        $dataop = \EOS\System\Util\ArrayHelper::fromJSON($cartDB['options']);

        $data['id_customer'] = $idcustomer;
        $data['id_address_delivery'] = $idaddress;
        $data['id_address_invoice'] = $idaddress;
        $data['id_agent'] = $idagent;
        $dataop['head_discount1'] = (isset($headparameters[0]) && $headparameters[0] != 0) ? $headparameters[0] : 0;
        $dataop['head_discount2'] = (isset($headparameters[1]) && $headparameters[1] != 0) ? $headparameters[1] : 0;
        $dataop['head_discount3'] = (isset($headparameters[2]) && $headparameters[2] != 0) ? $headparameters[2] : 0;
        $dataop['head_note'] = (isset($headparameters[3]) && $headparameters[3] != '') ? $headparameters[3] : '';
        $dataop['head_payment'] = '';
        if (isset($headparameters[4])) {
            $dataop['head_payment'] = $headparameters[4];
        }
        $dataop['head_payment_discount'] = '';
        if (isset($headparameters[5])) {
            $dataop['head_payment_discount'] = $headparameters[5];
        }
        if (isset($headparameters[6])) {
            $dataop['order_resid'] = $headparameters[6];
        }
        $data['options'] = \EOS\System\Util\ArrayHelper::toJSON($dataop);
        $tbl = $this->db->tableFix('#__cart');
        $query = $this->db->newFluent()->update($tbl)->set($data)->where('id', (int) $id);
        $query->execute();
        return true;
    }

    public function updateCartShipping(int $id, array $shipping): bool
    {
        // Recupero le options precedenti
        $cartDB = $this->getCartByID($id);
        if (empty($cartDB)) {
            throw new \Exception('Invalid cart id!');
        }
        $dateLang = ArrayHelper::getStr($shipping, 'date', '');
        $date = '';
        if (!empty($dateLang)) {
            $date = $this->lang->langDateToDbDate($dateLang);
        }
        $dataop = \EOS\System\Util\ArrayHelper::fromJSON($cartDB['options']);
        $dataop['shipping']['date'] = $date;
        $dataop['shipping']['type'] = ArrayHelper::getInt($shipping, 'type', 0);
        $dataop['shipping']['method'] = ArrayHelper::getInt($shipping, 'method', 0);

        $data['options'] = \EOS\System\Util\ArrayHelper::toJSON($dataop);
        $tbl = $this->db->tableFix('#__cart');
        $query = $this->db->newFluent()->update($tbl)->set($data)->where('id', (int) $id);
        $query->execute();

        return true;
    }

    public function updateOptions(&$data)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__cart');
        $values['options'] = $data['options'];
        $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $data['id']);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function updateHeadDiscount($headdiscount)
    {
        $this->db->beginTransaction();
        $rows = $this->getDBRows($this->id);
        $tblContent = $this->db->tableFix('#__cart_row');
        foreach ($rows as $row) {
            $options = ArrayHelper::fromJSON($row['options']);
            $options['data']['additional-discount'] = $headdiscount;
            $values['options'] = ArrayHelper::toJSON($options);
            $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $row['id']);
            $query->execute();
        }
        $this->db->commit();
        return true;
    }

    public function updateCartCashback($cashback)
    {
        $this->db->beginTransaction();
        $cart = $this->getCart($this->id);
        $tbl = $this->db->tableFix('#__cart');
        $options = ArrayHelper::fromJSON($cart['options']);
        $options['cashback'] = $cashback;
        $values['options'] = ArrayHelper::toJSON($options);
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', $this->id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function updateCustomer($customer)
    {
        $this->db->beginTransaction();
        $cart = $this->getCart($this->id);
        $tbl = $this->db->tableFix('#__cart');
        $values['id_customer'] = $customer;
        $query = $this->db->newFluent()->update($tbl)->set($values)->where('id', $this->id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    /* DELETE */

    public function deleteById($id)
    {
        $this->db->beginTransaction();
        $tblCart = $this->db->tableFix('#__cart');
        $values['status'] = 4;
        $query = $this->db->newFluent()->update($tblCart)->set($values)->where('id', $id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function deleteRowById($id)
    {
        $this->db->beginTransaction();
        $tblCartProduct = $this->db->tableFix('#__cart_row');
        $query = $this->db->newFluent()->deleteFrom($tblCartProduct)->where('id', (int) $id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    public function deleteAllRowById()
    {
        $this->db->beginTransaction();
        $tblCartProduct = $this->db->tableFix('#__cart_row');
        $query = $this->db->newFluent()->deleteFrom($tblCartProduct)->where('id_cart', (int) $this->id);
        $query->execute();
        $this->db->commit();
        return true;
    }

    /* CUSTOM */

    public function clearCart()
    {
        $this->id = 0;
        $this->rows = [''];
        $this->session->set('cart.id', null);
        $this->session->set('cart.time', 0);
    }

    public function restoreCart($id)
    {
        $cartrow = $this->getRowsByCart($id);
        foreach ($cartrow as $item) {
            $options = json_decode($item['options'], true);
            unset($options["data"]["free-row-discount"]);
            $item['options'] = json_encode($options);
            $this->saveDataRow($item['id_product'], $item['quantity'], [], null, [], false, $item['options'], true);
        }
        return true;
    }

    public function checkProductInCart($idproduct)
    {
        $sql = 'select ps.quantity_available ' .
            ' from #__product_stock ps ' .
            ' where ps.id = :id_product ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_product', $idproduct);
        $q->execute();
        $res = $q->fetch();
    }

    public function cartToOrder($id)
    {
        $this->db->beginTransaction();
        $tblContent = $this->db->tableFix('#__cart');
        $values['status'] = 2;
        $query = $this->db->newFluent()->update($tblContent)->set($values)->where('id', $id);
        $query->execute();
        $resList = $this->splitCart($id);
        foreach ($resList as $idres) {
            $this->decCartStock($idres);
        }
        $this->db->commit();
        return true;
    }

    public function checkDataCart()
    {
        if ($this->session->has('cart.id') && $this->session->has('cart.time')) {
            $cartlifetime = 120 * 60;
            if (($this->session->get('cart.time') + $cartlifetime) < time()) {
                $this->clearCart();
            }
        }
    }

    public function checkRow(int $id_product, int $id_cart): array
    {
        $sql = 'select cr.id ' .
            ' from #__cart_row cr' .
            ' where cr.id_cart = :idcart and cr.id_product = :idproduct';
        $q = $this->db->prepare($sql);
        $q->bindValue(':idcart', $id_cart);
        $q->bindValue(':idproduct', $id_product);
        $q->execute();
        $list = $q->fetchAll();
        $ev = new \EOS\System\Event\DataEvent($this, $list);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_DATA_CHECK_ROW, $ev);
        $checkedList = $ev->getData();
        $res = empty($checkedList) ? [] : array_values($checkedList)[0];
        return $res;
    }

    protected function storeTotal(object $v)
    {
        // Scrivo le informazioni su DB
        $inTrans = $this->db->inTransaction();
        if (!$inTrans) {
            $this->db->beginTransaction();
        }
        $options = ArrayHelper::fromJSON($v->head['options']);
        $options['shipping']['count'] = $v->shippingCount;
        $options['shipping']['total'] = $v->shipping;
        $options['shipping']['prices'] = $v->shippingPrices;
        $options['discount']['shipping1'] = -$v->discounttotal1;
        $options['discount']['shipping2'] = -$v->discounttotal2;
        $options['discount']['shipping1perc'] = -$v->discountpercshipping1;
        $options['discount']['shipping2perc'] = -$v->discountpercshipping2;

        //Campionatura - Split Carrello
        $options['allissample'] = $v->allissample;

        $cartID = $v->head['id'];
        $sql = 'update #__cart set
              options = :options,
              total = :total,
              up_date = :up_date
              where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', $cartID);
        $q->bindValue('total', $v->total);
        $q->bindValue('options', ArrayHelper::toJSON($options));
        $q->bindValue('up_date', $this->db->util->nowToDBDateTime());
        $q->execute();

        $sqlR = 'update #__cart_row set
              options = :options
              where id = :id and id_cart = :id_cart';
        $qR = $this->db->prepare($sqlR);

        foreach ($v->rows as &$row) {
            $options = ArrayHelper::fromJSON($row['options']);
            $stockStatus = $row['stock-status'];
            $sQty1 = $stockStatus['quantity1'];
            $sQty2 = $stockStatus['quantity2'] + $stockStatus['quantity3'];
            $options['data']['shipping-qty'] = $v->shippingCount > 1 ? [$sQty1, $sQty2] : [$sQty1 + $sQty2, 0];

            $qR->bindValue('id', $row['id']);
            $qR->bindValue('id_cart', $cartID);
            $qR->bindValue('options', ArrayHelper::toJSON($options));
            $qR->execute();
        }
        unset($row);

        if (!$inTrans) {
            $this->db->commit();
        }
    }

    public function prepareObjectAndSave(object $v, bool $calcShipping = true, bool $allissample = false)
    {
        // Leggo i dati dal DB
        $v->head = $this->getCartByID($this->id);
        $customertype = 1;
        // Riprendo i dati/gruppi dell'utente per capirne il tipo
        $usql = 'select id_group from #__account_user_group
                where id_user = :id and id_group in (1, 2, 42)';
        $uq = $this->db->prepare($usql);
        $uq->bindValue(':id', $v->head['id_customer']);
        $uq->execute();
        $ur = $uq->fetchAll();
        $group = $ur[0];
        if (!isset($group)) {
            $customertype = 0; //agente
            $v->rows = $this->getRows(0);
        } else {
            if (in_array(2, $group)) {
                $customertype = 1; //agente
                $v->rows = $this->getRows(0);
            } elseif (in_array(1, $group)) {
                $customertype = 2; //b2b
                $v->rows = $this->getRows(0);
            } else {
                $customertype = 3; //b2c
                $v->rows = $this->getRows(1);
            }
        }

        $v->total = 0;
        //$v->subtotal = 0;
        //$v->subtotal1 = 0;
        //$v->subtotal2 = 0;
        $v->shipping = 0;
        $v->numberproducts = 0;
        $v->numprodshipping1 = 0;
        $v->numprodshipping2 = 0;
        $v->discountshipping1 = 0;
        $v->discountshipping2 = 0;
        $v->discountpercshipping1 = 0;
        $v->discountpercshipping2 = 0;
        $v->discounttotal = 0;
        $v->productstotal = 0;
        $v->prodtotalshipping1 = 0;
        $v->prodtotalshipping2 = 0;
        $v->productsissampletotal = 0;
        $v->orderresid = 0;
        $v->discountpayment = 0;
        $v->cartcashback = 0;
        $v->discountqty = 0;
        // Preparo l'oggetto ed eseguo i calcoli
        $options = ArrayHelper::fromJSON($v->head['options']);
        $shipping = ArrayHelper::getArray($options, 'shipping');
        $discount1 = ArrayHelper::getFloat($options, 'head_discount1');
        $discount2 = ArrayHelper::getFloat($options, 'head_discount2');
        $discount3 = ArrayHelper::getFloat($options, 'head_discount3');
        $paymentdiscount = ArrayHelper::getFloat($options, 'head_payment_discount');
        $v->cartcashback = ArrayHelper::getFloat($options, 'cashback');
        // Calcolo la data di spedizione
        $shippingDate = \EOS\System\Util\DateTimeHelper::date();
        $shippingDateStr = ArrayHelper::getMdStr($options, ['shipping', 'date'], '');
        if (!empty($shippingDateStr)) {
            $shippingDateCalc = $this->db->util->dbDateToDateTime($shippingDateStr);
            if ($shippingDateCalc >= $shippingDate) {
                $shippingDate = $shippingDateCalc;
            }
        }
        $isUnder5Days = $shippingDate <= \EOS\System\Util\DateTimeHelper::incDay(\EOS\System\Util\DateTimeHelper::date(), 5);

        //Campionatura - Split Carrello
        $v->allissample = $allissample;

        $v->subTotal = 0;
        $v->subTotal1 = 0;
        $v->subTotal2 = 0;
        $v->lockConfirm = false;
        $v->showShppingExpress = true;
        $tot1 = 0;
        $tot2 = 0;
        $c1 = 0;
        $c2 = 0;
        $fTotDisc1 = 0;
        $fTotDisc2 = 0;
        $taxtotalproduct = 0;
        $taxtotalproduct1 = 0;
        $taxtotalproduct2 = 0;
        foreach ($v->rows as &$row) {
            $options = ArrayHelper::fromJSON($row['options']);
            $data = ArrayHelper::getArray($options, 'data');
            $discount = ArrayHelper::getFloat($data, 'additional-discount', 0);
            $taxrow = 0;
            $taxrow1 = 0;
            $taxrow2 = 0;
            $sampleQty = 0;
            $qtaship1 = 0;
            $qtaship2 = 0;
            if (ArrayHelper::getBool($data, 'issample', false) && $row['quantity'] > 0) {
                $sampleQty = 1;
                $v->productsissampletotal = $v->productsissampletotal + $row['price-prod'];
            }
            if ($row['type_condition'] == 1) {
                if ($discount < 10) {
                    $discount = 10;
                }
            }
            $row['discount'] = $discount;
            if (isset($options['data']['free-row-discount'][0]) && $options['data']['free-row-discount'][0] > 0) {
                $discount = $discount + (int) $options['data']['free-row-discount'][0];
            }
            $qtashipping = ArrayHelper::getArray($data, 'shipping-qty', []);
            if (isset($qtashipping[0])) {
                $qtaship1 = $qtashipping[0];
            } else {
                $qtaship1 = $row['quantity'];
            }
            if (isset($qtashipping[1])) {
                $qtaship2 = $qtashipping[1];
            } else {
                $qtaship2 = 0;
            }
            $row['price-disc'] = round(($row['price'] * ((1 - $discount1 / 100) * (1 - $discount2 / 100) * (1 - $discount3 / 100))) * (1 - $discount / 100), 4);
            $row['tot-price'] = round(($row['quantity'] - $sampleQty) * $row['price'], 4);
            $row['tot-price-disc'] = round(($row['quantity'] - $sampleQty) * $row['price-disc'], 4);
            $row['show-disc'] = $row['tot-price'] !== $row['tot-price-disc'];
            $taxrow = (($row['quantity'] - $sampleQty) * $row['price-disc']) * ($row['rate'] / 100);
            $taxtotalproduct += $taxrow;
            $v->numberproducts += $row['quantity'];
            $v->productstotal += ($row['quantity'] - $sampleQty) * $row['price-disc'];

            $stockStatus = $row['stock-status'];
            // Controllo le disponibilità
            if ($stockStatus['quantity1'] > 0) {
                $c1++;
            }
            if (($stockStatus['quantity2'] > 0) || ($stockStatus['quantity3'] > 0)) {
                $c2++;
            }
            $row['quantity-error'] = $row['quantity'] > $row['stock']['stock-available'];
            if ($row['quantity-error']) {
                $v->lockConfirm = true;
            }

            $v->subTotal += $row['tot-price-disc'];
            // Calcolo il totale scontato per quantità disponibile
            $totRow1 = round(($qtaship1 - $sampleQty) * $row['price-disc'], 2);
            $tot1 += $totRow1;
            // Se il campione è scalato dalla prima quantità lo azzero
            if ($stockStatus['quantity1'] > 0) {
                $sampleQty = 0;
            }
            $totRow2 = round(($qtaship2 - $sampleQty) * $row['price-disc'], 2);
            $tot2 += $totRow2;

            // Verifico i 5 giorni
            // 2019-06-13: Forzando gli stock di 5 giorni devo usare la differenza originale e compare lo stock originale
            if (
                $stockStatus['original-incoming-diff'] > 0 && $stockStatus['original-incoming-diff'] <= 5 &&
                ($stockStatus['quantity2'] > 0 || $stockStatus['quantity1'] > $row['stock']['stock-quantity']) &&
                $isUnder5Days
            ) {
                $v->showShppingExpress = false;
                $v->orderresid = 1;
            }

            // 2020-04-16 - Calcolo l'importo dello sconto relativo agli omaggi
            $fPercDisc1 = (float) ArrayHelper::getMdValue($data, ['free-extra-discount', 0]);
            $fPercDisc2 = (float) ArrayHelper::getMdValue($data, ['free-extra-discount', 1]);
            $fTotDisc1 += round($totRow1 * $fPercDisc1 / 100, 2);
            $fTotDisc2 += round($totRow2 * $fPercDisc2 / 100, 2);
            if ($fPercDisc1 > 0) {
                $v->discountpercshipping1 = $fPercDisc1;
            }
            if ($fPercDisc2 > 0) {
                $v->discountpercshipping2 = $fPercDisc2;
            }

            if ($qtaship1 > 0) {
                $v->prodtotalshipping1 += $qtaship1 * $row['price-disc'];
                $v->subTotal1 += round(($qtaship1) * $row['price-disc'] * (1 - ($fPercDisc1 / 100)), 4);
                $v->numprodshipping1 = $v->numprodshipping1 + $qtaship1;
                $taxrow1 = ($qtaship1 * $row['price-disc']) * ($row['rate'] / 100) * (1 - ($fPercDisc1 / 100));
                $taxtotalproduct1 += $taxrow1;
            }
            if ($qtaship2 > 0) {
                $v->prodtotalshipping2 += $qtaship2 * $row['price-disc'];
                $v->subTotal2 += round(($qtaship2) * $row['price-disc'] * (1 - ($fPercDisc2 / 100)), 4);
                $v->numprodshipping2 = $v->numprodshipping2 + $qtaship2 + $stockStatus['quantity3'];
                $taxrow2 = ($qtaship2 * $row['price-disc']) * ($row['rate'] / 100) * (1 - ($fPercDisc2 / 100));
                $taxtotalproduct2 += $taxrow2;
            }
        }
        unset($row);
        //print_r($c2);
        $splitShipping = ($c1 > 0 && $c2 > 0);
        $shippingType = $v->showShppingExpress ? ArrayHelper::getInt($shipping, 'type') : 0;
        if ($customertype == 3) {
            $shippingPrice = $shippingType === 1 ? $this->db->setting->getStr('lcdp', 'shipping.price.expressb2c', 9.5) : $this->db->setting->getStr('lcdp', 'shipping.price.standardb2c', 9.5);
            $shipPriceLimit = $this->db->setting->getStr('lcdp', 'shipping.freeportb2c', 90);
        } else {
            $shippingPrice = $shippingType === 1 ? $this->db->setting->getStr('lcdp', 'shipping.price.express', 35) : $this->db->setting->getStr('lcdp', 'shipping.price.standard', 15);
            $shipPriceLimit = $this->db->setting->getStr('lcdp', 'shipping.freeport', 250);
        }
        $v->shipping = 0;

        $v->shippingCount = 1;
        $v->shippingPrices = [0, 0];
        $taxshipping = 0;
        $v->taxPercent = (float) $this->db->setting->getStr('cart', 'tax', 22);
        if ($calcShipping) {
            if (ArrayHelper::getInt($shipping, 'method') == 0) {
                if (($v->subTotal1 * (1 - $paymentdiscount / 100)) < $shipPriceLimit && $shippingType === 0) {
                    $v->shippingPrices[0] = $shippingPrice;
                } elseif ($shippingType === 1) {
                    $v->shippingPrices[0] = $shippingPrice;
                }

                if ($splitShipping) {
                    if ($tot2 < $shipPriceLimit) {
                        //2019-04-03 Eliminata costo seconda spedizione
                        //2022-01-19 Riabilitata seconda spedizione
                        $v->shippingPrices[1] = $shippingPrice;
                        //$v->shippingPrices[1] = 0;
                    }
                    $v->shippingCount = 2;
                }
            } else {
                if ($v->subTotal < $shipPriceLimit) {
                    $v->shippingPrices[0] = $shippingPrice;
                }
            }
        }


        $v->discounttotal = $v->discounttotal * -1;
        $v->total = $v->subTotal + $v->shipping;
        $v->discounttotal = -1 * round($v->productstotal - ($v->subTotal * (1 - $paymentdiscount / 100)), 4);
        //

        $v->shipping = $v->shippingPrices[0] + $v->shippingPrices[1];
        $taxshipping1 = $v->shippingPrices[0] * ($v->taxPercent / 100);
        $taxshipping2 = $v->shippingPrices[1] * ($v->taxPercent / 100);
        $v->discounttotal1 = -$fTotDisc1;
        $v->discounttotal2 = -$fTotDisc2;
        $v->discountqty = $v->discounttotal1 + ($v->discounttotal2);
        $v->discounttotalpayment1 = -1 * round($v->prodtotalshipping1 - ($v->subTotal1 * (1 - $paymentdiscount / 100)), 4);
        $v->discounttotalpayment2 = -1 * round($v->prodtotalshipping2 - ($v->subTotal2 * (1 - $paymentdiscount / 100)), 4);
        $v->discountpayment1 = -1 * round($v->subTotal1 * ($paymentdiscount / 100), 4);
        $v->discountpayment2 = -1 * round($v->subTotal2 * ($paymentdiscount / 100), 4);
        $v->discountpayment = $v->discountpayment1 + ($v->discountpayment2);
        $v->discounttotal = $v->discounttotalpayment1 + ($v->discounttotalpayment2);
        //
        if ($customertype == 3) {
            $v->totalDisplay1 = round(($v->subTotal1 * (1 - $paymentdiscount / 100)), 4) + $v->shippingPrices[0];
            $v->totalDisplay2 = round(($v->subTotal2 * (1 - $paymentdiscount / 100)), 4) + $v->shippingPrices[1];
            $v->totalDisplay = $v->cartcashback + $v->totalDisplay1 + $v->totalDisplay2;
            $v->totalTax1 = 0;
            $v->totalTax2 = 0;
            $v->totalTax = $v->totalTax1 + $v->totalTax2;
            $v->totalWithTax1 = round($v->cartcashback + ($v->subTotal1 * (1 - $paymentdiscount / 100)) + $v->shippingPrices[0] + $v->totalTax1, 4);
            $v->totalWithTax2 = round(($v->subTotal2 * (1 - $paymentdiscount / 100)) + $v->shippingPrices[1] + $v->totalTax2, 4);
            $v->totalWithTax = $v->totalWithTax1 + $v->totalWithTax2;
        } else {
            $v->totalDisplay1 = round(($v->subTotal1 * (1 - $paymentdiscount / 100)), 4) + $v->shippingPrices[0];
            $v->totalDisplay2 = round(($v->subTotal2 * (1 - $paymentdiscount / 100)), 4) + $v->shippingPrices[1];
            $v->totalDisplay = $v->cartcashback + $v->totalDisplay1 + $v->totalDisplay2;
            //print_r($v->totalDisplay1);
            //exit();
            $v->totalTax1 = round((($taxtotalproduct1) * (1 - $paymentdiscount / 100)) + $taxshipping1, 4);
            $v->totalTax2 = round((($taxtotalproduct2) * (1 - $paymentdiscount / 100)) + $taxshipping2, 4);
            $v->totalTax = $v->totalTax1 + $v->totalTax2;
            $v->totalWithTax1 = round($v->cartcashback + ($v->subTotal1 * (1 - $paymentdiscount / 100)) + $v->shippingPrices[0] + $v->totalTax1, 4);
            $v->totalWithTax2 = round(($v->subTotal2 * (1 - $paymentdiscount / 100)) + $v->shippingPrices[1] + $v->totalTax2, 4);
            $v->totalWithTax = $v->totalWithTax1 + $v->totalWithTax2;
        }
        // Aggiorno i dati sul DB
        $this->storeTotal($v);
    }

    protected function splitCart(int $cartID1): array
    {
        $inTrans = $this->db->inTransaction();
        if (!$inTrans) {
            $this->db->beginTransaction();
        }
        $res = [$cartID1];
        $cartDB1 = $this->getCartByID($cartID1);
        if ((int) $cartDB1['status'] !== 2) {
            return $res;
        }
        $cartID3 = 0;
        // Devo estrapolare le righe degli campioni
        $rows1 = $this->getDBRows($cartID1);
        $sampleRows = [];
        $tblRow = $this->db->tableFix('#__cart_row');
        // Faccio un ciclo per verificare che non siano solo campioni ed evito lo split
        $isStandardCart = false;
        foreach ($rows1 as $r1) {
            $rOps = ArrayHelper::fromJSON($r1['options']);
            if ($r1['quantity'] != 1 || ArrayHelper::getMdBool($rOps, ['data', 'issample'], false) == false) {
                $isStandardCart = true;
                break;
            }
        }
        if ($isStandardCart || empty($rows1)) {
            foreach ($rows1 as $r1) {
                $rOps = ArrayHelper::fromJSON($r1['options']);
                if ($r1['quantity'] > 0 && ArrayHelper::getMdBool($rOps, ['data', 'issample'], false)) {
                    $rS = $r1; // Copia 
                    $rS['quantity'] = 1;
                    unset($rS['id']);
                    unset($rS['id_cart']);
                    $sampleRows[] = $rS;
                    // Devo riaggiornare le quantità sul database
                    // Se avevo quantità 1 cancello altrimenti aggiorno
                    if ($r1['quantity'] == 1) {
                        $query = $this->db->newFluent()
                            ->deleteFrom($tblRow)
                            ->where('id', (int) $r1['id'])
                            ->where('id_cart', $cartID1);
                        $query->execute();
                    } else {
                        $r1['quantity'] = $r1['quantity'] - 1;
                        $rOps['data']['issample'] = 0;
                        $r1['options'] = ArrayHelper::toJSON($rOps);

                        $query = $this->db->newFluent()
                            ->update($tblRow)->set($r1)
                            ->where('id', (int) $r1['id'])
                            ->where('id_cart', $cartID1);
                        $query->execute();
                    }
                }
            }
            if (!empty($sampleRows)) {
                $oldID = $this->id;
                $this->id = $cartID1;
                $this->prepareObjectAndSave(new \stdClass());
                $this->id = $oldID;
                $cartDB1 = $this->getCartByID($cartID1);
            }
        } else {
            // Questo è il caso che il carello contiene tutti campioni e forzo il ricalcolo in allSample  
            $cartID3 = $cartID1;
        }


        $option = ArrayHelper::fromJSON($cartDB1['options']);
        $shipping = ArrayHelper::getArray($option, 'shipping');
        $shippingCount = ArrayHelper::getInt($shipping, 'count');
        if ($shippingCount === 2) {
            $cartDB2 = $cartDB1; // Clona il cart1
            $option = ArrayHelper::fromJSON($cartDB2['options']);
            $option['cart-number'] = 2;
            $cartDB2['options'] = ArrayHelper::toJSON($option);
            unset($cartDB2['id']);
            $tbl = $this->db->tableFix('#__cart');
            $query = $this->db->newFluent()->insertInto($tbl, $cartDB2);
            $query->execute();
            $cartID2 = (int) $this->db->lastInsertId();
            $cartDB2['id'] = $cartID2;
            $res[] = $cartID2;
            $rows1 = $this->getDBRows($cartID1);
            $tblRow = $this->db->tableFix('#__cart_row');
            foreach ($rows1 as $r1) {
                $rOps = ArrayHelper::fromJSON($r1['options']);
                $shippingqty = ArrayHelper::getMdArray($rOps, ['data', 'shipping-qty']);
                $isFree = ArrayHelper::getMdBool($rOps, ['data', 'isfree'], false);
                if (!empty($shippingqty)) {
                    $qty1 = ArrayHelper::getFloat($shippingqty, 0);
                    $qty2 = ArrayHelper::getFloat($shippingqty, 1);
                    if ($qty1 <= 0) {
                        // Gli omaggi vanno vanno solo sulla prima spedizione e se a zero li lascio per indicare che non erano disponibilis
                        if (!$isFree) {
                            $query = $this->db->newFluent()
                                ->deleteFrom($tblRow)
                                ->where('id', (int) $r1['id'])
                                ->where('id_cart', $cartID1);
                            $query->execute();
                        }
                    } else {
                        $r1['quantity'] = $qty1;
                        $query = $this->db->newFluent()
                            ->update($tblRow)->set($r1)
                            ->where('id', (int) $r1['id'])
                            ->where('id_cart', $cartID1);
                        $query->execute();
                    }

                    if ($qty2 > 0) {
                        $r2 = $r1; // Clona la riga
                        unset($r2['id']);
                        $r2['id_cart'] = $cartID2;
                        $r2['quantity'] = $qty2;
                        $query = $this->db->newFluent()
                            ->insertInto($tblRow, $r2);
                        $query->execute();
                    }
                } else {
                    $isSample = ArrayHelper::getMdBool($rOps, ['data', 'issample'], false);
                    if (!empty($shippingqty) && $isSample) {
                        $this->reduceBudget($r1['id_product'], $cartDB1['id_agent']);
                    }
                }
            }
            // Devo ricalcolare i carrelli come totali e baro cambiando ID al cart
            $oldID = $this->id;
            $this->id = $cartID1;
            $this->prepareObjectAndSave(new \stdClass());
            $this->id = $cartID2;
            $this->prepareObjectAndSave(new \stdClass(), false);
            $this->id = $oldID;
        }

        //Campionatura - Split Carrello
        $tblRow = $this->db->tableFix('#__cart_row');
        foreach ($sampleRows as $r3) {
            if ($cartID3 == 0) {
                $cartDB3 = $cartDB1; // Clona il cart1
                $option = ArrayHelper::fromJSON($cartDB3['options']);
                $option['cart-number'] = 3;
                $cartDB3['options'] = ArrayHelper::toJSON($option);
                unset($cartDB3['id']);
                $tbl = $this->db->tableFix('#__cart');
                $query = $this->db->newFluent()->insertInto($tbl, $cartDB3);
                $query->execute();
                $cartID3 = (int) $this->db->lastInsertId();
                $cartDB3['id'] = $cartID3;
                $res[] = $cartID3;
            }
            unset($r3['id']);
            $r3['id_cart'] = $cartID3;
            $query = $this->db->newFluent()
                ->insertInto($tblRow, $r3);
            $query->execute();
            $this->reduceBudget($r3['id_product'], $cartDB1['id_agent']);
        }
        if ($cartID3 != 0) {
            $oldID = $this->id;
            $this->id = $cartID3;
            $this->prepareObjectAndSave(new \stdClass(), false, true);
            $this->id = $oldID;
        }
        if (!$inTrans) {
            $this->db->commit();
        }
        return $res;
    }

    protected function decCartStock(int $cartID)
    {
        $inTrans = $this->db->inTransaction();
        if (!$inTrans) {
            $this->db->beginTransaction();
        }
        $stkMng = new \EOS\Components\LCDPComp\Classes\StockManagerExt($this->container);
        $rows = $this->getDBRows($cartID);
        foreach ($rows as $r) {
            $stkMng->decStock($r['id_product'], 0, $r['quantity']);
        }

        if (!$inTrans) {
            $this->db->commit();
        }
    }

    protected function reduceBudget($idproduct, $idagent)
    {
        $sql = 'select options from #__account_user
           where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', $idagent);
        $q->execute();
        $r = $q->fetch();
        $sql = 'select price from #__product
           where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', $idproduct);
        $q->execute();
        $rp = $q->fetch();
        if (isset($rp) && isset($r)) {
            $options = json_decode($r['options'], true);
            $options['budget'] = $options['budget'] - $rp['price'];
            $values['options'] = json_encode($options);
            $tbl = $this->db->tableFix('#__account_user');
            $query = $this->db->newFluent()
                ->update($tbl)->set($values)
                ->where('id', (int) $idagent);
            $query->execute();
        }
    }

    // Funzione che calcola la data per generare le spedizioni legata agli stock
    public function getShippingStockDate(): ?\DateTimeImmutable
    {
        // 2019-06-13 - Parametro per evitare di calcolare la data di spedizione
        if ($this->db->setting->getBool('lcdp', 'shipping.stock.date.oldmode', false)) {
            return null;
        }
        $shippingDate = (new \EOS\Components\LCDPComp\Classes\StockManagerExt($this->container))->getStockDate();
        $cartDB = $this->getCartByID($this->id);
        $cartOp = \EOS\System\Util\ArrayHelper::fromJSON($cartDB['options']);
        $shippingDateStr = ArrayHelper::getMdStr($cartOp, ['shipping', 'date'], '');
        if (!empty($shippingDateStr)) {
            $shippingDateCalc = $this->db->util->dbDateToDateTime($shippingDateStr);
            if ($shippingDateCalc >= $shippingDate) {
                $shippingDate = $shippingDateCalc;
            }
        }
        // Sposto gli stock di 5 giorni se necessario
        $expressDate = \EOS\System\Util\DateTimeHelper::incDay(\EOS\System\Util\DateTimeHelper::date(), 5);
        if ($shippingDate < $expressDate) {
            $shippingDate = $expressDate;
        }
        return $shippingDate;
    }

    public function setAccountInCart($idaddress, $idagent)
    {
        $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
        $data_add = $ma->getData($idaddress);
        $datadiscount = $ma->getDataDiscount($idaddress);
        if (!empty($data_add)) {
            $data['id_customer'] = $data_add['id_customer'];
            $data['id_address_delivery'] = 0;
            $data['id_address_invoice'] = 0;
            $data['id_agent'] = $idagent;
            $dataop['head_discount1'] = isset($datadiscount['discount-0-reduction1']) ? $datadiscount['discount-0-reduction1'] : 0;
            $dataop['head_discount2'] = isset($datadiscount['discount-0-reduction2']) ? $datadiscount['discount-0-reduction2'] : 0;
            $dataop['head_discount3'] = isset($datadiscount['discount-0-reduction3']) ? $datadiscount['discount-0-reduction3'] : 0;
            $dataop['head_note'] = '';
            $dataop['head_payment'] = $datadiscount['id-payment'];
            $dataop['head_payment_discount'] = $datadiscount['payment-discount'];
            $data['options'] = \EOS\System\Util\ArrayHelper::toJSON($dataop);
            $tbl = $this->db->tableFix('#__cart');
            $query = $this->db->newFluent()->update($tbl)->set($data)->where('id', (int) $this->id);
            $query->execute();
        }
    }

    /* Cashback Manage */
    public function cashbackManage($data)
    {
        $idagent = 0;
        $b2b = 0;
        $idcustomer0 = $idcustomer1 = $idcustomer2 = $idcustomer3 = $idcustomer4 = $idcustomer5 = 0;
        $idcustomer0 = $this->getChain((int)$data['idcustomer']);
        $level = 0;
        if ($idcustomer0 != 0) {
            $idcustomer1 = $this->getChain($idcustomer0);
            if ($idcustomer1 != 0) {
                $level = 1;
                $idcustomer2 = $this->getChain($idcustomer1);
                if ($idcustomer2 != 0) {
                    $level = 2;
                    $idcustomer3 = $this->getChain($idcustomer2);
                    if ($idcustomer3 != 0) {
                        $level = 3;
                        $idcustomer4 = $this->getChain($idcustomer3);
                        if ($idcustomer4 != 0) {
                            $level = 4;
                            $idcustomer5 = $this->getChain($idcustomer4);
                            if ($idcustomer5 != 0) {
                                $level = 5;
                            } else {
                                $idagent = $this->getAgent($idcustomer4);
                            }
                        } else {
                            $idagent = $this->getAgent($idcustomer3);
                        }
                    } else {
                        $idagent = $this->getAgent($idcustomer2);
                    }
                } else {
                    $idagent = $this->getAgent($idcustomer1);
                }
            } else {
                $idagent = $this->getAgent($idcustomer0);
            }
        } else {
            $idagent = $this->getAgent($data['idcustomer']);
        }
        $chashbackb2c1 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c1', 5) / 100;
        $chashbackb2c2 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c2', 2.5) / 100;
        $chashbackb2c3 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c3', 1.25) / 100;
        $chashbackb2c4 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c4', 0.6) / 100;
        $chashbackb2c5 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c5', 0.3) / 100;
        $chashbackb2b1 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b1', 1) / 100;
        $chashbackb2b2 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b2', 2) / 100;
        $chashbackb2b3 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b3', 4) / 100;
        $chashbackb2b4 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b4', 8) / 100;
        $chashbackb2b5 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b5', 10) / 100;
        $chashbackb2b6 = $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b6', 16) / 100;
        $feeage1 = $this->db->setting->getStr('lcdp', 'cashback.feeage1', 0.5) / 100;
        $feeage2 = $this->db->setting->getStr('lcdp', 'cashback.feeage2', 1) / 100;
        $feeage3 = $this->db->setting->getStr('lcdp', 'cashback.feeage3', 2) / 100;
        $feeage4 = $this->db->setting->getStr('lcdp', 'cashback.feeage4', 4) / 100;
        $feeage5 = $this->db->setting->getStr('lcdp', 'cashback.feeage5', 6) / 100;
        $feeage6 = $this->db->setting->getStr('lcdp', 'cashback.feeage6', 8) / 100;
        $totalnotax = $data['totalTaxable'] / ((100 + (float) $this->db->setting->getStr('cart', 'tax', 22)) / 100);
        $totalusable = $data['totalTaxable'];
        $total = $data['total'];
        $status = $data['status'];
        $chain = [];
        if ($level == 5) {
            $this->historyCashback($data['cartid'], $idcustomer0, round(($totalusable * $chashbackb2c1), 2), 1, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer1, round(($totalusable * $chashbackb2c2), 2), 2, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer2, round(($totalusable * $chashbackb2c3), 2), 3, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer3, round(($totalusable * $chashbackb2c4), 2), 4, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer4, round(($totalusable * $chashbackb2c5), 2), 5, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer5, round(($totalnotax * $chashbackb2b1), 2), 6, $totalnotax, $status);
            $this->historyCashback($data['cartid'], $idagent, round(($totalnotax * $feeage1), 2), 6, $totalnotax, $status);
            if ($status != 0) {
                $this->updateCashback($idcustomer0, round(($totalusable * $chashbackb2c1), 2));
                $this->updateCashback($idcustomer1, round(($totalusable * $chashbackb2c2), 2));
                $this->updateCashback($idcustomer2, round(($totalusable * $chashbackb2c3), 2));
                $this->updateCashback($idcustomer3, round(($totalusable * $chashbackb2c4), 2));
                $this->updateCashback($idcustomer4, round(($totalusable * $chashbackb2c5), 2));
                $this->updateCashback($idcustomer5, round(($totalnotax * $chashbackb2b1), 2));
                $this->updateCashback($idagent, round(($totalnotax * $feeage1), 2), true);
            }
            $chain = [
                'customer0' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c1', 5),
                'customer1' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c2', 2.5),
                'customer2' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c3', 1.25),
                'customer3' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c4', 0.6),
                'customer4' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c5', 0.3),
                'customer5' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b1', 1),
                'agent' => $this->db->setting->getStr('lcdp', 'cashback.feeage1', 0.5)
            ];
            $this->updateCartChain($data['cartid'], $chain);
        } elseif ($level == 4) {
            $this->historyCashback($data['cartid'], $idcustomer0, round(($totalusable * $chashbackb2c1), 2), 1, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer1, round(($totalusable * $chashbackb2c2), 2), 2, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer2, round(($totalusable * $chashbackb2c3), 2), 3, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer3, round(($totalusable * $chashbackb2c4), 2), 4, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer4, round(($totalnotax * $chashbackb2b2), 2), 5, $totalnotax, $status);
            $this->historyCashback($data['cartid'], $idagent, round(($totalnotax * $feeage2), 2), 5, $totalnotax, $status);
            if ($status != 0) {
                $this->updateCashback($idcustomer0, round(($totalusable * $chashbackb2c1), 2));
                $this->updateCashback($idcustomer1, round(($totalusable * $chashbackb2c2), 2));
                $this->updateCashback($idcustomer2, round(($totalusable * $chashbackb2c3), 2));
                $this->updateCashback($idcustomer3, round(($totalusable * $chashbackb2c4), 2));
                $this->updateCashback($idcustomer4, round(($totalnotax * $chashbackb2b2), 2));
                $this->updateCashback($idagent, round(($totalnotax * $feeage2), 2), true);
            }
            $chain = [
                'customer0' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c1', 5),
                'customer1' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c2', 2.5),
                'customer2' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c3', 1.25),
                'customer3' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c4', 0.6),
                'customer4' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b2', 2),
                'agent' => $this->db->setting->getStr('lcdp', 'cashback.feeage2', 1)
            ];
            $this->updateCartChain($data['cartid'], $chain);
        } elseif ($level == 3) {
            $this->historyCashback($data['cartid'], $idcustomer0, round(($totalusable * $chashbackb2c1), 2), 1, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer1, round(($totalusable * $chashbackb2c2), 2), 2, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer2, round(($totalusable * $chashbackb2c3), 2), 3, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer3, round(($totalnotax * $chashbackb2b3), 2), 4, $totalnotax, $status);
            $this->historyCashback($data['cartid'], $idagent, round(($totalnotax * $feeage3), 2), 4, $totalnotax, $status);
            if ($status != 0) {
                $this->updateCashback($idcustomer0, round(($totalusable * $chashbackb2c1), 2));
                $this->updateCashback($idcustomer1, round(($totalusable * $chashbackb2c2), 2));
                $this->updateCashback($idcustomer2, round(($totalusable * $chashbackb2c3), 2));
                $this->updateCashback($idcustomer3, round(($totalnotax * $chashbackb2b3), 2));
                $this->updateCashback($idagent, round(($totalnotax * $feeage3), 2), true);
            }
            $chain = [
                'customer0' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c1', 5),
                'customer1' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c2', 2.5),
                'customer2' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c3', 1.25),
                'customer3' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b3', 4),
                'agent' => $this->db->setting->getStr('lcdp', 'cashback.feeage3', 2)
            ];
            $this->updateCartChain($data['cartid'], $chain);
        } elseif ($level == 2) {
            $this->historyCashback($data['cartid'], $idcustomer0, round(($totalusable * $chashbackb2c1), 2), 1, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer1, round(($totalusable * $chashbackb2c2), 2), 2, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer2, round(($totalnotax * $chashbackb2b4), 2), 3, $totalnotax, $status);
            $this->historyCashback($data['cartid'], $idagent, round(($totalnotax * $feeage4), 2), 3, $totalnotax, $status);
            if ($status != 0) {
                $this->updateCashback($idcustomer0, round(($totalusable * $chashbackb2c1), 2));
                $this->updateCashback($idcustomer1, round(($totalusable * $chashbackb2c2), 2));
                $this->updateCashback($idcustomer2, round(($totalnotax * $chashbackb2b4), 2));
                $this->updateCashback($idagent, round(($totalnotax * $feeage4), 2), true);
            }
            $chain = [
                'customer0' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c1', 5),
                'customer1' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c2', 2.5),
                'customer2' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b4', 8),
                'agent' => $this->db->setting->getStr('lcdp', 'cashback.feeage4', 4)
            ];
            $this->updateCartChain($data['cartid'], $chain);
        } elseif ($level == 1) {
            $this->historyCashback($data['cartid'], $idcustomer0, round(($totalusable * $chashbackb2c1), 2), 1, $totalusable, $status);
            $this->historyCashback($data['cartid'], $idcustomer1, round(($totalnotax * $chashbackb2b5), 2), 2, $totalnotax, $status);
            $this->historyCashback($data['cartid'], $idagent, round(($totalnotax * $feeage5), 2), 2, $totalnotax, $status);
            if ($status != 0) {
                $this->updateCashback($idcustomer0, round(($totalusable * $chashbackb2c1), 2));
                $this->updateCashback($idcustomer1, round(($totalnotax * $chashbackb2b5), 2));
                $this->updateCashback($idagent, round(($totalnotax * $feeage5), 2), true);
            }
            $chain = [
                'customer0' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2c1', 5),
                'customer1' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b5', 10),
                'agent' => $this->db->setting->getStr('lcdp', 'cashback.feeage5', 6)
            ];
            $this->updateCartChain($data['cartid'], $chain);
        } else {
            $this->historyCashback($data['cartid'], $idcustomer0, round(($totalnotax * $chashbackb2b6), 2), 1, $totalnotax, $status);
            $this->historyCashback($data['cartid'], $idagent, round(($totalnotax * $feeage6), 2), 1, $totalnotax, $status);
            if ($status != 0) {
                $this->updateCashback($idcustomer0, round(($totalnotax * $chashbackb2b6), 2));
                $this->updateCashback($idagent, round(($totalnotax * $feeage6), 2), true);
            }
            $chain = [
                'customer0' => $this->db->setting->getStr('lcdp', 'cashback.chashbackb2b6', 16),
                'agent' => $this->db->setting->getStr('lcdp', 'cashback.feeage6', 8)
            ];
            $this->updateCartChain($data['cartid'], $chain);
        }
    }

    public function getChain(int $id)
    {
        $sql = 'select (json_unquote(JSON_EXTRACT(options, "$.chain"))) as chain from #__account_user where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', (int) $id);
        $q->execute();
        $r = $q->fetch();
        return (!empty($r)) ? $r['chain'] : 0;
    }

    public function getAgent(int $id)
    {
        $sql = 'select (json_unquote(JSON_EXTRACT(options, "$.id_agent"))) as chain from #__address where id_customer = :id and (json_unquote(JSON_EXTRACT(options, "$.coddes"))) = 9999';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', (int) $id);
        $q->execute();
        $r = $q->fetch();
        return (!empty($r)) ? $r['chain'] : 0;
    }

    public function updateCartChain(int $id, $chain)
    {
        $sql = 'select options from #__cart where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', $id);
        $q->execute();
        $r = $q->fetch();
        $options = json_decode($r['options'], true);
        $tbl = $this->db->tableFix('#__cart');
        $data['id'] = $id;
        $options['chain'] = $chain;
        $data['options'] = json_encode($options);
        $query = $this->db->newFluent()->update($tbl)->set($data)->where('id', (int) $id);
        $query->execute();
    }

    public function updateCashback($id_user, $cashback, $agent = false)
    {
        $sql = 'select options from #__account_user where id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue('id', $id_user);
        $q->execute();
        $r = $q->fetch();
        $options = json_decode($r['options'], true);
        $tbl = $this->db->tableFix('#__account_user');
        $data['id'] = $id_user;
        if ($agent == true) {
            if (isset($options['fee'])) {
                $options['fee'] = $options['fee'] + ($cashback);
            } else {
                $options['fee'] = $cashback;
            }
        } else {
            $options['cashback'] = $options['cashback'] + ($cashback);
        }
        $data['options'] = json_encode($options);
        $query = $this->db->newFluent()->update($tbl)->set($data)->where('id', (int) $id_user);
        $query->execute();
    }

    public function historyCashback($id_cart, $id_user, $cashback, $level, $total, $status = 0)
    {
        $tbl = $this->db->tableFix('#__ext_cashback_history');
        $data['id_cart'] = $id_cart;
        $data['id_user'] = $id_user;
        $data['cashback'] = $cashback;
        $data['level'] = $level;
        $data['status'] = $status;
        $data['cart_total'] = $total;
        $data['ins_date'] = new \FluentLiteral('NOW()');
        $query = $this->db->newFluent()->insertInto($tbl, $data);
        $query->execute();
    }
}
