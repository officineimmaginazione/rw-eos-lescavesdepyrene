<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Classes;

use EOS\System\Util\ArrayHelper;

class CartSubscriber extends \EOS\System\Event\Subscriber
{

    public static function getSubscribedEvents()
    {
        return [
            \EOS\Components\Account\Classes\Events::USER_INDEX_TEMPLATE_RENDER => 'displayCartRender'];
    }

    public function displayCartRender(\EOS\System\Event\EchoEvent $event)
    {
        $view = $event->getSubject();
        \EOS\UI\Loader\Library::load($view, 'Bootbox');
        $customer = (int)$view->request->getQueryParam('customer', 0);
        $w = new \EOS\Components\Cart\Widgets\DisplayCartWidget($view);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $w->viewagent = false;
        if(isset($view->controller->user)) {
            $w->user = $view->controller->user->getInfo();
            $w->userlabel = $view->user->username;
        }
        if(in_array(2, $view->user->groups)){
            $w->viewagent = true;
        }
        if(isset($view->controller->user)) {
            $user = $view->controller->user->getInfo();
            $id = $user->id;
        }
        if (strpos($user->username, 'direzionale') !== false) {
            $id = 0;
        }
        if($customer == 0) {
            $w->listcart = $m->getCartByIDStatus($id, '0, 1', 200);
            $w->listcartconfirmed = $m->getCartByIDStatus($id, '2, 3', 200);
        } else {
            $w->listcart = $m->getCartByIDStatus($customer, '0, 1', 200);
            $w->listcartconfirmed = $m->getCartByIDStatus($customer, '2, 3', 200);
        }
        $w->write();
    }
   

}
