<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class CartController extends \EOS\Components\System\Classes\SiteController
{

    private function triggerBeforeUpdate()
    {
        $ev = new \EOS\System\Event\GenericEvent($this);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Cart\Classes\Events::CART_CONTROLLER_BEFORE_UPDATE, $ev);
    }

    /* Liste e dettagli */
    public function listCart($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        if ($v->user->id == 8363 || $v->user->id == 10004 || $v->user->id == 10005) {
            $id = (int)$request->getQueryParam('customer', 0);
        } else {
            $id = (int)$request->getQueryParam('customer', $v->user->id);
        }
        $v->customer = $id;
        $v->listcart = $m->getCartByIDStatus($id, '0, 1', 200);
        return $v->render('list/cart', true);
    }

    public function listCartConfirmed($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        if ($v->user->id == 8363 || $v->user->id == 10004 || $v->user->id == 10005) {
            $id = (int)$request->getQueryParam('customer', 0);
        } else {
            $id = (int)$request->getQueryParam('customer', $v->user->id);
        }
        $v->customer = $id;
        $v->listcartconfirmed = $m->getCartByIDStatus($id, '2, 3', 200);
        return $v->render('list/cartconfirmed', true);
    }

    public function details($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $v->datarow = $m->getRowsByCart($args['id'], $v->viewagent);
        $v->data = $m->getCartDetailsByID($args['id']);
        return $v->render('details/default', true);
    }

    /* Carrello */
    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
        $mu->userInfo($v);
        $headcheck = $m->getCartByID($m->id);
        if (!$v->viewagent && $headcheck['id_customer'] == 0) {
            $m->updateCustomer($v->user->id);
        }
        $m->prepareObjectAndSave($v);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        $v->displayside = false;
        $v->cartid = $m->id;
        $v->discount = $ma->getDataDiscount(ArrayHelper::getInt($v->head, 'id_address_delivery'));
        if (ArrayHelper::getInt($v->head, 'id_address_delivery') != 0) {
            $this->session->set('cart.set.address.' . $m->id, 1);
        } else {
            $this->session->set('cart.set.address.' . $m->id, 0);
        }
        $v->payments = $m->getPayments(null, null, null);
        $v->currency = new \EOS\System\Payments\Currencies\Currency($this->container);
        $v->user = $this->user->getInfo();
        $v->agentid = $v->user->id;
        $v->max = 0;
        if ($v->viewagent) {
            $v->customerinfo = $mu->getCustomerInfo(ArrayHelper::getInt($v->head, 'id_customer'));
            $v->customeroptions = ArrayHelper::fromJSON(ArrayHelper::getStr($v->customerinfo, 'customeroptions'));
            $v->listaddresses = $ma->getAddresses(ArrayHelper::getInt($v->head, 'id_customer'), 1, $v->user->id);
            if (((float)$v->customeroptions['cashback'] - 1) > (float) $v->total) {
                $v->max = $v->total - 1;
            } else {
                $v->max = $v->customeroptions['cashback'];
            }
            $v->cashback = $v->customeroptions['cashback'];
        } else {
            $v->customerinfo = $mu->getCustomerInfo($v->user->id);
            $v->customeroptions = ArrayHelper::fromJSON(ArrayHelper::getStr($v->customerinfo, 'customeroptions'));
            $v->listaddresses = $ma->getAddresses($v->user->id, 1);
            if (((float)$v->cashback - 1) > (float) $v->total) {
                $v->max = $v->total - 1;
            } else {
                $v->max = $v->cashback;
            }
        }
        $v->accountinfo = $mu->getInfoUser($v->agentid);
        $v->filterBy = (int)$request->getQueryParam('filterby', 0);
        $v->title = $this->lang->transEncode('cart.cart.title');
        return $v->render('cart/default', true);
    }

    public function preorder($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mc = new \EOS\Components\Product\Models\CatalogModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $query_str = parse_url($_SERVER["REQUEST_URI"], PHP_URL_QUERY);
        parse_str($query_str, $query_params);
        if ((!empty($query_params)) && !empty($query_params['esito']) && $query_params['esito'] == 'KO') {
            $uri = $this->path->getUrlFor('Cart', 'cart/index');
            return $response->withRedirect((string) $uri, 302);
        } else if (isset($_REQUEST['payment_id']) && !empty($_REQUEST['payment_id'])) {
            $satispay = new \EOS\System\Payments\Satispay($this->container);
            $paymentId = $_REQUEST['payment_id'];
            $payres = $satispay->checkPayment($paymentId);
            if ($payres['result'] == false) {
                $uri = $this->path->getUrlFor('Cart', 'cart/index');
                return $response->withRedirect((string) $uri, 302);
            }
        }
        $mu->userInfo($v);
        $m->clearCart();
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        $v->title = $this->lang->transEncode('cart.cart.title');
        return $v->render('preorder/default', true);
    }

    public function confirm($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $v->title = $this->lang->transEncode('order.order.title');
        return $v->render('cart/confirm', true);
    }

    public function verify($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $payment = new \EOS\System\Payments\XPay($this->container);
        $verify = $payment->verifyPayment($_REQUEST);
        if ($verify['result']) {
            $cartid = strtok($_REQUEST['codTrans'],  '-');
            $data['cartid'] = $cartid;
            $data['txn-id'] = $_REQUEST['codAut'];
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            $mua = new \EOS\Components\Account\Models\UserModel($this->container);
            $data = $m->getCartByID($cartid);
            $addressinvoice = $ma->getData($data['id_address_delivery']);
            $user = $mua->getCustomerInfo($data['id_customer']);
            $paymentData['amount'] = $data['total'];
            $paymentData['shipping-name'] = $addressinvoice['name'];
            $paymentData['shipping-surname'] = $addressinvoice['surname'];
            $paymentData['shipping-address'] = $addressinvoice['address1'];
            $paymentData['shipping-city'] = $addressinvoice['city'];
            //$paymentData['shipping-zipcode'] = $addressinvoice['zipcode'];
            $paymentData['shipping-phone'] = $addressinvoice['phone'];
            $paymentData['name'] = $addressinvoice['name'];
            $paymentData['surname'] = $addressinvoice['surname'];
            $paymentData['email'] = $user['email'];
            $dataoptions = [];
            $dataoptions['id'] = $cartid;
            $cartProd = $m->getRowsByCart($cartid);
            $countprod = 0;
            $optionscart = json_decode($data['options'], true);
            $paymentData['payment'] = $optionscart['head_payment'];
            $datacashback['cartid'] = $cartid;
            $datacashback['idcustomer'] = $data['id_customer'];
            $datacashback['total'] = $data['total'];
            if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                $datacashback['totalTaxable'] = (float)$optionscart['cashback'] + $data['total'] - (float) $optionscart['shipping']['total'];
            } else {
                $datacashback['totalTaxable'] = $data['total'] - (float) $optionscart['shipping']['total'];
            }
            $datacashback['status'] = 1;
            $countprod = 0;
            $amount = 0;
            foreach ($cartProd as $row) {
                $optionsrow = json_decode($row['options'], true);
                //$countprod += $row['quantity'];
                $priceprod = $row['price'];
                $paymentData['cart-product'][$countprod]['id'] = $row['id'];
                $paymentData['cart-product'][$countprod]['name'] = $row['name'];
                $paymentData['cart-product'][$countprod]['price'] = $row['price'];
                $paymentData['cart-product'][$countprod]['quantity'] = $row['quantity'];
                $paymentData['cart-product'][$countprod]['item_number_' . $countprod] = $countprod;
                $countprod++;
            }
            $sender = new \EOS\System\Mail\Mail($this->container);
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = $user['email'];
            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $cartid;
            $sender->message = $this->createMail($paymentData);
            if ($sender->sendEmail($error)) {
                if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                    $m->updateCashback($data['id_customer'], $optionscart['cashback']);
                    $m->historyCashback($cartid, $data['id_customer'], (float)$optionscart['cashback'], 0, 0, 5);
                }
                $m->cashbackManage($datacashback);
                $m->cartToOrder($cartid);
            }
        }
    }

    public function paypalipn()
    {
        $payment = new \EOS\System\Payments\Paypal($this->container);
        $payres = $payment->validate_ipn();
        if ($payres['result'] == true) {
            $ipndata = $payres['ipn_data'];
            $cartid = $ipndata['custom'];
            $data['txn-id'] = $ipndata['txn_id'];
            $m = new \EOS\Components\Cart\Models\CartModel($this->container);
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            $mua = new \EOS\Components\Account\Models\UserModel($this->container);
            $data = $m->getCartByID($cartid);
            $addressinvoice = $ma->getData($data['id_address_delivery']);
            $user = $mua->getCustomerInfo($data['id_customer']);
            $paymentData['amount'] = $data['total'];
            $paymentData['shipping-name'] = $addressinvoice['name'];
            $paymentData['shipping-surname'] = $addressinvoice['surname'];
            $paymentData['shipping-address'] = $addressinvoice['address1'];
            $paymentData['shipping-city'] = $addressinvoice['city'];
            //$paymentData['shipping-zipcode'] = $addressinvoice['zipcode'];
            $paymentData['shipping-phone'] = $addressinvoice['phone'];
            $paymentData['name'] = $addressinvoice['name'];
            $paymentData['surname'] = $addressinvoice['surname'];
            $paymentData['email'] = $user['email'];
            $dataoptions = [];
            $dataoptions['id'] = $cartid;
            $cartProd = $m->getRowsByCart($cartid);
            $countprod = 0;
            $optionscart = json_decode($data['options'], true);
            $paymentData['payment'] = $optionscart['head_payment'];
            $datacashback['cartid'] = $cartid;
            $datacashback['idcustomer'] = $data['id_customer'];
            $datacashback['total'] = $data['total'];
            if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                $datacashback['totalTaxable'] = (float)$optionscart['cashback'] + $data['total'] - (float) $optionscart['shipping']['total'];
            } else {
                $datacashback['totalTaxable'] = $data['total'] - (float) $optionscart['shipping']['total'];
            }
            $datacashback['status'] = 1;
            $countprod = 0;
            $amount = 0;
            foreach ($cartProd as $row) {
                $optionsrow = json_decode($row['options'], true);
                //$countprod += $row['quantity'];
                $priceprod = $row['price'];
                $paymentData['cart-product'][$countprod]['id'] = $row['id'];
                $paymentData['cart-product'][$countprod]['name'] = $row['name'];
                $paymentData['cart-product'][$countprod]['price'] = $row['price'];
                $paymentData['cart-product'][$countprod]['quantity'] = $row['quantity'];
                $paymentData['cart-product'][$countprod]['item_number_' . $countprod] = $countprod;
                $countprod++;
            }
            //$lastid = $m->saveData($data, $error);
            $sender = new \EOS\System\Mail\Mail($this->container);
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = $user['email'];
            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $cartid;
            $sender->message = $this->createMail($paymentData);
            if ($sender->sendEmail($error)) {
                if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                    $m->updateCashback($data['id_customer'], $optionscart['cashback']);
                    $m->historyCashback($cartid, $data['id_customer'], (float)$optionscart['cashback'], 0, 0, 5);
                }
                $m->cashbackManage($datacashback);
                $m->cartToOrder($cartid);
            }
        }
    }

    public function satispayCallBack($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $satispay = new \EOS\System\Payments\Satispay($this->container);
        $paymentId = $_REQUEST['payment_id'];
        $payres = $satispay->postProcess($paymentId);
        if ($payres['result'] == true) {
            $cartid = $payres['cartID'];
            $data['cartid'] = $cartid;
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            $mua = new \EOS\Components\Account\Models\UserModel($this->container);
            $data = $m->getCartByID($cartid);
            $addressinvoice = $ma->getData($data['id_address_delivery']);
            $user = $mua->getCustomerInfo($data['id_customer']);
            $paymentData['amount'] = $data['total'];
            $paymentData['shipping-name'] = $addressinvoice['name'];
            $paymentData['shipping-surname'] = $addressinvoice['surname'];
            $paymentData['shipping-address'] = $addressinvoice['address1'];
            $paymentData['shipping-city'] = $addressinvoice['city'];
            //$paymentData['shipping-zipcode'] = $addressinvoice['zipcode'];
            $paymentData['shipping-phone'] = $addressinvoice['phone'];
            $paymentData['name'] = $addressinvoice['name'];
            $paymentData['surname'] = $addressinvoice['surname'];
            $paymentData['email'] = $user['email'];
            $dataoptions = [];
            $dataoptions['id'] = $cartid;
            $cartProd = $m->getRowsByCart($cartid);
            $countprod = 0;
            $optionscart = json_decode($data['options'], true);
            $paymentData['payment'] = $optionscart['head_payment'];
            $datacashback['cartid'] = $cartid;
            $datacashback['idcustomer'] = $data['id_customer'];
            $datacashback['total'] = $data['total'];
            if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                $datacashback['totalTaxable'] = (float)$optionscart['cashback'] + $data['total'] - (float) $optionscart['shipping']['total'];
            } else {
                $datacashback['totalTaxable'] = $data['total'] - (float) $optionscart['shipping']['total'];
            }
            $datacashback['status'] = 1;
            $countprod = 0;
            $amount = 0;
            foreach ($cartProd as $row) {
                $optionsrow = json_decode($row['options'], true);
                //$countprod += $row['quantity'];
                $priceprod = $row['price'];
                $paymentData['cart-product'][$countprod]['id'] = $row['id'];
                $paymentData['cart-product'][$countprod]['name'] = $row['name'];
                $paymentData['cart-product'][$countprod]['price'] = $row['price'];
                $paymentData['cart-product'][$countprod]['quantity'] = $row['quantity'];
                $paymentData['cart-product'][$countprod]['item_number_' . $countprod] = $countprod;
                $countprod++;
            }
            $sender = new \EOS\System\Mail\Mail($this->container);
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = $user['email'];
            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $cartid;
            $sender->message = $this->createMail($paymentData);
            if ($sender->sendEmail($error)) {
                if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                    $m->updateCashback($data['id_customer'], $optionscart['cashback']);
                    $m->historyCashback($cartid, $data['id_customer'], (float)$optionscart['cashback'], 0, 0, 5);
                }
                $m->cashbackManage($datacashback);
                $m->cartToOrder($cartid);
            }
        }
    }

    public function error($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $v->test = $this->lang->transEncode('order.order.title');
        $v->title = $this->lang->transEncode('order.verify.title');
        $v->titleorder = $this->lang->transEncode('order.verify.error.title');
        $v->description = $this->lang->transEncode('order.verify.error.description');
        return $v->render('error/default', true);
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            $data = $fv->getInputData();
            $m = new \EOS\Components\Cart\Models\CartModel($this->container);
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            $mu = new \EOS\Components\Account\Models\UserModel($this->container);
            if (isset($data['command'])) {
                switch ($data['command']) {
                    case 'savedata':
                        $error = '';
                        $data = $fv->getInputData();
                        if ($data['payoncheckout'] == 1) {
                            $paymentData = $data;
                            $cart = $m->getCartByID($paymentData['cartid']);
                            $optionscart = json_decode($cart['options'], true);
                            $paymentData['shipping_cost'] = (isset($optionscart['shipping']['total'])) ? number_format((float)$optionscart['shipping']['total'], 2, '.', '') : 0;
                            $addressinvoice = $ma->getData($data['idaddressdelivery']);
                            $user = $mu->getCustomerInfo($data['idcustomer']);
                            /* Dati per i metodi di pagamento */
                            $paymentData['shipping-name'] = $addressinvoice['name'];
                            $paymentData['shipping-surname'] = $addressinvoice['surname'];
                            $paymentData['shipping-address'] = $addressinvoice['address1'];
                            $paymentData['shipping-city'] = $addressinvoice['city'];
                            //$paymentData['shipping-zipcode'] = $addressinvoice['zipcode'];
                            $paymentData['shipping-phone'] = $addressinvoice['phone'];
                            $paymentData['name'] = $addressinvoice['name'];
                            $paymentData['surname'] = $addressinvoice['surname'];
                            $paymentData['email'] = $user['email'];
                            $dataoptions = [];
                            $dataoptions['id'] = $paymentData['cartid'];
                            $cartProd = $m->getRowsByCart($paymentData['cartid']);
                            $countprod = 0;
                            $amount = 0;
                            foreach ($cartProd as $row) {
                                $optionsrow = json_decode($row['options'], true);
                                //$countprod += $row['quantity'];
                                $priceprod = $row['price'];
                                $paymentData['cart-product'][$countprod]['id'] = $row['id'];
                                $paymentData['cart-product'][$countprod]['name'] = $row['name'];
                                $paymentData['cart-product'][$countprod]['price'] = $row['price'];
                                $paymentData['cart-product'][$countprod]['quantity'] = $row['quantity'];
                                $paymentData['cart-product'][$countprod]['item_number_' . $countprod] = $countprod;
                                $countprod++;
                            }
                            $paymentData['discount'] = 0;
                            $paymentData['lc'] = 'it';
                            $paymentData['amount'] = number_format((float)$paymentData['total'], 2, '.', '');
                            /* Dati per il cashback */
                            $datacashback['cartid'] = $paymentData['cartid'];
                            $datacashback['idcustomer'] = $data['idcustomer'];
                            $datacashback['total'] = $data['total'];
                            $datacashback['totalTaxable'] = $data['total'] - (float) $optionscart['shipping']['total'];
                            if ($data['payment'] == 7) { //Pagamento con bonifico
                                $datacashback['status'] = $this->container->get('database')->setting->getValue('order', 'cashback.status.banktransfer');
                                $m->cartToOrder($data['cartid']);
                                $paymentData['cart-product'] = $cartProd = $m->getRowsByCart($paymentData['cartid']);
                                $sender = new \EOS\System\Mail\Mail($this->container);
                                $data['type-payment'] = $this->lang->transEncode('order.order.payment.creditcard');
                                $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
                                $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
                                $sender->to = $user['email'];
                                $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
                                $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $m->id;
                                $sender->message = $this->createMail($paymentData);
                                if ($sender->sendEmail($error)) {
                                    if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                                        $m->updateCashback($cart['id_customer'], $optionscart['cashback']);
                                        $paymentData['amount'] = (float)$optionscart['cashback'] + $paymentData['amount'];
                                        $m->historyCashback($data['cartid'], $cart['id_customer'], (float)$optionscart['cashback'], 0, 0, 5);
                                    }
                                    $m->cashbackManage($datacashback);
                                    $fv->setResult(true);
                                    $fv->setRedirect($this->path->getUrlFor('cart', 'cart/preorder'));
                                } else {
                                    $fv->setMessage($error);
                                }
                            } elseif ($data['payment'] == 76 || $data['payment'] == 24) {
                                $paymentData = $data; //Pagamento con CC
                                setcookie('orderdata', base64_encode(json_encode($data)), time() + 3600, "/");
                                $payment = new \EOS\System\Payments\XPay($this->container);
                                $paymentData['amount'] = $paymentData['total'];
                                $payres = $payment->payment($paymentData);
                                if ($payres['result']) {
                                    $fv->setResult(true);
                                    $fv->setRedirect($payres['url']);
                                } else {
                                    $fv->setMessage($payres['error']);
                                }
                            } elseif ($data['payment'] == 77) { //Pagamento con Paypal
                                $payment = new \EOS\System\Payments\Paypal($this->container);
                                if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                                    $paymentData['discount'] = -1 * $optionscart['cashback'];
                                }
                                $payres = $payment->paymentPaypal($paymentData);
                                if ($payres['result']) {
                                    $fv->setResult(true);
                                    $fv->setRedirect($payres['url']);
                                } else {
                                    $fv->setMessage($payres['error']);
                                }
                            } elseif ($data['payment'] == 78) { //Pagamento con Satispay
                                $payment = new \EOS\System\Payments\Satispay($this->container);
                                $payres = $payment->createPayment($paymentData);
                                if ($payres['result']) {
                                    $fv->setResult(true);
                                    $fv->setRedirect($payres['url']);
                                } else {
                                    $fv->setMessage($payres['error']);
                                }
                            } else {
                                $cart = $m->getCartByID($paymentData['cartid']);
                                $optionscart = json_decode($cart['options'], true);
                                if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                                    $m->updateCashback($cart['id_customer'], $optionscart['cashback']);
                                }
                                $m->cartToOrder($m->id);
                                $fv->setResult(true);
                                $fv->setRedirect($this->path->getUrlFor('cart', 'cart/preorder'));
                            }
                        } else {
                            $cart = $m->getCartByID($m->id);
                            $optionscart = json_decode($cart['options'], true);
                            if (isset($optionscart['cashback']) && $optionscart['cashback'] != 0) {
                                $m->updateCashback($cart['id_customer'], $optionscart['cashback']);
                            }
                            $m->cartToOrder($m->id);
                            $fv->setResult(true);
                            $fv->setRedirect($this->path->getUrlFor('cart', 'cart/preorder'));
                        }
                        break;
                    case 'saveadddata':
                        $this->triggerBeforeUpdate();
                        $this->setLockCart($m->id);
                        $error = '';
                        $data = $fv->getInputData();
                        if ($m->saveDataRow($data['id_product'], $data['quantity'], [], null, $data, true, [], false, $data['viewagent'])) {
                            $fv->setResult(true);
                            $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                        } else {
                            $fv->setMessage($error);
                        }
                        break;
                    case 'updaterow':
                        $this->triggerBeforeUpdate();
                        $this->setLockCart($m->id);
                        if ($m->saveDataRow($data['idproduct'], $data['qty'], $data['options'], $data['id'])) {
                            $fv->setResult(true);
                            $this->session->set('cart.set.calcfree.' . $m->id, 0);
                        }
                        break;
                    case 'updatecart':
                        if ($m->updateCart($data['id'], $data['idcustomer'], $data['idagent'], $data['headparameters'], $data['idaddress'])) {
                            if ($data['idaddress'] != 0) {
                                $this->session->set('cart.set.address.' . $m->id, 1);
                            }
                            $fv->setResult(true);
                        }
                        break;
                    case 'updateshipping':
                        $this->triggerBeforeUpdate(); // Devono ricalcolarsi gli omaggi
                        if ($m->updateCartShipping($data['id'], $data['params'])) {
                            $fv->setResult(true);
                            $this->session->set('cart.set.shipping.' . $m->id, 1);
                            $this->session->set('cart.set.calcfree.' . $m->id, 0);
                        }
                        break;
                    case 'deleterow':
                        $this->triggerBeforeUpdate();
                        if ($m->deleteRowById($data['id'])) {
                            $fv->setResult(true);
                            $this->session->set('cart.set.calcfree.' . $m->id, 0);
                        }
                        break;
                    case 'emptycart':
                        if ($m->deleteAllRowById($m->id)) {
                            $fv->setResult(true);
                        }
                        break;
                    case 'restorecart':
                        if ($m->restoreCart($data['id'])) {
                            $this->setLockCart($m->id);
                            $fv->setResult(true);
                            $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                        }
                        break;
                    case 'resetcart':
                        $m->clearCart();
                        $fv->setResult(true);
                        break;
                    case 'deletecart':
                        if ($m->deleteById($data['id'])) {
                            $fv->setResult(true);
                        }
                        break;
                    case 'updateheaddiscount':
                        if ($m->updateHeadDiscount($data['headdiscount'])) {
                            $fv->setResult(true);
                        }
                        break;
                    case 'addcashback':
                        $error = '';
                        if ($m->updateCartCashback(-1 * $data['cashback'])) {
                            $fv->setResult(true);
                            $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                        } else {
                            $fv->setMessage($error);
                        }
                        break;
                    default:
                        $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
                        break;
                }
            } else {
                $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }

    public function setLockCart($id)
    {
        $this->session->set('cart.set.shipping.' . $id, 0);
        $this->session->set('cart.set.address.' . $id, 0);
        $this->session->set('cart.set.calcfree.' . $id, 0);
    }

    public function createMail($data)
    {
        $m = new \EOS\Components\Cart\Models\CartModel($this->container);
        $content = '<!DOCTYPE html>';
        $content .= '<html lang="it" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">';
        $content .= '<head>';
        $content .= '<meta charset="utf-8">';
        $content .= '<meta name="viewport" content="width=device-width">';
        $content .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $content .= '<meta name="x-apple-disable-message-reformatting">';
        $content .= '<link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700" rel="stylesheet">';
        $content .= '<title>LesCavesDePyrene - ' . $this->lang->transEncode('order.order.email.subject') . ' ' . $m->id . '</title>';
        $content .= "<style>body,html{margin:0 auto!important;padding:0!important;height:100%!important;width:100%!important;background:#f1f1f1}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*='margin: 16px 0']{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}img{-ms-interpolation-mode:bicubic}a{text-decoration:none}.aBn,.unstyle-auto-detected-links *,[x-apple-data-detectors]{border-bottom:0!important;cursor:default!important;color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}.a6S{display:none!important;opacity:.01!important}.im{color:inherit!important}img.g-img+div{display:none!important}@media only screen and (min-device-width:320px) and (max-device-width:374px){u~div .email-container{min-width:320px!important}}@media only screen and (min-device-width:375px) and (max-device-width:413px){u~div .email-container{min-width:375px!important}}@media only screen and (min-device-width:414px){u~div .email-container{min-width:414px!important}}.bg_white{background:#fff}.bg_light{background:#fafafa}.bg_black{background:#000}.bg_dark{background:rgba(0,0,0,.8)}.email-section{padding:2.5em}.btn{padding:10px 15px;display:inline-block}.btn.btn-primary{border-radius:5px;background:#8b2329;color:#fff}.btn.btn-white{border-radius:5px;background:#fff;color:#000}.btn.btn-white-outline{border-radius:5px;background:0 0;border:1px solid #fff;color:#fff}.btn.btn-black-outline{border-radius:0;background:0 0;border:2px solid #000;color:#000;font-weight:700}.btn-custom{color:rgba(0,0,0,.3);text-decoration:underline}h1,h2,h3,h4,h5,h6{font-family:'Work Sans',sans-serif;color:#000;margin-top:0;font-weight:400}p{color:rgba(0,0,0,.7)}body{font-family:'Work Sans',sans-serif;font-weight:400;font-size:15px;line-height:1.8;color:rgba(0,0,0,.7)}a{color:#8b2329}.logo h1{margin:0}.logo h1 a{color:#8b2329;font-size:24px;font-weight:700;font-family:'Work Sans',sans-serif}.hero{position:relative;z-index:0}.hero .text{color:rgba(0,0,0,.3)}.hero .text h2{color:#000;font-size:24px;margin-bottom:15px;font-weight:300;line-height:1.2}.hero .text h3{font-size:21px;font-weight:200}.hero .text h2 span{font-weight:600;color:#000}.product-entry{display:block;position:relative;float:left;padding-top:20px}.product-entry .text{width:calc(100% - 0px);padding-left:20px}.product-entry .text h3{margin-bottom:0;padding-bottom:0}.product-entry .text p{margin-top:0}.product-entry .text,.product-entry img{float:left}ul.social{padding:0}ul.social li{display:inline-block;margin-right:10px}.footer{border-top:1px solid rgba(0,0,0,.05);color:rgba(0,0,0,.5)}.footer .heading{color:#000;font-size:20px}.footer ul{margin:0;padding:0}.footer ul li{list-style:none;margin-bottom:10px}.footer ul li a{color:#000}</style>";
        $content .= '</head>';
        $content .= '<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f1f1f1;">';
        $content .= '<center style="width: 100%; background-color: #f1f1f1;">';
        $content .= '<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">';
        $content .= '</div>';
        $content .= '<div style="max-width: 600px; margin: 0 auto;" class="email-container">';
        /* Testata */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="top" class="bg_white" style="padding: 1em 2.5em 0 2.5em;">';
        $content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td class="logo" style="text-align: left;">';
        $content .= '<h1><a href="https://www.lescaves.it"><img src="https://www.lescaves.it/data-customer/themes/default/img/logo-enotecaves.png" alt="Enotecaves" /></a></h1>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td valign="middle" class="hero bg_white" style="padding: 2em 0 2em 0;">';
        $content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="padding: 0 2.5em; text-align: left;">';
        $content .= '<div class="text">';
        if (isset($data['payment']) && $data['payment'] == 7) {
            $content .= '<h2>' . $this->lang->transEncode('order.order.confirm.email.msgbt') . '</h2>';
        } else {
            $content .= '<h2>' . $this->lang->transEncode('order.order.confirm.email.msg') . '</h2>';
        }
        $content .= '<h3>I tuoi dati</h3>';
        $content .= '<p>';
        if (isset($data['company']) && $data['company'] != '') {
            $content .= $this->lang->transEncode('order.order.company') . ': ' . $data['company'] . '<br>';
            $content .= $this->lang->transEncode('order.order.vat') . ': ' . $data['vat'] . '<br>';
        }
        (isset($data['cif']) && $data['cif'] != '') ? $content .= $this->lang->transEncode('order.order.cif') . ': ' . $data['cif'] . '<br>' : '';
        $content .= $this->lang->transEncode('order.order.email') . ': ' . $data['email'] . '<br>';
        //$content .= $this->lang->transEncode('order.order.address') . ': ' . $data['address'] . '<br>';
        //$content .= $this->lang->transEncode('order.order.city') . ': ' . $data['city'] . '<br>';
        $content .= '</p>';
        if (isset($data['shipping-name']) && $data['shipping-name'] != '') {
            $content .= '<h5>' . $this->lang->transEncode('order.order.datarecipient') . '</h5>';
            $content .= '<p>';
            (isset($data['shipping-name']) && $data['shipping-name'] != '') ? $content .= $this->lang->transEncode('order.order.name') . ': ' . $data['shipping-name'] . '<br>' : '';
            (isset($data['shipping-surname']) && $data['shipping-surname'] != '') ? $content .= $this->lang->transEncode('order.order.surname') . ': ' . $data['shipping-surname'] . '<br>' : '';
            (isset($data['shipping-email']) && $data['shipping-email'] != '') ? $content .= $this->lang->transEncode('order.order.email') . ': ' . $data['shipping-email'] . '<br>' : '';
            (isset($data['shipping-address']) && $data['shipping-address'] != '') ? $content .= $this->lang->transEncode('order.order.address') . ': ' . $data['shipping-address'] . '<br>' : '';
            (isset($data['shipping-city']) && $data['shipping-city'] != '') ? $content .= $this->lang->transEncode('order.order.city') . ': ' . $data['shipping-city'] . '<br>' : '';
            //(isset($data['shipping-zipcode']) && $data['shipping-zipcode'] != '') ? $content .= $this->lang->transEncode('order.order.zipcode') . ': ' . $data['shipping-zipcode'] . '<br>' : '';
            (isset($data['shipping-phone']) && $data['shipping-phone'] != '') ? $content .= $this->lang->transEncode('order.order.phone') . ': ' . $data['shipping-phone'] . '<br>' : '';
            $content .= '</p>';
        }
        //$privacy = (isset($data['consent1'])) ? $this->lang->transEncode('order.order.yes') : $this->lang->transEncode('order.order.no');
        //$content .= '<p>' . $this->lang->transEncode('order.order.email.privacy.consent1') . ' ' . $privacy . '</p>';
        $content .= '</div>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';

        $content .= '<tr>';
        $content .= '<table class="bg_white" role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr style="border-bottom: 1px solid rgba(0,0,0,.05);">';
        $content .= '<th width="80%" style="text-align:left; padding: 0 2.5em; color: #000; padding-bottom: 20px">Prodotti</th>';
        $content .= '<th width="20%" style="text-align:right; padding: 0 2.5em; color: #000; padding-bottom: 20px">Prezzo</th>';
        $content .= '</tr>';
        /* Product */
        foreach ($data['cart-product'] as $row) {
            $content .= '<tr style="border-bottom: 1px solid rgba(0,0,0,.05);">';
            $content .= '<td valign="middle" width="70%" style="text-align:left; padding: 0 2.5em;">';
            $content .= '<div class="product-entry">';
            //$content .= '<img src="" alt="" style="width: 100px; max-width: 600px; height: auto; margin-bottom: 20px; display: block;">';
            $content .= '<div class="text">';
            $content .= '<h3>' . $row['name'] . '</h3>';
            $content .= '<span>Quantità: ' . $row['quantity'] . '</span>';
            $content .= '<p></p>';
            $content .= '</div>';
            $content .= '</div>';
            $content .= '</td>';
            $content .= '<td valign="middle" width="30%" style="text-align:left; padding: 0 2.5em;">';
            $content .= '<span class="price" style="color: #000; font-size: 20px;">' . (float) $row['price'] . ' &euro;</span>';
            $content .= '</td>';
            $content .= '</tr>';
        }
        $content .= '<tr>';
        $content .= '<td valign="middle" style="text-align:left; padding: 1em 2.5em;">';
        $content .= '<h4>' . $this->lang->transEncode('order.order.amount') . ': ' . (float) $data['amount'] . ' &euro;</h4>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td valign="middle" style="text-align:left; padding: 1em 2.5em;">';
        //$content .= '<p><a href="#" class="btn btn-primary">Seguici su Facebook</a></p>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td valign="middle" style="text-align:left; padding: 1em 2.5em;">';
        if (isset($data['type-payment']) && $data['type-payment'] != '') {
            $content .= '<p>' . $this->lang->transEncode('order.order.payment.type') . ': ' . $data['type-payment'] . '</p>';
            if (isset($data['txn-id']) && $data['txn-id'] != '') {
                $content .= '<p>' . $this->lang->transEncode('order.order.txn-id') . ': ' . $data['txn-id'] . '</p>';
            }
        }
        if (isset($data['payment']) && $data['payment'] == 7) {
            $content .= '<p>Se hai scelto come metodo di pagamento il bonifico bancario di seguito l\'IBAN IT 41 Y 032682 25000 52699001710 , l\'intestazione Les Caves de Pyrene Srl e nella causale il numero dell\'ordine o la tua email.<br>Se hai già pagato con carta di credito o Paypal, non considerare questa parte.</p>';
        }
        $content .= '</table>';
        $content .= '</tr>';
        /* Footer */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="middle" class="bg_light footer email-section">';
        $content .= '<table>';
        $content .= '<tr>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 5px; padding-right: 5px;">';
        $content .= '<h3 class="heading">Les Caves de Pyrene</h3>';
        $content .= '<ul>';
        $content .= '<li><span class="text">Strada Cagnassi, 8 - 12050 Rodello</span></a></li>';
        $content .= '<li><span class="text">Corso Susa 22/A - 10098 Rivoli</span></a></li>';
        $content .= '<li><span class="text">Tel. +39 0173 617072</span></a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 10px;">';
        $content .= '<h3 class="heading">Link Utili</h3>';
        $content .= '<ul>';
        $content .= '<li><a href="https://www.lescaves.it">Chi siamo</a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td class="bg_white" style="text-align: center;">';
        $content .= '<p>&nbsp;</p>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</center>';
        $content .= '</body>';
        $content .= '</html>';
        return $content;
    }
}
