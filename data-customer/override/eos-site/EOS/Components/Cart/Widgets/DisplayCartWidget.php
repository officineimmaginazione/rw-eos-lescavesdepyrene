<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Cart\Widgets;

use EOS\System\Util\ArrayHelper;

class DisplayCartWidget extends \EOS\UI\Widget\Widget
{

    public $data = [];
    public $dataField = 'id_user';
    public $disabled = false; 
    public $multiple = false;

    public function write()
    {
        if($this->view->viewagent) {
            $this->renderTemplate('widget/displaycart');
        }
        $this->renderTemplate('widget/displaycartconfirmed');
    }

    public function getCurrentInfo()
    {
        $m = new \EOS\Components\Order\Models\OrderModel($this->view->controller->container);
        //$q = $m->getListQuery();
        //$q->where('u.id', ArrayHelper::getInt($this->data, $this->dataField));
        //return $m->getTextList($q);
    }

}
