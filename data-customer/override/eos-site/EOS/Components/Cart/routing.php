<?php
$this->mapComponent(['GET'], 'Cart', ['cart/index' => 'CartController:index']);
$this->mapComponent(['GET'], 'Cart', ['cart/preorder' => 'CartController:preorder']);
$this->mapComponent(['POST'], 'Cart', ['cart/ajaxcommand' => 'CartController:ajaxCommand']);
$this->mapComponent(['GET'], 'Cart', ['cart/getdatasource/{datasource}/{format}/{tokenname}/{tokenvalue}' => 'CartController:getDataSource']);
$this->mapComponent(['GET'], 'Cart', ['cart/details/{id}' => 'CartController:details']);
$this->mapComponent(['GET'], 'Cart', ['cart/listcart/' => 'CartController:listCart']);
$this->mapComponent(['GET'], 'Cart', ['cart/listcartconfirmed/' => 'CartController:listCartConfirmed']);
$this->mapComponent(['POST'], 'Cart', ['cart/verify' => 'CartController:verify']);
$this->mapComponent(['GET'], 'Cart', ['cart/confirm' => 'CartController:confirm']);
$this->mapComponent(['GET'], 'Cart', ['cart/error' => 'CartController:error']);
$this->mapComponent(['POST'], 'Cart', ['cart/paypalipn' => 'CartController:paypalipn']);
$this->mapComponent(['GET'], 'Cart', ['cart/satispaycallback' => 'CartController:satispayCallBack']);
