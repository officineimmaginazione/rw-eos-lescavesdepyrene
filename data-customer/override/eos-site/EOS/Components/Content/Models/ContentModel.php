<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2017
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Content\Models;

class ContentModel extends \EOS\System\Models\Model
{

    public function getItem($idContent, $idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('c.id, c.parent, cl.title, cl.subtitle, cl.content, cl.options')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent)
            ->where('cl.id_lang = ?', (int) $idLang);
        if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
            $q->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))->select('ca.publish_date');
        }
        $r = $q->fetch();
        if (!empty($r)) {
            if (empty($r['options'])) {
                $r['options']['template'] = 'default';
            } else {
                $r['options'] = json_decode($r['options'], true);
            }

            if ((isset($r['publish_date']) && (!empty($r['publish_date'])))) {
                $r['publish_date'] = $this->lang->dbDateToLangDate($r['publish_date']);
            }
            if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
                $parent_id = $r['parent'];
                $r['parent'] = array();
                $r['parent']['id'] = $parent_id;
                $r['parent']['title'] = $this->getContentTitle($r['parent']['id'], $idLang, \EOS\Components\Content\Classes\ContentType::Category);
                $r['parent']['options'] = $this->getContentOptions($r['parent']['id'], $idLang, \EOS\Components\Content\Classes\ContentType::Category);
            }
        }

        return $r;
    }

    public function getLastItem($idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('c.id, c.parent, cl.title, cl.subtitle, cl.content, cl.options')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('cl.id_lang = ?', (int) $idLang)
            ->limit('1,1');

        if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
            $q->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))->select('ca.publish_date');
            $q->where('ca.publish_date <= ?', $this->db->util->nowToDBDateTime());
            $q->orderBy('ca.publish_date desc');
        } else if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
            $q->orderBy('cl.ins_date desc');
        }
        $r = $q->fetch();
        if (!empty($r)) {
            if (empty($r['options'])) {
                $r['options']['template'] = 'default';
            } else {
                $r['options'] = json_decode($r['options'], true);
            }

            if ((isset($r['publish_date']) && (!empty($r['publish_date'])))) {
                $r['publish_date'] = $this->lang->dbDateToLangDate($r['publish_date']);
            }
        }
        return $r;
    }

    public function getLastItemByParent($idParent, $idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('c.id, c.parent, cl.title, cl.subtitle, cl.content, cl.options')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('cl.id_lang = ?', (int) $idLang);

        if (is_array($idParent))
            $q->where('c.parent in (' . implode(",", $idParent) . ')');
        else
            $q->where('c.parent = ?', (int) $idParent);


        if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
            $q->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))->select('ca.publish_date');
            $q->where('ca.publish_date <= ?', $this->db->util->nowToDBDateTime());
            $q->orderBy('ca.publish_date desc');
        }

        $r = $q->fetch();
        if (!empty($r)) {
            if (empty($r['options']))
                $r['options']['template'] = 'default';
            else
                $r['options'] = json_decode($r['options'], true);

            if ((isset($r['publish_date']) && (!empty($r['publish_date']))))
                $r['publish_date'] = $this->lang->dbDateToLangDate($r['publish_date']);
        }
        return $r;
    }

    public function getItems($idParent, $idLang, $type, $page = "", $pagesize = "")
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))
            ->select(null)->select('c.id, c.parent, cl.title, cl.subtitle, cl.content, cl.options, ca.publish_date')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            //->where('c.parent = ?', (int) $idParent) Remove to insert array or single value
            ->where('cl.id_lang = ?', (int) $idLang);

        if (is_array($idParent)) {
            $q->where('c.parent in (' . implode(",", $idParent) . ')');
        } else {
            $q->where('c.parent = ?', (int) $idParent);
        }

        if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
            $q->where('ca.publish_date <= ?', $this->db->util->nowToDBDateTime());
            $q->orderBy('ca.publish_date desc');
        } else {
            $q->orderBy('c.ins_date desc');
        }

        if ($page || $pagesize) {
            $start = ($page - 1) * $pagesize;
            $q->limit((int) $start . ',' . (int) $pagesize);
        }
        $list = $q->fetchAll();

        if (!empty($list)) {
            for ($i = 0; $i < count($list); $i++) {
                if ((isset($list[$i]['publish_date']) && (!empty($list[$i]['publish_date'])))) {
                    $list[$i]['publish_date'] = $this->lang->dbDateToLangDate($list[$i]['publish_date']);
                }
                if (empty($list[$i]['options'])) {
                    $list[$i]['options']['template'] = 'default';
                } else {
                    $list[$i]['options'] = json_decode($list[$i]['options'], true);
                }
                if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
                }
            }
        }

        $q->limit(null)->orderBy(null);


        return ['data' => $list, 'count' => $q->count()];
    }

    public function getParentList($idParent, &$res)
    {
        if (!empty($idParent)) {
            $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
                ->select(null)->select('c.id, c.parent')
                ->where('c.id = ?', (int) $idParent);

            $r = $q->fetch();
            if (!empty($r)) {
                $this->getParentList($r['parent'], $res);
                $res[] = $idParent;
            }
        }
    }

    public function getChildList($idParent, $type, &$res)
    {
        if (!empty($idParent)) {
            $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
                ->select(null)->select('c.id')
                ->where('c.parent = ?', (int) $idParent);

            if (!empty($type)) {
                $q->where('c.type = ?', $type);
            }

            $r = $q->fetchAll();

            if (!empty($r)) {
                foreach ($r as $rc) {
                    $this->getChildList($rc["id"], $type, $res);
                    $res[] = $rc["id"];
                }
            }
        }
    }

    public function getContentTitle($idContent, $idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('cl.title')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent)
            ->where('cl.id_lang = ?', (int) $idLang);
        $r = $q->fetch();
        if (empty($r)) {
            return '';
        } else {
            return $r["title"];
        }
    }

    public function getContentOptions($idContent, $idLang, $type)
    {
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('cl.options')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent)
            ->where('cl.id_lang = ?', (int) $idLang);
        $r = $q->fetch();
        if (empty($r)) {
            return '';
        } else {
            return (array)json_decode($r["options"]);
        }
    }

    public function getItemLangList($idContent, $type)
    {
        $res = [];
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('distinct cl.id_lang')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.id = ?', (int) $idContent);
        $list = $q->fetchAll();
        if (!empty($list)) {
            foreach ($list as $r) {
                $res[] = $r['id_lang'];
            }
        }
        return $res;
    }

    public function getRelatedItems($idContent, $parent, $type, $limit = null)
    {
        $res = [];
        $q = $this->db->newFluent()->from($this->db->tableFix('#__content c'))
            ->innerJoin($this->db->tableFix('#__content_lang cl ON cl.id_content = c.id'))
            ->select(null)->select('c.id, c.parent, cl.title, cl.subtitle, cl.content, cl.options')
            ->where('c.type = ?', $type)
            ->where('c.status = 1')
            ->where('c.parent = ?', (int) $parent)
            ->where('c.id != ?', (int) $idContent); //the current content shouldnt be related

        if ($type == \EOS\Components\Content\Classes\ContentType::Article) {
            $q->leftJoin($this->db->tableFix('#__content_article ca ON cl.id = ca.id_content_lang'))->select('ca.publish_date');
            $q->orderBy('ca.publish_date desc');
        } else {
            $q->orderBy('c.ins_date desc');
        }

        if ($limit) {
            $q->limit($limit);
        }

        $list = $q->fetchAll();

        if (!empty($list)) {
            foreach ($list as $r) {
                if (empty($r['options'])) {
                    $r['options']['template'] = 'default';
                } else {
                    $r['options'] = json_decode($r['options'], true);
                }

                if ((isset($r['publish_date']) && (!empty($r['publish_date'])))) {
                    $r['publish_date'] = $this->lang->dbDateToLangDate($r['publish_date']);
                }

                $res[] = $r;
            }
        }

        return $res;
    }


    public function getPagination($pageCount, $currPage, $link)
    {
        $pagination = array();
        if ($pageCount <= 5) {
            for ($i = 1; $i <= $pageCount; $i++) {
                $page = array(
                    "pageLink" => ($i != 1) ? $link . "?page=" . $i : $link,
                    "active" => ($i == $currPage) ? "active" : "",
                    "label" => $i
                );
                array_push($pagination, $page);
            }
        } else {
            $visibles = array();
            $visibles[] = 1;
            if ($currPage == 1 || $currPage == 2) {
                $visibles[] = 2;
                $visibles[] = 3;
            } else if ($currPage == ($pageCount - 1)) {
                $visibles[] = $pageCount - 2;
                $visibles[] = $pageCount - 1;
            } else {
                $visibles[] = $currPage - 1;
                $visibles[] = $currPage;
                $visibles[] = $currPage + 1;
            }
            $visibles[] = $pageCount;
            for ($i = 1; $i <= $pageCount; $i++) {
                if (in_array($i, $visibles)) {
                    $page = array(
                        "pageLink" => ($i != 1) ? $link . "?page=" . $i : $link,
                        "active" => ($i == $currPage) ? "active" : "",
                        "label" => $i
                    );
                    array_push($pagination, $page);
                } else {
                    if (end($pagination)['label'] != "...") {
                        $page = array(
                            "pageLink" => "",
                            "active" => "",
                            "label" => "..."
                        );
                        array_push($pagination, $page);
                    }
                }
            }
        }
        return $pagination;
    }
}
