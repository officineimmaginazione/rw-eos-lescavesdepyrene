<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Classes;

use EOS\System\Util\ArrayHelper;

class CartSubscriber extends \EOS\System\Event\Subscriber
{

    public function __construct($container)
    {
        parent::__construct($container);
        $this->lang = $container->get('language');
        $this->db = $container->get('database');
    }

    public static function getSubscribedEvents()
    {
        return [
            \EOS\Components\Cart\Classes\Events::CART_DATA_PARSE_ROWS => 'cartDataParseRows',
            \EOS\Components\Cart\Classes\Events::CART_DATA_CHECK_ROW => 'cartDataCheckRow',
            \EOS\Components\Cart\Classes\Events::CART_CONTROLLER_BEFORE_UPDATE => 'cartControllerBeforeUpdate'
        ];
    }

    public function cartDataParseRows(\EOS\System\Event\DataEvent $event)
    {
        $data = $event->getData();
        foreach ($data as &$r) {
            $options = ArrayHelper::fromJSON($r['options']);
            if (ArrayHelper::getMdBool($options, ['data', 'isfree'])) {
                $r['name'] .= '  [OMAGGIO]';
                $r['price-prod'] = $r['price'];
                $r['price'] = 0;
            }
            if (ArrayHelper::getMdBool($options, ['data', 'issample'])) {
                $r['price-prod'] = $r['price'];
                if ($r['quantity'] <= 1) {
                    $r['price'] = 0;
                }
            }
        }
        unset($r);
        $shippingDate = $event->getSubject()->getShippingStockDate();
        $stockManagerExt = new \EOS\Components\LCDPComp\Classes\StockManagerExt($this->container);
        $stockManagerExt->calcRowsStock($data, $shippingDate);
        $event->setData($data);
    }

    public function cartDataCheckRow(\EOS\System\Event\DataEvent $event)
    {
        $data = $event->getData();
        $cart = $event->getSubject();
        // La Cart->checkRow dovrebbe cercare la riga dell'articolo
        // in particolari condizioni possono essere più di una 
        // in questo evento forzo io la prima che deve essere valida
        if (count($data) > 1) {
            // Salto le campionature  
        }

        $event->setData($data);
    }

    public function cartControllerBeforeUpdate(\EOS\System\Event\GenericEvent $event)
    {
        // Prima di aggiornare il carrello dall'interfaccia utente azzero gli sconti
        $model = new \EOS\Components\LCDPComp\Models\CartExtModel($this->container);
        $model->deleteFreeRows();
    }
}
