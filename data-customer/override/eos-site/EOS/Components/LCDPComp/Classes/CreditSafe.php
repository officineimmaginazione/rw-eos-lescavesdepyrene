<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2020
  www.ecosoft.it - www.officineimmaginazione.com
 * */

declare(strict_types=1);

namespace EOS\Components\LCDPComp\Classes;

use Error;

class CreditSafe extends \EOS\System\Models\Model
{
    private $username;
    private $password;
    private $mode;

    private $url;
    private $endpoints;

    public $headersparams = array('Content-Type: application/json');
    private $token;

    public function __construct(string $username, string $password, string $mode)
    {
        $this->username = $username;
        $this->password = $password;
        $this->mode = $mode;

        if ($mode == "prod") {
            //production endpoints

        } elseif ($mode == "dev") {
            //development endpoints
            $this->url = "https://connect.creditsafe.com/";

            $this->endpoints = [
                "authenticate" => [
                    "ed" => "v1/authenticate",
                    "post" => "1"
                ],
                "companies" => [
                    "ed" => "v1/companies/",
                    "post" => "0"
                ],
                "companyreport" => [
                    "ed" => "v1/companies/",
                    "post" => "0"
                ],
                "addcompanytoportfolio" => [
                    "ed" => "v1/monitoring/portfolios/:portfolioId/companies",
                    "post" => "1"
                ]
            ];
        }
    }

    protected function httpRequest($ed, $post, $params = [], &$error)
    {
        $res = '{}';

        $connection = curl_init();

        curl_setopt($connection, CURLOPT_URL, $this->url . $ed);
        curl_setopt($connection, CURLOPT_POST, $post);
        curl_setopt($connection, CURLOPT_HTTPHEADER, $this->headersparams);
        if (!empty($params)) {
            curl_setopt($connection, CURLOPT_POSTFIELDS, json_encode($params, JSON_FORCE_OBJECT));
        }
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false); //questa riga server per ignorare il certificato di sicurezza

        //scommentare se serve vedere gli header della chiamata in caso di errore
        //curl_setopt($connection, CURLOPT_HEADER, true);

        $result = curl_exec($connection);
        $statusCode = curl_getinfo($connection, CURLINFO_HTTP_CODE);

        curl_close($connection);

        if (!$result) {
            $error .= 'Errore nella richiesta http!';
            error_log('CreditSafe HTTP: ' . $error);
            //throw new \Exception($error);
        } else {
            $res = $result;
        }

        return $res;
    }

    //controllo se la response della chiamata API contiene errori
    protected function errorHandler($endpoint = "", $params = [], $res, &$error)
    {
        $hasErrors = false;
        $error_message = " Endpoint=" . $endpoint;
        if (isset($res->errorCode) && $res->errorCode != "") {
            $hasErrors = true;
            $error_message .= " Codice=" . $res->errorCode;
            $error .= "Errore!<br/>";
        }
        if (isset($res->errorMessage) && $res->errorMessage != "") {
            $error_message .= " Messaggio=" . $res->errorMessage;
            $error .= $res->errorMessage . '<br/>';
        }
        if ($hasErrors) {
            error_log('CreditSafe Error: ' . $error_message . ' Parametri:' . json_encode($params, JSON_FORCE_OBJECT));
        }
    }

    //get Token
    public function getToken($username, $password, &$error)
    {
        $params = [
            'username' => $username,
            'password' => $password
        ];
        $res = $this->httpRequest($this->endpoints['authenticate']['ed'], $this->endpoints['authenticate']['post'], $params, $error);
        $this->errorHandler("authenticate", $params, $res, $error);
        return $res;
    }

    //get Company
    public function getCompany($vat, &$error)
    {
        $getparams = '?countries=IT&vatNo=' . $vat;
        $res = $this->httpRequest($this->endpoints['companies']['ed'] . $getparams, $this->endpoints['companies']['post'], [], $error);
        $this->errorHandler("companies", $getparams, $res, $error);
        return $res;
    }

    //get Company
    public function getReportCompany($csid, &$error)
    {
        $getparams = $csid . '?language=it&template=summary';
        $res = $this->httpRequest($this->endpoints['companyreport']['ed'] . $getparams, $this->endpoints['companyreport']['post'], [], $error);
        $this->errorHandler("companyreport", $getparams, $res, $error);
        return $res;
    }

    //add Company to Portfolio
    public function addCompanyToPortfolio($csid, $portfolioid, &$error)
    {
        $endpoint = str_replace(":portfolioId", $portfolioid, $this->endpoints['addcompanytoportfolio']['ed']);
        $params = [
            'id' => $csid,
            'personalReference' => 'Azienda Inserita da Portale',
            'freeText' => 'Azienda Inserita da Portale',
            'personalLimit' => '0'
        ];
        $res = $this->httpRequest($endpoint, $this->endpoints['addcompanytoportfolio']['post'], $params, $error);
        $this->errorHandler("addcompanytoportfolio", $params, $res, $error);
    }
}
