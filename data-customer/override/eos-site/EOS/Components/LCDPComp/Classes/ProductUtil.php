<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Classes;

use EOS\System\Util\DateTimeHelper;

class ProductUtil extends \EOS\System\Models\Model
{

    private $stockManager;

    private function getCategories(int $idProduct): array
    {
        $sql = 'select pc.id_category ' .
            ' from #__product_product_category pc ' .
            ' where pc.id_product = :id_product ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_product', $idProduct);
        $q->execute();
        $res = [];
        $list = $q->fetchAll();
        foreach ($list as $r)
        {
            $res[] = (int) $r['id_category'];
        }
        return $res;
    }

    public function __construct($container)
    {
        parent::__construct($container);
        $this->stockManager = new \EOS\Components\Product\Classes\StockManager($this->container);
    }

    public function getInfo(int $idProduct): array
    {
        $sql = 'select p.id, p.id_manufacturer, p.price, p.on_sale, p.pack, p.quantity_per_pack ' .
            ' from #__product p ' .
            ' where p.id = :id_product ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_product', $idProduct);
        $q->execute();
        $res = $q->fetch();
        if (!empty($res))
        {
            $res['stock'] = $this->stockManager->getDBStock($idProduct, 0);
            $res['categories'] = $this->getCategories($idProduct);
        }
        return empty($res) ? [] : $res;
    }

}
