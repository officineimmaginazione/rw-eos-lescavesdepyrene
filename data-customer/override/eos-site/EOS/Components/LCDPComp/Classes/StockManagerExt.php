<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Classes;

use EOS\System\Util\DateTimeHelper;

class StockManagerExt extends \EOS\Components\Product\Classes\StockManager
{

    private $defDate;
    private $incomingDays;
    private $maxIncomingDate;
    private $negativeStock = true;

    public function __construct($container)
    {
        parent::__construct($container);
        $incDay = $this->db->setting->getInt('lcdpcomp', 'stock.days', 0); // Giorni da cui considerare giacenza
        $this->defDate = DateTimeHelper::incDay(DateTimeHelper::date(), $incDay);
        $this->incomingDays = $this->db->setting->getInt('lcdpcomp', 'stock.incoming.days', 60); // Giorni da considerare come in arrivo
        //
        $this->maxIncomingDate = DateTimeHelper::dateOf((new \DateTimeImmutable())->setDate(2099, 12, 31));
        $this->negativeStock = $this->db->setting->getBool('lcdpcomp', 'stock.negative', false);
    }

    public function getStockDate(): \DateTimeImmutable
    {
        return $this->defDate;
    }

    public function getAcceptOutOfStock(int $idProduct): bool
    {
        $q = $this->db->prepare('select id, out_of_stock from #__product' .
            ' where id = :id_product');
        $q->bindValue(':id_product', $idProduct, \EOS\System\Database\Database::PARAM_INT);
        $q->execute();
        $r = $q->fetch();
        $res = false;
        if (!empty($r))
        {
            $v = (int) $r['out_of_stock'];
            if ($v === 2)
            {
                $v = (int) $this->db->setting->getInt('lcdpcomp', 'stock.outofstock', 0);
            }
            $res = $v === 1;
        }
        return $res;
    }

    public function getStock2(int $idProduct, float $decQty = 0, ?\DateTimeImmutable $date = null): array
    {
        $stock = $this->getStock($idProduct, 0, true);
        if ($decQty > 0)
        {
            $this->decStockArrayQty($stock, $decQty);
        }
        $res = [];
        $res['stock-available'] = $stock['quantity_available'];
        // Può succedere di avere meno quantità dell'ordinato e devo ristribuire
        $res['stock-quantity'] = $this->roundDecimal($stock['quantity_available'] - $stock['quantity_ordered1']);
        $res['stock-incoming'] = $stock['quantity_ordered1'];
        if ($res['stock-quantity'] < 0)
        {
            $res['stock-incoming'] = $this->roundDecimal($res['stock-incoming'] + $res['stock-quantity']);
            $res['stock-quantity'] = 0;
        }
        if ($res['stock-incoming'] < 0)
        {
            $res['stock-quantity'] = $this->roundDecimal($res['stock-quantity'] + $res['stock-incoming']);
            $res['stock-incoming'] = 0;
        }
        $res['stock-incoming-date'] = $stock['date_ordered1'];
        if (is_null($date))
        {
            $date = $this->defDate;
        }
        if (!empty($res['stock-incoming-date']))
        {
            // Se la data di arrivo è minore o uguale alla data previsto imposto tutto come fosse a magazzino
            if ($res['stock-incoming-date'] <= $date)
            {
                $res['stock-quantity'] = $this->roundDecimal($res['stock-quantity'] + $res['stock-incoming']);
                $res['stock-incoming'] = 0;
                $res['stock-incoming-date'] = null;
            }
        }
        if (!$this->negativeStock)
        {
            if ($res['stock-quantity'] < 0)
            {
                $res['stock-quantity'] = 0;
            }

            if ($res['stock-incoming'] < 0)
            {
                $res['stock-incoming'] = 0;
            }
        }
        // Ricalcolo il disponibile
        $res['stock-available'] = $this->roundDecimal($res['stock-quantity'] + $res['stock-incoming']);
        // Verifico se accettare ordini quando esaurisce la merce
        $res['stock-accept-out-of-stock'] = $this->getAcceptOutOfStock($idProduct);
        $res['stock-is-out-of-stock'] = false;
        // 2019-06-12 - Forzo sempre lo stock a un valore altissimo
        if ($res['stock-accept-out-of-stock'] /* && $res['stock-available'] <= 0 */)
        {
            // Imposto un valore altissimo per fa avanzare l'ordine
            $res['stock-available'] = $this->db->setting->getInt('lcdpcomp', 'stock.outofstock.qty', 100000);
            $res['stock-is-out-of-stock'] = true;
        }
        return $res;
    }

    public function getStock2Str(int $idProduct, float $decQty = 0, ?\DateTimeImmutable $date = null): array
    {
        // Leggo lo stock
        $res = $this->getStock2($idProduct, $decQty, $date);
        $res['stock-incoming-date-lng'] = '';
        $res['stock-incoming-defined'] = false;
        if (!empty($res['stock-incoming-date']))
        {
            $res['stock-incoming-date-lng'] = $this->lang->dateTimeToLangDate($res['stock-incoming-date']);
            $res['stock-incoming-defined'] = ($res['stock-incoming-date'] < $this->maxIncomingDate);
        }

        $res['stock-quantity-str'] = $res['stock-quantity'] . ' in stock';
        $res['stock-incoming-str'] = '';
        if ($res['stock-incoming'] > 0)
        {
            if ($res['stock-incoming-defined'])
            {
                $res['stock-incoming-str'] = $res['stock-incoming'] . ' in arrivo il ' . $res['stock-incoming-date-lng'];
            } else
            {
                $res['stock-incoming-str'] = $res['stock-incoming'] . ' data da destinarsi';
            }
        }
        // Verifico se accetto ordini sotto scorta
        if ($res['stock-is-out-of-stock'] && $res['stock-quantity'] <= 0)
        {
            $res['stock-quantity-str'] = 'Non ancora in stock';
        }

        return $res;
    }

    public function calcRowsStock(array &$data, ?\DateTimeImmutable $dateIncoming = null)
    {
        $currentDate = DateTimeHelper::date();
        $qtyList = [];
        foreach ($data as &$r)
        {
            $idProduct = (int) $r['id_product'];
            if (!isset($qtyList[$idProduct]))
            {
                $qtyList[$idProduct] = 0;
            }
            $decQty = $qtyList[$idProduct];
            // Leggo lo stock
            $stock = $this->getStock2Str($idProduct, $decQty);
            $r['stock'] = $stock;
            // Salvo le quantità già utilizzate
            $qty = (float) $r['quantity'];
            $qtyList[$idProduct] = $qtyList[$idProduct] + $qty;
            $stockStatus = [];
            // Calcolo la differenza di giorni dell'ordinato originale
            $stockStatus['original-incoming-diff'] = 0;
            $dtIncoming = $stock['stock-incoming-date'];
            if (!empty($dtIncoming))
            {
                if ($dtIncoming >= $currentDate)
                {
                    $stockStatus['original-incoming-diff'] = $currentDate->diff($dtIncoming)->days;
                }
                $stockStatus['original-incoming-date'] = $dtIncoming;
            }
            else
            {
                $stockStatus['original-incoming-date'] = $currentDate;
            }
            // Se ho una data di arrivo ricalcolo la giacenza senza salvarla
            if (!is_null($dateIncoming))
            {
                // Ricalcolo lo stock alla data
                // facendo così "stock-incoming" potrebbe essere inglobato in "stock-quantity"
                // e il codice dopo lo vede come disponibile oppure continua a lavorare sulla data
                $stock = $this->getStock2Str($idProduct, $decQty, $dateIncoming);
            }

            // Divido in 3 quantità
            // 1 disponbile
            // 2 <= 60 giorni
            // 3 > 60 giorni
            // Se ho una data di consegna ricalcolo lo stock a quella data

            $dtIncoming = $stock['stock-incoming-date'];
            if ($qty > $stock['stock-available'])
            {
                $qty = $stock['stock-available'];
            }
            $qtyStock = $stock['stock-quantity'];
            if ($qty <= $qtyStock)
            {
                $stockStatus['quantity1'] = $qty;
                $qty = 0;
            } else
            {
                $stockStatus['quantity1'] = $qtyStock;
                $qty = $qty - $qtyStock;
            }
            $stockStatus['quantity2'] = 0;
            $stockStatus['quantity3'] = 0;
            $stockStatus['incoming-days'] = $this->incomingDays;
            $stockStatus['incoming-diff'] = 0;
            // Se per qualche motivo la data è nulla imposto il valore più lontano
            if (empty($dtIncoming))
            {
                $stockStatus['quantity3'] = $qty;
            } else
            {
                if ($dtIncoming > DateTimeHelper::incDay($currentDate, $stockStatus['incoming-days']))
                {
                    $stockStatus['quantity3'] = $qty;
                } else
                {
                    $stockStatus['quantity2'] = $qty;
                }
                if ($dtIncoming >= $currentDate)
                {
                    $stockStatus['incoming-diff'] = $currentDate->diff($dtIncoming)->days;
                }
            }
            $qList = [];
            if (($stockStatus['quantity2'] != 0 || $stockStatus['quantity3'] != 0) && $stockStatus['quantity1'] != 0)
            {
                $qList[] = $stockStatus['quantity1'] . ' prodotto/i disponibile/i nella 1° spedizione';
            }
            if ($stockStatus['quantity2'] != 0)
            {
                $qList[] = $stockStatus['quantity2'] . ' prodotto/i consegna entro ' . $stockStatus['incoming-days'] . ' giorni';
            }
            if ($stockStatus['quantity3'] != 0)
            {
                //$qList[] = $stockStatus['quantity3'] . ' consegna oltre ' . $stockStatus['incoming-days'] . ' giorni';
                $qList[] = $stockStatus['quantity3'] . ' prodotto/i a data data da destinarsi';
            }

            $stockStatus['quantity-str-list'] = $qList;
            $r['stock-status'] = $stockStatus;
        }
        unset($r);
    }

}
