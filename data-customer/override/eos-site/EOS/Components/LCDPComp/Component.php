<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp;

class Component extends \EOS\System\Component\Component
{

    public function load()
    {
        parent::load();
        // Aggiungo il subscriber per i componenti
        if ($this->application->componentManager->componentExists('Cart'))
        {
            $this->getEventDispatcher()->addSubscriber(new Classes\CartSubscriber($this->getContainer()));
        }
    }

}
