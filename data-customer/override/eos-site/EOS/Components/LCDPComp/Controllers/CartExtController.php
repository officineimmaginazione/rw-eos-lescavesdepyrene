<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Controllers;

use EOS\System\Util\FormValidator;

class CartExtController extends \EOS\Components\System\Classes\SiteController
{

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        if ($fv->prepareInputDataFromJson()) {
            $data = $fv->getInputData();
            $model = new \EOS\Components\LCDPComp\Models\CartExtModel($this->container);
            if (isset($data['command'])) {
                switch ($data['command']) {
                    case 'calcfreerows':
                        $model->calcFreeRows();
                        $fv->setResult(true);
                        $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                        break;
                    case 'deletefreerows':
                        $model->deleteFreeRows();
                        $fv->setResult(true);
                        $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                        break;
                    case 'updatefreerow':
                        $model->updateFreeRow($data);
                        $fv->setResult(true);
                        $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                    default:
                        $fv->setMessage($this->lang->transEncode('system.common.invalid.command'));
                        break;
                }
            } else {
                $fv->setMessage($this->lang->transEncode('system.common.invalid.command'));
            }
        }
        return $fv->toJsonResponse();
    }
}
