<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Models;

use EOS\System\Util\ArrayHelper;

class CartExtModel extends \EOS\System\Models\Model
{

    protected $debugCalc;
    protected $checkStock;
    protected $lockCartStockMinutes;
    protected $sortPriceAsc;
    protected $stockDate = null;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->checkStock = $this->db->setting->getBool('lcdpcomp', 'cart.checkstock', true);
        $this->debugCalc = false; // Usato per testare i sotto zero
        // 2019-04-17 - Il cliente ha detto che si parte dal prezzo più alto per ordinare/calcolare i blocchi!
        $this->sortPriceAsc = $this->db->setting->getBool('lcdpcomp', 'free.block.sortpriceasc', false);
    }

    private function getRowOptions(int $cartID, int $rowID): array
    {
        $sql = 'select cr.* ' .
        ' from #__cart_row cr ' .
        ' where cr.id_cart = :id_cart and cr.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $cartID);
        $q->bindValue(':id', $rowID);
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            throw new \Exception('Errore di sistema manca la riga del carrello!!!!!');
        }
        $options = ArrayHelper::fromJSON($r['options']);   
        return $options; 
    }

    private function setRowOptions(int $cartID, int $rowID, array $options): void
    {
        $sqlUpdate = 'update #__cart_row set options = :options' .
            ' where id_cart = :id_cart and id = :id';
        $qUp = $this->db->prepare($sqlUpdate);
        $qUp->bindValue(':id_cart', $cartID);
        $qUp->bindValue(':id', $rowID);
        $qUp->bindValue(':options', ArrayHelper::toJSON($options));
        $qUp->execute();
    }


    private function getCartFreeProduct(array $list): int
    {
        $res = 0;
        $lastPrice = PHP_FLOAT_MAX;
        foreach ($list as $r)
        {
            $price = (float) $r['price'];
            if ($price < $lastPrice)
            {
                // NB: Devo verificare che sia disponibile!
                $res = $r['id_product'];
                $lastPrice = $price;
            }
        }
        return $res;
    }

    // Hook SQL per aggiungere parametro "isfree"
    private function addFreeRowFlag(int $cartID, int $rowID, bool $changeProduct, float $qty, array $parents)
    {   
        $sql = 'select cr.* ' .
            ' from #__cart_row cr ' .
            ' where cr.id_cart = :id_cart and cr.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_cart', $cartID);
        $q->bindValue(':id', $rowID);
        $q->execute();
        $r = $q->fetch();
        if (empty($r))
        {
            throw new \Exception('Errore di sistema manca la riga del carrello!!!!!');
        }
        $options = ArrayHelper::fromJSON($r['options']);
        
        $options['data']['isfree'] = true;
        $options['data']['change-product'] = $changeProduct;
        $options['data']['free-qty'] = $qty;
        $options['data']['parents'] = $parents;
        $options['data']['id-product-free'] = (int) $r['id_product'];

        $sqlUpdate = 'update #__cart_row set options = :options' .
            ' where id_cart = :id_cart and id = :id';
        $qUp = $this->db->prepare($sqlUpdate);
        $qUp->bindValue(':id_cart', $cartID);
        $qUp->bindValue(':id', $rowID);
        $qUp->bindValue(':options', ArrayHelper::toJSON($options));
        $qUp->execute();
    }

    private function sortByPrice(array &$list)
    {
        if ($this->sortPriceAsc)
        {
            // Crescente
            $sortFunc = function ($a, $b)
            {
                return ($a['price'] < $b['price']) ? -1 : 1;
            };
        } else
        {
            // Decrescente
            $sortFunc = function ($a, $b)
            {
                return ($a['price'] > $b['price']) ? -1 : 1;
            };
        }
        usort($list, $sortFunc);
    }

    private function setGlobalQty(int $id, float $qty, array &$globalList)
    {
        foreach ($globalList as $k => &$r)
        {
            if ($r['id'] === $id)
            {
                $r['quantity'] = $qty;
                if (($this->debugCalc) && ($qty < 0))
                {
                    throw new \Exception('Quantità sotto zero: ' . json_encode($r));
                }
                if ($qty <= 0)
                {
                    unset($globalList[$k]);
                }
                break;
            }
        }
        unset($r);
    }

    private function getPack(int $id, array &$globalList)
    {
        $pack = [];
        foreach ($globalList as $k => &$r)
        {
            if ($r['id'] === $id)
            {
                $pack['pack'] = $r['pack'];
                $pack['quantity_per_pack'] = $r['quantity_per_pack'];
                break;
            }
        }
        unset($r);
        return $pack;
    }

    private function addFreePart(array $calcList, array $rule, float $qtyFree, array &$addList)
    {
        $idProduct = $this->getCartFreeProduct($calcList);
        if (!empty($idProduct))
        {
            if (isset($addList[$idProduct]))
            {
                $qtyFree = $qtyFree + $addList[$idProduct]['quantity'];
            }
            if ($qtyFree < 0)
            {
                $qtyFree = 0;
            }
            $addList[$idProduct]['quantity'] = (float) $qtyFree;
            $addList[$idProduct]['change_product'] = (int) $rule['change_product'] === 1;
            if (!isset($addList[$idProduct]['parents']))
            {
                $addList[$idProduct]['parents'] = [];
            }
            $addList[$idProduct]['parents'] = array_unique(array_merge($addList[$idProduct]['parents'], array_column($calcList, 'id')));
        }
    }
    
    private function calcFreePartQty(array $calcList, array $rule, array &$addList, array &$globalList): void
    {
        $qtyCalc = $rule['qty_calc'];
        $qtyFree = $rule['qty_free'];
        if ((!empty($calcList)) && ($qtyCalc > 0) && ($qtyFree <= $qtyCalc))
        {
            $this->sortByPrice($calcList);
            $sum = 0;
            $partialList = [];
            foreach ($calcList as $r)
            {
                $sum = $sum + $r['quantity'];
                $mul = $sum / $qtyCalc;
                if ($mul >= 1)
                {
                    // 60 + 20 = 80 / 66 = 1,2121 
                    // 80-66 = 14 
                    $qtyFreeCalc = $qtyFree * floor($mul);
                    // Calcolo la rimanenza
                    $sum = $sum - ($qtyCalc * floor($mul));
                    $r['quantity'] = $sum;
                    $partialList[] = $r;
                    $this->addFreePart($partialList, $rule, $qtyFreeCalc, $addList);
                    // Imposto la nuova quantità sulle righe
                    foreach ($partialList as $plr)
                    {
                        $this->setGlobalQty($plr['id'], $plr['quantity'], $globalList);
                    }
                    // Rivalorizza la riga precedente se ho del residuo
                    $partialList = $sum > 0 ? [$r] : [];
                } else
                {
                    $r['quantity'] = 0;
                    $partialList[] = $r;
                }
            }
        }
    }

    private function calcFreePartTotalDiscount(array $calcList, array $rule, float &$globalDiscount, array &$globalList): void
    {
        $qtyCalc = (int) $rule['qty_calc'];
        $qtyDiscount = (float) $rule['qty_free'];
        //print_r($qtyDiscount); exit();
        if ((!empty($calcList)) && ($qtyCalc > 0) && ($qtyDiscount > 0))
        {
            $sum = 0;
            foreach ($calcList as $r)
            {
                $pack = $this->getPack($r['id'], $globalList);
                if($pack['pack'] == 0) {
                    $sum = $sum + $r['quantity'];
                } else {
                    $sum = $sum + ($r['quantity'] * $pack['quantity_per_pack']);
                }
            }
            if ($sum >= $qtyCalc)
            {
                $globalDiscount = $qtyDiscount;
            }
        }
        
    }

    private function calcFreePartRowDiscount(array $calcList, array $rule, array &$discountList, array &$globalList): void
    {
        $qtyCalc = (int) $rule['qty_calc'];
        $qtyDiscount = (float) $rule['qty_free'];
        if ((!empty($calcList)) && ($qtyCalc > 0) && ($qtyDiscount > 0))
        {
            $sum = 0;
            foreach ($calcList as $r)
            {
                $pack = $this->getPack($r['id'], $globalList);
                if($pack['pack'] == 0) {
                    $sum = $sum + $r['quantity'];
                } else {
                    $sum = $sum + ($r['quantity'] * $pack['quantity_per_pack']);
                }
            }
            if ($sum >= $qtyCalc)
            {
                foreach ($calcList as $r)
                {
                    $discountList[$r['id']] = $qtyDiscount;
                    $this->setGlobalQty($r['id'], 0, $globalList);     
                }
            }
        }
    }

    // 2020-04-16 - Esistono 2 modalità per ottenere lo sconto o l'omaggio
    private function calcFreePart(array $calcList, array $rule, array &$addList, array &$globalList, float &$globalDiscount, array &$discountList): void
    {
        $mode = (int) $rule['qty_free_mode'];
        switch ($mode)
        { 
            case 2:
                // Calcola lo sconto su tutta la spedizione (eccetto omaggi) 
                $this->calcFreePartTotalDiscount($calcList, $rule, $globalDiscount, $globalList);
            break;
            case 3:
                // Calcola lo sconto raggiunta una certa quantità e viene applicato alle righe
                $this->calcFreePartRowDiscount($calcList, $rule, $discountList, $globalList);
            break;
            default:
                // Se 0 o 1 calcolo gli omaggi
                $this->calcFreePartQty($calcList, $rule, $addList, $globalList);
            break;
        }
    }

    private function getFreeRulesItems(string $itemType, array $list): array
    {
        $res = [];
        foreach ($list as $r)
        {
            if ($itemType === $r['item_type'])
            {
                $res[] = (int) $r['id_item_type'];
            }
        }
        return $res;
    }

    private function getFreeRules(): array
    {
        $cartModel = new \EOS\Components\Cart\Models\CartModel($this->container);
        $cart = $cartModel->getCart();
        //$idAddress = (int) $cart['id_address_delivery'];
        $idAccount = (int) $cart['id_customer'];
        $sql = 'select id, name, free_calc_type, qty_calc, qty_free, qty_free_mode,
              product_include, product_exclude, change_product, stock_incoming
             from #__lcdp_cart_rule_free  
              where status = 1 and :data between date_start and date_end
              order by priority asc, free_calc_type asc, qty_calc asc ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':data', $this->db->util->nowToDBDateTime());
        $q->execute();
        $list = $q->fetchAll();

        $sqlDetail = 'select item_type, id_item_type  
              from #__lcdp_cart_rule_free_item
              where id_rule = :id_rule and link_type = :link_type';
        $qDetail = $this->db->prepare($sqlDetail);
        $resList = [];
        foreach ($list as $r)
        {
            $addRes = true;
            $r['qty_calc'] = (int) $r['qty_calc'];
            $r['qty_free'] = (int) $r['qty_free'];
            $r['free_calc_type'] = (int) $r['free_calc_type'];
            $r['product_include'] = (int) $r['product_include'];
            $r['product_exclude'] = (int) $r['product_exclude'];
            $r['_i_products'] = [];
            $r['_i_manufacturers'] = [];
            $r['_e_products'] = [];
            $r['_e_manufacturers'] = [];
            // Includi
            $qDetail->bindValue(':id_rule', $r['id']);
            $qDetail->bindValue(':link_type', 1);
            $qDetail->execute();
            $tmpList = $qDetail->fetchAll();
            
        
            if ($r['product_include'] === 1)
            {

                $r['_i_products'] = $this->getFreeRulesItems('product', $tmpList);
                $r['_i_manufacturers'] = $this->getFreeRulesItems('manufacturer', $tmpList);
                $r['_i_categories'] = $this->getFreeRulesItems('category', $tmpList);
            }
            /* $iAList = $this->getFreeRulesItems('address', $tmpList);
            if (!empty($iAList))
            {
                if (!in_array($idAddress, $iAList))
                {
                    $addRes = false;
                }
            } */
            $iAList = $this->getFreeRulesItems('account', $tmpList);
            if (!empty($iAList))
            {
                if (!in_array($idAccount, $iAList))
                {
                    $addRes = false;
                }
            }
            // Escludi
            $qDetail->bindValue(':id_rule', $r['id']);
            $qDetail->bindValue(':link_type', 2);
            $qDetail->execute();
            $tmpList = $qDetail->fetchAll();
            if ($r['product_exclude'] === 1)
            {
                $r['_e_products'] = $this->getFreeRulesItems('product', $tmpList);
                $r['_e_manufacturers'] = $this->getFreeRulesItems('manufacturer', $tmpList);
                $r['_e_categories'] = $this->getFreeRulesItems('category', $tmpList);
            }
            /*
            $eAList = $this->getFreeRulesItems('address', $tmpList);
            if (!empty($eAList))
            {
                if (in_array($idAddress, $eAList))
                {
                    $addRes = false;
                }
            }
            */
            $eAList = $this->getFreeRulesItems('account', $tmpList);
            if (!empty($eAList))
            {
                if (in_array($idAccount, $eAList))
                {
                    $addRes = false;
                }
            }
            if ($addRes)
            {
                $resList[] = $r;
            }
        }
        return $resList;
    }

    private function inArray(array $values, array $list): bool
    {
        $res = false;
        foreach ($values as $v)
        {
            if (in_array($v, $list))
            {
                $res = true;
                break;
            }
        }
        return $res;
    }

    private function getStockValue(object $stManager, int $idProduct, float $decQty, string $stockField, float $qtyFree): float
    {
        if ($stockField === '')
        {
            $stockTmp = $stManager->getStock2($idProduct, 0, $this->stockDate);
            $stockField = $stockTmp['stock-quantity'] > 0 ? 'stock-quantity' : 'stock-incoming';
        }
        $stock = $stManager->getStock2($idProduct, $decQty, $this->stockDate);
        if ($stock['stock-accept-out-of-stock'])
        {
            $stockField = 'stock-available';
        }
        return $stock[$stockField];
    }

    // 2019-06-12: Creo una funzione che calcoli gli omaggi internamente
    private function internalCalcFreeRow(object $cart, array $rows, string $stockField = '', int $discountIndex = 0)
    {
        $pUtil = new \EOS\Components\LCDPComp\Classes\ProductUtil($this->container);
        $globalList = [];
        $quantityList = [];
        $globalDiscount = 0.0;
        // Copio le righe valide dentro la lista da elaborare
        foreach ($rows as $r)
        {
            $options = ArrayHelper::fromJSON($r['options']);
            if (ArrayHelper::getStr($options, 'type') === 'product')
            {
                $pInfo = $pUtil->getInfo($r['id_product']);
                if (!empty($pInfo))
                {
                    //On sale disabilitato perchè inclusioni ed esclusioni vengono fatte dalle regole
                    //$onSale = (int) $pInfo['on_sale'];
                    //if ($onSale !== 0)
                    //{
                    $r['id'] = (int) $r['id'];
                    $r['id_product'] = (int) $r['id_product'];
                    $r['price'] = (float) $pInfo['price'];
                    $r['quantity'] = (float) $r['quantity'];
                    $r['id_manufacturer'] = (int) $pInfo['id_manufacturer'];
                    $r['categories'] = $pInfo['categories'];
                    $r['pack'] = (int) $pInfo['pack'];
                    $r['quantity_per_pack'] = (int) $pInfo['quantity_per_pack'];
                    // Scalo il campione dal calcolo degli omaggi
                    if (ArrayHelper::getMdBool($options, ['data', 'issample'], false))
                    {
                        $r['quantity'] = $r['quantity'] - 1;
                    }
                    // Calcolo gli omaggi quando ho quantità maggiore di 0 (Così esclude i campioni a 1)
                    if ($r['quantity'] > 0)
                    {
                        $r['original_quantity'] = $r['quantity'];
                        $globalList[] = $r;
                        // Salvo le quantità iniziali
                        $idProduct = $r['id_product'];
                        if (isset($quantityList[$idProduct]))
                        {
                            $quantityList[$idProduct] = $quantityList[$idProduct] + $r['quantity'];
                        } else
                        {
                            $quantityList[$idProduct] = $r['quantity'];
                        }
                    }
                    //}
                }
            }
        }
        // 2020-04-17 Clono l'array delle righe valide per aggiornare le righe con il nuovo sconto percentuale
        $validList = $globalList;
        // Estrapolo gli omaggi attivi
        $addList = [];
        // Lista di righe scontate
        $discountList = [];
        $this->sortByPrice($globalList); // Ordino per prezzo
        $freeRules = $this->getFreeRules();
        foreach ($freeRules as $rule)
        {
            // Se la lista arriva vuota o si azzera con le regole esco dal ciclo
            if (empty($globalList))
            {
                break;
            }
            // Filtro la lista per i parametri dello sconto
            $filteredList = $this->getFilteredList($rule, $globalList);
            if (!empty($filteredList))
            {
                switch ($rule['free_calc_type'])
                {
                    case 1: // Prodotto
                        // Calcolo l'omaggio per singolo prodotto
                        foreach ($filteredList as $r)
                        {
                            $this->calcFreePart([$r], $rule, $addList, $globalList, $globalDiscount, $discountList);
                        }
                        break;
                    case 2: // Produttore
                        // Raggruppo le righe per produttore e calcolo gli omaggi
                        $manList = [];
                        foreach ($filteredList as $r)
                        {
                            $manList[$r['id_manufacturer']][] = $r;
                        }
                        foreach (array_values($manList) as $calcList)
                        {
                            $this->calcFreePart($calcList, $rule, $addList, $globalList, $globalDiscount, $discountList);
                        }
                        unset($manList);
                        break;
                    case 3: // Carrello
                        // Calcolo gli omaggi per tutto il carrello
                        $this->calcFreePart($filteredList, $rule, $addList, $globalList, $globalDiscount, $discountList);
                        break;
                }
            }
        }

        
        // Aggiungo gli omaggi al carrello
        $stManager = new \EOS\Components\LCDPComp\Classes\StockManagerExt($this->container);
        foreach ($addList as $k => $r)
        {
            if ($r['quantity'] > 0)
            {
                $qtyFree = $r['quantity'];
                if ($this->checkStock)
                {
                    $decQty = isset($quantityList[$k]) ? $quantityList[$k] : 0;
                    $stockValue = $this->getStockValue($stManager, $k, $decQty, $stockField, $qtyFree);
                    if ($qtyFree > $stockValue)
                    {
                        //2019-05-13 Abilitazione in base alla quantità
                        $r['change_product'] = true;
                        $qtyFree = $stockValue;
                    }
                }

                if ($qtyFree < 0)
                {
                    $qtyFree = 0;
                }

                //$idRow = $cart->addRow($k, $qtyFree);
                $idRow = $cart->saveDataRow($k, $qtyFree);
                $this->addFreeRowFlag($cart->id, $idRow, $r['change_product'], $r['quantity'], $r['parents']);
            }
        }

        // 2020-06-16 Aggiorno le righe valide con l'eventuale nuovo sconto
        foreach ($validList as $r)
        {
            $ops = $this->getRowOptions($cart->id, (int) $r['id']);
            //
            $extraDisc = ArrayHelper::getMdArray($ops, ['data', 'free-extra-discount']);
            if (empty($extraDisc))
            {
                $extraDisc = [0, 0];    
            }
            $extraDisc[$discountIndex] = $globalDiscount;
            $ops['data']['free-extra-discount'] = $extraDisc;
            //
            $rowDisc = ArrayHelper::getMdArray($ops, ['data', 'free-row-discount']);
            if (empty($rowDisc))
            {
                $rowDisc = [0, 0];    
            }
            if (isset($discountList[$r['id']]))
            {
                $rowDisc[$discountIndex] = $discountList[$r['id']];  
            }
            $ops['data']['free-row-discount'] = $rowDisc;
            $this->setRowOptions($cart->id, (int) $r['id'], $ops);

        }


        return $addList;
    }

    public function getFilteredList(array $rule, array $list): array
    {
        $res = [];
        foreach ($list as $r)
        {
            $add = true;
            if ($rule['product_include'] === 1)
            {
                $add = in_array($r['id_product'], $rule['_i_products']) ||
                    in_array($r['id_manufacturer'], $rule['_i_manufacturers']) ||
                    $this->inArray($r['categories'], $rule['_i_categories']);
            }
            if (($add) && ($rule['product_exclude'] === 1))
            {
                if (in_array($r['id_product'], $rule['_e_products']) ||
                    in_array($r['id_manufacturer'], $rule['_e_manufacturers']) ||
                    $this->inArray($r['categories'], $rule['_e_categories']))
                {
                    $add = false;
                }
            }
            if ($add)
            {
                $res[] = $r;
            }
        }
        return $res;
    }

    public function deleteFreeRows()
    {
        $cart = new \EOS\Components\Cart\Models\CartModel($this->container);
        if (!empty($cart->id))
        {
            $cartDB = $cart->getCart();
            if ($cartDB['status'] <= 1)
            {
                $rows = $cart->getDBRows();
                foreach ($rows as $r)
                {
                    $options = ArrayHelper::fromJSON($r['options']);
                    if (ArrayHelper::getStr($options, 'type') === 'product')
                    {
                        if (ArrayHelper::getMdBool($options, ['data', 'isfree']))
                        {
                            $cart->deleteRowById($r['id']);
                        }
                        else
                        {
                            // 2020-04-16 - Rimuovo il nuovo sconto dalle righe
                            $options['data']['free-extra-discount'] = [0,0];
                            // 2020-04-23 - Rimuovo il secondo nuovo sconto 
                            $options['data']['free-row-discount'] = [0,0];
                            $this->setRowOptions($cart->id, (int) $r['id'], $options); 
                        }
                    } 
                }
            }
        }
    }

    public function calcFreeRows(): array
    {
        $res = [];
        $this->deleteFreeRows();
        $cart = new \EOS\Components\Cart\Models\CartModel($this->container);
        $this->container['session']->set('cart.set.calcfree.'.$cart->id, 1);
        if (!empty($cart->id))
        {
            $cartDB = $cart->getCart();
            if ($cartDB['status'] <= 1)
            {
                // 2019-06-12 - Ricalcolo le spedizioni
                $objBuild = new \stdClass();
                $cart->prepareObjectAndSave($objBuild);
                $options = ArrayHelper::fromJSON($objBuild->head['options']);
                $shippingMethod = ArrayHelper::getMdInt($options, ['shipping', 'method']);
                // Assegno la data di competenza degli stock
                $this->stockDate = $cart->getShippingStockDate();
                //
                $rows = $cart->getDBRows();
                // Se ho una spedizione calcolo sulla prima disponibilità
                if ($objBuild->shippingCount === 1)
                {
                    // Se ho la spedizione il prima possibile ogni riga deve calcolare il campo di competenza
                    $res = $this->internalCalcFreeRow($cart, $rows, $shippingMethod === 0 ? '' : 'stock-available', 0);
                } else
                {
                    $rows1 = [];
                    $rows2 = [];
                    foreach ($rows as $r)
                    {
                        $options = ArrayHelper::fromJSON($r['options']);
                        $qty1 = (float) ArrayHelper::getMdValue($options, ['data', 'shipping-qty', 0]);
                        $qty2 = (float) ArrayHelper::getMdValue($options, ['data', 'shipping-qty', 1]);
                        if ($qty1 > 0)
                        {
                            $r['quantity'] = $qty1;
                            $rows1[] = $r;
                        }

                        if ($qty2 > 0)
                        {
                            $r['quantity'] = $qty2;
                            $rows2[] = $r;
                        }
                    }
                    $res = $this->internalCalcFreeRow($cart, $rows1, 'stock-quantity', 0);
                    $this->internalCalcFreeRow($cart, $rows2, 'stock-incoming', 1);
                }
            }
        }
        return $res;
    }

    public function updateFreeRow(array $data)
    {
        $idRow = ArrayHelper::getInt($data, 'idrow');
        $idProduct = ArrayHelper::getInt($data, 'idproduct');
        $cart = new \EOS\Components\Cart\Models\CartModel($this->container);

        $rows = $cart->getDBRows();
        $decQty = 0;
        $qtyFree = 0;
        foreach ($rows as $r)
        {
            $options = ArrayHelper::fromJSON($r['options']);
            $opData = ArrayHelper::getArray($options, 'data');
            // Cerco la riga per recuperare la quantity
            if ($idRow === (int) $r['id'])
            {
                $qtyFree = ArrayHelper::getFloat($opData, 'free-qty');
            } else
            {
                // Conteggio le quantità dello stesso prodotto
                if (ArrayHelper::getStr($options, 'type') === 'product')
                {
                    if (ArrayHelper::getInt($r, 'id_product') === $idProduct)
                    {
                        $decQty += (float) $r['quantity'];
                    }
                }
            }
        }
        $stManager = new \EOS\Components\LCDPComp\Classes\StockManagerExt($this->container);
        if ($this->checkStock)
        {
            $stock = $stManager->getStock2($idProduct, $decQty);
            if ($qtyFree > $stock['stock-available'])
            {
                $qtyFree = $stock['stock-available'];
            }
        }

        if ($qtyFree < 0)
        {
            $qtyFree = 0;
        }

        $sqlUpdate = 'update #__cart_row ' .
            'set id_product = :id_product, quantity = :quantity ' .
            ' where id_cart = :id_cart and id = :id';
        $qUp = $this->db->prepare($sqlUpdate);
        $qUp->bindValue(':id_product', $idProduct);
        $qUp->bindValue(':quantity', $qtyFree);
        $qUp->bindValue(':id_cart', $cart->id);
        $qUp->bindValue(':id', $idRow);
        $qUp->execute();
    }

}
