<?php

/**
EcoSoft e Officine Immaginazione Copyright (c) 2015-2020
www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\LCDPComp\Models;

use EOS\System\Util\ArrayHelper;
use EOS\Components\LCDPComp\Classes\CreditSafe;

class CreditSafeModel extends \EOS\System\Models\Model
{
    private $ws;
    private $username;
    private $password;
    private $portfolioid;

    public function __construct($container)
    {
        parent::__construct($container);
        $params = ArrayHelper::fromJSON($this->db->setting->getStr('lcdp', 'creditsafe', ''));

        if (_EOS_MODE_ == "prod") {
            $this->username = ArrayHelper::getStr($params, 'username', '');
            $this->password = ArrayHelper::getStr($params, 'password', '');
            $this->portfolioid = ArrayHelper::getStr($params, 'portfolioid', '1581676');
            $mode = ArrayHelper::getStr($params, 'mode', 'prod');
        } else {
            $this->username = ArrayHelper::getStr($params, 'username', 'info@lescaves.it');
            $this->password = ArrayHelper::getStr($params, 'password', 'RA3O0pcoq3(GV*pqJeVP3u');
            $this->portfolioid = ArrayHelper::getStr($params, 'portfolioid', '1581676');
            $mode = ArrayHelper::getStr($params, 'mode', 'dev');
        }

        $this->ws = new CreditSafe($this->username, $this->password, $mode);
    }

    public function companyCreditSafe($vat, string &$error)
    {
        $this->checkToken($error);
        $res = $this->ws->getCompany($vat, $error);
        return $res;
    }

    public function companyReportCreditSafe($csid, string &$error)
    {
        $this->checkToken($error);
        $res = $this->ws->getReportCompany($csid, $error);
        return $res;
    }

    public function addCompanyToPortfolioCreditSafe($csid, string &$error)
    {
        $this->checkToken($error);
        $res = $this->ws->addCompanyToPortfolio($csid, $this->portfolioid, $error);
        //return $res;
    }

    private function checkToken(&$error)
    {
        $tdt = strtotime($this->db->setting->getValue('lcdp', 'creditsafe.api.datetime'));
        $now = strtotime("now");
        $diff = $tdt - $now;
        $hours = $diff / (60 * 60);
        if ($hours < -1) {
            $res = $this->ws->getToken($this->username, $this->password, $error);
            $token = json_decode($res, true);
            $this->db->setting->setValue('lcdp', 'creditsafe.api.token', $token['token']);
            $this->db->setting->setValue('lcdp', 'creditsafe.api.datetime', strtotime("now"));
            if (!isset($this->ws->headersparams[1])) {
                array_push($this->ws->headersparams, 'Authorization: Bearer ' . $token['token']);
            }
        } else {
            $token = $this->db->setting->getValue('lcdp', 'creditsafe.api.token');
            array_push($this->ws->headersparams, 'Authorization: Bearer ' . $token['token']);
        }
    }
}
