<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Classes;

use EOS\System\Util\ArrayHelper;

class OrderSubscriber extends \EOS\System\Event\Subscriber
{

    public static function getSubscribedEvents()
    {
        return [
            \EOS\Components\Account\Classes\Events::USER_INDEX_TEMPLATE_RENDER => 'displayOrderRender'];
    }

    public function displayOrderRender(\EOS\System\Event\EchoEvent $event)
    {
        $view = $event->getSubject();
        $customer = (int)$view->request->getQueryParam('customer', 0);
        $w = new \EOS\Components\Order\Widgets\DisplayOrderWidget($view);
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $w->viewagent = false;
        if(isset($view->controller->user)) {
            $w->user = $view->controller->user->getInfo();
            $w->userlabel = $view->user->username;
        }
        if(in_array(2, $view->user->groups)){
            $w->viewagent = true;
        }
        if(isset($view->controller->user)) {
            $user = $view->controller->user->getInfo();
            $id = $user->id;
        }
        if (strpos($user->username, 'direzionale') !== false) {
            $id = 0;
        }
        if($customer == 0) {
            $w->openDebit = $m->getOpenDebit($id, 200);
            $w->orderList = $m->getOrderByAccount($id, 200, false);
            $w->orderListConfirmed = $m->getOrderByAccount($id, 200, true);
        } else {
            $w->openDebit = $m->getOpenDebit($customer, 200);
            $w->orderList = $m->getOrderByAccount($customer, 200, false);
            $w->orderListConfirmed = $m->getOrderByAccount($customer, 200, true);
            $w->displaydebit = true;
        }
        //$w->listorder = $m->getOrderByAccount($id);
        $w->write();
    }

}
