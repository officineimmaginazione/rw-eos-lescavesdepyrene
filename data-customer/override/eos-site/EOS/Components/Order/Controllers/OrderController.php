<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Controllers;

use EOS\System\Util\FormValidator;
use EOS\System\Util\ArrayHelper;

class OrderController extends \EOS\Components\System\Classes\SiteController
{

    public function index($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mc = new \EOS\Components\Cart\Models\CartModel($this->container);
        $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        $v->product = $m->getProduct();
        $v->title = $this->lang->transEncode('order.order.title');
        $cart = $mc->getCart();
        if ($cart['id_customer'] != '') {
            $v->address = $ma->getData(ArrayHelper::getInt($cart, 'id_customer'));
        }
        $v->rows = $mc->getRows();
        $v->total = $cart['total'];
        $v->cartid = $cart['id'];
        // ??  require_once _EOS_PATH_SYSTEM_ . 'EOS' . DIRECTORY_SEPARATOR . 'System' . DIRECTORY_SEPARATOR . 'ClassLoader.php';
        $v->addMetaRaw('http-equiv_refresh', ['http-equiv' => 'refresh', 'content' => (19 * 60) . '; URL=' . $this->path->getUrlFor('order', 'order/index')]);
        // ??? $loader = new \EOS\System\ClassLoader();
        return $v->render('order/default/default', true);
    }

    public function details($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $v->datarow = $m->getDataRow($args['id'], $this->lang->getCurrent()->id, $v->viewagent);
        $v->data = $m->getDetails($args['id']);
        return $v->render('order/details/default', true);
    }

    public function listOrder($request, $response, $args)
    {
        $this->displayOrder($request, $response, $args, 'prof');
    }

    public function listOrderConfirmed($request, $response, $args)
    {
        $this->displayOrder($request, $response, $args, 'ordcl');
    }

    public function listProf($request, $response, $args)
    {
        $this->displayOrder($request, $response, $args, 'prof');
    }

    public function listResid($request, $response, $args)
    {
        $this->displayOrder($request, $response, $args, 'resid');
    }

    public function listOrdte($request, $response, $args)
    {
        $this->displayOrder($request, $response, $args, 'ordte');
    }

    public function listOrsos($request, $response, $args)
    {
        $this->displayOrder($request, $response, $args, 'orsos');
    }

    public function listProsc($request, $response, $args)
    {
        $this->displayOrder($request, $response, $args, 'prosc');
    }

    private function displayOrder($request, $response, $args, $status)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        if ($v->user->id == 8363 || $v->user->id == 10004 || $v->user->id == 10005) {
            $id = (int)$request->getQueryParam('customer', 0);
        } else {
            $id = (int)$request->getQueryParam('customer', $v->user->id);
        }
        $v->customer = $id;
        switch ($status) {
            case 'prof':
                $v->orderList = $m->getOrderByAccount($id, 200, false, '2');
                $v->title = 'proforma';
                return $v->render('list/order', true);
                break;
            case 'resid':
                $v->orderList = $m->getOrderByAccount($id, 200, false, '5');
                $v->title = 'ordini attesa merce';
                return $v->render('list/order', true);
                break;
            case 'ordte':
                $v->orderList = $m->getOrderByAccount($id, 200, false, '1');
                $v->title = 'ordini temporanei';
                return $v->render('list/order', true);
                break;
            case 'orsos':
                $v->orderList = $m->getOrderByAccount($id, 200, false, '6');
                $v->title = 'ordini sospesi';
                return $v->render('list/order', true);
                break;
            case 'prosc':
                $v->orderList = $m->getOrderByAccount($id, 200, false, '7');
                $v->title = 'proforma scadute';
                return $v->render('list/order', true);
                break;
            case 'ordcl':
                $v->orderListConfirmed = $m->getOrderByAccount($id, 200, true, '4,8,11');
                $v->title = ($this->viewagent) ? 'ordini spediti' : 'ordini spediti';;
                return $v->render('list/orderconfirmed', true);
                break;
            default:
                $v->orderListConfirmed = $m->getOrderByAccount($id, 200, false, '1,2,3,4,5,6,7,8,9,10,11');
                $v->title = 'ordini in gestione';
                return $v->render('list/orderconfirmed', true);
                break;
        }
    }

    public function listDebit($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $id = (int)$request->getQueryParam('customer', 0);
        $v->customer = $id;
        $v->openDebit = $m->getOpenDebit($id, 200);
        return $v->render('list/opendebit', true);
    }

    public function listCashback($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Order\Models\CashbackModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $id = (int)$request->getQueryParam('customer', 0);
        $v->customer = $id;
        $v->cashbackList = $m->getCashbackByID($id, 200);
        return $v->render('list/cashback', true);
    }

    public function listCashbackAgent($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        \EOS\UI\Loader\Library::load($v, 'DataTablesBootstrap');
        $m = new \EOS\Components\Order\Models\CashbackModel($this->container);
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $id = (int)$request->getQueryParam('customer', 0);
        $v->customer = $id;
        $v->cashbackList = $m->getCashbackAdvisor($id, 200, (int) $v->user->id);
        return $v->render('list/cashbackadvisor', true);
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        $fv->sanitizeMultilineFields = ['notes'];
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson()) {
            $data = $fv->getInputData();
            $m = new \EOS\Components\Order\Models\OrderModel($this->container);
            if (isset($data['command'])) {
                switch ($data['command']) {
                    case 'savedata':
                        $error = '';
                        $price = 0;
                        if ($this->container->get('database')->setting->getValue('order', 'debug.save')) {
                            $lastid = $m->saveData($data, $error);
                            if (!empty($lastid)) {
                                $fv->setResult(true);
                                $fv->setRedirect($this->path->getUrlFor('order', 'order/confirm'));
                            } else {
                                $fv->setMessage($error);
                            }
                        } elseif (!$this->container->get('database')->setting->getValue('order', 'payment.status')) {
                            // Da verificare!!!!
                            if (isset($data['product-options'])) {
                                $product = $m->getProductByID((int) $data['product-options']);
                                $data['product-options-name'] = $product['name'];
                                $data['product-options-price'] = $product['price'];
                                $price = (float) $product['price'];
                            }
                            $product = $m->getProductByID((int) $data['product']);
                            $data['product-name'] = $product['name'];
                            $data['product-price'] = $product['price'];
                            $data['amount'] = ((float) $product['price'] + $price) * (int) $data['number-product'];
                            $emaildata = $m->getLocationEmail();
                            $sender = new \EOS\System\Mail\Mail($this->container);
                            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
                            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
                            $sender->to = $data['email'];
                            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
                            $sender->subject = $this->lang->transEncode('order.order.email.subject');
                            $sender->message = $this->createMail($data);
                            $lastid = $m->saveData($data, $error);
                            if ((isset($lastid) && $lastid != 0) && $sender->sendEmail($error)) {
                                $fv->setResult(true);
                                $fv->setRedirect($this->path->getUrlFor('order', 'order/confirm'));
                            } else {
                                $fv->setMessage($error);
                            }
                        } else {
                            if ($m->check($data, $error)) {

                                if ($data['type-payment'] == 'pagonline') {
                                    setcookie('orderdata', base64_encode(json_encode($data)), time() + 3600, "/");
                                    $payment = new \EOS\System\Payments\UnicreditPagonline($this->container);
                                    $paymentData = array('cart_id' => $data['cartid'], 'amount' => $data['amount']);
                                    $payres = $payment->payment($paymentData);
                                    if ($payres['result']) {
                                        $fv->setResult(true);
                                        $fv->setRedirect($payres['url']);
                                    } else {
                                        $fv->setMessage($payres['error']);
                                    }
                                } elseif ($data['type-payment'] == 'bankwire') {
                                    $emaildata = $m->getLocationEmail();
                                    $sender = new \EOS\System\Mail\Mail($this->container);
                                    $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
                                    $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
                                    $sender->to = $data['email'];
                                    $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
                                    $sender->subject = $this->lang->transEncode('order.order.email.subject');
                                    $sender->message = $this->createMail($data);
                                    $lastid = $m->saveData($data, $error);
                                    if ((isset($lastid) && $lastid != 0) && $sender->sendEmail($error)) {
                                        $fv->setResult(true);
                                        $fv->setRedirect($this->path->getUrlFor('order', 'order/confirm'));
                                    } else {
                                        $fv->setMessage($error);
                                    }
                                } elseif ($data['type-payment'] == 'paypal') {
                                    $paymentData = $data;
                                    $mc = new \EOS\Components\Order\Models\CartModel($this->container);
                                    $dataoptions = [];
                                    $dataoptions['id'] = $paymentData['cartid'];
                                    $dataoptions['options'] = json_encode($paymentData);
                                    $mc->updateOptions($dataoptions);
                                    $paymentData['cart-product'] = $cartProd = $mc->getRowsByCart($paymentData['cartid']);
                                    $countprod = 0;
                                    foreach ($cartProd as $row) {
                                        $countprod += $row['quantity'];
                                        $priceprod = $row['price'];
                                    }
                                    $npg = floor($countprod / 4);
                                    if ($countprod > 1) {
                                        $paymentData['discount'] = number_format(($npg * $priceprod) + (($countprod - $npg) * 2.00), 2);
                                    } else {
                                        $paymentData['discount'] = 0;
                                    }
                                    $payment = new \EOS\System\Payments\Paypal($this->container);
                                    $payres = $payment->paymentPaypal($paymentData);
                                    if ($payres['result']) {
                                        $fv->setResult(true);
                                        $fv->setRedirect($payres['url']);
                                    } else {
                                        $fv->setMessage($payres['error']);
                                    }
                                } elseif ($data['type-payment'] == 'xpay') {
                                    $paymentData = $data;
                                    setcookie('orderdata', base64_encode(json_encode($data)), time() + 3600, "/");
                                    $payment = new \EOS\System\Payments\XPay($this->container);
                                    //$paymentData = array('cart_id' => $data['cartid'], 'amount' => $data['amount']);
                                    $payres = $payment->payment($paymentData);
                                    if ($payres['result']) {
                                        $fv->setResult(true);
                                        $fv->setRedirect($payres['url']);
                                    } else {
                                        $fv->setMessage($payres['error']);
                                    }
                                }
                            } else {
                                $fv->setMessage($error);
                            }
                        }

                        break;
                    default:
                        $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
                        break;
                }
            } else {
                $fv->setMessage($this->lang->transEncode('order.order.error.invalidcommand'));
            }
        }
        return $fv->toJsonResponse();
    }

    public function verify($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = new \EOS\Components\Order\Models\OrderModel($this->container);
        $mc = new \EOS\Components\Order\Models\CartModel($this->container);
        $payment = new \EOS\System\Payments\UnicreditPagonline($this->container);
        $v->test = $this->lang->transEncode('order.order.title');
        $v->title = $this->lang->transEncode('order.verify.title');
        $verify = $payment->verifyPayment();
        if (isset($_COOKIE['orderdata']) && $verify['result']) {
            $cookie = $_COOKIE['orderdata'];
            $data = json_decode(base64_decode($cookie), true);
            $cartid = ((int) $_COOKIE['cart_id']);
            $data['cartid'] = $cartid;
            $data['txn-id'] = $verify['txn-id'];
            $mc = new \EOS\Components\Order\Models\CartModel($this->container);
            $data['cart-product'] = $mc->getRowsByCart($cartid);
            $data['status'] = 6;
            $lastid = $m->saveData($data, $error);
            $data['cart-product'] = $m->getRowsByOrder($lastid);
            $sender = new \EOS\System\Mail\Mail($this->container);
            $data['type-payment'] = $this->lang->transEncode('order.order.payment.creditcard');
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = $data['email'];
            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $lastid;
            $sender->message = $this->createMail($data);
            if ((isset($lastid) && $lastid != 0) && $sender->sendEmail($error)) {
                $v->titleorder = $this->lang->transEncode('order.verify.confirm.title');
                $v->description = $this->lang->transEncode('order.verify.confirm.description');
                $mc->cartToOrder($cartid);
            } else {
                $v->titleorder = $this->lang->transEncode('order.verify.error.title');
                $v->description = $this->lang->transEncode('order.verify.error.description');
            }
        }
        return $v->render('order/verify', true);
    }

    public function paypalipn()
    {
        $payment = new \EOS\System\Payments\Paypal($this->container);
        $payres = $payment->validate_ipn();
        if ($payres['result'] == true) {
            $ipndata = $payres['ipn_data'];
            $cartid = $ipndata['custom'];
            $data['txn-id'] = $ipndata['txn_id'];
            $m = new \EOS\Components\Cart\Models\CartModel($this->container);
            $ma = new \EOS\Components\Account\Models\AddressModel($this->container);
            $mua = new \EOS\Components\Account\Models\UserModel($this->container);
            $data = $m->getCartByID($cartid);
            $addressinvoice = $ma->getData($data['id_address_delivery']);
            $user = $mua->getCustomerInfo($data['id_customer']);
            $paymentData['amount'] = $data['total'];
            $paymentData['shipping-name'] = $addressinvoice['name'];
            $paymentData['shipping-surname'] = $addressinvoice['surname'];
            $paymentData['shipping-address'] = $addressinvoice['address1'];
            $paymentData['shipping-city'] = $addressinvoice['city'];
            //$paymentData['shipping-zipcode'] = $addressinvoice['zipcode'];
            $paymentData['shipping-phone'] = $addressinvoice['phone'];
            $paymentData['name'] = $addressinvoice['name'];
            $paymentData['surname'] = $addressinvoice['surname'];
            $paymentData['email'] = $user['email'];
            $dataoptions = [];
            $dataoptions['id'] = $cartid;
            $datacartoptions = json_decode($data['options']);
            $paymentData['cart-product'] = $cartProd = $m->getRowsByCart($cartid);
            $countprod = 0;
            $optionscart = json_decode($data['options'], true);
            $datacashback['cartid'] = $cartid;
            $datacashback['idcustomer'] = $data['id_customer'];
            $datacashback['total'] = $data['total'];
            $datacashback['totalTaxable'] = $data['total'] - (float) $optionscart['shipping']['total'];
            $amount = 0;
            foreach ($cartProd as $row) {
                $optionsrow = json_decode($row['options'], true);
                $countprod += $row['quantity'];
                $priceprod = $row['price'];
                $paymentData['cart-product'][0]['name'] = $row['name'];
                $paymentData['cart-product'][0]['price'] = $row['price'];
                $paymentData['cart-product'][0]['quantity'] = $row['quantity'];
                $paymentData['cart-product'][0]['item_number_' . $countprod] = $countprod;
                $countprod++;
            }
            //$lastid = $m->saveData($data, $error);
            $sender = new \EOS\System\Mail\Mail($this->container);
            $sender->from = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->fromname = $this->container->get('database')->setting->getValue('order', 'email.from.name');
            $sender->to = $user['email'];
            $sender->bcc = $this->container->get('database')->setting->getValue('order', 'email.from');
            $sender->subject = $this->lang->transEncode('order.order.email.subject') . ' ' . $cartid;
            $sender->message = $this->createMail($paymentData);
            if ($sender->sendEmail($error)) {
                $m->cashbackManage($datacashback);
                $m->cartToOrder($cartid);
            }
        }
    }

    public function confirm($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->test = $this->lang->transEncode('order.order.title');
        $v->title = $this->lang->transEncode('order.verify.title');
        $v->titleorder = $this->lang->transEncode('order.verify.confirm.title');
        $v->description = $this->lang->transEncode('order.verify.confirm.description');
        return $v->render('order/confirm', true);
    }

    public function error($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $v->test = $this->lang->transEncode('order.order.title');
        $v->title = $this->lang->transEncode('order.verify.title');
        $v->titleorder = $this->lang->transEncode('order.verify.error.title');
        $v->description = $this->lang->transEncode('order.verify.error.description');
        return $v->render('order/error', true);
    }

    public function createMail($data)
    {
        $m = new \EOS\Components\Reservation\Models\BookingModel($this->container);
        $content = '<!DOCTYPE html>';
        $content .= '<html lang="it" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">';
        $content .= '<head>';
        $content .= '<meta charset="utf-8">';
        $content .= '<meta name="viewport" content="width=device-width">';
        $content .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $content .= '<meta name="x-apple-disable-message-reformatting">';
        $content .= '<link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700" rel="stylesheet">';
        $content .= '<title>LesCavesDePyrene - Accettazione contratto presentatore</title>';
        $content .= "<style>body,html{margin:0 auto!important;padding:0!important;height:100%!important;width:100%!important;background:#f1f1f1}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*='margin: 16px 0']{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}img{-ms-interpolation-mode:bicubic}a{text-decoration:none}.aBn,.unstyle-auto-detected-links *,[x-apple-data-detectors]{border-bottom:0!important;cursor:default!important;color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}.a6S{display:none!important;opacity:.01!important}.im{color:inherit!important}img.g-img+div{display:none!important}@media only screen and (min-device-width:320px) and (max-device-width:374px){u~div .email-container{min-width:320px!important}}@media only screen and (min-device-width:375px) and (max-device-width:413px){u~div .email-container{min-width:375px!important}}@media only screen and (min-device-width:414px){u~div .email-container{min-width:414px!important}}.bg_white{background:#fff}.bg_light{background:#fafafa}.bg_black{background:#000}.bg_dark{background:rgba(0,0,0,.8)}.email-section{padding:2.5em}.btn{padding:10px 15px;display:inline-block}.btn.btn-primary{border-radius:5px;background:#8b2329;color:#fff}.btn.btn-white{border-radius:5px;background:#fff;color:#000}.btn.btn-white-outline{border-radius:5px;background:0 0;border:1px solid #fff;color:#fff}.btn.btn-black-outline{border-radius:0;background:0 0;border:2px solid #000;color:#000;font-weight:700}.btn-custom{color:rgba(0,0,0,.3);text-decoration:underline}h1,h2,h3,h4,h5,h6{font-family:'Work Sans',sans-serif;color:#000;margin-top:0;font-weight:400}p{color:rgba(0,0,0,.7)}body{font-family:'Work Sans',sans-serif;font-weight:400;font-size:15px;line-height:1.8;color:rgba(0,0,0,.7)}a{color:#8b2329}.logo h1{margin:0}.logo h1 a{color:#8b2329;font-size:24px;font-weight:700;font-family:'Work Sans',sans-serif}.hero{position:relative;z-index:0}.hero .text{color:rgba(0,0,0,.3)}.hero .text h2{color:#000;font-size:24px;margin-bottom:15px;font-weight:300;line-height:1.2}.hero .text h3{font-size:21px;font-weight:200}.hero .text h2 span{font-weight:600;color:#000}.product-entry{display:block;position:relative;float:left;padding-top:20px}.product-entry .text{width:calc(100% - 0px);padding-left:20px}.product-entry .text h3{margin-bottom:0;padding-bottom:0}.product-entry .text p{margin-top:0}.product-entry .text,.product-entry img{float:left}ul.social{padding:0}ul.social li{display:inline-block;margin-right:10px}.footer{border-top:1px solid rgba(0,0,0,.05);color:rgba(0,0,0,.5)}.footer .heading{color:#000;font-size:20px}.footer ul{margin:0;padding:0}.footer ul li{list-style:none;margin-bottom:10px}.footer ul li a{color:#000}</style>";
        $content .= '</head>';
        $content .= '<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f1f1f1;">';
        $content .= '<center style="width: 100%; background-color: #f1f1f1;">';
        $content .= '<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">';
        $content .= '</div>';
        $content .= '<div style="max-width: 600px; margin: 0 auto;" class="email-container">';
        /* Testata */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="top" class="bg_white" style="padding: 1em 2.5em 0 2.5em;">';
        $content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td class="logo" style="text-align: left;">';
        $content .= '<h1><a href="https://www.lescaves.it">LesCavesDePyrene</a></h1>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td valign="middle" class="hero bg_white" style="padding: 2em 0 2em 0;">';
        $content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="padding: 0 2.5em; text-align: left;">';
        $content .= '<div class="text">';
        if ($data['payment'] == 7) {
            $content .= '<h2>' . $this->lang->transEncode('order.order.confirm.email.msgbt') . '</h2>';
        } else {
            $content .= '<h2>' . $this->lang->transEncode('order.order.confirm.email.msg') . '</h2>';
        }
        $content .= '<h3>I tuoi dati</h3>';
        $content .= '<p>';
        if (isset($data['company']) && $data['company'] != '') {
            $content .= $this->lang->transEncode('order.order.company') . ': ' . $data['company'] . '<br>';
            $content .= $this->lang->transEncode('order.order.vat') . ': ' . $data['vat'] . '<br>';
        }
        (isset($data['cif']) && $data['cif'] != '') ? $content .= $this->lang->transEncode('order.order.cif') . ': ' . $data['cif'] . '<br>' : '';
        $content .= $this->lang->transEncode('order.order.email') . ': ' . $data['email'] . '<br>';
        //$content .= $this->lang->transEncode('order.order.address') . ': ' . $data['address'] . '<br>';
        //$content .= $this->lang->transEncode('order.order.city') . ': ' . $data['city'] . '<br>';
        $content .= '</p>';
        if (isset($data['shipping-name']) && $data['shipping-name'] != '') {
            $content .= '<h5>' . $this->lang->transEncode('order.order.datarecipient') . '</h5>';
            $content .= '<p>';
            (isset($data['shipping-name']) && $data['shipping-name'] != '') ? $content .= $this->lang->transEncode('order.order.name') . ': ' . $data['shipping-name'] . '<br>' : '';
            (isset($data['shipping-surname']) && $data['shipping-surname'] != '') ? $content .= $this->lang->transEncode('order.order.surname') . ': ' . $data['shipping-surname'] . '<br>' : '';
            (isset($data['shipping-email']) && $data['shipping-email'] != '') ? $content .= $this->lang->transEncode('order.order.email') . ': ' . $data['shipping-email'] . '<br>' : '';
            (isset($data['shipping-address']) && $data['shipping-address'] != '') ? $content .= $this->lang->transEncode('order.order.address') . ': ' . $data['shipping-address'] . '<br>' : '';
            (isset($data['shipping-city']) && $data['shipping-city'] != '') ? $content .= $this->lang->transEncode('order.order.city') . ': ' . $data['shipping-city'] . '<br>' : '';
            //(isset($data['shipping-zipcode']) && $data['shipping-zipcode'] != '') ? $content .= $this->lang->transEncode('order.order.zipcode') . ': ' . $data['shipping-zipcode'] . '<br>' : '';
            (isset($data['shipping-phone']) && $data['shipping-phone'] != '') ? $content .= $this->lang->transEncode('order.order.phone') . ': ' . $data['shipping-phone'] . '<br>' : '';
            $content .= '</p>';
        }
        //$privacy = (isset($data['consent1'])) ? $this->lang->transEncode('order.order.yes') : $this->lang->transEncode('order.order.no');
        //$content .= '<p>' . $this->lang->transEncode('order.order.email.privacy.consent1') . ' ' . $privacy . '</p>';
        $content .= '</div>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';

        $content .= '<tr>';
        $content .= '<table class="bg_white" role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">';
        $content .= '<tr style="border-bottom: 1px solid rgba(0,0,0,.05);">';
        $content .= '<th width="80%" style="text-align:left; padding: 0 2.5em; color: #000; padding-bottom: 20px">Prodotti</th>';
        $content .= '<th width="20%" style="text-align:right; padding: 0 2.5em; color: #000; padding-bottom: 20px">Prezzo</th>';
        $content .= '</tr>';
        /* Product */
        foreach ($data['cart-product'] as $row) {
            $options = json_decode($row['options'], true);
            $content .= '<tr style="border-bottom: 1px solid rgba(0,0,0,.05);">';
            $content .= '<td valign="middle" width="70%" style="text-align:left; padding: 0 2.5em;">';
            $content .= '<div class="product-entry">';
            //$content .= '<img src="" alt="" style="width: 100px; max-width: 600px; height: auto; margin-bottom: 20px; display: block;">';
            $content .= '<div class="text">';
            $content .= '<h3>' . $row['name'] . '</h3>';
            $content .= '<span>Quantità: ' . $row['quantity'] . '</span>';
            $content .= '<p></p>';
            $content .= '</div>';
            $content .= '</div>';
            $content .= '</td>';
            $content .= '<td valign="middle" width="30%" style="text-align:left; padding: 0 2.5em;">';
            $content .= '<span class="price" style="color: #000; font-size: 20px;">' . (float) $row['price'] . ' &euro;</span>';
            $content .= '</td>';
            $content .= '</tr>';
        }
        $content .= '<tr>';
        $content .= '<td valign="middle" style="text-align:left; padding: 1em 2.5em;">';
        $content .= '<h4>' . $this->lang->transEncode('order.order.amount') . ': ' . (float) $data['amount'] . ' &euro;</h4>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td valign="middle" style="text-align:left; padding: 1em 2.5em;">';
        //$content .= '<p><a href="#" class="btn btn-primary">Seguici su Facebook</a></p>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td valign="middle" style="text-align:left; padding: 1em 2.5em;">';
        if (isset($data['type-payment']) && $data['type-payment'] != '') {
            $content .= '<p>' . $this->lang->transEncode('order.order.payment.type') . ': ' . $data['type-payment'] . '</p>';
            if (isset($data['txn-id']) && $data['txn-id'] != '') {
                $content .= '<p>' . $this->lang->transEncode('order.order.txn-id') . ': ' . $data['txn-id'] . '</p>';
            }
        }
        if ($data['payment'] == 7) {
            $content .= '<p>Se hai scelto come metodo di pagamento il bonifico bancario di seguito l\'IBAN IT 98 X 03111 01008 000000008241, l\'intestazione Les Caves de Pyrene Srl e nella causale il numero dell\'ordine o la tua email.<br>Se hai già pagato con carta di credito o Paypal, non considerare questa parte.</p>';
        }
        $content .= '</table>';
        $content .= '</tr>';
        /* Footer */
        $content .= '<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">';
        $content .= '<tr>';
        $content .= '<td valign="middle" class="bg_light footer email-section">';
        $content .= '<table>';
        $content .= '<tr>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 5px; padding-right: 5px;">';
        $content .= '<h3 class="heading">Les Caves de Pyrene</h3>';
        $content .= '<ul>';
        $content .= '<li><span class="text">Strada Cagnassi, 8 - 12050 Rodello</span></a></li>';
        $content .= '<li><span class="text">Corso Susa 22/A - 10098 Rivoli</span></a></li>';
        $content .= '<li><span class="text">Tel. +39 0173 617072</span></a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '<td valign="top" width="33.333%" style="padding-top: 20px;">';
        $content .= '<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">';
        $content .= '<tr>';
        $content .= '<td style="text-align: left; padding-left: 10px;">';
        $content .= '<h3 class="heading">Link Utili</h3>';
        $content .= '<ul>';
        $content .= '<li><a href="https://www.lescaves.it">Chi siamo</a></li>';
        $content .= '</ul>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td class="bg_white" style="text-align: center;">';
        $content .= '<p>&nbsp;</p>';
        $content .= '</td>';
        $content .= '</tr>';
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</center>';
        $content .= '</body>';
        $content .= '</html>';
        return $content;
    }
}
