<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

use EOS\System\Util\ArrayHelper;

class CashbackModel extends \EOS\System\Models\Model
{

    public function getCashbackByID($id, $maxitem = 500, $idagent = 0)
    {
        if ($idagent !== 0) {
            $sql = 'select ech.*, au.name ' .
                ' from #__ext_cashback_history ech ' .
                ' left outer join #__cart c on ech.id_cart = c.id ' .
                ' left outer join #__account_user au on au.id = ech.id_user ';
        } else {
            $sql = 'select ech.*, au.name ' .
                ' from #__ext_cashback_history ech ' .
                ' left outer join #__cart c on ech.id_cart = c.id ' .
                ' left outer join #__account_user au on au.id = c.id_customer ';
        }
        if ($idagent !== 0 && $id === 0) {
            $sql .= ' where au.id in (select id_customer from eos_address where JSON_CONTAINS(eos_address.options, \'\"' . $idagent . '\"\', \'$.id_agent\')) ';
        } else {
            $sql .= ' where id_user = ' . $id;
        }
        $sql .= '  ' .
            ' order by id desc limit ' . $maxitem;
        $q = $this->db->prepare($sql);
        //$q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    public function getCashbackAdvisor($id, $maxitem = 500, $idagent = 0)
    {
        $sql = 'select au.id, au.email, au.name, json_unquote(json_extract(au.options, "$.cashback")) as cashback ' .
            ' from #__account_user au ';
        if ($idagent !== 8363 && $idagent !== 10004 && $idagent !== 10005) {
            $sql .= ' where au.id in (select id_customer from eos_address where JSON_CONTAINS(eos_address.options, \'\"' . $idagent . '\"\', \'$.id_agent\')) and json_unquote(json_extract(au.options, "$.cashback")) is not null and json_unquote(json_extract(au.options, "$.cashback")) > 0 ';
        } else {
            $sql .= ' where json_unquote(json_extract(au.options, "$.cashback")) is not null and json_unquote(json_extract(au.options, "$.cashback")) > 0 ';
        }
        $q = $this->db->prepare($sql);
        //$q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }
}
