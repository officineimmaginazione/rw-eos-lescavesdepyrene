<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Order\Models;

use EOS\System\Util\ArrayHelper;
use EOS\Components\Cart\Classes\CartHelper;

class OrderModel extends \EOS\System\Models\Model
{

    public function getProduct()
    {
        $sql = 'select p.id, p.price, pl.name, pl.description, ppc.id_category ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' inner join #__product_product_category ppc on p.id = ppc.id_product' .
            ' where pl.id_lang = :id_lang  order by p.id ASC';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    public function getProductByID($id)
    {
        $sql = 'select p.price as price, pl.name as name ' .
            ' from #__product p' .
            ' inner join #__product_lang pl on p.id = pl.id_product' .
            ' where p.id = :id and pl.id_lang = :id_lang  order by pl.name';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function saveData(&$data, &$error)
    {
        if (!$this->prepareData($data, $error)) {
            return false;
        } else {
            $this->db->beginTransaction();
            $tbl = $this->db->tableFix('#__order');
            $values = [
                //'name' => ArrayHelper::getStr($data['name']),
                //'surname' => ArrayHelper::getStr($data['surname']),
                'company' => ArrayHelper::getStr($data, 'company'),
                //'cif' => ArrayHelper::getStr($data, 'cif'),
                'vat' => ArrayHelper::getStr($data, 'vat'),
                'phone' => (isset($data['phone'])) ? $data['phone'] : '',
                //'email' => $data['email'],
                'address' => $data['address'],
                //'zip_code' => $data['zipcode'],
                'city' => $data['city'],
                'amount' => str_replace(',', '.', $data['total'])
            ];
            if (isset($data['status']) && $data['status'] != '') {
                $values['status'] = $data['status'];
            } else {
                $values['status'] = 3;
            }
            /* $values['privacy'] = (isset($data['consent1'])) ? 1 : 0;*/
            $options = ArrayHelper::getArray($data, 'custom-fields');
            $values['id_address_delivery'] = $data['id_address_delivery'];
            $values['id_address_invoice'] = $data['id_address_delivery'];
            $values['id_agent'] = $data['id_agent'];
            $values['options'] = $data['options'];
            $values['total_paid'] = str_replace(',', '.', $data['total']);
            /* Copia nelle options i campi personalizzati degli eventi */
            /* $options['shipping-name'] = ArrayHelper::getStr($data, 'shipping-name');
            $options['shipping-surname'] = ArrayHelper::getStr($data, 'shipping-surname');
            $options['shipping-phone'] = ArrayHelper::getStr($data, 'shipping-phone');
            $options['shipping-email'] = ArrayHelper::getStr($data, 'shipping-email');
            $options['shipping-address'] = ArrayHelper::getStr($data, 'shipping-address');
            $options['shipping-zip_code'] = ArrayHelper::getStr($data, 'shipping-zipcode');
            $options['shipping-city'] = ArrayHelper::getStr($data, 'shipping-city');
            $options['shipping-state'] = ArrayHelper::getStr($data, 'shipping-province');
            $options['shipping-country'] = ArrayHelper::getStr($data, 'shipping-country');
            $options['txn-id'] = ArrayHelper::getStr($data, 'txn-id');
            $options['cartid'] = ArrayHelper::getStr($data, 'cartid'); */
            //$values['options'] = json_encode($options);
            //$values['notes'] = $this->lang->transEncode('order.order.datarecipient').' '.$data['namedest'].' '.$data['surnamedest'].' '.$data['emaildest'].' '.$this->lang->transEncode('order.order.datarecipient').' '.$data['notes'].' '.$data['order-notes-add'];
            $values['ins_id'] = 0;
            $values['ins_date'] = new \FluentLiteral('NOW()');
            //print_r($values); exit();
            $query = $this->db->newFluent()->insertInto($tbl, $values);
            $query->execute();
            $lastid = $this->db->lastInsertId();
            $lastidorder = $this->db->lastInsertId();
            if (isset($data['number-product'])) {
                for ($i = 1; $i <= $data['number-product']; $i++) {
                    $tbl = $this->db->tableFix('#__order_row');
                    $values = [
                        'id_order' => $lastid,
                        'id_object' => $data['product'],
                        'product_name' => $data['product-name'],
                        'price' => $data['product-price']
                    ];
                    $values['ins_id'] = 0;
                    $values['ins_date'] = new \FluentLiteral('NOW()');
                    $query = $this->db->newFluent()->insertInto($tbl, $values);
                    $query->execute();
                }
                if (isset($data['product-options'])) {
                    for ($i = 1; $i <= $data['number-product']; $i++) {
                        $tbl = $this->db->tableFix('#__order_row');
                        $values = [
                            'id_order' => $lastid,
                            'id_object' => $data['product-options'],
                            'product_name' => $data['product-options-name'],
                            'price' => $data['product-options-price']
                        ];
                        $values['ins_id'] = 0;
                        $values['ins_date'] = new \FluentLiteral('NOW()');
                        $query = $this->db->newFluent()->insertInto($tbl, $values);
                        $query->execute();
                    }
                }
            } elseif ($data['cartid']) {
                $i = 0;
                $cartModel = new \EOS\Components\Cart\Models\CartModel($this->container);
                $data['cart-product'] = $cartModel->getRowsByCart($data['cartid']);
                foreach ($data['cart-product'] as $row) {
                    $tbl = $this->db->tableFix('#__order_row');
                    $typeObject = CartHelper::getCustomRowType($row);
                    if (empty($typeObject)) {
                        $options = ArrayHelper::fromJSON($row['options']);
                    } else {
                        // La struttura delle "options" del cart Custom è "type=>, data=>"
                        $options = CartHelper::getCustomRowData($row);
                    }

                    $values = [
                        'id_order' => $lastid,
                        'id_object' => $row['id_product'],
                        'type_object' => $typeObject,
                        'product_name' => $row['name'],
                        'price' => $row['price'],
                        'quantity' => $row['quantity'],
                        'options' => ArrayHelper::toJSON($options)
                    ];
                    $values['ins_id'] = 0;
                    $values['ins_date'] = new \FluentLiteral('NOW()');
                    $query = $this->db->newFluent()->insertInto($tbl, $values);
                    $query->execute();
                    $i++;
                }
            } else {
            }
            $this->db->commit();

            $ev = new \EOS\System\Event\DataEvent($this, ['id' => $lastidorder]);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_DATA_CREATE, $ev);
            return $lastidorder;
        }
    }

    private function prepareData(&$data, &$error)
    {
        $res = true;
        /* if (empty($data['name']))
        {
            $error = $this->lang->trans('order.order.error.name');
            $res = false;
        } else if (empty($data['surname']))
        {
            $error = $this->lang->trans('order.order.error.surname');
            $res = false;
        } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
        {
            $error = $this->lang->trans('order.order.error.email');
            $res = false;
        } else if (empty($data['address']))
        {
            $error = $this->lang->trans('order.order.error.address');
            $res = false;
        } else if (empty($data['city']))
        {
            $error = $this->lang->trans('order.order.error.city');
            $res = false;
        } else if (empty($data['province']))
        {
            $error = $this->lang->trans('order.order.error.province');
            $res = false;
        } else if (empty($data['zipcode']))
        {
            $error = $this->lang->trans('order.order.error.zipcode');
            $res = false;
        } else if (empty($data['country']))
        {
            $error = $this->lang->trans('order.order.error.country');
            $res = false;
        } else if (empty($data['phone']))
        {
            $error = $this->lang->trans('order.order.error.phone');
            $res = false;
        } else if (!isset($data['consent1']))
        {
            $error = $this->lang->trans('order.order.error.privacy');
            $res = false;
        } */


        if ($res) {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            // Rileggo i dati del prepare se modificati
            $data = $ev->getData();
        }

        return $res;
    }

    public function getLocationEmail()
    {
        $q = $this->db->prepare('select email, name from #__reservation_location');
        $q->execute();
        $r = $q->fetch();
        if (!empty($r)) {
            return $r;
        } else {
            return 0;
        }
    }

    public function check(&$data, &$error)
    {
        if (!$this->prepareData($data, $error)) {
            return false;
        }
        return true;
    }

    public function getRowsByOrder($id)
    {
        $sql = 'select orow.id, orow.id_object, orow.type_object, orow.product_name, orow.quantity, orow.options, orow.price ' .
            ' from #__order_row orow ' .
            ' left outer join #__product p on orow.id_object = p.id ' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang' .
            ' where orow.id_order = :id_order ';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_order', $id);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();

        $ev = new \EOS\System\Event\DataEvent($this, $res);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Order\Classes\Events::ORDER_DATA_PARSE_ROWS, $ev);
        return $ev->getData();
    }

    public function getOrder($id)
    {
        $sql = 'select o.*' .
            ' from #__order o' .
            ' where o.id = :id';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id);
        $q->execute();
        $res = $q->fetch();
        return $res;
    }

    public function getOrderByAccount($id, $maxitem = 500, $confirmed = false, $statusorder = 0)
    {
        /*
        if ($id != 0) {
            $sql = 'select o.id, o.ins_date, o.reference, o.notes_transaction, o.company, o.amount, a.alias, GROUP_CONCAT(orow.product_name) as products, osl.name as typeorder ' .
                ' from #__order o ' .
                ' left outer join #__address a on o.id_address_delivery = a.id ' .
                ' left outer join #__order_state_lang osl on osl.id_order_state = o.status and osl.id_lang = ' . $this->lang->getCurrentID() . ' ' .
                ' left outer join #__order_row orow on orow.id_order = o.id ' .
                ' where o.id_address_delivery in (select asub.id from #__address asub where JSON_CONTAINS(options, \'"' . $id . '"\', \'$.id_agent\') or asub.id_customer = ' . $id . ') ' .
                ' and o.id_address_delivery != 0 and o.amount > 0 ';
            if ($confirmed) {
                $sql .= " and o.id_transaction = 'I' ";
            } else {
                $sql .= ' and o.id_transaction is null ';
            }
            $sql .= ' GROUP BY o.id, a.alias, o.ins_date, o.status, o.id_address_delivery, typeorder ';
        } else {
            $sql = 'select o.id, o.ins_date, o.reference, o.notes_transaction, o.company, o.amount, a.alias, GROUP_CONCAT(orow.product_name) as products, osl.name as typeorder ' .
                ' from #__order o ' .
                ' left outer join #__order_state_lang osl on osl.id_order_state = o.status and osl.id_lang = ' . $this->lang->getCurrentID() . ' ' .
                ' left outer join #__address a on o.id_address_delivery = a.id ' .
                ' left outer join #__order_row orow on orow.id_order = o.id ' .
                ' where o.id_address_delivery != 0 and o.amount > 0 ';
            if ($confirmed) {
                $sql .= " and o.id_transaction = 'I' ";
            } else {
                $sql .= ' and o.id_transaction is null ';
            }
            $sql .= ' GROUP BY o.id, a.alias, o.ins_date, o.status, o.id_address_delivery, typeorder ';
        }
        */
        if ($id != 0) {
            $sql = 'select o.id, o.ins_date, o.reference, o.notes_transaction, o.company, o.amount, a.alias, osl.name as typeorder, o.status ' .
                ' from #__order o ' .
                ' left outer join #__address a on o.id_address_delivery = a.id ' .
                ' left outer join #__order_state_lang osl on osl.id_order_state = o.status and osl.id_lang = ' . $this->lang->getCurrentID() . ' ' .
                ' where o.id_address_delivery in (select asub.id from #__address asub where JSON_CONTAINS(options, \'"' . $id . '"\', \'$.id_agent\') or asub.id_customer = ' . $id . ') ' .
                ' and o.id_address_delivery != 0 and o.amount > 0 and o.status in (' . $statusorder . ') ';
            if ($confirmed) {
                $sql .= " and o.id_transaction = 'I' ";
            } else {
                $sql .= ' and o.id_transaction is null ';
            }
        } else {
            $sql = 'select o.id, o.ins_date, o.reference, o.notes_transaction, o.company, o.amount, a.alias, osl.name as typeorder, o.status ' .
                ' from #__order o ' .
                ' left outer join #__order_state_lang osl on osl.id_order_state = o.status and osl.id_lang = ' . $this->lang->getCurrentID() . ' ' .
                ' left outer join #__address a on o.id_address_delivery = a.id ' .
                ' where o.id_address_delivery != 0 and o.amount > 0 and o.status in (' . $statusorder . ') ';
            if ($confirmed) {
                $sql .= " and o.id_transaction = 'I' ";
            } else {
                $sql .= ' and o.id_transaction is null ';
            }
        }
        $order = 'order by o.id DESC ';
        $limit = 'limit ' . $maxitem;
        //print_r($sql . $order . $limit);
        //exit();
        $q = $this->db->prepare($sql . $order . $limit);
        $q->bindValue(':id', $id);
        //$q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    public function getDetails($id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__order o'))
            ->select(null)
            ->select('o.id')
            ->select('o.name')
            ->select('o.surname')
            ->select('o.phone')
            ->select('o.email')
            ->select('o.address')
            ->select('o.city')
            ->select('o.state')
            ->select('o.country')
            ->select('o.company')
            ->select('o.vat')
            ->select('o.cif')
            ->select('o.notes')
            ->select('o.status')
            ->select('o.amount')
            ->select('o.reference')
            ->select('o.id_transaction')
            ->select('o.notes_transaction')
            ->select('o.id_customer')
            ->select('o.id_address_delivery')
            ->select('o.id_address_invoice')
            ->select('o.id_agent')
            ->select('o.ins_date')
            ->where('o.id = ?', (int) $id);
        $rc = $query->fetch();
        if ($rc == null) {
            $res = [];
        } else {
            $res['id'] = $rc['id'];
            $res['notes'] = $rc['notes'];
            $res['status'] = $rc['status'];
            $res['amount'] = $rc['amount'];
            $res['reference'] = $rc['reference'];
            $res['id-transaction'] = $rc['id_transaction'];
            $res['notes-transaction'] = $rc['notes_transaction'];
            $res['ins_date'] = $rc['ins_date'];
            $res['email'] = $rc['email'];
            $query2 = $this->db->newFluent()->from($this->db->tableFix('#__address a'))
                ->where('a.id = ?', (int) $rc['id_address_delivery']);
            $rc2 = $query2->fetch();
            if (!empty($rc2)) {
                $res['name'] = $rc2['name'];
                $res['surname'] = $rc2['surname'];
                $res['phone'] = $rc2['phone'];
                $res['address'] = $rc2['address1'];
                $res['company'] = $rc2['company'];
                $res['vat'] = $rc2['vat'];
                $res['cif'] = $rc2['cif'];
                $options = json_decode($rc2['cif'], true);
                $res['notecustomer'] = $options['note'];
                $ml = new \EOS\Components\Account\Models\LocalizationModel($this->container);
                $current_city = $ml->getCity(ArrayHelper::getInt($rc2, 'id_city'));
                $res['city'] = (isset($current_city['name']) && $current_city['name'] != '')  ? $current_city['name'] : '';
                $res['state'] = (isset($current_city['statecode']) && $current_city['statecode'] != '') ? $current_city['statecode'] : '';
                $res['country'] = (isset($current_city['country']) && $current_city['name'] != '') ? $current_city['country'] : '';
            } else {
                $res['name'] = $rc['name'];
                $res['surname'] = $rc['surname'];
                $res['phone'] = $rc['phone'];
                $res['email'] = $rc['email'];
                $res['address'] = $rc['address'];
                $res['city'] = $rc['city'];
                $res['state'] = $rc['state'];
                $res['country'] = $rc['country'];
                $res['company'] = $rc['company'];
                $res['vat'] = $rc['vat'];
                $res['cif'] = $rc['cif'];
            }
            $query3 = $this->db->newFluent()->from($this->db->tableFix('#__account_user au'))
                ->select('au.name')
                ->where('au.id = ?', (int) $rc['id_agent']);
            $rc3 = $query3->fetch();
            if (!empty($rc3)) {
                $res['agent-name'] = $rc3['name'];
            }
        }

        return $res;
    }

    public function getDataRow($id, $idlang, $viewagent = false)
    {
        $sql = 'select oro.id, oro.id_order, oro.product_name, oro.price, oro.options, oro.quantity, ml.name as manufacture ' .
            ' from #__order_row oro' .
            ' left outer join #__product p on oro.id_object = p.id ' .
            ' left outer join #__product_lang pl on p.id = pl.id_product and pl.id_lang = :id_lang ' .
            ' left outer join #__tax t on t.id = p.id_tax ' .
            ' left outer join #__manufacturer_lang ml on p.id_manufacturer = ml.id_manufacturer ' .
            ' where oro.id_order = :id_order';
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_order', (int)$id);
        $q->bindValue(':id_lang', (int)$idlang);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }

    public function getOpenDebit($id, $maxitem = 500)
    {
        if ($id != 0) {
            $sql = "SELECT `reference` FROM `eos_address` WHERE id_customer = " . $id;
            $query = $this->db->prepare($sql);
            $query->execute();
            $reference = $query->fetch();
            $sql = 'SELECT PTNUMPAR,PTNUMDOC as NumeroDocumento,PTDATDOC as DataDocumento,PTDATSCA as ScadenzaDocumento, ' .
                ' SUM(PTTOTIMP_DARE) as Dare,SUM(PTTOTIMP_AVERE) as Avere,MAX(PTDATREG) as UltimaRegistrazione ' .
                ' FROM(SELECT PTNUMPAR,PTNUMDOC,PTDATDOC,PTDATSCA,PTDATREG,PTTOTIMP as PTTOTIMP_DARE,0 as PTTOTIMP_AVERE ' .
                ' FROM eos_ext_open_debit ' .
                ' where PTCODCON = "' . $reference['reference'] . '" and ifnull(PTTIPCON,"")="C" and ifnull(PT_SEGNO,"")="D" and ifnull(PTFLIMPE,"")<>"CG" and ifnull(PTFLSOSP,"")<>"S" ' .
                ' union all ' .
                ' SELECT PTNUMPAR,PTNUMDOC,PTDATDOC,PTDATSCA,PTDATREG,0 as PTTOTIMP_DARE,PTTOTIMP as PTTOTIMP_AVERE ' .
                ' FROM eos_ext_open_debit ' .
                ' where PTCODCON = "' . $reference['reference'] . '" and ifnull(PTTIPCON,"")="C" and ifnull(PT_SEGNO,"")="A" and ifnull(PTFLIMPE,"")<>"CG" and ifnull(PTFLSOSP,"")<>"S" ' .
                ' ) Partite ' .
                ' group by PTNUMPAR,PTNUMDOC,PTDATDOC,PTDATSCA order by ScadenzaDocumento DESC limit ' . $maxitem;
            //$sql = 'SELECT * FROM `eos_ext_open_debit` WHERE Codice_Cliente = "'.$reference['reference'].'"';
        } else {
            $sql = 'SELECT PTNUMPAR,PTNUMDOC as NumeroDocumento,PTDATDOC as DataDocumento,PTDATSCA as ScadenzaDocumento, ' .
                ' SUM(PTTOTIMP_DARE) as Dare,SUM(PTTOTIMP_AVERE) as Avere,MAX(PTDATREG) as UltimaRegistrazione ' .
                ' FROM(SELECT PTNUMPAR,PTNUMDOC,PTDATDOC,PTDATSCA,PTDATREG,PTTOTIMP as PTTOTIMP_DARE,0 as PTTOTIMP_AVERE ' .
                ' FROM eos_ext_open_debit ' .
                ' where ifnull(PTCODCON,"") not like "%3407%" and ifnull(PTCODCON,"") not like "%5609%" and ifnull(PTTIPCON,"")="C" and ifnull(PT_SEGNO,"")="D" and ifnull(PTFLIMPE,"")<>"CG" and ifnull(PTFLSOSP,"")<>"S" ' .
                ' union all ' .
                ' SELECT PTNUMPAR,PTNUMDOC,PTDATDOC,PTDATSCA,PTDATREG,0 as PTTOTIMP_DARE,PTTOTIMP as PTTOTIMP_AVERE ' .
                ' FROM eos_ext_open_debit ' .
                ' where ifnull(PTCODCON,"") not like "%3407%" and ifnull(PTCODCON,"") not like "%5609%" and ifnull(PTTIPCON,"")="C" and ifnull(PT_SEGNO,"")="A" and ifnull(PTFLIMPE,"")<>"CG" and ifnull(PTFLSOSP,"")<>"S" ' .
                ' ) Partite ' .
                ' group by PTNUMPAR,PTNUMDOC,PTDATDOC,PTDATSCA order by ScadenzaDocumento DESC limit ' . $maxitem;
            //$sql = 'SELECT * FROM `eos_ext_open_debit` WHERE 1';
        }
        $q = $this->db->prepare($sql);
        $q->execute();
        $res = $q->fetchAll();
        return $res;
    }
}
