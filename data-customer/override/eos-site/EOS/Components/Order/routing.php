<?php
$this->mapComponent(['GET'], 'Order', ['order/index' => 'OrderController:index']);
$this->mapComponent(['POST'], 'Order', ['order/ajaxcommand' => 'OrderController:ajaxCommand']);
$this->mapComponent(['POST'], 'Order', ['order/verify' => 'OrderController:verify']);
$this->mapComponent(['GET'], 'Order', ['order/confirm' => 'OrderController:confirm']);
$this->mapComponent(['GET'], 'Order', ['order/error' => 'OrderController:error']);
$this->mapComponent(['POST'], 'Order', ['order/paypalipn' => 'OrderController:paypalipn']);
$this->mapComponent(['GET'], 'Order', ['order/details/{id}' => 'OrderController:details']);
$this->mapComponent(['GET'], 'Order', ['order/listorder/' => 'OrderController:listOrder']);
$this->mapComponent(['GET'], 'Order', ['order/listorderconfirmed/' => 'OrderController:listOrderConfirmed']);
$this->mapComponent(['GET'], 'Order', ['order/listprof/' => 'OrderController:listProf']);
$this->mapComponent(['GET'], 'Order', ['order/listresid/' => 'OrderController:listResid']);
$this->mapComponent(['GET'], 'Order', ['order/listordte/' => 'OrderController:listOrdte']);
$this->mapComponent(['GET'], 'Order', ['order/listorsos/' => 'OrderController:listOrsos']);
$this->mapComponent(['GET'], 'Order', ['order/listprosc/' => 'OrderController:listProsc']);
$this->mapComponent(['GET'], 'Order', ['order/listopendebit/' => 'OrderController:listDebit']);
$this->mapComponent(['GET'], 'Order', ['order/cashback/' => 'OrderController:listCashback']);
$this->mapComponent(['GET'], 'Order', ['order/cashbackagent/' => 'OrderController:listCashbackAgent']);
