<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Controllers;

use \EOS\System\Util\ArrayHelper;
use EOS\System\Util\FormValidator;
use EOS\System\Util\StringHelper;

class CatalogController extends \EOS\Components\System\Classes\SiteController
{

    protected function newModel()
    {
        return new \EOS\Components\Product\Models\CatalogModel($this->container);
    }

    private function prepareView($request, $response, $args, $view)
    {
        $v = $this->newView($request, $response);
        $m = $this->newModel();
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        //* Set by view */
        $mu->userInfo($v);
        $v->filters = \EOS\System\Util\StringHelper::sanitizeFieldArray($request->getQueryParams());
        if ($view != 'showcase') {
            // Security
            $page = ArrayHelper::getInt($v->filters, 'page', 0);
            $pagesize = $request->getQueryParam('pagesize', 0);
            $order = ArrayHelper::getStr($v->filters, 'order', 'name');
            $v->typecondition = ArrayHelper::getInt($v->filters, 'typecondition', 0);
            $this->session->set('searchSearch', ArrayHelper::getValue($v->filters, 'search'));
            switch ($view) {
                case 'category':
                    $filter = ["category" => $args['id']];
                    if (!isset($v->user->groups) || in_array(42, $v->user->groups)) {
                        $filter["quantitycheck"] = 1;
                    }
                    $pl = $m->getGroupedProductList($page, $order, $filter, $pagesize);
                    break;
                case 'brand':
                    $filter = ["manufacturer" => $args['id']];
                    if (!isset($v->user->groups) || in_array(42, $v->user->groups)) {
                        $filter["quantitycheck"] = 1;
                    }
                    $pl = $m->getGroupedProductList($page, $order, $filter, $pagesize);
                    break;
                case 'search':
                    if (!isset($v->user->groups) || in_array(42, $v->user->groups)) {
                        $v->filters["quantitycheck"] = 1;
                    }
                    $this->session->set('searchSearch', ArrayHelper::getValue($v->filters, 'search'));
                    $pl = $m->getGroupedProductList($page, $order, $v->filters, $pagesize);
                    break;
            }
            $v->orderby = $order;
            $v->pagesize = $pagesize;
            $v->products = $pl['products'];
            $v->productsCount = $pl['products-count'];
            $v->pageIndex = $pl['page-index'];
            $v->pageCount = $pl['page-count'];
            $v->pageSize = $pl['page-size'];
            /* Evidenza */
            $order = $request->getQueryParam('order', 'name');
            $filterfeatured = ["typecondition" => 1];
            $plf = $m->getGroupedProductList($page, $order, $filterfeatured);
            $v->productsfeatured = $plf['products'];
            /* filter */
            if (isset($pl['idlist']) && !empty($pl['idlist'])) {
                $v->manufacturer = $m->getManufacturerList(1, $pl['idlist']);
                $v->vintage = $m->getAttribute(1, 1, $pl['idlist']);
                $v->region = $m->getAttribute(1, 2, $pl['idlist']);
                $v->vineyards = $m->getAttribute(1, 3, $pl['idlist']);
                $v->size = $m->getAttribute(1, 4, $pl['idlist']);
                $v->country = $m->getAttribute(1, 5, $pl['idlist']);
                $v->selection = $m->getAttribute(1, 8, $pl['idlist']);
                $v->categories = $m->getCategoriesList(1, null, $pl['idlist']);
            }
            /* cart */
            $cart = new \EOS\Components\Cart\Classes\CartBase($this->container);
            $v->isCartMoode = $m->isCartMode();
            $v->rowShowAddCart = $m->rowShowAddCart();
            $v->displayside = true;
        }
        switch ($view) {
            case 'showcase':
                $v->title = $this->lang->transEncode('product.catalog.title');
                break;
            case 'category':
                $v->title = $this->lang->transEncode('product.catalog.title');
                break;
            case 'brand':
                $v->title = $this->lang->transEncode('product.catalog.title');
                break;
            case 'showcase':
                $v->title = $this->lang->transEncode('product.catalog.title');
                break;
        }
        \EOS\UI\Loader\Library::load($v, 'Bootbox');
        \EOS\UI\Loader\Library::load($v, 'Datepicker');
        $v->addScriptLink($v->path->getExtraUrlFor('system', 'resource/app.js'));
        return $v->render('default/default', true);
    }

    public function showcase($request, $response, $args)
    {
        return $this->prepareView($request, $response, $args, 'showcase');
    }

    public function category($request, $response, $args)
    {
        return $this->prepareView($request, $response, $args, 'category');
    }

    public function brand($request, $response, $args)
    {
        return $this->prepareView($request, $response, $args, 'brand');
    }

    public function search($request, $response, $args)
    {
        /* return $this->loadSearch($request, $response, $args, false); */
        return $this->prepareView($request, $response, $args, 'search');
    }

    public function item($request, $response, $args)
    {
        $v = $this->newView($request, $response);
        $m = $this->newModel();
        $mu = new \EOS\Components\Account\Models\UserModel($this->container);
        $mu->userInfo($v);
        $quantitycheck = 0;
        if (!isset($v->user->groups) || in_array(42, $v->user->groups)) {
            $quantitycheck = 1;
        }
        $cart = new \EOS\Components\Cart\Classes\CartBase($this->container);
        $v->manufacturer = $m->getManufacturerList(1);
        $v->categories = $m->getCategoriesList(1);
        $v->data = $m->getDataAndGroup((int)$args['id'], $quantitycheck);
        $v->relatedproduct = $m->getRelatedProducts($v->data['id_category'], $v->data['attribute-id-attribute-1'], $v->data['id'], $quantitycheck);
        $v->displayside = true;

        /* meta */
        $v->title = $v->data['name-' . $this->lang->getCurrent()->id];
        if ($v->data['manufacturer-name'] != "") {
            $v->title .= " - " . $v->data['manufacturer-name'];
        }
        $v->addMeta("og:site_name", "Les Caves de Pyrene");
        $v->addMeta("og:title", $v->title);
        $v->addMeta("og:type", "product");
        $v->addMeta("og:url", $v->path->getDomainUrl() . $v->path->getPageUrl());
        $v->addMeta("og:description", StringHelper::htmlPreview($v->data['descr-' . $this->lang->getCurrent()->id], 150));
        if (isset($v->data["url-0"])) {
            $v->addMeta("og:image", $v->path->getBaseUrl() . $v->data["url-0"]);
        } else {
            $v->addMeta("og:image", $v->path->getBaseUrl() . "data-customer/themes/default/img/wine-default.jpg");
        }
        $v->addMeta("product:price:amount", $v->data['price']);
        $v->addMeta("product:price:currency", "EUR");

        return $v->render('item/default');
    }

    public function ajaxCommand($request, $response, $args)
    {
        $fv = new FormValidator($this, $request, $response, $args);
        // Richiamo un evento per cambiare i parametri dei custom fields
        $ev = new \EOS\System\Event\SystemEvent($fv);
        $mc = new \EOS\Components\Cart\Models\CartModel($this->container);
        $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::CATALOG_FORM_AJAX_CONFIG, $ev);
        if ($fv->prepareInputDataFromJson()) {
            $data = $fv->getInputData();
            $command = ArrayHelper::getStr($data, 'command');
            $m = $this->newModel();
            switch ($command) {
                case 'addcart':
                    if ($m->saveCart($data, $error)) {
                        $fv->setResult(true);
                        $fv->setRedirect($this->path->getUrlFor('cart', 'cart/index'));
                    } else {
                        $fv->setMessage($error);
                    }
                    break;
                case 'deleterow':
                    if ($mc->deleteRowById($data['id'])) {
                        $fv->setResult(true);
                    }
                    break;
                default:
                    $fv->setMessage($this->lang->transEncode('product.catalog.error.invalidcommand'));
                    break;
            }
        }
        return $fv->toJsonResponse();
    }
}
