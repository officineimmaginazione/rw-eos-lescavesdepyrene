<?php

/**
  EcoSoft e Officine Immaginazione Copyright (c) 2015-2019
  www.ecosoft.it - www.officineimmaginazione.com
 * */

namespace EOS\Components\Product\Models;

use EOS\System\Util\ArrayHelper;
use EOS\System\Util\DateTimeHelper;

class CatalogModel extends \EOS\System\Models\Model
{

    private $_currency = null;
    private $_stockManager = null;

    private function getGroupedProductListSQL(array $filter, int $quantitycheck): string
    {
        if (!$quantitycheck) {
            $sql = 'select p.id_manufacturer, ml.name as manufacturer, pl.name, 
                min(p.id) as id, min(p.price) as price,
                count(p.id) as gplcount
                from eos_product p 
                inner join #__product_lang pl on p.id = pl.id_product 
                inner join #__manufacturer m on p.id_manufacturer = m.id
                inner join #__manufacturer_lang ml on m.id = ml.id_manufacturer and ml.id_lang = pl.id_lang ';
        } else {
            $sql = 'select p.id_manufacturer, ml.name as manufacturer, pl.name, 
                min(p.id) as id, min(cr.price) as price,
                count(p.id) as gplcount
                from eos_product p 
                inner join #__product_lang pl on p.id = pl.id_product 
                inner join #__catalog_rules cr on p.id = cr.id_product and cr.id_group = 42
                inner join #__manufacturer m on p.id_manufacturer = m.id
                inner join #__manufacturer_lang ml on m.id = ml.id_manufacturer and ml.id_lang = pl.id_lang ';
        }
        $sql .= ArrayHelper::getStr($filter, 'join');
        $sql .= sprintf(' where pl.id_lang = %d and p.status = %d and m.status = %d', $this->lang->getCurrentID(), 1, 1);
        if ($quantitycheck) {
            //$sql .= ' and ps.quantity_available > 0 ';
        }
        $fWhere = ArrayHelper::getStr($filter, 'where');
        if ($fWhere !== '') {
            $sql .= ' and (' . $fWhere . ')';
        }
        $sql .= ' group by p.id_manufacturer, ml.name, pl.name ';
        return $sql;
    }

    private function getGroupedItems(int $idManufacturer, string $name, array $fields = [], int $typecondition = 0, $quantitycheck = 0): array
    {
        $sql = 'select p.id
                from #__product p 
                inner join #__product_lang pl on p.id = pl.id_product  ';
        if ($quantitycheck == 1) {
            $sql .= ' inner join #__catalog_rules cr on p.id = cr.id_product and cr.id_group = 42 ';
        }
        $sql .= sprintf(' where pl.id_lang = %d and p.status =  %d', $this->lang->getCurrentID(), 1);
        $sql .= ' and p.id_manufacturer = :id_manufacturer and pl.name = :name';
        if ($quantitycheck == 1) {
            //$sql .= ' and p.type_condition = 0 ';
        } else {
            if ($typecondition > 0) {
                $sql .= ' and p.type_condition = ' . $typecondition;
            }
        }
        $q = $this->db->prepare($sql);
        $q->bindValue(':id_manufacturer', $idManufacturer);
        $q->bindValue(':name', $name);
        $q->execute();
        $list = $q->fetchAll();
        $res = [];
        foreach ($list as $prod) {
            $r = $this->getData($prod['id'], $quantitycheck);
            $r['attribute-name-1'] = ArrayHelper::getStr($r, 'attribute-name-1');
            $r['url'] = $this->container->get('path')->getUrlFor('product', 'catalog/item/' . $r['id']);
            if (empty($fields)) {
                $r2 = $r;
            } else {
                $r2 = [];
                foreach ($fields as $f) {
                    if (isset($r[$f])) {
                        $r2[$f] = $r[$f];
                    }
                }
            }
            //$r2['disabled'] = ($r['price'] === 0.0) || ($r['stock-available'] <= 0.0);
            $r2['disabled'] = false;
            $res[] = $r2;
        }
        uasort($res, function ($a, $b) {
            $vA = mb_strtolower($a['attribute-name-1']);
            $vB = mb_strtolower($b['attribute-name-1']);
            if ($vA === $vB) {
                return 0;
            }
            return ($vA < $vB) ? -1 : 1;
        });

        return $res;
    }

    private function addStock(array &$res)
    {
        $stock = $this->getStockManager()->getStock2Str($res['id']);
        $res = array_merge($res, $stock);
    }

    public function getCurrency()
    {
        if (is_null($this->_currency)) {
            $this->_currency = new \EOS\System\Payments\Currencies\Currency($this->container);
        }
        return $this->_currency;
    }

    public function getStockManager(): \EOS\Components\LCDPComp\Classes\StockManagerExt
    {
        if (is_null($this->_stockManager)) {
            $this->_stockManager = new \EOS\Components\LCDPComp\Classes\StockManagerExt($this->container);
        }
        return $this->_stockManager;
    }

    public function getData($id, $quantitycheck = 0)
    {
        if ($quantitycheck) {
            $sql = 'select p.id, p.status, cr.price, p.ean13, p.isbn, p.upc, p.width, p.height, p.depth, p.weight, p.id_manufacturer, p.out_of_stock,
                p.expected_date, p.type_condition, p.min_quantity, extra_display_info
                from #__product p 
                inner join #__catalog_rules cr on p.id = cr.id_product and id_group = 42
                where p.id = ' . $id;
        } else {
            $sql = 'select p.id, p.status, p.price, p.ean13, p.isbn, p.upc, p.width, p.height, p.depth, p.weight, p.id_manufacturer, p.out_of_stock,
                p.expected_date, p.type_condition, p.min_quantity, extra_display_info
                from #__product p 
                where p.id = ' . $id;
        }
        $q = $this->db->prepare($sql);
        $q->execute();
        $rc = $q->fetch();
        if ($rc == null) {
            $res = [];
        } else {
            $ll = $this->lang->getArrayFromDB(true);
            $res['id'] = $rc['id'];
            $res['price'] = (float) $rc['price'];
            $res['price-formatted'] = $this->getCurrency()->format($res['price']);
            $res['ean13'] = $rc['ean13'];
            $res['isbn'] = $rc['isbn'];
            $res['upc'] = $rc['upc'];
            $res['width'] = $rc['width'];
            $res['height'] = $rc['height'];
            $res['depth'] = $rc['depth'];
            $res['weight'] = $rc['weight'];
            $res['out-of-stock'] = $rc['out_of_stock'];
            $res['min-quantity'] = $rc['min_quantity'];
            $res['extra-display-info'] = $rc['extra_display_info'];
            $res['type-condition'] = $rc['type_condition'];
            if (isset($rc['expected_date']))
                $res['expected-date'] = $this->lang->dbDateToLangDate($rc['expected_date']);
            $res['status'] = $rc['status'] == 1;
            /* foreach ($ll as $l)
              { */
            $idlang = $this->lang->getCurrentID();
            $tblContent = $this->db->tableFix('#__product_lang');
            $query = $this->db->newFluent()->from($tblContent)->select('id')
                ->where('id_product', $rc['id'])
                ->where('id_lang', $idlang);
            $rcl = $query->fetch();
            if ($rcl != null) {
                // Versione con ID
                $res['name-' . $idlang] = $rcl['name'];
                $res['descr-' . $idlang] = $rcl['description'];
                $res['descr-short-' . $idlang] = $rcl['description_short'];
                $res['options-' . $idlang] = $rcl['options'];
                // Versione senza ID
                $res['name'] = $rcl['name'];
                $res['descr'] = $rcl['description'];
                $res['descr-short'] = $rcl['description_short'];
                $res['options'] = $rcl['options'];
            }
            /* } */


            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($rc['id']);
            foreach ($listimage as $r) {
                $img = $image->getObject($rc['id'], $r['id'], \EOS\System\Media\Image::IMAGE_LR);
                if (!is_null($img)) {
                    $res['url-0'] = $img->url;
                }
                //$res['pos-' . $i] = $r['pos'];
                //$i++;
            }
            $query = $this->db->newFluent()->from($this->db->tableFix('#__product_product_category ppc'))
                ->select(null)->select('pcl.name')->select('pcl.id_category')
                ->where('ppc.id_product', $rc['id'])
                ->where('id_lang = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = ppc.id_category'));
            $rcc = $query->fetch();
            $res['category'] = $rcc['name'];
            $res['id_category'] = $rcc['id_category'];
            $res['id_category'] = $rcc['id_category'];
            $res['id_manufacturer'] = $rc['id_manufacturer'];
            $tblContent = $this->db->tableFix('#__manufacturer_lang');
            $query = $this->db->newFluent()->from($tblContent)
                ->select(null)
                ->select('name')
                ->select('description')
                ->where('id_manufacturer', $rc['id_manufacturer'])
                ->where('id_lang = 1');
            $rcm = $query->fetch();
            $res['manufacturer-name'] = $rcm['name'];
            $res['manufacturer-description'] = $rcm['description'];
            $tblAttribute = $this->db->tableFix('#__product_product_attribute_combination ppac');
            $query = $this->db->newFluent()->from($tblAttribute)->select('pal.name as attribute')->select('pagl.name as attributegroup')
                ->select('pagl.id as id')
                ->select('pal.id_attribute as id_attribute')
                ->innerJoin($this->db->tableFix('#__product_attribute pa ON pa.id = ppac.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pa.id = pal.id_attribute'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group pag ON pa.id_attribute_group = pag.id'))
                ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pag.id = pagl.id_attribute_group'))
                ->where('id_product', $rc['id']);
            $rca = $query->fetchAll();

            foreach ($rca as $ra) {
                $res['attribute-name-' . $ra['id']] = $ra['attribute'];
                $res['attribute-id-attribute-' . $ra['id']] = $ra['id_attribute'];
                $res['attribute-group-' . $ra['id']] = $ra['attributegroup'];
            }
            $this->addStock($res);

            $res['type-condition'] = $rc['type_condition'];
            $attr1 = ArrayHelper::getStr($res, 'attribute-name-1');
            if ($res['type-condition'] == 1 && ($attr1 !== '')) {
                $res['attribute-name-1'] = $attr1 . ' C';
            }
        }
        return $res;
    }

    public function getDataAndGroup(int $id, int $quantitycheck = 0): array
    {
        $data = $this->getData($id, $quantitycheck);
        if (!empty($data)) {
            $data['group-items'] = $this->getGroupedItems($data['id_manufacturer'], $data['name'], [], 0, $quantitycheck);
        }
        //$data['disabled'] = ($data['price'] === 0.0) || ($data['stock-available'] <= 0.0);
        $data['disabled'] = false;
        return $data;
    }

    public function prepareFilter($filterArray)
    {
        $fields = [];
        $where = [];
        $params = [];
        $join = [];
        $order = [];

        // Accetto solo quelle approvate
        // $where[] = 'f.status = 1';
        $quantitycheck = ArrayHelper::getInt($filterArray, 'quantitycheck');

        $category = ArrayHelper::getInt($filterArray, 'category');
        if (!empty($category) && $category != 0) {
            $category = $this->getCategoryChild($category);
            $join[] = 'inner join eos_product_product_category ppc on p.id = ppc.id_product';
            $where[] = 'ppc.id_category in (' . $category . ') ';
        }

        $manufacturer = ArrayHelper::getInt($filterArray, 'manufacturer');
        if (!empty($manufacturer) && $manufacturer != 0) {
            $where[] = 'p.id_manufacturer = :manufacturer ';
            $params[':manufacturer'] = $manufacturer;
        }

        $price = ArrayHelper::getStr($filterArray, 'price');
        if ($quantitycheck) {
            if (!empty($price)) {
                $lowhigh = explode('-', $price);
                $where[] = 'cr.price >= :low and cr.price <= :high ';
                $params[':low'] = $lowhigh[0];
                $params[':high'] = $lowhigh[1];
            }
        } else {
            if (!empty($price)) {
                $lowhigh = explode('-', $price);
                $where[] = 'p.price >= :low and p.price <= :high ';
                $params[':low'] = $lowhigh[0];
                $params[':high'] = $lowhigh[1];
            }
        }

        if ($quantitycheck) {
            $where[] = 'p.type_condition <> :typecondition ';
            $params[':typecondition'] = 1;
        } else {
            $typecondition = ArrayHelper::getInt($filterArray, 'typecondition');
            if (!empty($typecondition)) {
                $where[] = 'p.type_condition = :typecondition ';
                $params[':typecondition'] = $typecondition;
            }
        }

        $extradisplayinfo = ArrayHelper::getInt($filterArray, 'extrainfo');
        if (!empty($extradisplayinfo)) {
            $where[] = 'p.extra_display_info = :extrainfo ';
            $params[':extrainfo'] = $extradisplayinfo;
        }

        $vintage = ArrayHelper::getInt($filterArray, 'vintage');
        $region = ArrayHelper::getInt($filterArray, 'region');
        $vineyards = ArrayHelper::getInt($filterArray, 'vineyards');
        $country = ArrayHelper::getInt($filterArray, 'country');
        $size = ArrayHelper::getInt($filterArray, 'size');
        $selection = ArrayHelper::getInt($filterArray, 'selection');

        $notsingle = false;
        if (!empty($vintage) && $vintage != 0) {
            $join[] = 'inner join eos_product_product_attribute_combination ppac1 on ppac1.id_product = p.id';
            $where[] = 'ppac1.id_attribute = :vintage ';
            $params[':vintage'] = $vintage;
            $notsingle = true;
        }
        if (!empty($region) && $region != 0) {
            $join[] = 'inner join eos_product_product_attribute_combination ppac2 on ppac2.id_product = p.id';
            $where[] = 'ppac2.id_attribute = :region ';
            $params[':region'] = $region;
            $notsingle = true;
        }
        if (!empty($vineyards) && $vineyards != 0) {
            $join[] = 'inner join eos_product_product_attribute_combination ppac3 on ppac3.id_product = p.id';
            $where[] = 'ppac3.id_attribute = :vineyards ';
            $params[':vineyards'] = $vineyards;
            $notsingle = true;
        }
        if (!empty($size) && $size != 0) {
            $join[] = 'inner join eos_product_product_attribute_combination ppac4 on ppac4.id_product = p.id';
            $where[] = 'ppac4.id_attribute = :size ';
            $params[':size'] = $size;
            $notsingle = true;
        }
        if (!empty($country) && $country != 0) {
            $join[] = 'inner join eos_product_product_attribute_combination ppac5 on ppac5.id_product = p.id';
            $where[] = 'ppac5.id_attribute = :country ';
            $params[':country'] = $country;
            $notsingle = true;
        }
        if (!empty($selection) && $selection != 0) {
            $join[] = 'inner join eos_product_product_attribute_combination ppac8 on ppac8.id_product = p.id';
            $where[] = 'ppac8.id_attribute = :selection ';
            $params[':selection'] = $selection;
            $notsingle = true;
        }

        if ($quantitycheck) {
            $join[] = 'inner join eos_product_stock ps on p.id = ps.id_product';
            $where[] = 'ps.quantity_available >= :minquantity and ps.quantity_physical >= :minquantity and ps.quantity_physical > ps.quantity_reserved';
            $params[':minquantity'] = 1;
        }

        $search = addslashes(ArrayHelper::getStr($filterArray, 'search'));
        if (!empty($search)) {
            $where[] = " (pl.name like '%$search%' or ml.name like '%$search%' or p.reference like '%$search%') ";
        }

        $res['fields'] = implode(', ', $fields);
        $res['where'] = implode(' and ', $where);
        $res['params'] = $params;
        $res['order'] = empty($order) ? '' : implode(";", $order);
        $res['join'] = implode("\n", $join);

        return $res;
    }

    public function getProductList($page, $order, $filterArray = [], $pagesize = null)
    {
        $filter = $this->prepareFilter($filterArray);
        $res['products'] = [];
        $res['products-count'] = 0;
        $res['page-index'] = 0;
        $res['page-size'] = ($pagesize != null) ? $pagesize : $this->db->setting->getInt('product', 'catalog.page.size', 12);
        $res['page-count'] = 0;

        $quantitycheck = ArrayHelper::getInt($filterArray, 'quantitycheck');

        $count = 0;
        $sqlSelect = 'select count(p.id) as res';
        $sqlFrom = ' from eos_product p ';
        $sqlInner = ' inner join eos_product_lang pl on p.id = pl.id_product ';
        $sqlInner .= ' inner join eos_manufacturer m on p.id_manufacturer = m.id ';
        $sqlInner .= ' inner join eos_manufacturer_lang ml on m.id = ml.id_manufacturer ';
        if ($quantitycheck) {
            $sqlInner .= ' inner join #__catalog_rules cr on p.id = cr.id_product and id_group = 42 ';
        }
        $sqlInner .= $filter['join'];
        if ($quantitycheck) {
            $sqlWhere = ' where pl.id_lang = :id_lang and p.status = :status and m.status = :mstatus and p.type_condition = 0 ';
        } else {
            $sqlWhere = ' where pl.id_lang = :id_lang and p.status = :status and m.status = :mstatus ';
        }

        if (!empty($filter['where'])) {
            $sqlWhere .= ' and ' . $filter['where'];
        }

        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->bindValue(':status', 1);
        $q->bindValue(':mstatus', 1);
        foreach ($filter['params'] as $p => $v) {
            $q->bindValue($p, $v);
        }
        $q->execute();
        $r = $q->fetch();
        if (!empty($r)) {
            $count = (int) $r['res'];
        }

        if ($count > 0) {
            if ($quantitycheck) {
                $sqlSelect = 'select p.id, p.price, p.isbn, p.ean13, p.expected_date, pl.name, pl.description_short, p.type_condition, p.on_sale, p.extra_display_info, p.out_of_stock, pl.options, ml.name as manufacturer ';
            } else {
                $sqlSelect = 'select p.id, cr.price, p.isbn, p.ean13, p.expected_date, pl.name, pl.description_short, p.type_condition, p.on_sale, p.extra_display_info, p.out_of_stock, pl.options, ml.name as manufacturer ';
            }

            //$sqlSelect = 'select p.id, p.price, p.isbn, p.ean13, p.expected_date, pl.name, pl.description_short, pl.options ';
            switch ($order) {
                case 'price-asc':
                    $sqlOrderFields = ' price ASC ';
                    break;
                case 'price-desc':
                    $sqlOrderFields = ' price DESC ';
                    break;
                case 'vintage-asc':
                    $sqlOrderFields = ' isbn ASC ';
                    break;
                case 'vintage-desc':
                    $sqlOrderFields = ' isbn DESC ';
                    break;
                case 'name-asc':
                    $sqlOrderFields = ' name ASC ';
                    break;
                case 'name-desc':
                    $sqlOrderFields = ' name DESC ';
                    break;
                default:
                    $sqlOrderFields = ' id ';
                    break;
            }
            if (isset($ordertype) && $ordertype != '') {
                $ordertype = $sqlOrder = ' order by ' . $sqlOrderFields . $ordertype;
            } else {
                $sqlOrder = ' order by ' . $sqlOrderFields;
            }
            $page = (int) $page > 0 ? (int) $page : 1;
            $sqlLimit = ' limit ' . (($page - 1) * $res['page-size']) . ',' . $res['page-size'];
            //print_r($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlOrder . $sqlLimit); exit();
            $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlOrder . $sqlLimit);
            $q->bindValue(':id_lang', $this->lang->getCurrentID());
            $q->bindValue(':status', 1);
            $q->bindValue(':mstatus', 1);
            foreach ($filter['params'] as $p => $v) {
                $q->bindValue($p, $v);
            }
            $q->execute();
            $r = $q->fetchAll();
            if (!empty($r)) {
                $currency = $this->getCurrency();
                $tblimg = $this->db->tableFix('#__product_image');
                foreach ($r as &$item) {
                    $item['price-formatted'] = $currency->format($item['price']);
                    $item['extra-display-info'] = $currency->format($item['extra_display_info']);
                    $item['type-condition'] = $item['type_condition'];
                    $item['min-quantity'] = $item['min_quantity'];
                    $item['out-of-stock'] = $item['out_of_stock'];
                    if (isset($item['expected_date']))
                        $item['expected-date'] = $this->lang->dbDateToLangDate($item['expected_date']);
                    $image = new \EOS\System\Media\Image($this->container, 'media.product');
                    $listimage = $image->getList($item['id']);
                    foreach ($listimage as $single) {
                        $img = $image->getObject($item['id'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
                        if (isset($img)) {
                            $item['url-0'] = $img->url;
                        }
                    }

                    $tblAttribute = $this->db->tableFix('#__product_product_attribute_combination ppac');
                    $query = $this->db->newFluent()->from($tblAttribute)->select('pal.name as attribute')->select('pagl.name as attributegroup')->select('pagl.id as id')
                        ->innerJoin($this->db->tableFix('#__product_attribute pa ON pa.id = ppac.id_attribute'))
                        ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pa.id = pal.id_attribute'))
                        ->innerJoin($this->db->tableFix('#__product_attribute_group pag ON pa.id_attribute_group = pag.id'))
                        ->innerJoin($this->db->tableFix('#__product_attribute_group_lang pagl ON pag.id = pagl.id_attribute_group'))
                        ->where('id_product', $item['id']);
                    $rca = $query->fetchAll();

                    foreach ($rca as $ra) {
                        $item['attribute-name-' . $ra['id']] = $ra['attribute'];
                        $item['attribute-group-' . $ra['id']] = $ra['attributegroup'];
                    }
                }
                $ev = new \EOS\System\Event\DataEvent($this, $r);
                $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::CATALOG_DATA_PARSE_ROWS, $ev);
                $r = $ev->getData();
                $res['products'] = $r;
                $res['page-count'] = (int) ceil($count / $res['page-size']);
                $res['page-index'] = $page > $res['page-count'] ? $res['page-count'] : $page;
                $res['products-count'] = $count;
            }
        }
        return $res;
    }

    public function getGroupedProductList($page, $order, array $filterArray = [], $pagesize = null): array
    {

        $filter = $this->prepareFilter($filterArray);

        $res['products'] = [];
        $res['products-count'] = 0;
        $res['page-index'] = 0;
        $res['page-size'] = ($pagesize != null) ? $pagesize : $this->db->setting->getInt('product', 'catalog.page.size', 12);
        $res['page-count'] = 0;

        $count = 0;
        $quantitycheck = ArrayHelper::getInt($filterArray, 'quantitycheck');
        $sqlSelect = 'select count(*) as res ';
        $sqlFrom = ' from (' . $this->getGroupedProductListSQL($filter, $quantitycheck) . ' ) as pgroup ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom);
        foreach ($filter['params'] as $p => $v) {
            $q->bindValue($p, $v);
        }

        //print_r($q); exit();
        $q->execute();
        $r = $q->fetch();
        if (!empty($r)) {
            $count = (int) $r['res'];
        }

        if ($count > 0) {
            $sqlSelect = 'select pgroup.*';
            //$sqlSelect = 'select p.id, p.price, p.isbn, p.ean13, p.expected_date, pl.name, pl.description_short, pl.options ';
            switch ($order) {
                case 'price-asc':
                    $sqlOrderFields = ' price ASC ';
                    break;
                case 'price-desc':
                    $sqlOrderFields = ' price DESC ';
                    break;
                    /* case 'vintage-asc':
                  $sqlOrderFields = ' isbn ASC ';
                  break;
                  case 'vintage-desc':
                  $sqlOrderFields = ' isbn DESC ';
                  break; */
                case 'name-asc':
                    $sqlOrderFields = ' name ASC ';
                    break;
                case 'name-desc':
                    $sqlOrderFields = ' name DESC ';
                    break;
                default:
                    // $sqlOrderFields = ' id ';
                    $sqlOrderFields = ' name ASC ';
                    break;
            }
            if (isset($ordertype) && $ordertype != '') {
                $ordertype = $sqlOrder = ' order by ' . $sqlOrderFields . $ordertype;
            } else {
                $sqlOrder = ' order by ' . $sqlOrderFields;
            }
            $page = (int) $page > 0 ? (int) $page : 1;
            $sqlLimit = ' limit ' . (($page - 1) * $res['page-size']) . ',' . $res['page-size'];
            //print_r($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlOrder . $sqlLimit); exit();
            $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlOrder . $sqlLimit);
            foreach ($filter['params'] as $p => $v) {
                $q->bindValue($p, $v);
            }
            $q->execute();
            $r = $q->fetchAll();
            if (!empty($r)) {
                $currency = $this->getCurrency();
                foreach ($r as &$item) {
                    // Copio i dati dal gruppo così imposta il primo come corretto
                    $fields = ['id', 'name', 'url', 'manufacturer-name', 'price', 'price-formatted', 'out-of-stock', 'min-quantity',  'attribute-name-1', 'url-0', 'disabled', 'stock-quantity', 'stock-incoming', 'extra-display-info', 'type-condition', 'stock-incoming-date-lng', 'stock-quantity-str', 'stock-incoming-str'];
                    $grpList = $this->getGroupedItems($item['id_manufacturer'], $item['name'], $fields, ArrayHelper::getInt($filterArray, 'typecondition', 0), $quantitycheck);
                    $item['group-items'] = $grpList;
                    foreach ($fields as $f) {
                        if (isset($grpList[0][$f])) {
                            $item[$f] = $grpList[0][$f];
                        }
                    }
                }

                unset($item);
                $ev = new \EOS\System\Event\DataEvent($this, $r);
                $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::CATALOG_DATA_PARSE_ROWS, $ev);
                $r = $ev->getData();
                $res['products'] = $r;
                $res['page-count'] = (int) ceil($count / $res['page-size']);
                $res['page-index'] = $page > $res['page-count'] ? $res['page-count'] : $page;
                $res['products-count'] = $count;
            }

            //Remove limits
            $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlOrder);
            foreach ($filter['params'] as $p => $v) {
                $q->bindValue($p, $v);
            }
            $q->execute();
            $r = $q->fetchAll();
            if (!empty($r)) {
                $idlist = [];
                foreach ($r as &$item) {
                    array_push($idlist, $item['id']);
                }
                unset($item);
                $res['idlist'] = $idlist;
            }
        }

        return $res;
    }

    public
    function isCartMode()
    {
        $res = $this->db->setting->getBool('product', 'catalog.cart', false);

        if (!$this->container['componentManager']->componentExists('Order')) {
            $res = false;
        }
        return $res;
    }

    public
    function rowShowAddCart()
    {
        return $this->isCartMode() && $this->db->setting->getBool('product', 'catalog.row.show.addcart', false);
    }

    protected
    function prepareData(&$data, &$error)
    {
        $res = true;
        $data['product'] = ArrayHelper::getInt($data, 'product');
        $data['quantity'] = ArrayHelper::getInt($data, 'quantity', 1);

        if (empty($data['product'])) {
            $error = $this->lang->trans('product.catalog.error.product');
            $res = false;
        }
        if ($res) {
            $ev = new \EOS\System\Event\DataEvent($this, $data);
            $this->getEventDispatcher()->dispatch(\EOS\Components\Product\Classes\Events::CATALOG_DATA_PREPARE, $ev);
            $res = $ev->isValid();
            $error = $ev->getErrorsString();
            $data = $ev->getData();
        }
        return $res;
    }

    public
    function saveCart(&$data, &$error)
    {
        if ($this->isCartMode()) {
            if ($this->prepareData($data, $error)) {
                $cart = new \EOS\Components\Order\Models\CartModel($this->container);
                $options = ArrayHelper::getArray($data, 'custom-fields');
                $cart->addRow($data['product'], $data['quantity'], $options);
                return true;
            }
        } else {
            $error = $this->lang->transEncode('product.catalog.error.invalidcommand');
        }
        return false;
    }

    public function getRelatedProducts($category, $vineyards, $id, $quantitycheck = 0)
    {
        $res = [];
        $count = 0;
        if ($quantitycheck) {
            $sqlSelect = 'select p.id, cr.price, pl.name, p.isbn, pl.description, pl.options, ml.name as manufacturer ';
        } else {
            $sqlSelect = 'select p.id, p.price, pl.name, p.isbn, pl.description, pl.options, ml.name as manufacturer ';
        }

        $sqlFrom = ' from eos_product p ';
        $sqlInner = ' inner join eos_product_lang pl on p.id = pl.id_product ';
        $sqlInner .= ' inner join eos_manufacturer m on p.id_manufacturer = m.id ';
        $sqlInner .= ' inner join eos_manufacturer_lang ml on m.id = ml.id_manufacturer and ml.id_lang = pl.id_lang';
        $sqlInner .= ' inner join eos_product_product_attribute_combination ppac on p.id = ppac.id_product ';
        $sqlInner .= ' inner join eos_product_product_category ppc on p.id = ppc.id_product ';
        if ($quantitycheck) {
            $sqlInner .= ' inner join #__catalog_rules cr on p.id = cr.id_product and id_group = 42 ';
        }
        $sqlWhere = ' where pl.id_lang = :id_lang and p.status = :status ';
        $sqlWhere .= ' and p.id != ' . $id . ' ';
        $sqlWhereAdd = ' and ppac.id_attribute = :vineyards ';
        $sqlLimit = ' ORDER BY RAND() LIMIT 4 ';

        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlWhereAdd . $sqlLimit);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->bindValue(':status', 1);
        //$q->bindValue(':id', (int)$id);
        $q->bindValue(':vineyards', (int) $vineyards);
        $q->execute();
        $r = $q->fetchAll();
        if (empty($r)) {
            $sqlWhereAdd = ' and ppc.id_category = :category ';
            $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlWhereAdd . $sqlLimit);
            $q->bindValue(':id_lang', $this->lang->getCurrentID());
            $q->bindValue(':status', 1);
            //$q->bindValue(':id', (int)$id);
            $q->bindValue(':category', (int) $category);
            $q->execute();
            $r = $q->fetchAll();
            if (empty($r)) {
                $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlLimit);
                $q->bindValue(':id_lang', $this->lang->getCurrentID());
                $q->bindValue(':status', 1);
                //$q->bindValue(':id', (int)$id);
                $q->execute();
                $r = $q->fetchAll();
            }
        }

        $i = 0;
        foreach ($r as $item) {
            $currency = $this->getCurrency();
            $res[$i]['id'] = $item['id'];
            $res[$i]['price'] = $item['price'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['name'] = $item['name'];
            $res[$i]['options'] = $item['options'];
            $res[$i]['manufacturer'] = $item['manufacturer'];
            $this->addStock($res[$i]);

            $image = new \EOS\System\Media\Image($this->container, 'media.product');
            $listimage = $image->getList($item['id']);
            foreach ($listimage as $single) {
                $img = $image->getObject($item['id'], $single['id'], \EOS\System\Media\Image::IMAGE_LR);
                $res[$i]['url-0'] = (!empty((array)$img)) ? $img->url : '';
            }
            $i++;
        }

        return $res;
    }

    public
    function getManufacturerList($id_lang, $listid = [])
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__manufacturer m'))
            ->select(null)->select('m.id, ml.name')
            ->where('ml.id_lang = ' . (int) $id_lang)
            ->where('m.status = 1')
            ->innerJoin($this->db->tableFix('#__manufacturer_lang ml ON ml.id_manufacturer = m.id'))
            ->orderBy('ml.name');
        if (!empty($listid)) {
            $query->where('m.id in (select id_manufacturer from #__product where id in (' . implode(", ", $listid) . '))');
        }
        $list = $query->fetchAll();
        return $list;
    }

    public
    function getCategoriesList($id_lang, $id_category = null, $listid = [])
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pc.id, pcl.name')
            ->where('pcl.id_lang = ' . (int) $id_lang)
            ->where('pc.status = 1')
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'))
            ->orderBy('pcl.name');
        if (isset($id_category) && $id_category) {
            $query->where('pc.parent = ' . (int) $id_category);
        } else {
            $query->where('pc.parent = 0');
        }
        if (!empty($listid)) {
            $query->where('pc.id in (select id_category from #__product_product_category where id_product in (' . implode(", ", $listid) . '))');
        }
        $list = $query->fetchAll();
        if (count($list) == 0) {
            $queryparent = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                ->select(null)->select('pc.parent')
                ->where('pc.id = ' . (int) $id_category)
                ->where('pc.status = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
            $parent = $queryparent->fetch();
            $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                ->select(null)->select('pc.id, pcl.name')
                ->where('pcl.id_lang = ' . (int) $id_lang)
                ->where('pc.parent = ' . (int) $parent['parent'])
                ->where('pc.status = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'))
                ->orderBy('pcl.name');
            if (!empty($listid)) {
                $query->where('pc.id in (select id_category from #__product_product_category where id_product in (' . implode(", ", $listid) . '))');
            }
            $list = $query->fetchAll();
        }
        return $list;
    }

    public function getCategoryParent($id_lang, $id_category)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pc.parent')
            ->where('pc.id = ' . (int) $id_category)
            ->where('pc.status = 1')
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
        $r = $query->fetch();
        if ($r['parent'] != 0) {
            $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                ->select(null)->select('pc.id, pcl.name')
                ->where('pc.id = ' . (int) $r['parent'])
                ->where('pc.status = 1')
                ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
            $res = $query->fetch();
        } else {
            $res['id'] = 0;
            $res['name'] = "Tutte le categorie";
        }
        return $res;
    }

    public
    function getCategoryChild($id_category)
    {
        $list = $id_category;
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pc.id')
            ->where('pc.parent = ' . (int) $id_category)
            ->where('pc.status = 1');
        $childs = $query->fetchAll();
        if (isset($childs) && $childs != '') {
            foreach ($childs as $child) {
                $list .= ',' . $child['id'];
                $querychild = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
                    ->select(null)->select('pc.id')
                    ->where('pc.parent = ' . (int) $child['id'])
                    ->where('pc.status = 1');
                $childsofchild = $querychild->fetchAll();
                if (isset($childsofchild) && $childsofchild != '') {
                    foreach ($childsofchild as $childofchild) {
                        $list .= ',' . $childofchild['id'];
                    }
                }
            }
        }
        //print_r($list); exit();
        return $list;
    }

    public
    function getManufacturerTitle($id_lang, $id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__manufacturer m'))
            ->select(null)->select('ml.name')
            ->where('ml.id_lang = ' . (int) $id_lang)
            ->where('ml.id = ' . (int) $id)
            ->where('m.status = 1')
            ->innerJoin($this->db->tableFix('#__manufacturer_lang ml ON ml.id_manufacturer = m.id'));
        $title = $query->fetch();
        return $title['name'];
    }

    public
    function getCategoryTitle($id_lang, $id)
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_category pc'))
            ->select(null)->select('pcl.name')
            ->where('pcl.id_lang = ' . (int) $id_lang)
            ->where('pc.id = ' . (int) $id)
            ->where('pc.status = 1')
            ->innerJoin($this->db->tableFix('#__product_category_lang pcl ON pcl.id_category = pc.id'));
        $title = $query->fetch();
        return $title['name'];
    }

    public
    function getAttribute($id_lang, $id_group, $listid = [])
    {
        $query = $this->db->newFluent()->from($this->db->tableFix('#__product_attribute pa'))
            ->select(null)->select('pal.name')->select('pa.id')
            ->where('pal.id_lang = ' . (int) $id_lang)
            ->where('pa.id_attribute_group = ' . (int) $id_group)
            ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pal.id_attribute = pa.id'))
            ->orderBy('pal.name');
        if (!empty($listid)) {
            $query->where('pa.id in (select id_attribute from #__product_product_attribute_combination where id_product in (' . implode(", ", $listid) . '))');
        }
        $r = $query->fetchAll();
        return $r;
    }

    public function getProductsChangeFreeByPrice($price)
    {
        $count = 0;
        //print_r('price '.$price); exit();
        $sqlSelect = 'select p.id, p.price, pl.name, pl.description, pl.options, ml.name as manufacturer ';
        $sqlFrom = ' from eos_product p ';
        $sqlInner = ' inner join eos_product_lang pl on p.id = pl.id_product ';
        $sqlInner .= ' inner join eos_manufacturer m on p.id_manufacturer = m.id ';
        $sqlInner .= ' inner join eos_manufacturer_lang ml on m.id = ml.id_manufacturer ';
        $sqlInner .= ' inner join eos_product_stock ps on p.id = ps.id_product ';
        $sqlWhere = ' where pl.id_lang = :id_lang and p.status = :status and p.type_condition <> 1 ';
        $sqlWhereAdd = ' and p.price <= :price ';
        $sqlWhereAdd .= ' and ps.quantity_available > 5 '; // or p.out_of_stock = 1
        $sqlLimit = ' order by pl.name ';
        $q = $this->db->prepare($sqlSelect . $sqlFrom . $sqlInner . $sqlWhere . $sqlWhereAdd . $sqlLimit);
        $q->bindValue(':id_lang', $this->lang->getCurrentID());
        $q->bindValue(':status', 1);
        //$q->bindValue(':id', (int)$id);
        $q->bindValue(':price', $price);
        $q->execute();
        $r = $q->fetchAll();
        $res = [];
        $i = 0;
        if ($r != null) {
            foreach ($r as $item) {
                $res[$i]['id'] = $item['id'];
                $res[$i]['price'] = $item['price'];
                $res[$i]['name'] = $item['name'];
                $res[$i]['description'] = $item['description'];
                $res[$i]['options'] = $item['options'];
                $res[$i]['manufacturer'] = $item['manufacturer'];
                $tblAttribute = $this->db->tableFix('#__product_product_attribute_combination ppac');
                $query = $this->db->newFluent()->from($tblAttribute)
                    ->select('pal.name as attribute')
                    ->innerJoin($this->db->tableFix('#__product_attribute pa ON pa.id = ppac.id_attribute'))
                    ->innerJoin($this->db->tableFix('#__product_attribute_lang pal ON pa.id = pal.id_attribute'))
                    ->where('id_attribute_group', 1)
                    ->where('id_product', $item['id']);
                $rca = $query->fetch();
                $res[$i]['vintage'] = $rca['attribute'];
                $i++;
            }
        }
        return $res;
    }
}
