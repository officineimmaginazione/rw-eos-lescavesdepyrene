<?php

// Css
$css = \EOS\System\Util\ArrayHelper::getMdStr($this->data['options'], ["css"]);

?>

<!DOCTYPE html>
<html lang="<?php echo $this->lang->getCurrent()->iso; ?>">
<?php include 'inc/head.inc.php'; ?>

<body class="blog <?php echo $css; ?>">
	<?php include 'inc/header.inc.php'; ?>
	<section class="container blog-body">
		<?php $this->writeContent(); ?>
	</section>
	<?php include 'inc/footer.inc.php'; ?>

	<?php
	//(new \EOS\Components\Content\Widgets\CategoryWidget($this, '43'))->write();
	$this->addScriptLink($this->path->getExtraUrlFor('system', 'resource/keepalive.js'), true);
	$this->writeScript();
	?>
</body>

</html>