<?php

// Css
$css = \EOS\System\Util\ArrayHelper::getMdStr($this->data['options'], ["css"]);

// Prendo tutti i campi extra
$extra = \EOS\System\Util\ArrayHelper::getArray($this->data['options'], 'extra');

// Controllo che la variabile sia stata riempita
if (isset($extra) && count($extra)) {
	
	// Recupero form dei contatti
	$contact_form = \EOS\System\Util\ArrayHelper::getMdStr($extra, [0, "html", 0]);
	
}

// De alloco la variabile per non occupare spazio inutile
unset($extra);

?>

<!DOCTYPE html>
<html lang="<?php echo $this->lang->getCurrent()->iso; ?>">
    <?php include 'inc/head.inc.php'; ?>
    <body class="contacts <?php echo $css; ?>">
			<?php include 'inc/header.inc.php'; ?>
			<section class="container contacts-body">
				<?php if (($this->data['options']['showtitle']) && (!empty($this->data['title']))) : ?>
				<section class="text-center">
					<h1 class="section-title"><?php echo $this->data["title"]; ?></h1>
				</section>
				<?php endif; ?>
				<div class="row align-items-center">
					<div class="col-12 col-md-5 col-lg-6">
						<?php echo $this->data["content"]; ?>
					</div>
					<div class="col-12 col-md-7 col-lg-6">
						<div class="form-container">
							<?php echo $contact_form; ?>
						</div>
					</div>
				</div>
			</section>
			<?php include 'inc/footer.inc.php'; ?>
			<?php
			
			//(new \EOS\Components\Content\Widgets\CategoryWidget($this, '43'))->write();
			$this->addScriptLink($this->path->getExtraUrlFor('system', 'resource/keepalive.js'), true);
			$this->writeScript();
			?>
    </body>
</html>