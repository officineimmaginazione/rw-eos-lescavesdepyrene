<?php

$fv = new EOS\System\Util\FormValidator($this->controller, $this->request, $this->response, []);
$fv->sanitizeMultilineFields = ['info'];
if ($fv->prepareInputDataFromJson())
{
    $data = $fv->getInputData();
    $error = '';
    $urlRequest = EOS\System\Routing\PathHelper::removeTrailingSlash(EOS\System\Util\ArrayHelper::getStr($data, 'url'));
    $urlDomain = EOS\System\Routing\PathHelper::removeTrailingSlash($this->path->getDomainUrl());
    if (!EOS\System\Util\StringHelper::startsWith($urlRequest, $urlDomain))
    {
        $error = $this->lang->trans('content.formcontacts.error.general');
    } else
    if (empty(EOS\System\Util\ArrayHelper::getStr($data, 'name')))
    {
        $error = $this->lang->trans('content.formcontacts.error.name');
    } else
    if (filter_var(EOS\System\Util\ArrayHelper::getStr($data, 'email'), FILTER_VALIDATE_EMAIL) === false)
    {
        $error = $this->lang->trans('content.formcontacts.error.email');
    } else
    if (empty(EOS\System\Util\ArrayHelper::getStr($data, 'message')))
    {
        $error = $this->lang->trans('content.formcontacts.error.message');
    } else
    if (empty(EOS\System\Util\ArrayHelper::getStr($data, 'privacy')))
    {
        $error = $this->lang->trans('content.formcontacts.error.privacy');
    }
    if (empty($error))
    {
        $mail = new \EOS\System\Mail\Mail($this->controller->container);
        $mail->subject = 'Les Caves de Pyrene: richiesta informazioni';
        $mail->to = 'info@lescaves.it';
        $mail->replyto = $this->encodeHtml(EOS\System\Util\ArrayHelper::getStr($data, 'email'));
        $mail->CharSet = "text/html; charset=UTF-8;";
        $mail->message .= 'Nome e cognome: ' . $this->encodeHtml(EOS\System\Util\ArrayHelper::getStr($data, 'name')) . '<br>';
        $mail->message .= 'Email: ' . $this->encodeHtml(EOS\System\Util\ArrayHelper::getStr($data, 'email')) . '<br>';
        $mail->message .= 'Messaggio: <br>' . nl2br($this->encodeHtml(EOS\System\Util\ArrayHelper::getStr($data, 'message'))) . '<br><br>';
        if ($this->encodeHtml(EOS\System\Util\ArrayHelper::getBool($data, 'privacy')) == 1)
            $mail->message .= 'Privacy:  Approvato<br><br>';
        if ($this->encodeHtml(EOS\System\Util\ArrayHelper::getBool($data, 'newsletter')) == 1)
            $mail->message .= 'Newsletter:  Approvato<br><br>';
        $mail->altbody = $mail->message;
        $ip = $this->controller->container->get('environment')['REMOTE_ADDR'];
        //$mail->message .= 'IP: '.$ip. '<br>';
        //$browser = $this->controller->container->get('environment')['HTTP_USER_AGENT'];
        //$mail->message .= 'Browser: '.$browser;
        if ($this->encodeHtml(EOS\System\Util\ArrayHelper::getBool($data, 'newsletter')) == 1)
        {
            $sendinblue = new \Mailin("https://api.sendinblue.com/v2.0", "");
            if($this->lang->getCurrent()->iso == 'it') {
                $data = array("email" => EOS\System\Util\ArrayHelper::getStr($data, 'email'),
                    "attributes" => array("NAME" => $this->encodeHtml(EOS\System\Util\ArrayHelper::getStr($data, 'name'))),
                    "listid" => array(14)
                );
            }
            else {
                $data = array("email" => EOS\System\Util\ArrayHelper::getStr($data, 'email'),
                    "attributes" => array("NAME" => $this->encodeHtml(EOS\System\Util\ArrayHelper::getStr($data, 'name'))),
                    "listid" => array(14)
                );
            }
            $sendinblue->create_update_user($data);
        }
        if ($mail->sendEmail($error))
        {
            $fv->setMessage($this->lang->trans('content.formcontacts.thankyou'));
            $fv->setResult(true);
        } else
        {
            $error = $this->lang->trans('content.formcontacts.error.general') . '<br>' . $error;
        }
        if (!empty($error))
        {
            $fv->setMessage($error);
        }
    } else
    {
        $fv->setMessage($error);
    }
    $this->response = $fv->toJsonResponse();
} else
{
    // Forzo un 404 facendo finta di non esistere
    //throw new \Slim\Exception\NotFoundException($this->request, $this->response); // Rompe i webmaster tool
    $fv->setMessage('Error');
    $this->response = $fv->toJsonResponse();
}