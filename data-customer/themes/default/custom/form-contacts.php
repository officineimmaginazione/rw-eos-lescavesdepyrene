<h3 class="form-title"><?php $this->view->transP('content.formcontacts.title'); ?></h3>
<form id="contacts" method="POST" class="form">
    <div class="form-group">
				<label for="contacts-name"><?php $this->view->transP('content.formcontacts.name'); ?></label>
				<input type="text" class="form-control" name="name" id="name" placeholder="<?php $this->view->transP('content.formcontacts.name.example'); ?>">
		</div>
		<div class="form-group">
				<label for="contacts-email"><?php $this->view->transP('content.formcontacts.email'); ?></label>
				<input type="email" class="form-control" name="email" id="email" placeholder="<?php $this->view->transP('content.formcontacts.email.example'); ?>">
		</div>
    <div class="form-group">
        <label for="contacts-message"><?php $this->view->transP('content.formcontacts.message'); ?></label>
        <textarea class="form-control" id="message" name="message" rows="6" placeholder="<?php $this->view->transP('content.formcontacts.message.example'); ?>"></textarea>
    </div>
    <div class="form-group">
			<div class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input" id="privacy" name="privacy">
				<label class="custom-control-label" for="privacy"><a href="<?php //echo $this->view->path->getUrlFor('Content', 'page/53'); ?>#"><?php $this->view->transP('content.formcontacts.privacy.message'); ?></a></label>
			</div>
    </div>
    <div class="text-center">
        <button id="contacts-send" class="btn btn-primary send"><?php $this->view->transP('content.formcontacts.send'); ?></button>
    </div>
    <?php $this->view->writeTokenHtml(); ?>
    <input type="hidden" value="savedata" id="contacts-url" name="url">
</form>


<?php
\EOS\UI\Loader\Library::load($this->view, 'Bootbox');
$this->view->addScriptLink($this->view->path->getExtraUrlFor('system', 'resource/app.js/'));
$this->view->startCaptureScript();
?>
<script>
    function sendContact()
    {
        var data = $('#contacts').serializeFormJSON();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->view->path->getUrlFor('Content', 'page/75'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    $('#contacts input:text').val('');
                    $('#contacts input[type=email]').val('');
                    $('#contacts textarea').val('');
                    $('#contacts input[type=checkbox]').prop('checked', false);
                    bootbox.alert(json.message);
                }
                else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
        return false;
    }
    $(function ()
    {
        $('#contacts-send').click(function (ev)
        {
            $('#contacts-send').prop("disabled", true);
            ev.preventDefault();
            sendContact();
            setTimeout(function ()
            {
                $('#contacts-send').prop("disabled", false);
            }, 800);
        });
        $('#contacts-url').val((window.location != window.parent.location) ? document.referrer : document.location);
    });
</script>
<?php
$this->view->endCaptureScript();
