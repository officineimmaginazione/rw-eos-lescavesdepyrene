<?php if (!empty($this->data['options']['extra'])) { ?>
    <?php foreach ($this->data['options']['extra'] as $extra) { ?>
        <?php if (isset($extra['image'][0])) { ?>
            <?php
            $label = (isset($extra['text']) && $extra['text'][0] != "") ? $extra['text'][0] : $this->view->trans('content.article.banner.label');
            $label = strip_tags($label);
            ?>
            <div class="banner-image">
                <?php if (isset($extra['link'])) { ?>
                    <a title="<?php echo $label; ?>" href="<?php echo $extra['link'][0]; ?>">
                        <img class="img-fluid" loading="lazy" src="<?php echo $extra['image'][0]; ?>" alt="<?php echo $label; ?>" />
                    </a>
                <?php } else { ?>
                    <img class="img-fluid" loading="lazy" src="<?php echo $extra['image'][0]; ?>" alt="<?php echo $label; ?>" />
                <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>