<?php

// Css
$css = \EOS\System\Util\ArrayHelper::getMdStr($this->data['options'], ["css"]);

// Prendo i campi extra della sezione "Carousel"
$carousel = \EOS\System\Util\ArrayHelper::getArray((new \EOS\Components\Content\Widgets\SectionWidget($this, "carousel"))->data["options"], "extra")[0];


// Prendo tutti i campi extra
$extra = \EOS\System\Util\ArrayHelper::getArray($this->data['options'], 'extra');

// Controllo che la variabile sia stata riempita
if (isset($extra) && count($extra)) {

	// Sezione 0
	$s0 = array(
		"title" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [0, "text", 0]),
		"img" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [0, "image", 0]),
		"content" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [0, "html", 0])
	);

	// Sezione 1
	$s1 = array(
		"title" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [1, "text", 0]),
		"img" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [1, "image", 0]),
		"content" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [1, "html", 0])
	);
}

// De alloco la variabile per non occupare spazio inutile
unset($extra);

?>

<!DOCTYPE html>
<html lang="<?php echo $this->lang->getCurrent()->iso; ?>">
<?php include 'inc/head.inc.php'; ?>

<body class="<?php echo $css; ?>">
	<?php include 'inc/header.inc.php'; ?>
	<?php if (isset($carousel) && !empty($carousel)) : ?>
		<div id="home-carousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<?php for ($i = 0; $i < count($carousel["image"]); $i++) : ?>
					<li data-target="#home-carousel" data-slide-to="<?php echo $i; ?>" <?php echo ($i == 0) ? 'class="active"' : ''; ?>></li>
				<?php endfor; ?>
			</ol>
			<div class="carousel-inner">
				<?php foreach ($carousel["image"] as $i => $image) : ?>
					<div class="carousel-item <?php echo ($i == 0) ? "active" : ""; ?>">
						<img class="img-fluid" src="<?php echo $image; ?>" alt="Carousel slide">
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (array_filter($s0)) : ?>
		<section class="homepage-section" id="scroll-depth">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-md-7">
						<?php if ($s0["title"]) : ?>
							<h2 class="section-title"><?php echo $s0["title"]; ?></h2>
						<?php endif; ?>
						<?php if ($s0["content"]) : ?>
							<div class="homepage-section-content"><?php echo $s0["content"]; ?></div>
						<?php endif; ?>
					</div>
					<div class="col-12 col-md-5 text-center">
						<?php if ($s0["img"]) : ?>
							<img class="img-fluid mt-4 mt-md-0" src="<?php echo $s0["img"]; ?>" alt="<?php echo $s0["title"]; ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
		<hr class="hr">
	<?php endif; ?>
	<?php if (array_filter($s1)) : ?>
		<section class="homepage-section" id="scroll-depth2">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-md-5 text-center img-box">
						<?php if ($s1["img"]) : ?>
							<img class="img-fluid mt-4 mb-md-0" src="<?php echo $s1["img"]; ?>" alt="<?php echo $s1["title"]; ?>" />
						<?php endif; ?>
					</div>
					<div class="col-12 col-md-7 content-box">
						<?php if ($s1["title"]) : ?>
							<h2 class="section-title mobile-title"><?php echo $s1["title"]; ?></h2>
						<?php endif; ?>
						<?php if ($s1["content"]) : ?>
							<div class="homepage-section-content"><?php echo $s1["content"]; ?></div>
						<?php endif; ?>
						<?php if ($s1["title"]) : ?>
							<h2 class="section-title"><?php echo $s1["title"]; ?></h2>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
		<hr class="hr-100">
	<?php endif; ?>
	<?php (new \EOS\Components\Content\Widgets\ArticleWidget($this))->write(); ?>
	<hr class="hr-100">
	<div class="container download-link">
		<?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'pre-footer'))->write(); ?>
	</div>
	<?php include 'inc/footer.inc.php'; ?>
	<?php
	//(new \EOS\Components\Content\Widgets\CategoryWidget($this, '43'))->write();
	$this->addScriptLink($this->path->getExtraUrlFor('system', 'resource/keepalive.js'), true);
	$this->writeScript();
	?>
</body>

</html>