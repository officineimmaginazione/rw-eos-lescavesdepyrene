<footer class="footer">
	<?php if (isset($this->controller->user)) { ?>
		<div class="container shop-add-footer">
			<?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'shop-add-footer'))->write(); ?>
		</div>
	<?php } ?>
	<div class="footer-top container">
		<hr class="hr" />
		<div class="row">
			<div class="col-12 col-md-4">
				<?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'footer-info-1'))->write(); ?>
			</div>
			<div class="col-12 col-md-4">
				<?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'footer-info-2'))->write(); ?>
			</div>
			<div class="col-12 col-md-4 d-none d-md-block">
				<img class="img-fluid" src="data-customer/themes/default/img/automobile.png" alt="" />
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'footer-credits'))->write(); ?>
		</div>
	</div>
</footer>
<?php
$this->addScriptLink($this->path->getThemeUrl() . "js/matchheight.min.js");
$this->addScriptLink($this->path->getThemeUrl() . "js/owl.carousel.min.js");
$this->addScriptLink($this->path->getThemeUrl() . "js/gdpr-cookie-notice.js");
$this->addScriptLink($this->path->getThemeUrl() . "js/gdpr-cookie-locales/" . $this->lang->getCurrent()->iso . ".js");
$this->addScriptLink($this->path->getThemeUrl() . "js/bootstrap3-typeahead.min.js");

$this->startCaptureScript();
?>
<script>
	gdprCookieNotice({
		locale: '<?php echo $this->lang->getCurrent()->iso; ?>',
		timeout: 500,
		expiration: 30,
		//statement: 'http://www.treasuresgarden.lo:8888/it/',
		performance: ['JSESSIONID'],
		analytics: ['ga'],
		marketing: ['SSID']
	});
</script>
<script async type="text/javascript">
</script>

<?php
$this->endCaptureScript();
