<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- <meta http-equiv="refresh" content="283"> -->
	<?php
	$this->writeHead();
	\EOS\UI\Loader\Library::load($this, 'Bootstrap');
	\EOS\UI\Loader\Library::load($this, 'Icons');
	\EOS\UI\Loader\Library::load($this, 'Lightbox');
	$this->addStyleSheetLink($this->path->getThemeUrl() . "css/owl-carousel/owl.carousel.min.css");
	$this->addStyleSheetLink($this->path->getThemeUrl() . "css/gdpr-cookie-notice.css");
	$this->addStyleSheetLink($this->path->getLibraryUrl() . "datepicker/1.9.0/css/bootstrap-datepicker3.min.css");
	$this->addStyleSheetLink($this->path->getThemeUrl() . "css/style.min.css");
	$this->writeLink();
	?>
	<link rel="stylesheet" href="/data-customer/themes/default/css/print.min.css" media="print">
	<!--[if lt IE 9]>
		<script src="<?php echo $this->path->getThemeUrl() . "js/html5shiv.min.js"; ?>"></script>
		<script src="<?php echo $this->path->getThemeUrl() . "js/respond.min.js"; ?>"></script>
		<script src="<?php echo $this->path->getThemeUrl() . "js/placeholder-fix.js"; ?>"></script>
	<![endif]-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-2077MK8ZC1"></script>
	<script>
		document.addEventListener('gdprCookiesEnabled', function(e) {
			if (e.detail.marketing) { //check if marketing cookies are enabled
				//marketing cookie code
			}
			if (e.detail.performance) { //check if performance cookies are enabled
				//performance cookie code
			}
			if (e.detail.analytics) { //check if analytics cookies are enabled
				//analytics cookie code
				window.dataLayer = window.dataLayer || [];

				function gtag() {
					dataLayer.push(arguments);
				}
				gtag('js', new Date());
				gtag('config', 'G-2077MK8ZC1');
			}
		});
	</script>
	<meta name="google-site-verification" content="00000000000000000" />
	<meta name="msvalidate.01" content="00000000000000000" />
</head>