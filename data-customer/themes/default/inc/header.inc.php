<!-- 
-
-	HEADER - INIZIO 
-	Descrizione: Testata contiene: logo, menu e sottomenu
-	Stili: header.scss
-
-->

<?php if (!isset($this->controller->user)) { ?>
    <header class="header header-istitutional">
        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container flex-column">
                <div class="navbar-top row w-100 align-items-center">
                    <div class="col-6 col-md-3 link-img">
                        <?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'header-btn-left'))->write(); ?>
                    </div>
                    <div class="col-6 col-md-3 order-md-last text-right link-img">
                        <?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'header-btn-right'))->write(); ?>
                    </div>
                    <div class="col-12 col-md-6">
                        <a class="navbar-brand" href="<?php echo $this->lang->getCurrent()->iso; ?>/">
                            <?php
                            if (isset($this->user->groups) && in_array(42, $this->user->groups)) {
                                echo '<img class="img-fluid" src="data-customer/themes/default/img/logo-enotecaves.png" alt="Les Caves de Pyrene" />';
                            } else {
                                echo '<img class="img-fluid" src="data-customer/themes/default/img/logo-institutional.png" alt="Les Caves de Pyrene" />';
                            } ?>
                        </a>
                    </div>
                </div>
                <div class="navbar-bottom row w-100">
                    <button class="navbar-toggler collapsed mr-auto" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'social'))->write(); ?>
                    <div class="navbar-collapse justify-content-md-center collapse" id="navbar-collapse">
                        <?php (new \EOS\Components\System\Widgets\MenuWidget($this, 'main-menu'))->write(); ?>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- HEADER - FINE -->
<?php } else { ?>
    <header class="header header-shop">
        <div class="shop-user-interface">
            <div class="container">
                <div class="row">
                    <div class="d-flex align-items-center col-9 col-sm-6 col-md-7 col-lg-8 col-xl-9 box-left">
                        <?php if (isset($this->user->groups) && !in_array(42, $this->user->groups)) { ?>
                            <?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'shop-header-left'))->write(); ?>
                        <?php } ?>
                    </div>
                    <div class="col-3 col-sm-6 col-md-5 col-lg-4 col-xl-3 box-right">
                        <?php echo ($this->viewagent) ? '<p>Profilo agente</p>' : '<p>Il mio account</p>'; ?>
                        <div class="dropdown ml-2">
                            <button class="btn btn-default btn-circle btn-outline dropdown-toggle" type="button" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user"></i>
                                <span class="sr-only"><?php echo ($this->viewagent) ? 'Profilo agente' : 'Il mio account'; ?></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUser">
                                <li class="dropdown-item"><a href="<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>">Profilo</a></li>
                                <li class="dropdown-item"><a href="<?php echo $this->path->getUrlFor('Account', 'user/change-pass'); ?>">Cambia password</a></li>
                                <li class="dropdown-item"><a href="<?php echo $this->path->getUrlFor('Account', 'user/logout'); ?>">Log out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container flex-column">
                <div class="navbar-top row w-100 align-items-center">
                    <div class="col-6 col-md-3 link-img">

                    </div>
                    <div class="col-6 col-md-3 order-md-last text-right">
                        <a href="<?php echo $this->path->getUrlFor('Cart', 'cart/index'); ?>" id="btn-new" class="btn btn-primary btn-circle"><i class="fas fa-shopping-cart"></i></a>
                    </div>
                    <div class="col-12 col-md-6">
                        <a class="navbar-brand" href="<?php echo $this->path->getUrlFor('Product', 'search/index'); ?>?category=0&price=0-10000&manufacturer=0&vintage=0&region=0&vineyards=0&country=0&typecondition=0">
                            <?php
                            if (isset($this->user->groups) && in_array(42, $this->user->groups)) {
                                echo '<img class="img-fluid" src="data-customer/themes/default/img/logo-enotecaves.png" alt="Les Caves de Pyrene" />';
                            } else {
                                echo '<img class="img-fluid" src="data-customer/themes/default/img/logo-institutional.png" alt="Les Caves de Pyrene" />';
                            } ?>
                        </a>
                    </div>
                </div>
                <div class="navbar-bottom row w-100">
                    <button class="navbar-toggler collapsed mr-auto" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse justify-content-md-center collapse" id="navbar-collapse">
                        <?php
                        if (!in_array(42, $this->user->groups)) {
                            (new \EOS\Components\System\Widgets\MenuWidget($this, 'shop-menu'))->write();
                        } else {
                            (new \EOS\Components\System\Widgets\MenuWidget($this, 'shop-menu-2'))->write();
                        }
                        ?>
                    </div>
                </div>
            </div>
        </nav>
    </header>
<?php } ?>