<!-- 
-
-	SIDEBAR - INIZIO 
-	Descrizione: Colonna sulla sinitra, contiene: logo, menu e sottomenu
-	Stili: sidebar.scss
-
-->
<aside class="sidebar">
	<div class="d-flex sidebar-menu">
		<?php (new \EOS\Components\System\Widgets\MenuWidget($this, 'side-menu'))->write(); ?>
	</div>
</aside>
<!-- SIDEBAR - FINE -->



<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->