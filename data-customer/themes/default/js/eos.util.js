(function ($)
{
    $.fn.serializeFormJSON = function ()
    {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function ()
        {
            if (o[this.name])
            {
                if (!o[this.name].push)
                {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else
            {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

function hideOtherLanguage(id)
{
    $('.translatable-field').hide();
    $('.lang-' + id).show();
}

function changeLanguage(item)
{
    id = item.getAttribute("data-key1");
    hideOtherLanguage(id);
    $('.select-lang').html(item.getAttribute("data-key2")+" <span class=\"caret\"></span>");
}