gdprCookieNoticeLocales.it = {
    description:
        "Utilizziamo i cookie per offrirti una migliore esperienza di navigazione, personalizzare i contenuti e gli annunci, per fornire le funzionalità dei social media e per analizzare il nostro traffico. Leggi come utilizziamo i cookie e come puoi controllarli facendo clic su Impostazioni cookie. Acconsenti ai nostri cookie se continui a utilizzare questo sito web.",
    settings: "Impostazioni cookie",
    accept: "Accetto",
    statement: "Scopri di più",
    save: "Salva impostazioni",
    always_on: "Sempre attive",
    cookie_essential_title: "Cookie essenziali del sito web",
    cookie_essential_desc:
        "I cookie necessari aiutano a rendere fruibile un sito web abilitando le funzioni di base come la navigazione della pagina e l'accesso alle aree protette del sito. Il sito web non può funzionare correttamente senza questi cookie.",
    cookie_performance_title: "Cookie per le performance",
    cookie_performance_desc:
        "Questi cookie vengono utilizzati per migliorare le prestazioni e la funzionalità del sito web ma non sono essenziali per l'utilizzo. Ad esempio, memorizza la tua lingua preferita o la regione in cui ti trovi.",
    cookie_analytics_title: "Cookie analitici",
    cookie_analytics_desc:
        "Utilizziamo i cookie analitici per aiutarci a misurare il modo in cui gli utenti interagiscono con il contenuto del sito Web, il che ci aiuta a personalizzare la piattaforma al fine di migliorare la tua esperienza.",
    cookie_marketing_title: "Cookie di marketing",
    cookie_marketing_desc:
        "Questi cookie vengono utilizzati per rendere i messaggi pubblicitari più pertinenti per te e per i tuoi interessi. L'intenzione è quella di visualizzare annunci pertinenti e coinvolgenti per il singolo utente e quindi più preziosi per editori e inserzionisti di terze parti.",
};
