var mapService = {
    debugMode: false,
    mapDiv: 'map',
    map: null,
    geocoder: null,
    infoWindow: null,
    placesService: null,
    asyncInit: true,
    defLat: 41.890251,
    defLng: 12.492373,
    defZoom: 6,
    defMapTypeId: 'roadmap',
    defMarkerIcon: '',
    defScaleControl: true,
    markers: [],
    useCluster: false,
    markerClusterer: null,
    searchId: "searchCity",
    searchSubmit: "searchSubmit",
    addDebug: function (message)
    {
        if (this.debugMode)
        {
            console.log(message);
        }
    },
    getContainer: function ()
    {
        return document.getElementById(this.mapDiv);
    },
    init: function ()
    {
        this.map = new google.maps.Map(this.getContainer(), {
            center: {lat: this.defLat, lng: this.defLng},
            zoom: this.defZoom,
            scaleControl: this.defScaleControl,
            mapTypeId: this.defMapTypeId,
        });
        this.geocoder = new google.maps.Geocoder();
        this.infoWindow = new google.maps.InfoWindow;
        this.placesService = new google.maps.places.PlacesService(this.map);
        this.initAutocomplete();

        if (this.asyncInit)
        {
            this.trigger('init');
        } else
        {
            setTimeout(function ()
            {
                mapService.trigger('init');
            }, 50);
        }
    },
    on: function (eventName, eventFunc)
    {
        this.addDebug('on.' + eventName);
        $(this.getContainer()).on('mapservice-' + eventName, eventFunc);
    },
    trigger: function (eventName)
    {
        this.addDebug('trigger.' + eventName);
        $(this.getContainer()).trigger('mapservice-' + eventName);

    },
    getCurrentPosition: function (loadEv, errorEv)
    {
        // Try HTML5 geolocation.
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(function (position)
            {
                mapService.addDebug(position);
                loadEv(position);
            }, function (err)
            {
                mapService.addDebug(err);
                errorEv(err);
            }, {maximumAge: 60000, timeout: 5000, enableHighAccuracy: true});
        } else
        {
            mapService.addDebug("Browser doesn't support Geolocation");
            errorEv(null);
        }
    },
    setCurrentPosition: function (loadEv)
    {
        this.getCurrentPosition(function (position)
        {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            mapService.map.setCenter(pos);
            if (loadEv === undefined)
            {
                mapService.showInfoWindow('Location found.');
            } else
            {
                loadEv();
            }


        }, function (err)
        {
            var message = '';
            if (err === null)
            {
                message = "Browser doesn't support Geolocation";
            } else
            {
                message = 'Unable load position';
            }
            mapService.showInfoWindow(message);
        });
    },
    showInfoWindow: function (message)
    {
        this.infoWindow.setPosition(this.map.getCenter());
        this.infoWindow.setContent(message);
        this.infoWindow.open(this.map);
    },
    textSearch: function (queryText, responseEv)
    {
        var request = {
            location: this.this.map.getCenter(),
            radius: 1500,
            query: queryText
        };

        this.placesService.textSearch(request, responseEv);
    },
    addCustomMarker: function (params)
    {
        if (this.useCluster)
        {
            params.map = undefined;
        }
        var marker = new google.maps.Marker(params);
        this.markers.push(marker);
        return marker;
    },
    addMarker: function (position, title, label)
    {
        return this.addCustomMarker({
            position: position,
            map: this.map,
            title: title,
            label: label,
            icon: this.defMarkerIcon
        });
    },
    addMarkerDraggable: function (position, title, label)
    {
        var marker = this.addCustomMarker({
            position: position,
            map: this.map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            title: title,
            label: label,
            icon: this.defMarkerIcon
        });
        marker.addListener('click', function ()
        {
            if (marker.getAnimation() !== null)
            {
                marker.setAnimation(null);
            } else
            {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        });

        return marker;
    },
    clearMarkers: function ()
    {
        for (var i = 0; i < this.markers.length; i++)
        {
            this.markers[i].setMap(null);
        }
    },
    clearMarkersClusterer: function ()
    {
        if (this.markerClusterer !== null)
        {
            this.markerClusterer.clearMarkers();
            this.markers = [];
        }
        this.markerClusterer = null;
    },
    asyncShiftList: function (list, asyncMethod, blockSize, blockTimer)
    {
        var asyncSize = (typeof blockSize !== 'undefined') ? blockSize : 200;
        var timeOut = (typeof blockTimer !== 'undefined') ? blockTimer : 10;
        var asyncExecute = function ()
        {
            mapService.addDebug('asyncLoadList: ' + list.length.toString());
            if (list.length > asyncSize)
            {
                var count = asyncSize;
            } else
            {
                var count = list.length;
                mapService.addDebug('asyncLoadList - End');
            }
            for (i = 0; i < count; i++)
            {
                asyncMethod(list.shift());
            }
            if (list.length > 0)
            {
                // Rilancio la funzione se la lista è ancora piena
                // questo evita di crea X timer e devo aver finito la procedura in attiva
                // inoltre dovrebbe dare respiro al pc/dispositivo
                setTimeout(asyncExecute, timeOut);
            }
        };
        // Eseguo la funzione 1 volta
        setTimeout(asyncExecute, timeOut);
    },
    addApproximatelyMeter: function (v, meters)
    {
        // Non è preciso il calcolo ma indicativo
        return  (0.0000089 * meters) + v;
    },
    loadMarkerClusterer: function (options)
    {
        this.markerClusterer = new MarkerClusterer(this.map, this.markers, options);
        return this.markerClusterer;
    },
    geocodeAddress: function(geocoder, resultsMap) {
        var address = document.getElementById(this.searchId).value;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });
            } else {
            alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    },
    initAutocomplete: function() {
        // Create the search box and link it to the UI element.
        const input = document.getElementById(this.searchId + 'Input');
        const searchBox = new google.maps.places.SearchBox(input);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        this.map.addListener("bounds_changed", () => {
            searchBox.setBounds(this.map.getBounds());
        });

        
        let markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener("places_changed", () => {
            const places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            // Clear out the old markers.
            markers.forEach((marker) => {
                marker.setMap(null);
            });
            markers = [];
            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                const icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25),
                };
                // Create a marker for each place.
                markers.push(
                    new google.maps.Marker({
                        map,
                        icon,
                        title: place.name,
                        position: place.geometry.location,
                    })
                );
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            this.map.fitBounds(bounds);
        });
    }
};