<?php

// Css
$css = \EOS\System\Util\ArrayHelper::getMdStr($this->data['options'], ["css"]);

// Prendo tutti i campi extra
$extra = \EOS\System\Util\ArrayHelper::getArray($this->data['options'], 'extra');

// Controllo che la variabile sia stata riempita
if (isset($extra) && count($extra)) {

	// Sezione 0
	$s0 = array(
		"title" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [0, "text", 0]),
		"img" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [0, "image", 0]),
		"content" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [0, "html", 0])
	);

	// Sezione 1
	$s1 = array(
		"title" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [1, "text", 0]),
		"img" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [1, "image", 0]),
		"content" => \EOS\System\Util\ArrayHelper::getMdStr($extra, [1, "html", 0])
	);
}

// De alloco la variabile per non occupare spazio inutile
unset($extra);

// De alloco la variabile per non occupare spazio inutile
unset($extra);
\EOS\UI\Loader\Library::load($this, 'Bootbox');
?>

<!DOCTYPE html>
<html lang="<?php echo $this->lang->getCurrent()->iso; ?>">
<?php include 'inc/head.inc.php'; ?>

<body class="contacts <?php echo $css; ?>">
	<?php include 'inc/header.inc.php'; ?>
	<section class="container contacts-body">
		<div class="row align-items-center">
			<div class="col-12 col-md-7">
				<?php if ($this->data["title"]) : ?>
					<h2 class="section-title"><?php echo $this->data["title"]; ?></h2>
				<?php endif; ?>
				<?php if ($this->data["content"]) : ?>
					<div class="homepage-section-content"><?php echo $this->data["content"]; ?></div>
				<?php endif; ?>
			</div>
			<div class="col-12 col-md-5 text-center">
				<?php if ($s0["img"]) : ?>
					<img class="img-fluid mt-4 mt-md-0" src="<?php echo $s0["img"]; ?>" alt="<?php echo $this->data["title"]; ?>" />
				<?php endif; ?>
			</div>
		</div>
		<div class="row mt-5">
			<?php if (!empty($s0["title"])) : ?>
				<div class="col-12">
					<div class="text-center">
						<h1 class="section-title"><?php echo $s0["title"]; ?></h1>
					</div>
				</div>
			<?php endif; ?>
			<div class="col-12 offset-md-2 col-md-8 offset-lg-3 col-lg-6 mb-3">
				<div class="input-group justify-content-center search-city">
					<input id="searchCityInput" class="form-control" type="text" placeholder="Inserisci indirizzo, città, CAP" aria-label="Inserisci indirizzo, città, CAP" />
				</div>
			</div>
			<div class="col-12">
				<div id="map-container">
					<div id="map"></div>
				</div>
			</div>
		</div>
		<div class="row mt-5 align-items-center">
			<div class="col-12 col-md-5 text-center">
				<?php if ($s1["img"]) : ?>
					<img class="img-fluid mt-4 mt-md-0" src="<?php echo $s1["img"]; ?>" alt="<?php echo $s1["title"]; ?>" />
				<?php endif; ?>
			</div>
			<div class="col-12 col-md-7" id="sec3">
				<?php if ($s1["title"]) : ?>
					<h2 class="section-title"><?php echo $s1["title"]; ?></h2>
				<?php endif; ?>
				<?php if ($s1["content"]) : ?>
					<div class="homepage-section-content"><?php echo $s1["content"]; ?></div>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<div class="modal" id="requestInviteModal" tabindex="-1" role="dialog" aria-labelledby="requestInviteModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Compila i dati sottostanti per richidere l'invito</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form class="form-container add-to-cart" id="add-cashback" method="POST">
					<div class="modal-body">
						<div class="form-group">
							<label for="total">Inserisci la tua email.</label>
							<input class="form-control" id="email" name="email" aria-describedby="email" placeholder="es. mario.rossi@gmail.com" value="">
						</div>
						<div class="form-group">
							<label for="total">Inserisci il tuo nome.</label>
							<input class="form-control" id="name" name="name" aria-describedby="email" placeholder="Mario Rossi" value="">
						</div>
						<?php $this->writeTokenHtml(); ?>
						<input type="hidden" value="requestinvite" name="command">
						<input type="hidden" value="0" id="advisor" name="advisor">
						<input type="hidden" value="0" id="advisoraddress" name="advisoraddress">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light" data-dismiss="modal">Annulla</button>
						<button type="button" class="btn btn-primary" id="btnSendRequestInvite">Invia</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php include 'inc/footer.inc.php'; ?>
	<?php

	//(new \EOS\Components\Content\Widgets\CategoryWidget($this, '43'))->write();
	$this->addScriptLink($this->path->getExtraUrlFor('system', 'resource/keepalive.js'), true);
	$this->writeScript();
	?>
	<script src="<?php echo $this->path->getThemeUrl() ?>js/mapservice.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDM7i1IQF3yD_nQbNMZJsSSb6kRyzRGSTs&callback=mapService.init&libraries=places" async defer></script>
	<script src="<?php echo $this->path->getThemeUrl() ?>js/markerclusterer.js"></script>
	<script>
		$(document).ready(function() {
			loadCoords();
		});

		function runAjax(urlAjax, data, resultEvent) {
			$.ajax({
					type: 'POST',
					dataType: "json",
					url: urlAjax,
					data: JSON.stringify(data),
					contentType: "application/json",
				})
				.done(function(json) {
					if (json.result == true) {
						resultEvent(json);
					} else {
						if (json.message !== undefined) {
							bootbox.alert(json.message);
						}
					}
				})
				.fail(function(jqxhr, textStatus, error) {
					var err = textStatus + ", " + error;
					bootbox.alert(err);
				});
		}

		function loadCoords() {
			var url = "<?php echo $this->path->getUrlFor('Content', '/advisor/list') ?>";
			var data = {
				'command': 'coordsadvisor',
				"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>"
			};
			runAjax(url, data,
				function(data) {
					mapService.clearMarkersClusterer();

					mapService.useCluster = true;
					mapService.defMarkerIcon = '<?php echo $this->path->getThemeUrl() ?>img/marker.png'

					for (i = 0; i < data.coords.length; i++) {
						var v = data.coords[i];
						var position = new google.maps.LatLng(v.latitude, v.longitude);
						var m = mapService.addMarker(position, v.title, v.label);
						m.customData = data.coords[i];

						m.customData.contentString = '<div id="content">' +
							'<div id="siteNotice">' +
							'</div>' +
							'<h6 id="firstHeading" class="firstHeading">' + v.alias + '</h6>' +
							'<div id="bodyContent">' +
							'<p>Indirizzo: <b>' + v.address1 + ' - ' + v.city + '</b><br>' +
							'Telefono: <b>' + v.phone + '</b><br>';

						if (typeof v.phone_mobile !== 'undefined') {
							if (v.phone_mobile != null) {
								m.customData.contentString += 'WhatsApp: <b><a class="tertiary-color" href="https://wa.me/39' + v.phone_mobile.replace(/\s+/g, '') + '" target="_blank" title="WhatsApp" onclick="ga(\'send\', \'event\', \'sito\', \'whatsapp\', \'' + v.company + '\');" > ' + v.phone_mobile + '</a></b></p><br>';
							}
						}

						m.customData.contentString +=
							'<br><a class="btn btn-primary" id="btnModalRequestInvite" href="" onclick="event.preventDefault(); updateModal(' + v.id + ', ' + v.idaddress + '); $(\'#requestInviteModal\').modal(\'show\'); ">Richiedi invito</>' +
							'</div>' +
							'</div>';

						m.addListener('click', function() {
							mapService.showInfoWindow(this.customData.company);
							mapService.infoWindow.setPosition(this.getPosition());
							mapService.infoWindow.setContent(this.customData.contentString);
							mapService.infoWindow.open(mapService.map);
						});

					}

					var options = {
						imagePath: '<?php echo $this->path->getThemeUrl() ?>img/maps/m'
					};

					mapService.loadMarkerClusterer(options);

				});
		}

		$(function() {
			$("#submit-search").off('click').on('click', function(event) {
				event.preventDefault();
				loadCoords($('#search').serializeFormJSON());
				var scrollPos = $("h1").offset().top;
				$(window).scrollTop(scrollPos);
			});
			mapService.on('init',
				function() {
					mapService.setCurrentPosition(function() {
						mapService.map.setZoom(11);
					});

					loadCoords({
						"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>",
						'command': 'coordsfacilities'
					});
				});
		});

		$("#btnSendRequestInvite").click(function(event) {
			event.preventDefault();
			var url = "<?php echo $this->path->getUrlFor('Account', '/access/ajaxrequest') ?>";
			var data = {
				'iduser': $("#advisor").val(),
				'idaddress': $("#advisoraddress").val(),
				'email': $("#email").val(),
				'name': $("#name").val(),
				'command': 'coordsadvisor',
				"<?php echo $this->session->getTokenName() ?>": "<?php echo $this->session->getTokenValue() ?>"
			};
			runAjax(url, data,
				function(data) {
					if (data.result == true) {
						if (data.message !== undefined) {
							bootbox.alert(data.message);
							setTimeout(function() {
								window.location.reload(false);
							}, 1500);
						}
					} else {
						if (data.message !== undefined) {
							bootbox.alert(data.message);
						}
					}
				});
			$('#requestInviteModal').modal('hide');
		});

		function updateModal(id, idaddress) {
			$("#advisor").val(id);
			$("#advisoraddress").val(idaddress);
		}
	</script>
</body>

</html>