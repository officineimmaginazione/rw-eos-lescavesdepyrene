<section class="container register-container">
    <?php if ($this->valid) { ?>
        <div class="title-box">
            <h1>nuovo cliente</h1>
        </div>
        <div class="row form-header">
            <!-- <div class="d-flex justify-content-end flex-column col-4">
            <p>Aggiungi un<br />nuovo cliente</p>
        </div> -->
            <div class="d-flex justify-content-end flex-column col-12 typeuser">
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input radio-primary" type="radio" name="usertype" id="usertype1" value="0" checked>
                        <label class="form-check-label" for="usertype1">Privato</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input radio-primary" type="radio" name="usertype" id="usertype2" value="1">
                        <label class="form-check-label" for="usertype2">Azienda</label>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end flex-column col-6">
                <p>Informazioni<br />cliente</p>
            </div>
            <div class="d-flex justify-content-end flex-column col-6">
                <p>Indirizzo</p>
            </div>
            <!--
        <div class="d-flex justify-content-end flex-column col-4">
            <p>Indirizzo spedizione</p>
            <p>da inserire, se diverso da quello<br />di fatturazione</p>
        </div>
        -->
        </div>
        <form id="account-register" method="POST" autocomplete="off">
            <div id="account-register-container" class="container">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div class="form-group">
                            <input id="account-register-username" name="reg-username" placeholder="Nome utente (es. mariorossi, mariorossi1980, ecc.)" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <input id="account-register-password" name="reg-pwd1" placeholder="Password" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <input id="account-register-password2" name="reg-pwd2" placeholder="Ripeti password" type="password" class="form-control">
                        </div>
                        <div class="form-group company">
                            <input id="account-register-company" name="company" placeholder="Ragione Sociale" type="text" class="form-control">
                        </div>
                        <div class="form-group company">
                            <input id="account-register-vat" name="vat" placeholder="Partita Iva" type="text" class="form-control">
                        </div>
                        <div class="form-group company">
                            <input id="account-register-sdi" name="unicode" placeholder="Codice Univoco" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <input id="account-register-cif" name="cif" placeholder="Codice Fiscale*" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <input id="account-register-name" name="name" type="text" placeholder="Nome (es. Mario)*" class="form-control">
                        </div>
                        <div class="form-group">
                            <input id="account-register-surname" name="surname" type="text" placeholder="Cognome (es. Rossi)*" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="birthday" id="birthday" placeholder="Data di nascita (es. 01/01/1980)*" class="form-control" maxlength="10">
                        </div>
                        <div class="form-group">
                            <input id="account-register-phone" name="phone" type="text" placeholder="Telefono*" class="form-control">
                        </div>
                        <div class="form-group">
                            <input id="account-register-email" name="email" placeholder="Email (es. mario.rossi@gmail.com)" type="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-offset-6 col-sm-6">
                        <div class="form-group">
                            <input id="account-register-address" name="address1" type="text" placeholder="Indirizzo fatturazione* (via/strada/ecc. n° civico)" class="form-control">
                        </div>
                        <div class="form-group">
                            <select id="id-city-1" class="form-control" name="city1" type="text" placeholder="Città fatturazione*"></select>
                        </div>
                        <div class="form-group">
                            <input id="account-register-address2" name="address2" type="text" placeholder="Indirizzo spedizione (se diverso da fatturazione)" class="form-control">
                        </div>
                        <div class="form-group">
                            <select id="id-city-2" class="form-control" name="city2" type="text" placeholder="Città spedizione*"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="overflow-auto contract">
                            <h4>CONDIZIONI GENERALI DI VENDITA</h4>
                            <p>L'utente che utilizza la piattaforma www.lescaves.it dichiara di conoscere e di accettare le presenti condizioni generali di vendita.</p>
                            <h5>1 – INFORMAZIONI SULLA PIATTAFORMA DIGITALE</h5>
                            <p>La piattaforma digitale è resa disponibile, organizzata e gestita da LES CAVES DE PYRENE (P. I.: _______11528420018________), d'ora in poi anche LES CAVES, avente propria sede legale in ______Corso Susa 22/A_____, località ______10098 Rivoli (Torino)_________ (indirizzo di posta elettronica certificata:__lescavessrl@pec.it).
                                L’acquisto di prodotti tramite la piattaforma www.lescaves.it è riservato a soli utenti privati persone fisiche e persone giuridiche al di fuori del settore ho.re.ca. Non sono consentiti acquisti di prodotti finalizzati alla rivendita degli stessi.
                                LES CAVES è responsabile delle attività di gestione, aggiornamento, manutenzione della piattaforma, ricezione degli ordini di acquisto, trasmissione della conferma d’ordine, spedizione del prodotto ordinato, svolgimento del servizio di assistenza post vendita.
                                I prodotti esposti e illustrati sono posti in vendita da LES CAVES DE PYRENE in nome e per conto proprio.
                                Con il completamento della procedura di acquisto con contestuale invio dell’ordine, l’utente accetta le presenti condizioni generali di vendita.</p>
                            <h5>2 – DISPOSIZIONI GENERALI</h5>
                            <p>Le presenti condizioni generali di vendita sono valide tra la società LES CAVES DE PYRENE quale venditrice e la persona fisica che effettua acquisti online tramite il sito www.lescaves.it.
                                L’acquisto dei prodotti è consentito esclusivamente a persone fisiche maggiori di età secondo la legge loro applicabile che, presa visione e accettata l’informativa relativa al trattamento dei dati personali, hanno accettato le presenti condizioni generali di vendita.
                                Inviando gli ordinativi, il consumatore garantisce che il soggetto ordinante e, nel caso fosse diverso, il destinatario della merce, siano entrambi maggiori di età secondo la legge loro applicabile.
                                L’utente si dichiara informato del fatto che il consumo di sostanze alcoliche deve essere ispirato al principio di misura.</p>
                            <h5>3 – OGGETTO DEL CONTRATTO</h5>
                            <p>Con le presenti condizioni generali di vendita LES CAVES DE PYRENE vende a distanza all’utente maggiore di età, che acquista, prodotti vitivinicoli esposti dal catalogo online disponibile accedendo al sito internet www.lescaves.it.
                                I prezzi, le descrizioni e/o la disponibilità dei prodotti esposti sono soggetti a modifica senza preavviso con efficacia dalla data di pubblicazione sul sito internet. Le immagini dei prodotti hanno carattere puramente illustrativo e non costituiscono elemento contrattuale.
                                Il contratto di acquisto si perfeziona tramite la rete internet, mediante accesso dell’utente al sito ed il successivo completamento della procedura di acquisto con contestuale invio dell'ordine di acquisto secondo quanto previsto dal successivo articolo 5.
                                L’utente si impegna a prendere visione, prima di procedere alla conferma del proprio ordine, delle presenti condizioni generali di vendita con specifico riferimento alle informazioni precontrattuali riservate al consumatore.
                                L’utente dovrà fornire prova di presa visione delle presenti condizioni generali di vendita mediante apposizione del flag sulla casella digitale di dichiarazione di presa visione.
                                La comunicazione di conferma dell'avvenuto acquisto sarà data da LES CAVES DE PYRENE tramite e-mail che sarà recapitata all'indirizzo di posta elettronica indicato dall'utente; tale comunicazione indicherà all'utente acquirente il link dal quale accedere alle presenti condizioni generali di vendita.</p>
                            <h5>4 – INFORMATIVA PRECONTRATTUALI PER IL CONSUMATORE</h5>
                            <p>Prima di concludere il contratto di acquisto l’utente dovrà prendere visione delle caratteristiche dei beni illustrate dalla scheda del prodotto che intende acquistare.
                                Prima di procedere alla convalida dell’ordine con obbligo di pagamento l’utente potrà visualizzare il riepilogo dell’ordine contenente le seguenti informazioni: a) prezzo totale dei beni, comprensivo delle imposte, con indicazione specifica delle spese di spedizione nonché di ogni altro costo posto a carico dell’utente; b) modalità di pagamento; c) termine entro il quale LES CAVES DE PYRENE si impegna a consegnare la merce; d) condizioni, termini, procedura per esercitare il diritto di recesso; e) necessità di sostenere il costo della restituzione dei beni in caso di recesso; f) condizioni di assistenza post vendita.</p>
                            <h5>5 – PROCEDURA DI ACQUISTO</h5>
                            <p>Preso atto delle condizioni dell’offerta, l’utente potrà concludere l’acquisto seguendo la procedura di acquisto sul sito www.lescaves.it.
                                I prezzi di vendita dei prodotti indicati sul sito sono espressi in euro e comprensivi dell’imposta sul valore aggiunto.
                                I costi di spedizione non sono compresi nel prezzo di acquisto; tali costi vengono resi noti all’utente al momento della conclusione dell'ordine, prima che l’utente effettui il pagamento.
                                Prima della finalizzazione dell’acquisto verrà infatti visualizzato apposito riepilogo in cui sarà indicato il costo unitario del bene selezionato e il totale, in caso di acquisto di più quantità dello stesso articolo o di articoli diversi. Il costo di eventuali spese di spedizione, di consegna o postali può essere fisso oppure variabile, calcolato in base al peso della merce, al numero degli articoli selezionati e/o all’indirizzo di destinazione indicato dall’utente durante il procedimento di acquisto. Le spese di spedizione possono inoltre essere incluse nel prezzo di vendita di alcuni articoli o essere gratuite nel caso in cui l’importo totale dell’ordine sia superiore a un determinato valore o a seguito di specifica promozione. L’utente è comunque sempre informato dell’importo delle spese di spedizione prima di concludere la procedura di acquisto ed effettuare il pagamento.
                                Concluso l’acquisto, l’utente dovrà procedere al pagamento, che verrà effettuato nei confronti di LES CAVES DE PYRENE.
                                A seguito del buon fine del pagamento, l’utente riceverà e-mail di conferma contenente le informazioni relative a quanto acquistato. La e-mail conterrà inoltre lo specifico rinvio, tramite link, alle presenti condizioni generali di contratto.
                                La spedizione della merce avviene nei tempi indicati nella conferma d’ordine. I tempi di consegna sono indicati anche prima di concludere la procedura di acquisto, in fase di riepilogo dell'ordine.
                                L'utente si dichiara informato del fatto che le informazioni rese sulla tempistica della consegna devono essere intese come indicative, considerato che non è sempre possibile prevedere eventuali ritardi dovuti al corriere incaricato della consegna al cliente o altre ragioni di eventuale ritardo dovute a terzi o forza maggiore, quindi non imputabili a LES CAVES DE PYRENE.
                                Il metodo di pagamento accettato, se non diversamente specificato o concordato con l’utente, è il sistema di account PayPal, o carta di credito o bonifico bancario o Satispay.
                                L’addebito delle somme spettanti avviene, se non diversamente specificato nei termini dell’offerta, al momento della conferma del pagamento.
                                Qualora l’utente si renda conto di aver fornito indicazioni errate e/o incomplete circa le sue generalità e/o l’indirizzo di spedizione della merce, è necessario che lo comunichi tempestivamente a LES CAVES DE PYRENE entro i termini di evasione degli ordinativi, inviandone comunicazione scritta all'indirizzo e-mail: shop@lescaves.it. In ogni caso, l’utente è l’unico responsabile per l’eventuale indicazione di generalità e/o indirizzo di consegna errati e/o incompleti.
                                LES CAVES DE PYRENE non tratta e non conserva i dati dei titoli di pagamento; tali dati sono trattati e/o conservati dai relativi fornitori del servizio di pagamento. L’effettivo pagamento avviene infatti attraverso campi di inserimento dei dati di pagamento protetti, criptati, nonché serviti direttamente dal servizio di pagamento. A transazione avvenuta, il fornitore del servizio di pagamento comunica a LES CAVES DE PYRENE l'esito del pagamento, senza fornire informazioni sensibili.</p>
                            <h5>6 – CONSEGNA</h5>
                            <p>LES CAVES DE PYRENE accetta ordini con consegna esclusivamente sul territorio italiano, isole comprese.
                                Le consegne vengono effettuate all’indirizzo indicato dall’utente, secondo le modalità e nel rispetto delle tempistiche specificate dal riepilogo d'ordine.
                                Al momento della consegna, l'acquirente deve verificare il contenuto dell’imballo, specificando eventuali anomalie riscontrate tramite annotazione nel modulo di consegna.
                                LES CAVES DE PYRENE non è responsabile per errori nella consegna dovuti a inesattezze o incompletezze nella compilazione dell’ordine d’acquisto da parte dell’utente.
                                LES CAVES DE PYRENE non è responsabile per danni eventualmente occorsi ai prodotti dopo la consegna dei prodotti al vettore né per ritardi nella consegna che siano imputabili al vettore.</p>
                            <h5>7 – ASSISTENZA POST VENDITA</h5>
                            <p>L’utente può fruire del servizio di assistenza post vendita contattando telefonicamente il servizio clienti al numero telefonico + 39 0173 617 072 dalle 9 alle 13 e dalle ore 14 alle ore 18 o tramite posta elettronica all’indirizzo: shop@lescaves.it.
                                In caso di riscontrati difetti di conformità del prodotto acquistato l’utente acquirente dovrà tempestivamente inviare all’indirizzo e-mail shop@lescaves.it la denuncia del vizio del prodotto riscontrato, indicando espressamente: la descrizione del vizio riscontrato; il codice prodotto; il numero progressivo d’ordine rilasciato al momento dell’acquisto; gli estremi identificativi della fattura emessa dal venditore relativamente ai prodotti acquistati.</p>
                            <h5>8 – DIRITTO DI RECESSO</h5>
                            <p>L’acquirente ha diritto di recedere dal contratto entro il termine di quattordici giorni decorrenti dalla data di ricevimento dei prodotti, restituendo al venditore il bene consegnato.
                                Il diritto di recesso non si applica ai prodotti consegnati sigillati che siano stati aperti dall’acquirente.
                                La comunicazione di recesso dev’essere inviata a mezzo posta elettronica all’indirizzo shop@lescaves.it entro il termine di quattordici giorni decorrenti dalla data di consegna e deve contenere: a) la manifestazione di volontà da parte dell’utente di avvalersi del diritto di recesso; b) l’indicazione dei prodotti per i quali l’acquirente intende avvalersi del diritto di recesso, inclusa la specificazione del codice articolo del prodotto; c) il numero progressivo d’ordine rilasciato al momento dell’acquisto; d) gli estremi identificativi della fattura emessa dal venditore relativamente ai prodotti acquistati. L’acquirente che ha esercitato il diritto di recesso deve restituire i beni integri entro il termine di quattordici giorni decorrenti dalla data di comunicazione del recesso. Le spese di restituzione dei beni sono a carico dell’acquirente. La merce dovrà essere spedita a LES CAVES DE PYRENE c/o Arco Spedizioni in Interporto Sito Nord, Str. Decima, 56, 10098 Rivoli (TO).
                                L’acquirente è responsabile per il confezionamento dei beni che intendere restituire, si impegna pertanto ad assumere le necessarie cautele al fine di salvaguardare l’integrità del prodotto o dei prodotti per preservarli da qualsiasi danneggiamento e/o da qualsiasi alterazione.
                                I beni dovranno essere restituiti integri, nella loro confezione originale, completa in ogni sua parte, corredati dalla documentazione fiscale annessa.
                                Fatta salva la facoltà di verificare il rispetto di quanto sopra, LES CAVES DE PYRENE procederà accreditando in favore del contraente receduto l’importo corrisposto per l’acquisto del / dei prodotto / prodotti entro il termine massimo di quattordici giorni decorrenti dalla data in cui il venditore ha ricevuto i beni restituiti dall'utente receduto. Entro il medesimo termine LES CAVES DE PYRENE si impegna a comunicare all'utente receduto eventuali danni riscontrati sui beni restituiti.
                                Pertanto, l’accredito dell’importo corrisposto non avverrà prima che: a) LES CAVES DE PYRENE abbia ricevuto la spedizione del/dei prodotto/prodotti a cura e spese dell'utente receduto; b) LES CAVES DE PYRENE abbia verificato l’integrità del/dei prodotto/prodotti.</p>
                            <h5>9 – RESPONSABILITÀ DELLA PARTE ACQUIRENTE</h5>
                            <p>L’acquirente è tenuto a garantire che i dati indicati in fase di completamento dell'ordine di acquisto siano veritieri, completi, aggiornati e a comunicare tempestivamente eventuali modifiche dei propri dati.</p>
                            <h5>10 – FUNZIONALITÀ E INTERRUZIONE DEL SERVIZIO</h5>
                            <p>LES CAVES DE PYRENE si riserva il diritto di aggiungere e/o rimuovere funzionalità e/o caratteristiche della piattaforma digitale, nonché il diritto di sospendere e/o interrompere la fruibilità del servizio, sia in via temporanea che in via definitiva.
                                LES CAVES DE PYRENE non assume responsabilità per disservizi imputabili a causa di forza maggiore o a caso fortuito anche ove dipendenti da disservizi della rete internet.</p>
                            <h5>11 – UTILIZZO NON CONSENTITO</h5>
                            <p>L’utente ha diritto di accedere al sito per la consultazione dello stesso e per l’effettuazione degli ordini. Non è consentito alcun altro utilizzo del sito o di parti del sito. L’integrità degli elementi del sito internet e la relativa tecnologia utilizzata restano di proprietà esclusiva di LES CAVES DE PYRENE, titolare di ogni diritto relativo al contenuto grafico e concettuale del sito internet, nonché relativo ai segni distintivi propri in esso resi visibili. È quindi vietata la riproduzione, anche parziale, del contenuto e della grafica del sito internet, così come dei segni distintivi nello stesso resi visibili.
                                La piattaforma digitale deve essere utilizzata nel rispetto delle presenti condizioni generali di vendita. All’utente è vietato: a) effettuare reverse engineering, decompilare, disassemblare, modificare o creare lavori derivati basati sulla presente piattaforma digitale o su qualunque porzione di essa; b) aggirare i sistemi informatici di cui si avvale la presente piattaforma digitale per proteggere il contenuto accessibile tramite di essa; c) di copiare, conservare, modificare, cambiare, preparare lavori derivati o alterare in qualunque modo qualunque dei contenuti forniti dalla presente piattaforma digitale; d) di utilizzare qualunque robot, spider, applicazione di ricerca e/o di reperimento di siti, ovvero qualunque altro dispositivo, processo o mezzo automatico per accedere, recuperare, effettuare scraping o indicizzare qualunque porzione della presente piattaforma digitale ovvero dei suoi contenuti; e) di affittare, licenziare o sublicenziare la presente piattaforma digitale; f) diffamare, offendere, molestare, mettere in atto pratiche minatorie, minacciare o in altro modo – violare i diritti di altri utenti; g) di diffondere o pubblicare contenuti illegali, osceni, illegittimi, diffamatori o inappropriati; h) di utilizzare la piattaforma digitale in qualunque altra modalità impropria tale da violare questi termini.
                                L’utente non è autorizzato a riprodurre, duplicare, copiare, vendere, rivendere, ovvero sfruttare qualunque porzione di www.lescaves.it e dei servizi di www.lescaves.it senza il previo consenso espresso, da rilasciare in forma scritta, da parte di LES CAVES DE PYRENE.
                                L’utente si impegna a mantenere indenne LES CAVES DE PYRENE da qualsiasi obbligo o responsabilità, incluse le eventuali spese legali sostenute per assistenza legale, che dovessero sorgere a fronte di danni provocati ad altri utenti o a terzi in relazione ai contenuti caricati online per effetto della violazione dei termini di legge e/o delle presenti condizioni generali di contratto.</p>
                            <h5>12 – LIMITAZIONI DI RESPONSABILITÀ</h5>
                            <p>LES CAVES DE PYRENE non risponde di eventuali ritardi nella consegna né della mancata consegna del /dei prodotto / prodotti acquistato/i nel caso in cui il ritardo o la mancata consegna dipendano da circostanze eccezionali non prevedibili.
                                LES CAVES DE PYRENE risponde dei danni di natura contrattuale e/o extracontrattuale che siano eventualmente sofferti dagli utenti nel solo caso in cui il danno subito costituisca conseguenza immediata e diretta dell’attività esercitata da LES CAVES DE PYRENE tramite il sito internet.</p>
                            <h5>13 – MODIFICHE DELLE CONDIZIONI GENERALI DI VENDITA</h5>
                            <p>LES CAVES DE PYRENE riserva il proprio diritto di apportare modifiche e/o integrazioni alle condizioni generali di vendita.
                                Le modifiche e/o le integrazioni si applicano agli acquisti effettuati a decorrere dalla data della loro pubblicazione. L’utente che utilizza la piattaforma digitale dopo la pubblicazione delle modifiche sul sito internet, accetta senza riserva i nuovi termini contrattuali.
                                Gli utenti sono pertanto tenuti a consultare periodicamente questa pagina, in modo da essere sempre informati circa le condizioni applicate. Resta inteso l’utilizzo del sito successivo a tali modifiche comporta la tacita accettazione delle medesime.</p>
                            <h5>14 – CESSIONE DEL CONTRATTO</h5>
                            <p>LES CAVES DE PYRENE riserva il proprio diritto di trasferire, cedere, disporre per novazione o subappaltare integralmente ovvero in parte l’esercizio dei diritti e/o la soddisfazione degli obblighi derivanti dalle presenti condizioni generali, purché nel rispetto dei diritti riconosciuti all’utente dalle presenti condizioni generali di vendita. L’utente non può cedere e/o trasferire i propri diritti e/o i propri obblighi.</p>
                            <h5>15 – COMUNICAZIONI</h5>
                            <p>Le comunicazioni inerenti le presenti condizioni generali di vendita debbono essere inviate a mezzo lettera raccomandata con avviso di ricevimento a LES CAVES DE PYRENE presso l'indirizzo geografico di propria sede legale ovvero tramite posta elettronica certificata all’indirizzo lescavessrl@pec.it.</p>
                            <h5>16 – LEGGE APPLICABILE; COMPETENZA GIURISDIZIONALE</h5>
                            <p>Le presenti condizioni generali di vendita nonché le controversie che possano sorgere in punto di validità, interpretazione e/o esecuzione del contratto sono soggette alla legge italiana e sottoposte alla giurisdizione dello Stato italiano.</p>
                            <h4>Accetto di essere abilitato come “Cliente presentatore Les Caves de Pyrene s.r.l.” e le condizioni ed i termini di seguito riportati.</h4>
                            <h5>OGGETTO</h5>
                            <p>L’abilitazione come “Cliente presentatore Les Caves de Pyrene s.r.l.” mi consentirà esclusivamente di richiedere a Les Caves de Pyrene s.r.l., per conto di amici e conoscenti eventualmente interessati, le credenziali di accesso per acquistare i prodotti Les Caves de Pyrene s.r.l. sul sito on line www.lescaves.it.
                                Non mi assumo alcun obbligo di promozione e/o pubblicizzazione dei prodotti e/o dei servizi Les Caves de Pyrene s.r.l. e sono consapevole del divieto assoluto di attuare una simile promozione e/o pubblicizzazione dei Vostri prodotti via internet, attraverso social media e/o social network.
                                La presentazione ai miei amici e conoscenti del Vostro servizio di vendita on line non costituisce oggetto di obbligazione nei Vostri confronti e potrà essere da me fatto con mia totale discrezione.
                                Mi impegno a fornire informazioni veritiere e corrette dichiarandomi edotto che non mi sarà consentito tentare di persuadere chiunque ad effettuare acquisti promettendo vantaggi economici di qualsivoglia genere e/o vantaggi derivanti dal mero coinvolgimento di altre persone.
                                Les Caves de Pyrene s.r.l. non assume alcuna obbligazione nei miei confronti se non la disponibilità a valutare la concessione delle credenziali di accesso al sito di acquisto on line www.lescaves.it alle persone (esclusivamente soggetti privati) che gli presenterò e, nel caso questi effettuino acquisti, a riconoscermi un Cashback da utilizzare sui miei futuri acquisti di prodotti sul sito di Les Caves de Pyrene s.r.l. www.lescaves.it.</p>
                            <h5>SCONTI</h5>
                            <p>Confermo che il motivo della mia richiesta di abilitazione è quello di poter fornire ai miei amici la possibilità di acquistare sul vostro sito on line.
                                Rispetto agli sconti che ne possono derivare per i miei futuri acquisti, mi dichiaro a conoscenza che gli stessi ed il loro ammontare (come visionabile sul sito alla sezione Cashback/Piano degli sconti) sono meramente indicativi e non rappresentano obbligazione irrevocabile a carico di Les Caves de Pyrene s.r.l. che, in qualsiasi momento, potrà modificarli e revocarli.</p>
                            <h5>DURATA E REVOCA</h5>
                            <p>L’abilitazione non ha limitazioni di durata ma Les Caves de Pyrene s.r.l. si riserva il diritto di revocarla a suo insindacabile giudizio in qualsiasi momento e senza necessità di preavviso.</p>
                            <h5>ASSENZA DI VINCOLI E DI RAPPRESENTANZA</h5>
                            <p>L’abilitazione non comporterà per me alcun vincolo nei confronti di Les Caves de Pyrene s.r.l. e non mi conferisce alcun potere di rappresentanza di Les Caves de Pyrene s.r.l. per conto della quale, pertanto, non potrò assumere nessuna obbligazione.</p>
                            <h5>MARCHI</h5>
                            <p>L’abilitazione NON mi conferisce alcun diritto sui marchi Les Caves de Pyrene s.r.l. che sono consapevole di non poter utilizzare.</p>
                            <h5>GRATUITA’ ETICA E TRASPARENZA</h5>
                            <p>Sono a conoscenza che le credenziali di accesso al sito di vendita on line www.lescaves.it vengono rilasciate GRATUITAMENTE da Les Caves de Pyrene s.r.l. e mi obbligo a non richiedere alcun importo e/o corrispettivo ai soggetti che Vi presenterò come soggetti interessati ad ottenere le credenziali di accesso al sito.
                                Non predisporrò, né utilizzerò alcun materiale che possa apparire come proveniente da Les Caves de Pyrene s.r.l..
                                Non promuoverò e/o pubblicizzerò la mia abilitazione a presentatore Les Caves de Pyrene s.r.l. via internet, attraverso social media e/o social network.</p>
                            <h5>TRATTAMENTO DEI DATI PERSONALI</h5>
                            <p>Dichiaro di aver letto e compreso l’informativa prevista in materia di protezione dei dati personali ai sensi dell’art. 12 e ss Regolamento UE 679/2016 e del D.Lgs. 196/2003 così come modificato dal D.Lgs. 101/2018.</p>
                        </div>
                    </div>
                </div>
                <div class="form-group reg-privacy-consent custom-control custom-checkbox">
                    <input id="account-register-consent" name="reg-consent" type="checkbox" class="custom-control-input">
                    <label class="custom-control-label" for="account-register-consent"><?php $this->transP('account.access.register.consent'); ?></label>
                </div>
                <div class="form-group reg-privacy-consent custom-control custom-checkbox">
                    <input id="account-register-consent2" name="reg-consent2" type="checkbox" class="custom-control-input">
                    <label class="custom-control-label" for="account-register-consent2"><?php $this->transP('account.access.register.consent2'); ?></label>
                </div>
                <div class="row">
                    <div class="offset-4 col-4 submit-box">
                        <div class="form-group ">
                            <button id="account-register-send" class="btn btn-primary">Salva</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            $this->util->writeAsyncTokenHtml();
            $this->util->writeAsyncRequestUrl();
            ?>
            <input type="hidden" name="idagent" value="0">
            <input type="hidden" name="pec" value="">
            <input type="hidden" name="note" value="">
            <input type="hidden" name="payment" value="1">
            <input type="hidden" name="chain" value="<?php echo $this->chain; ?>">
        </form>
    <?php } else { ?>
        <div class="title-box">
            <h1>invito scaduto</h1>
        </div>
        <div class="row form-header">
            <div class="d-flex justify-content-end flex-column col-12">
                <p>Il periodo di validità del tuo invito è scaduto.</p>
                <p>Contatta l'amico o l'attività che ti hanno inviato questo link di iscrizione per richiedere un nuovo invito.</p>
            </div>
        </div>
    <?php }  ?>
</section>

<div class="modal fade alert-modal" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">La password non corrisponde</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->addScriptLink($this->path->getLibraryUrl() . "datepicker/1.9.0/js/bootstrap-datepicker.min.js");
$this->addScriptLink($this->path->getLibraryUrl() . "datepicker/1.9.0/locales/bootstrap-datepicker.it.min.js");

$this->endCaptureScript();
