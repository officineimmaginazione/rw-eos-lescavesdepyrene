<script>
    $(function() {
        $('#account-register-send').click(function(event) {
            event.preventDefault();
            var btn = $(this);
            btn.attr('disabled', true);
            btn.on('unlock', function() {
                btn.removeAttr('disabled');
            });
            var data = $('#account-register').serializeFormJSON();
            $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('account', ['access', 'ajaxregister']); ?>',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                })
                .done(function(json) {
                    if (json.result == true) {
                        bootbox.alert(json.message, function() {
                            location.href = '<?php echo $this->path->urlFor('account', ['access', 'login']); ?>';
                        });
                    } else {
                        bootbox.alert(json.message);
                    }
                    btn.trigger('unlock');
                })
                .fail(function(jqxhr, textStatus, error) {
                    btn.trigger('unlock');
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });
    });

    $('#id-city-1').select2({
        ajax: {
            delay: 600,
            url: "<?php echo $this->path->getUrlFor('account', 'access/getdatasource/city/select2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue()); ?>",
            minimumInputLength: 2,
            language: "it",
            format: "select2"
        },
        placeholder: "Seleziona città/provincia/CAP"
    });

    $('#id-city-2').select2({
        ajax: {
            delay: 600,
            url: "<?php echo $this->path->getUrlFor('account', 'access/getdatasource/city/select2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue()); ?>",
            minimumInputLength: 2,
            language: "it",
            format: "select2"
        },
        placeholder: "Seleziona città/provincia/CAP"
    });

    $(document).on('click', 'input[name="usertype"]', function() {
        if ($(this).val() == 0) {
            $(".company").css("display", "none");
        } else {
            $(".company").css("display", "block");
        }
    });

    $("#birthday").datepicker({
        language: "<?php echo $this->lang->getCurrent()->iso; ?>",
        format: "dd/mm/yyyy",
        todayHighlight: true,
        autoclose: true,
        endDate: new Date(),
    });
</script>