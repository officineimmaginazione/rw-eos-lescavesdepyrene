<section class="container register-container">
    <div class="title-box">
        <h1>nuovo indirizzo</h1>
    </div>
    <form id="account-register" method="POST">
        <div id="account-register-container" class="container">
            <div class="row">
                <div class="col-sm-offset-6 col-sm-6">
                    <div class="column-header">
                        <h4 class="text-center">Informazioni</h4>
                    </div>
                    <div class="form-group">
                        <input id="account-register-alias" name="alias" placeholder="Nome sul campanello* (es. Ristorante da Mario, Marco Rossi)" type="text" maxlength="35" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="account-register-name" name="name" type="text" placeholder="Nome (es. Mario)" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="account-register-surname" name="surname" type="text" placeholder="Cognome (es. Rossi)" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="account-register-phone" name="phone" type="text" placeholder="Telefono*" maxlength="18" class="form-control">
                    </div>
                </div>
                <div class="col-sm-offset-6 col-sm-6">
                    <div class="column-header">
                        <h4 class="text-center">Indirizzo</h4>
                    </div>
                    <div class="form-group">
                        <input id="account-register-address" name="address1" type="text" placeholder="Indirizzo fatturazione* (via/strada/ecc. n° civico)" maxlength="35" class="form-control vat-check">
                    </div>
                    <div class="form-group">
                        <select id="id-city-1" class="form-control" name="city1" type="text" placeholder="Città fatturazione*"></select>
                    </div>
                    <div class="form-group">
                        <textarea id="account-note" name="note" type="text" placeholder="Note di spedizione*" class="form-control vat-check" maxlength="200"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group reg-privacy-consent">
                <input id="account-register-consent" name="reg-consent" type="checkbox" class="form-control">
                <?php $this->transP('account.access.register.consent'); ?>
            </div>
            <div class="row">
                <div class="offset-4 col-4 submit-box">
                    <div class="form-group ">
                        <button id="account-register-send" class="btn btn-primary">Salva</button>
                    </div>
                    <p><a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Torna Alla pagina</a> | <a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Account</a></p>
                </div>
            </div>

        </div>
        <?php
        $this->util->writeAsyncTokenHtml();
        $this->util->writeAsyncRequestUrl();
        ?>
        <input type="hidden" name="id_agent" value="<?php echo $this->user->id; ?>">
        <input type="hidden" name="id_customer" value="<?php echo $this->customer; ?>">
        <input type="hidden" name="company" value="<?php echo $this->customerinfo['company']; ?>">
        <input type="hidden" name="id_country" value="1">
        <input type="hidden" name="payment" value="<?php echo $this->customerinfo['payment']; ?>">
        <input type="hidden" name="pec" value="<?php echo (isset($this->customerinfo['pec'])) ? $this->customerinfo['pec'] : ''; ?>">
        <input type="hidden" name="unicode" value="<?php echo (isset($this->customerinfo['unicode'])) ? $this->customerinfo['unicode'] : ''; ?>">
        <input type="hidden" name="vat" value="<?php echo (isset($this->customerinfo['vat'])) ? $this->customerinfo['vat'] : ''; ?>">
        <input type="hidden" name="cif" value="<?php echo $this->customerinfo['cif']; ?>">
        <input type="hidden" name="reference" value="0">
    </form>
</section>
<div class="modal fade alert-modal" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">La password non corrisponde</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
