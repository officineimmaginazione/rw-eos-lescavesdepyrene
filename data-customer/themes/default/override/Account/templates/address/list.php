<div class="admin-agent">
    <section class="admin-main-header">
        <div class="container">
            <div id="cart-left" class="order-section col-12">
                <div class="row section-header">
                    <div class="col-12 offset-md-3 col-md-6 col-center">
                        <h2>indirizzi</h2>
                    </div>
                </div>
                <div class="row mb-5">
                    <?php foreach ($this->listaddresses as $address) { ?>
                        <?php $options = json_decode($address['options'], true); ?>
                        <div class="col-12 col-sm-6 col-lg-4 mb-4 mb-sm-2 single-address<?php echo ($address['status'] == 0) ? ' disable' : ''; ?>">
                            <div class="content">
                                <h6>Sede di spedizione</h6>
                                <h5><?php echo $address['alias'] ?></h5>
                                <p class="address"><?php echo $address['address1'] ?></p>
                                <p class="city"><?php echo $address['cityname'] ?><?php echo (!is_array($address['address2']) && $address['address2'] != '') ? ' - ' . $address['address2'] : ''; ?> (<?php echo $address['state_code'] ?>) <?php echo $address['zip_code'] ?></p>
                                <p class="telephone"><?php echo $address['phone'] ?></p>
                                <p class="note"><?php echo $options['note'] ?></p>
                                <div class="btn-box">
                                    <?php if ($address['status'] == 0) { ?>
                                        <button class="btn  btn-info btn-sm btn-enable-address" id="enable-address-<?php echo $address["id"]; ?>"><i class="fa fa-map-marker-alt"></i> Abilita indirizzo</button>
                                    <?php } else { ?>
                                        <button class="btn  btn-info btn-sm btn-disable-address" id="disable-address-<?php echo $address["id"]; ?>"><i class="fa fa-map-marker"></i> Disabilita indirizzo</button>
                                    <?php } ?>
                                    <?php if ($this->viewagent) { ?>
                                        <button class="btn btn-info btn-sm btn-advisor-address" id="advisor-address-<?php echo $address["id"]; ?>" data-id-customer="<?php echo $address["id_customer"]; ?>" data-alias="<?php echo $address["alias"]; ?>" data-company="<?php echo $address["company"]; ?>"><i class="fas fa-store"></i> Abilita presentatore</button>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <a href="<?php echo $this->path->getUrlFor('Account', 'user/index'); ?><?php echo (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : ''; ?>" class="btn btn-primary" id="return-shipping"><i class="far fa-arrow-alt-circle-left"></i> Ritorna al menù</a>
                        <a href="<?php echo $this->path->getUrlFor('Account', 'address/insert'); ?><?php echo (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : ''; ?>" class="btn btn-primary" id="return-shipping"><i class="far fa-address-book"></i> Inserisci nuovo indirizzo</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php
$this->startCaptureScript();
?>
<script>
    function runAjaxAddress(data, resultEvent) {
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Account', 'address/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    $(".btn-disable-address").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("disable-address-", "");
        updateaddress($id, 0);
    });

    $(".btn-enable-address").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("enable-address-", "");
        updateaddress($id, 1);
    });

    $(".btn-advisor-address").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("advisor-address-", "");
        var data = {};
        data.id = $id;
        data.status = 1;
        data.idcustomer = jQuery(this).data('idCustomer');
        data.alias = jQuery(this).data('alias');
        data.company = jQuery(this).data('company');
        data.command = 'updateadvisorstatus';
        data.<?php echo $this->session->getTokenName() ?> = '<?php echo $this->session->getTokenValue() ?>';
        runAjaxAddress(data, function(json) {
            bootbox.alert(json.message);
        });
    });

    function updateaddress($id, $status) {
        var data = {};
        data.id = $id;
        data.status = $status;
        data.command = 'updatestatus';
        data.<?php echo $this->session->getTokenName() ?> = '<?php echo $this->session->getTokenValue() ?>';
        runAjaxAddress(data, function(json) {
            location.reload();
        });
    }
</script>
<?php
$this->endCaptureScript();
?>