<section class="container register-container">
    <div class="title-box">
        <?php if (isset($this->user->groups) && in_array(42, $this->user->groups)) {
            echo '<h1>invita un amico</h1>';
        } else {
            echo '<h1>invita un cliente</h1>';
        } ?>

    </div>
    <div class="row form-header">
    </div>
    <form id="account-register" method="POST">
        <div id="account-register-container" class="container">
            <div class="row">
                <div class="col-md-offset-6 col-md-6">
                    <?php if (isset($this->user->groups) && in_array(42, $this->user->groups)) { ?>
                        <p>Fai conoscere Les Caves de Pyrene ai tuoi amici bevitori e invitali a scoprire i migliori vini d'Italia e del mondo.</p>
                        <p>Sai cosa succede quando si registrano attraverso il tuo invito?<br>Facile! Ogni volta che fanno un ordine guadagnerai un Cashback (cioè un credito da spendere per gli acquisti futuri) pari al 5% dell’ordine e lo stesso avverrà proporzionalmente, a caduta, quando anche un amico dell’amico dovesse fare un ordine: dunque un vantaggio sia per te sia per gli amici che avrai invitato.</p>
                        <p>Ora quindi c’è solo una cosa da fare: suggerisci ai tuoi amici di comprare i prodotti Caves e approfittane per aumentare il tuo credito.</p>
                        <p>Attenzione però. Spedisci sempre gli inviti attraverso il modulo a fianco: solo così potremo registrare le iscrizioni effettuate attraverso il tuo invito e farti ricevere un bonus da parte nostra.</p>
                    <?php } else { ?>
                        <p>Fai conoscere Les Caves de Pyrene ai tuoi clienti bevitori e invitali a scoprire i migliori vini d'Italia e del mondo.</p>
                        <p>Sai cosa succede quando si registrano attraverso il tuo invito?<br>Facile! Ogni volta che fanno un ordine guadagnerai un Cashback (cioè un credito da spendere per gli acquisti futuri) pari al 16% dell’ordine e lo stesso avverrà proporzionalmente, a caduta, quando anche un amico del cliente dovesse fare un ordine: dunque un vantaggio sia per te sia per i clienti che avrai invitato.</p>
                        <p>Ora quindi c’è solo una cosa da fare: suggerisci ai tuoi amici di comprare i prodotti Caves e approfittane per aumentare il tuo credito.</p>
                        <p>Attenzione però. Spedisci sempre gli inviti attraverso il modulo a fianco: solo così potremo registrare le iscrizioni effettuate attraverso il tuo invito e farti ricevere un bonus da parte nostra.</p>
                    <?php } ?>

                </div>
                <div class="col-md-offset-6 col-md-6 mt-4 mt-md-0">
                    <div class="form-group">
                        <input id="account-register-email" name="email" placeholder="Email (es. mario.rossi@gmail.com)" type="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="account-register-name" name="name" placeholder="Nome (es. Mario)" class="form-control">
                    </div>
                    <!--
                    <div class="form-group reg-privacy-consent">
                        <input id="account-register-consent" name="reg-consent" type="checkbox" class="form-control">
                        <?php $this->transP('account.access.register.consent'); ?>
                    </div>
                    -->
                    <div class="form-group ">
                        <button id="btn-invitation-send" class="btn btn-primary">Invita</button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="offset-4 col-4 submit-box">
                    <p><a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Torna Alla pagina</a> | <a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Account</a></p>
                </div>
            </div>
        </div>
        <?php
        $this->util->writeAsyncTokenHtml();
        $this->util->writeAsyncRequestUrl();
        ?>
        <input type="hidden" name="iduser" value="<?php echo $this->user->id; ?>">
        <input type="hidden" name="idaddress" value="0">
    </form>
</section>

<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
