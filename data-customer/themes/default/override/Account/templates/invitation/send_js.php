<script>
    $(function ()
    {
        $('#btn-invitation-send').click(function (event){
            
            event.preventDefault();
            var btn = $(this);
            btn.attr('disabled', true);
            btn.on('unlock', function ()
            {
                btn.removeAttr('disabled');
            });
            var data = $('#account-register').serializeFormJSON();
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('account', ['registration', 'ajaxinvitation']); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function (json)
            {
                if (json.result == true)
                {
                    bootbox.alert(json.message, function ()
                    {
                        location.href = '<?php echo $this->path->urlFor('account', ['user', 'index']); ?>';
                    });
                } else
                {
                    bootbox.alert(json.message);
                }
                btn.trigger('unlock');
            })
            .fail(function (jqxhr, textStatus, error)
            {
                btn.trigger('unlock');
                var err = textStatus + ", " + error;
                bootbox.alert(err);
            });
        });
    });
    
    $('#account-register-vat').on('input', function() { 
        if($(this).val().length == 11) {
            var data = {};
            data.vatnumber = $(this).val();
            data.command = 'checkvatnumber';
            data.<?php echo $this->session->getTokenName(); ?> = '<?php echo $this->session->getTokenValue(); ?>';
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->urlFor('account', ['registration', 'ajaxCommand']); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function (json)
            {
                if (json.result == false)
                {
                    $('.vat-check').prop('disabled', false);
                } else
                {
                    $('.vat-check').prop('disabled', true);
                    var err = 'Partita Iva già inserita.';
                    bootbox.alert(err);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                $('.vat-check').prop('disabled', true);
                var err = textStatus + ", " + error;
                bootbox.alert(err);
            });
        } else {
            $('.vat-check').prop('disabled', true);
        }
    });
    
    
    $('#id-city-1').select2({
        ajax: {
            delay:600,
            url: "<?php echo $this->path->getUrlFor('account', 'registration/getdatasource/city/select2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue()); ?>",
            minimumInputLength:2,
            language:"it",
            format:"select2"
        },
        placeholder: "Seleziona città/provincia/CAP"
    });
    
    $('#id-city-2').select2({
        ajax: {
            delay:600,
            url: "<?php echo $this->path->getUrlFor('account', 'registration/getdatasource/city/select2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue()); ?>",
            minimumInputLength:2,
            language:"it",
            format:"select2"
        },
        placeholder: "Seleziona città/provincia/CAP"
    });  
    
</script>