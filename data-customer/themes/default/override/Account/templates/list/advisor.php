<div class="admin-agent">
    <section class="container admin-main-body">
        <div id="order-list" class="order-section col-12">
            <div class="row section-header">
                <div class="col-12 offset-md-3 col-md-6 col-center">
                    <h2>riepilogo presentatori</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table id="advisor-list-table" class="table table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>Azienda</th>
                                <th>Stato presentatore</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->listadvisor as $row) : ?>
                                <?php $options = json_decode($row["options"], true); ?>
                                <tr>
                                    <td><?php echo $row["id"]; ?></td>
                                    <td><?php echo $row["email"]; ?></td>
                                    <td><?php echo $row["name"]; ?></td>
                                    <?php if (isset($options['advisor']) && $options['advisor'] == 0) { ?>
                                        <td>Non accettato</td>
                                    <?php } else if (isset($options['advisor']) && $options['advisor'] == 1) { ?>
                                        <td>Attivo</td>
                                    <?php } else { ?>
                                        <td>In attesa</td>
                                    <?php } ?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <a href="<?php echo $this->path->getUrlFor('Account', 'user/index'); ?><?php echo (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : ''; ?>" class="btn btn-primary" id="return-shipping"><i class="far fa-arrow-alt-circle-left"></i> Ritorna al menù</a>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$this->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#advisor-list-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
<?php
$this->endCaptureScript();
