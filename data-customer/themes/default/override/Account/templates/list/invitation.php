<div class="admin-agent">
    <section class="container admin-main-body">
        <div id="order-list" class="order-section col-12">
            <div class="row section-header">
                <div class="col-12 offset-md-3 col-md-6 col-center">
                    <h2>riepilogo inviti</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table id="invitation-list-table" class="table table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Data</th>
                                <th>Email</th>
                                <th>Attivo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->listinvitation as $row) : ?>
                                <tr>
                                    <td><?php echo $row["id"]; ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($row["ins_date"])); ?></td>
                                    <td><?php echo $row["email"]; ?></td>
                                    <td><?php echo (isset($row["idcustomer"]) && $row["idcustomer"] != '') ? 'Sì' : 'No'; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <a href="<?php echo $this->path->getUrlFor('Account', 'user/index'); ?><?php echo (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : ''; ?>" class="btn btn-primary" id="return-shipping"><i class="far fa-arrow-alt-circle-left"></i> Ritorna al menù</a>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$this->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#invitation-list-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
<?php
$this->endCaptureScript();
