<section class="container register-container">
    <div class="title-box">
        <h1>nuovo cliente</h1>
    </div>
    <form id="account-register" method="POST">
        <div id="account-register-container" class="container">
            <div class="row">
                <?php /* <div class="col-sm-offset-4 col-sm-4">
                    <div class="form-group">
                        <input id="account-register-username" name="reg-username" placeholder="Nome utente" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="account-register-password" name="reg-pwd1" placeholder="Password" type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="account-register-password2" name="reg-pwd2" placeholder="Ripeti password" type="password" class="form-control">
                    </div> 
                </div> */ ?>
                <div class="col-sm-offset-6 col-sm-6">
                    <div class="column-header">
                        <h4 class="text-center">Informazioni<br />cliente</h4>
                    </div>
                    <div class="form-group">
                        <input id="account-register-vat" name="vat" placeholder="Partita IVA* (non deve essere già presente e di 11 numeri)" type="text" maxlength="11" class="form-control" number>
                    </div>
                    <div class="form-group">
                        <button id="vat-checker" class="btn btn-primary">Controlla azienda</button>
                    </div>
                    <div class="form-group">
                        <input id="account-register-cif" name="cif" placeholder="Codice Fiscale (dell'aziendale/del titolare se individuale)" type="text" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <input id="account-register-company" name="company" placeholder="Ragione sociale* (es. BuonVino S.r.l.)" type="text" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <input id="account-register-name" name="name" type="text" placeholder="Nome (es. Mario)" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <input id="account-register-surname" name="surname" type="text" placeholder="Cognome (es. Rossi)" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <input id="account-register-phone" name="phone" type="text" placeholder="Telefono*" maxlength="18" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <input id="account-register-email" name="email" placeholder="Email (es. info@buonvinosrl.com)" type="email" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <input id="account-register-pec" name="pec" type="text" placeholder="PEC (es. buonvinosrl@pec.it)" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <input id="account-register-uniccode" name="unicode" type="text" placeholder="Codice univoco (SDI)" class="form-control vat-check" maxlength="7" disabled>
                    </div>
                    <div class="form-group">
                        <select id="payment" name="payment" class="form-control custom-select vat-check" disabled readonly="readonly">
                            <?php foreach ($this->payment as $row) :  ?>
                                <option value="<?php echo $row['id']; ?>" <?php echo ($row['id'] == 6) ? 'selected' : 'disabled' ?>><?php echo $row['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-offset-6 col-sm-6">
                    <div class="column-header">
                        <h4 class="text-center">Indirizzo</h4>
                    </div>
                    <div class="form-group">
                        <input id="account-register-address" name="address1" type="text" placeholder="Indirizzo fatturazione* (via/strada/ecc. n° civico)" maxlength="35" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <select id="id-city-1" class="form-control vat-check" name="city1" type="text" placeholder="Città fatturazione*" disabled></select>
                    </div>
                    <div class="form-group">
                        <input id="account-register-address2" name="address2" type="text" placeholder="Indirizzo spedizione (se diverso da fatturazione)" maxlength="35" class="form-control vat-check" disabled>
                    </div>
                    <div class="form-group">
                        <select id="id-city-2" class="form-control vat-check" name="city2" type="text" placeholder="Città fatturazione*" disabled></select>
                    </div>
                    <div class="form-group">
                        <textarea id="account-note" name="note" type="text" placeholder="Note (giorno chiusura, orari, ecc.)*" class="form-control vat-check" maxlength="200" disabled></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="offset-4 col-4 submit-box">
                    <div class="form-group ">
                        <button id="account-register-send" class="btn btn-primary">Salva</button>
                    </div>
                    <p><a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Torna Alla pagina</a> | <a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Account</a></p>
                </div>
            </div>

        </div>
        <?php
        $this->util->writeAsyncTokenHtml();
        $this->util->writeAsyncRequestUrl();
        ?>
        <input type="hidden" name="idagent" value="<?php echo $this->user->id; ?>">
        <input type="hidden" id="cscommonvalue" name="cscommonvalue" value="">
        <input type="hidden" id="cscreditlimit" name="cscreditlimit" value="">
        <input type="hidden" id="cscommondescription" name="cscommondescription" value="">
        <input type="hidden" id="csid" name="csid" value="">
        <input type="hidden" name="reg-consent" id="account-register-consent" value="1">
    </form>
</section>
<div class="modal fade alert-modal" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">La password non corrisponde</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal" id="CSCheckData" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Controllo dati</h5>
            </div>
            <div class="modal-body">
                <p id="CSdataCompany">Azienda</p>
                <p id="CSdataAddress">Indirizzo</p>
            </div>
            <div class="modal-footer">
                <button id="CSConfirm" type="button" class="btn btn-primary">Conferma</button>
                <button id="InsertManual" type="button" class="btn btn-manual-insert" data-dismiss="modal">Inserimento Manuale</button>
                <button id="CSClear" type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>

            </div>
        </div>
    </div>
</div>

<div class="modal" id="CSRating" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Monitoraggio</h5>
            </div>
            <div class="modal-body">
                <p id="CScommonValue">Score: <span id="CScommonValueV"></span></p>
                <!-- <p id="CScreditLimit">Limite Credito: <span id="CScreditLimitV"></span> €</p> -->
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
