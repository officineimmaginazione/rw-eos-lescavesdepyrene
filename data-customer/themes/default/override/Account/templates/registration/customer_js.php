<script>
    $(function() {
        $('#account-register-send').click(function(event) {

            event.preventDefault();
            var btn = $(this);
            btn.attr('disabled', true);
            btn.on('unlock', function() {
                btn.removeAttr('disabled');
            });
            var data = $('#account-register').serializeFormJSON();
            $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('account', ['registration', 'ajaxregister']); ?>',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                })
                .done(function(json) {
                    if (json.result == true) {
                        bootbox.alert(json.message, function() {
                            location.href = '<?php echo $this->path->urlFor('account', ['user', 'index']); ?>';
                        });
                    } else {
                        bootbox.alert(json.message);
                    }
                    btn.trigger('unlock');
                })
                .fail(function(jqxhr, textStatus, error) {
                    btn.trigger('unlock');
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });

        $('#vat-checker').click(function(event) {
            event.preventDefault();
            if ($('#account-register-vat').val().length == 11) {
                bootbox.alert('Attendere. Il sistema sta recuperando i dati');
                var data = {};
                data.vatnumber = $('#account-register-vat').val();
                data.command = 'checkcreditsafe';
                data.<?php echo $this->session->getTokenName(); ?> = '<?php echo $this->session->getTokenValue(); ?>';
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '<?php echo $this->path->urlFor('account', ['registration', 'ajaxCommand']); ?>',
                        data: JSON.stringify(data),
                        contentType: "application/json",
                    })
                    .done(function(json) {
                        bootbox.hideAll();
                        if (json.result == false) {
                            //$('.vat-check').prop('disabled', false);
                            //bootbox.alert(json.message);
                            $('#CSdataCompany').text(json.name);
                            $('#CSdataAddress').text(json.addresscomplete);
                            $('#account-register-cif').val(json.cif);
                            $('#account-register-company').val(json.name);
                            $('#account-register-address').val(json.address);
                            $('#account-register-city').val(json.city);
                            $('#csid').val(json.id);
                            $('#CSCheckData').modal('show');
                        } else {
                            $('.vat-check').prop('disabled', true);
                            bootbox.alert(json.message);
                        }
                    })
                    .fail(function(jqxhr, textStatus, error) {
                        bootbox.hideAll();
                        $('.vat-check').prop('disabled', true);
                        var err = textStatus + ", " + error;
                        bootbox.alert(err);
                    });
            } else {
                var err = 'Partita Iva non completa.';
                bootbox.alert(err);
                $('.vat-check').prop('disabled', true);
            }
        });

        $('#CSConfirm').click(function(event) {
            event.preventDefault();
            $('#CSCheckData').modal('hide');
            bootbox.alert('Attendere. Il sistema sta recuperando i dati');
            var data = {};
            data.csid = $('#csid').val();
            data.command = 'retrieveCSdata';
            data.<?php echo $this->session->getTokenName(); ?> = '<?php echo $this->session->getTokenValue(); ?>';
            $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('account', ['registration', 'ajaxCommand']); ?>',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                })
                .done(function(json) {
                    bootbox.hideAll();
                    var company = $('#account-register-company').val();
                    if (json.commonValue < 40 || json.commonValue == 'Not rated') {
                        $('#CScommonValueV').addClass('danger');
                        $('#CScommonValueV').text(json.commonValue + " - " + 'Rischio Alto - Solo anticipato');
                        $('#cscommondescription').val('Rischio Alto - Solo anticipato');
                    } else if (company.indexOf("SOCIETA A RESPONSABILITA LIMITATA SEMPLIFICATA") >= 0) {
                        $('#CScommonValueV').text(json.commonValue + " - " + 'S.r.l.s - Solo anticipato');
                        $('#CScommonValueV').addClass('danger');
                        $('#cscommondescription').val('S.r.l.s - Solo anticipato');
                        //$("#payment").attr("readonly", false);
                        //$('#payment').prop("disabled", false);
                        //$("#payment option").prop('disabled', false);
                    } else if (json.commonValue >= 40 && json.commonValue < 60) {
                        $('#CScommonValueV').addClass('alert');
                        $('#CScommonValueV').text(json.commonValue + " - " + 'Rischio Medio - Possibile modificare pagamento');
                        $('#cscommondescription').val('Rischio Medio - Possibile modificare pagamento');
                        $("#payment").attr("readonly", false);
                        $('#payment').prop("disabled", false);
                        $("#payment option").prop('disabled', false);
                    } else {
                        $('#cscommondescription').val('Rischio Basso - Possibile modificare pagamento');
                        $('#CScommonValueV').text(json.commonValue + " - " + 'Rischio Basso - Possibile modificare pagamento');
                        $('#CScommonValueV').addClass('success');
                        $("#payment").attr("readonly", false);
                        $('#payment').prop("disabled", false);
                        $("#payment option").prop('disabled', false);
                    }
                    $('#CScreditLimitV').text(json.creditLimit);

                    $('#cscommonvalue').val(json.commonValue);
                    $('#cscreditlimit').val(json.creditLimit);


                    $('#account-register-pec').val(json.pec);
                    $('#account-register-phone').val(json.phone);

                    $('#CSRating').modal('show');
                    $('.vat-check').prop('disabled', false);
                    //bootbox.alert(json.message);
                })
                .fail(function(jqxhr, textStatus, error) {
                    bootbox.hideAll();
                    $('.vat-check').prop('disabled', true);
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });

        $('#InsertManual').click(function(event) {
            event.preventDefault();
            $('#account-register-vat').val('');
            $('#account-register-company').val('');
            $('#account-register-cif').val('');
            $('.vat-check').prop('disabled', false);
            bootbox.alert("Inserendo manualmente i dati l'azienda non verrà monitorata.");
        });

        $('#CSClear').click(function(event) {
            event.preventDefault();
            $(':input', '#account-register')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .prop('checked', false)
                .prop('selected', false);
            $("#payment").attr("readonly", true);
            $('#payment').prop("disabled", true);
            $("#payment option").prop('disabled', true);
            $("#payment option[value=6]").prop('disabled', false);
        });
    });

    $('#id-city-1').select2({
        ajax: {
            delay: 600,
            url: "<?php echo $this->path->getUrlFor('account', 'registration/getdatasource/city/select2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue()); ?>",
            minimumInputLength: 2,
            language: "it",
            format: "select2"
        },
        placeholder: "Seleziona città/provincia/CAP"
    });

    $('#id-city-2').select2({
        ajax: {
            delay: 600,
            url: "<?php echo $this->path->getUrlFor('account', 'registration/getdatasource/city/select2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue()); ?>",
            minimumInputLength: 2,
            language: "it",
            format: "select2"
        },
        placeholder: "Seleziona città/provincia/CAP"
    });
</script>