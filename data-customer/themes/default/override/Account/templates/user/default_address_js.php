<?php
$this->startCaptureScript();
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
?>
<script>
    function runAjaxUser(data, resultEvent)
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Account', 'user/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
        .done(function (json)
        {
            if (json.result == true)
            {
                resultEvent(json);
            }
            else
            {
                bootbox.alert(json.message);
            }
        })
        .fail(function (jqxhr, textStatus, error)
        {
            var err = textStatus + ", " + error;
            alert(err);
        });
    }
    
    $(function ()
    {
        <?php
        if($this->filterBy == 1) {
            $url = $this->path->getUrlFor('account', 'user/getdatasource/user/typeahead1/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue());
        } elseif($this->filterBy == 2) {
            $url = $this->path->getUrlFor('account', 'user/getdatasource/user/typeahead2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue());
        } else {
            $url = $this->path->getUrlFor('account', 'user/getdatasource/user/typeahead0/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue());
        }
        ?>
        $.get("<?php echo $url; ?>", function (data)
        {
            var customerEdit = $('#customer');
            customerEdit.typeahead({
                source: data,
                delay: 100,
                minLength: 2,
                autoSelect: true,
                items: 30
            });
            
            customerEdit.change(function ()
            {
                var current = customerEdit.typeahead("getActive");
                if (current)
                {
                    if (current.name == customerEdit.val())
                    {
                        var data = {'command': "customerdata", 'idcustomer': current.id, 'status': false, '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'};
                        runAjaxAddress(data, function (json)
                        {
                            //displayInfoCustomer(json);
                            location.href = '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>'+'?customer='+current.id;
                        });
                        customerEdit.val(current.id);
                    }
                    else
                    {
                        customerEdit.val('');
                    }
                }
                else
                {
                    // Nothing is active so it is a new value (or maybe empty value)
                }
            });
        }, 'json');
    });
    
    function dataCustomer()
    {
        <?php if (isset($this->customer) && $this->customer != 0) { ?>
        var data = {'command': "customerdata", 'id-address': <?php echo $this->customer; ?>, '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'};
        runAjaxAddress(data, function (json)
        {
            //alert(JSON.stringify(json))
            displayInfoCustomer(json);
        });
        <?php } ?>
    }
    
//    function displayInfoCustomer(json)
//    {
//        $( "#customer-data" ).empty();
//        var company = json.address['company'];
//        if (company === undefined || company === null)
//        {
//            company = '';
//        }
//        var name = json.address['name'];
//        if (name === undefined || name === null)
//        {
//            name = '';
//        }
//        var surname = json.address['surname'];
//        if (surname === undefined || surname === null)
//        {
//            surname = '';
//        }
//        var address1 = json.address['address1'];
//        if (address1 === undefined || address1 === null)
//        {
//            address1 = '';
//        }
//        var city = json.address['city'];
//        if (city === undefined || city === null)
//        {
//            city = '';
//        }
//        var phone = json.address['phone'];
//        if (phone === undefined || phone === null)
//        {
//            phone = '';
//        }
//        var piva = json.address['vat'];
//        if (piva === undefined || piva === null)
//        {
//            piva = '';
//        }
//        var payment = json.address['payment'];
//        if (payment === undefined || payment === null)
//        {
//            payment = '';
//        }
//
//        var customerdata = '<div class="row mt-3"><div class="col-12 col-sm-5 offset-sm-1 single-address"><div class="content"><h6>Indirizzo di consegna</h6>';
//        if (name != "" || surname != "")
//            customerdata = customerdata + '<p class="name">' + name + ' ' + surname + '</p>';
//        if (company != "")
//            customerdata = customerdata + '<p class="company">' + company + '</p>';
//        if (address1 != "")
//            customerdata = customerdata + '<p class="address">' + address1 + '</p>';
//        if (city != "")
//            customerdata = customerdata + '<p class="city">' + city + '</p>';
//        if (phone != "")
//            customerdata = customerdata + '<p class="telephone">Tel: ' + phone + '</p>';
//        customerdata = customerdata + '</div></div><div class="col-12 col-sm-5 single-address"><div class="content"><h6>Sconti e pagamento</h6>';
//        if (payment != "")
//            customerdata = customerdata + '<p class="piva">Pagamento cliente: ' + payment + '</p>';
//        for (index = 0; index < json.address['discount-count']; ++index)
//        {
//            customerdata = customerdata + '<p class="piva">' + json.address['discount-' + index + '-name'] + '</p>';
//            $('input[name=\'discount1\']').val(json.address['discount-' + index + '-reduction1']);
//            $('input[name=\'discount2\']').val(json.address['discount-' + index + '-reduction2']);
//            $('input[name=\'discount3\']').val(json.address['discount-' + index + '-reduction3']);
//        }
//        customerdata = customerdata + '</div></div></div>';
//        $("#customer-data").html(customerdata);
//    }
//    
//    $('document').ready(function(){
//        dataCustomer();
//    });
    
</script>
<?php
$this->endCaptureScript();
