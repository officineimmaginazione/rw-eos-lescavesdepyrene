<?php
$this->startCaptureScript();
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
?>
<script>
	$(function() {
		$("#select-customer").on('change', function() {
			var select_id = document.getElementById("select-customer");
			location.href = '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>' + '?customer=' + select_id.options[select_id.selectedIndex].value;
		});

		$('#vat-checker').click(function(event) {
			event.preventDefault();
			bootbox.alert('Attendere. Il sistema sta recuperando i dati');
			var data = {};
			data.vatnumber = <?php echo (isset($this->customerinfo['vat']) && $this->customerinfo['vat'] != '') ? '"' . $this->customerinfo['vat'] . '"' : '""'; ?>;
			data.update = 1;
			data.command = 'checkcreditsafe';
			data.<?php echo $this->session->getTokenName(); ?> = '<?php echo $this->session->getTokenValue(); ?>';
			$.ajax({
					type: 'POST',
					dataType: "json",
					url: '<?php echo $this->path->urlFor('account', ['registration', 'ajaxCommand']); ?>',
					data: JSON.stringify(data),
					contentType: "application/json",
				})
				.done(function(json) {
					bootbox.hideAll();
					if (json.result == false) {
						//$('.vat-check').prop('disabled', false);
						//bootbox.alert(json.message);
						$('#CSdataCompany').text(json.name);
						$('#CSdataAddress').text(json.addresscomplete);
						$('#account-register-cif').val(json.cif);
						$('#account-register-company').val(json.name);
						//$('#account-register-address').val(json.address);
						$('#csid').val(json.id);
						$('#CSCheckData').modal('show');
					} else {
						$('.vat-check').prop('disabled', true);
						bootbox.alert(json.message);
					}
				})
				.fail(function(jqxhr, textStatus, error) {
					bootbox.hideAll();
					$('.vat-check').prop('disabled', true);
					var err = textStatus + ", " + error;
					bootbox.alert(err);
				});
		});

		$('#CSConfirm').click(function(event) {
			event.preventDefault();
			$('#CSCheckData').modal('hide');
			bootbox.alert('Attendere. Il sistema sta recuperando i dati');
			var data = {};
			data.csid = $('#csid').val();
			data.idaddress = $('#idaddress').val();
			data.update = 1;
			data.command = 'retrieveCSdata';
			data.<?php echo $this->session->getTokenName(); ?> = '<?php echo $this->session->getTokenValue(); ?>';
			$.ajax({
					type: 'POST',
					dataType: "json",
					url: '<?php echo $this->path->urlFor('account', ['registration', 'ajaxCommand']); ?>',
					data: JSON.stringify(data),
					contentType: "application/json",
				})
				.done(function(json) {
					bootbox.hideAll();
					/*
					if (json.commonValue < 40 || json.commonValue == 'Not rated') {
						$('#CScommonValueV').addClass('danger');
						$('#CScommonValueV').text(json.commonValue + " - " + 'Rischio Alto - Solo anticipato');
						$('#cscommondescription').val('Rischio Alto - Solo anticipato');
					} else if (company.indexOf("SOCIETA A RESPONSABILITA LIMITATA SEMPLIFICATA") >= 0) {
						$('#CScommonValueV').text(json.commonValue + " - " + 'S.r.l.s - Solo anticipato');
						$('#CScommonValueV').addClass('danger');
						$('#cscommondescription').val('S.r.l.s - Solo anticipato');
					} else if (json.commonValue >= 40 && json.commonValue < 60) {
						$('#CScommonValueV').addClass('alert');
						$('#CScommonValueV').text(json.commonValue + " - " + 'Rischio Medio');
						$('#cscommondescription').val('Rischio Medio - Possibile modificare pagamento');
					} else {
						$('#cscommondescription').val('Rischio Basso - Possibile modificare pagamento');
						$('#CScommonValueV').text(json.commonValue + " - " + 'Rischio Basso');
						$('#CScommonValueV').addClass('success');
					}
					$('#CScommonValueV').text(json.commonValue + " - " + json.commonDescription);
					$('#CScreditLimitV').text(json.creditLimit); */
					//$('#CSRating').modal('show');
					bootbox.alert('I dati sono stati aggiornati', function() {
						location.reload();
					});
					//location.href = '<?php echo $this->path->urlFor('account', ['user', 'index']); ?>';
					//bootbox.alert(json.message);
				})
				.fail(function(jqxhr, textStatus, error) {
					bootbox.hideAll();
					$('.vat-check').prop('disabled', true);
					var err = textStatus + ", " + error;
					bootbox.alert(err);
				});
		});

	});

	$(".btn-disable-address").on("click", function(event) {
		event.preventDefault();
		$id = jQuery(this).attr("id");
		$id = $id.replace("disable-address-", "");
		updateaddress($id, 0);
	});

	$(".btn-enable-address").on("click", function(event) {
		event.preventDefault();
		$id = jQuery(this).attr("id");
		$id = $id.replace("enable-address-", "");
		updateaddress($id, 1);
	});

	function updateaddress($id, $status) {
		var data = {};
		data.id = $id;
		data.status = $status;
		data.command = 'updatestatus';
		data.<?php echo $this->session->getTokenName() ?> = '<?php echo $this->session->getTokenValue() ?>';
		runAjaxAddress(data, function(json) {
			location.reload();
		});
	}

	function runAjaxAddress(data, resultEvent) {
		$.ajax({
				type: 'POST',
				dataType: "json",
				url: '<?php echo $this->path->getUrlFor('Account', 'address/ajaxcommand'); ?>',
				data: JSON.stringify(data),
				contentType: "application/json",
			})
			.done(function(json) {
				if (json.result == true) {
					resultEvent(json);
				} else {
					bootbox.alert(json.message);
				}
			})
			.fail(function(jqxhr, textStatus, error) {
				var err = textStatus + ", " + error;
				alert(err);
			});
	}

	$(document).on('click', 'input[name="filterBy"]', function() {
		location.href = '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>' + '?filterby=' + $(this).val();
	});

	$("#opendebit").on("click", function(event) {
		event.preventDefault();
		if (<?php echo $this->customer ?> > 0) {
			location.href = '<?php echo $this->path->getUrlFor('Order', 'order/listopendebit'); ?>' + '?customer=<?php echo $this->customer ?>';
		} else {
			$('#alertDebit').modal('show');
		}
	});

	$("#openaddresses").on("click", function(event) {
		event.preventDefault();
		if (<?php echo $this->customer ?> > 0) {
			location.href = '<?php echo $this->path->getUrlFor('Account', 'address/list'); ?>' + '?customer=<?php echo $this->customer ?>';
		} else {

			$('#alertAddress').modal('show');
		}
	});

	$("#openadvisor").on("click", function(event) {
		event.preventDefault();
		if (<?php echo $this->customer ?> > 0) {
			location.href = '<?php echo $this->path->getUrlFor('Account', 'address/list'); ?>' + '?customer=<?php echo $this->customer ?>';
		} else {

			$('#alertAdvisor').modal('show');
		}
	});

	<?php
	//print_r($this->lastaccess);
	//exit();
	if ($this->changepassword) { ?>
		// $('document').ready(function() {
		//	$('#alertPassword').modal('show');
		//});
	<?php }
	if ($this->debit) { ?>
		$('document').ready(function() {
			$('#alertDebit2').modal('show');
		});
	<?php }
	if ($this->controller->container->get('database')->setting->getInt('lcdp', 'force.condition', 0) == 1) {
	?>
		$('document').ready(function() {
			$('#advisorModal').modal({
				backdrop: 'static',
				keyboard: false
			})
			$('#advisorModal').modal('show');
		});
		$('#advisorModal .btn-light').click(function() {
			location.href = '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>' + '?user=' + <?php echo $this->user->id; ?> + '&advisor=0';
		});

		$('#advisorModal .btn-primary').click(function() {
			location.href = '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>' + '?user=' + <?php echo $this->user->id; ?> + '&advisor=1';
		});
	<?php } else if (isset($this->advisor) && $this->advisor == 2 && in_array(1, $this->user->groups)) {
	?>
		<?php if ($this->enablecheckcondition) { ?>
			$('document').ready(function() {
				$('#advisorModal').modal({
					backdrop: 'static',
					keyboard: false
				})
				$('#advisorModal').modal('show');
			});
		<?php } ?>
		$('#advisorModal .btn-light').click(function() {
			location.href = '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>' + '?user=' + <?php echo $this->user->id; ?> + '&advisor=0';
		});

		$('#advisorModal .btn-primary').click(function() {
			location.href = '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>' + '?user=' + <?php echo $this->user->id; ?> + '&advisor=1';
		});
	<?php } else { ?>
		$(document).ready(function() {
			$('#newsBanner').modal('show');
		});
	<?php } ?>
</script>
<?php
$this->endCaptureScript();
