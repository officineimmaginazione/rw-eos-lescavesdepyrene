<!--  pannello agente-->
<div class="admin-agent">
    <section class="admin-main-header">
        <div class="container">
            <?php if ($this->viewagent) { ?>
                <div class="customers-box">
                    <h2>Scegli Cliente</h2>
                    <div class="row">
                        <div class="d-flex align-items-center offset-1 col-10 offset-sm-0 col-sm-6 col-md-4 offset-md-4 col-lg-4 offset-lg-4 user-box">
                            <img class="user-img" src="data-customer/themes/default/img/user.png" alt="user" />
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="SELEZIONA CLIENTE" id="customer" aria-label="Cerca" name="customer_holder" aria-describedby="customer" autocomplete="off">
                                <input type="hidden" name="idagent" value="<?php echo $this->agentid; ?>">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-search search-submit"><img src="data-customer/themes/default/img/lent.png" alt="search" /></button>
                                </div>
                                <div class="input-group radioFilterBy">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="filterBy" id="filterBy1" value="0" <?php echo ($this->filterBy == 0) ? 'checked' : ''; ?>>
                                        <label class="form-check-label" for="filterBy1">Rag. Soc.</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="filterBy" id="filterBy2" value="1" <?php echo ($this->filterBy == 1) ? 'checked' : ''; ?>>
                                        <label class="form-check-label" for="filterBy2">P. Iva</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="filterBy" id="filterBy3" value="2" <?php echo ($this->filterBy == 2) ? 'checked' : ''; ?>>
                                        <label class="form-check-label" for="filterBy3">Sede</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="d-flex align-items-end offset-1 col-10 offset-sm-0 col-sm-6 col-md-4 col-lg-3 box-right">
                            <a href="<?php echo $this->path->getUrlFor('Account', 'registration/customer'); ?>">Nuovo cliente<span><i class="fa fa-user"></i></span></a>
                        </div> -->
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
    <section class="container admin-main-body">
        <div class="row mt-5">
            <?php if ($this->viewagent) { ?>
                <div class="mt-3 col-12 <?php echo (isset($this->customerinfo['name']) && $this->customerinfo['name'] != '') ? '' : 'offset-md-3'; ?> col-md-6 single-address ">
                    <div class="content">
                        <?php $useroption = json_decode($this->accountinfo['options'], true); ?>
                        <h6>Informazioni agente</h6>
                        <h5><?php echo $this->accountinfo['name']; ?></h5>
                        <h6>Dati commerciali</h6>
                        <p class="">Budget campionatura: <b><?php echo $useroption['budget']; ?> &euro;</b> (iva esclusa)</p>
                        <p class="">Provvigioni privati: <b><?php echo $this->fee; ?> &euro;</b></p>
                        <h6>Altre informazioni</h6>
                        <p class="email"><b><?php echo $this->accountinfo['email']; ?></b></p>
                        <p class="">Ultimo accesso: <b><?php echo $this->accountinfo['last_access']; ?></b></p>
                    </div>
                </div>
            <?php } ?>
            <?php if (isset($this->customerinfo['name']) && $this->customerinfo['name'] != '') { ?>
                <?php $custsomeroption = json_decode($this->customerinfo['customeroptions'], true); ?>
                <div class="mt-3 col-12 <?php echo (!$this->viewagent) ? 'offset-md-3' : ''; ?> col-md-6 single-address">
                    <div class="content">
                        <h6>Informazioni cliente</h6>
                        <h5><?php echo $this->customerinfo['name'] ?></h5>
                        <h6>Dati amministrativi</h6>
                        <?php if (isset($custsomeroption['account_type'])) {
                            if ($custsomeroption['account_type'] == 0) {
                                echo '<p><b>Cliente nuovo / S.r.l.s</b></p>';
                            } else {
                                echo '<p><b>Cliente verificato</b></p>';
                            }
                        } ?>
                        <p><?php echo (isset($this->customerinfo['vat']) && $this->customerinfo['vat'] != '') ? 'P. Iva: <b>' . $this->customerinfo['vat'] . '</b><br>' : ''; ?>C.F.: <b><?php echo $this->customerinfo['cif']; ?></b></p>
                        <h6>Dati commerciali</h6>
                        <?php
                        for ($i = 0; $i < $this->customerinfo['discount-count']; $i++) {
                            echo '<p class="discount">Sconto: <b>' . $this->customerinfo['discount-' . $i . '-name'] . '</b></p>';
                        }
                        ?>
                        <p class="payment">Pagamento: <b><?php echo $this->customerinfo['payment']; ?></b></p>

                        <?php if (isset($this->customerinfo['cscommonvalue']) && $this->customerinfo['cscommonvalue'] != '') {
                            if ($this->customerinfo['cscommonvalue'] < '40' || strpos($this->customerinfo['cscommondescription'], 'S.r.l.s') !== false) {
                                $class = 'danger';
                            } else if ($this->customerinfo['cscommonvalue'] >= '40' && $this->customerinfo['cscommonvalue'] < '60') {
                                $class = 'alert';
                            } else {
                                $class = 'success';
                            }
                            echo '<p class="csinfo">Rischio: <span class="' . $class . '">' . $this->customerinfo['cscommondescription'] . '</span> - Score:  <span class="' . $class . '">' . $this->customerinfo['cscommonvalue'] . '</span></p>';
                        } ?>
                        <h6>Altre informazioni</h6>
                        <p class="email"><b><?php echo $this->customerinfo['email-alternative']; ?></b></p>
                        <p class="cashback">Cashback: <b><?php echo $this->cashback; ?> &euro;</b></p>
                        <input type="hidden" id="csid" name="csid" value="">
                        <input type="hidden" id="idaddress" name="idaddress" value="<?php echo $this->customerinfo['idaddress']; ?>">
                        <?php if (isset($this->customerinfo['cscommonvalue']) && $this->customerinfo['cscommonvalue'] != '') {
                        } else {
                            echo '</br><button id="vat-checker" class="btn btn-primary">Controlla azienda</button>';
                        } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row mt-3 boxes-section">
            <div class="col-12 col-title">
                <?php echo ($this->viewagent) ? '<h3>ordini</h3>' : '<h3>ordini</h3>'; ?>
            </div>

            <?php
            $account_boxes = array();
            $buy_link = $this->path->getUrlFor('Product', 'search/index') . '?category=0&price=0-10000&manufacturer=0&vintage=0&region=0&vineyards=0&country=0&typecondition=0';
            if ($this->viewagent) {
                $account_boxes['buy'] = array(
                    "label" => "Ordina",
                    "link" => $buy_link,
                    "icon_class" => "fas fa-wine-bottle",
                    "id" => "openbuy"
                );
            } else {
                $account_boxes['buy'] = array(
                    "label" => "Acquista",
                    "link" => $buy_link,
                    "icon_class" => "fas fa-wine-bottle",
                    "id" => "openbuy"
                );
            }
            if (!in_array(42, $this->user->groups)) {
                $abandoned_carts_link = (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : '';
                $account_boxes['abandoned-carts'] = array(
                    "label" => "Carrelli abbandonati",
                    "link" => $this->path->getUrlFor('Cart', 'cart/listcart') . $abandoned_carts_link,
                    "icon_class" => "fas fa-hourglass-end"
                );
            }
            $confirmed_carts_link = (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : '';
            $account_boxes['confirmed-carts'] = array(
                "label" => "Carrelli confermati",
                "link" => $this->path->getUrlFor('Cart', 'cart/listcartconfirmed') . $confirmed_carts_link,
                "icon_class" => "fas fa-cart-arrow-down",
            );
            if ($this->viewagent) {
                $orders_under_management_link = (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : '';
                $account_boxes['orders-proforma'] = array(
                    "label" => "Proforma",
                    "link" => $this->path->getUrlFor('Order', 'order/listprof') . $orders_under_management_link,
                    "icon_class" => "fas fa-truck-loading",
                );
                $account_boxes['orders-resid'] = array(
                    "label" => "Ordini attesa merce",
                    "link" => $this->path->getUrlFor('Order', 'order/listresid') . $orders_under_management_link,
                    "icon_class" => "fas fa-pallet",
                );
                $account_boxes['orders-ordte'] = array(
                    "label" => "Ordini Temporanei",
                    "link" => $this->path->getUrlFor('Order', 'order/listordte') . $orders_under_management_link,
                    "icon_class" => "fas fa-box-open",
                );
                $account_boxes['orders-orsos'] = array(
                    "label" => "Ordini Sospesi",
                    "link" => $this->path->getUrlFor('Order', 'order/listorsos') . $orders_under_management_link,
                    "icon_class" => "fas fa-parking",
                );
                $account_boxes['orders-prosc'] = array(
                    "label" => "Proforma Scadute",
                    "link" => $this->path->getUrlFor('Order', 'order/listprosc') . $orders_under_management_link,
                    "icon_class" => "fas fa-cash-register",
                );
            }
            $shipped_orders_link = (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : '';
            $account_boxes['shipped-orders'] = array(
                "label" => "Ordini spediti",
                "link" => $this->path->getUrlFor('Order', 'order/listorderconfirmed') . $shipped_orders_link,
                "icon_class" => "fas fa-shipping-fast",
            );
            ?>
            <?php foreach ($account_boxes as $id => $box) { ?>
                <div class="col-6 col-sm-4 mb-5 text-center single-box">
                    <?php if (isset($box['class']) && $box['class'] == 'disabled') { ?>
                        <div class="btn-user-nav <?php echo (isset($box['class'])) ? $box['class'] : ''; ?>" id="<?php echo (array_key_exists("id", $box) && $box['id'] != "") ? $box['id'] : "box-" . $id; ?>" title="<?php echo $box['label']; ?>" href="<?php echo $box['link']; ?>">
                            <span>
                                <i class="<?php echo $box['icon_class']; ?>"></i>
                            </span>
                            <h5>
                                <?php echo $box['label']; ?>
                            </h5>
                        </div>
                    <?php } else { ?>
                        <a class="btn-user-nav <?php echo (isset($box['class'])) ? $box['class'] : ''; ?>" id="<?php echo (array_key_exists("id", $box) && $box['id'] != "") ? $box['id'] : "box-" . $id; ?>" title="<?php echo $box['label']; ?>" href="<?php echo $box['link']; ?>">
                            <span>
                                <i class="<?php echo $box['icon_class']; ?>"></i>
                            </span>
                            <h5>
                                <?php echo $box['label']; ?>
                            </h5>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>

            <div class="col-12 col-title">
                <?php echo ($this->viewagent) ? '<h3>clienti</h3>' : '<h3>anagrafica</h3>'; ?>
            </div>
            <?php
            $account_boxes = array();
            if ($this->viewagent) {
                $account_boxes['account'] = array(
                    "label" => "Aggiungi cliente",
                    "link" => $this->path->getUrlFor('Account', 'registration/customer'),
                    "icon_class" => "fas fa-user-friends"
                );
            }
            $account_boxes['addresses'] = array(
                "label" => "Indirizzi",
                "link" => $this->path->getUrlFor('Account', 'address/list'),
                "icon_class" => "fas fa-building",
                "id" => "openaddresses"
            );
            if (!in_array(42, $this->user->groups)) {
                $account_boxes['debit'] = array(
                    "label" => "Scadenziario",
                    "link" => $this->path->getUrlFor('Order', 'order/listopendebit'),
                    "icon_class" => "fas fa-comments-dollar",
                    "id" => "opendebit"
                );
            }
            ?>
            <?php foreach ($account_boxes as $id => $box) { ?>
                <div class="col-6 col-sm-4 mb-5 text-center single-box">
                    <?php if (isset($box['class']) && $box['class'] == 'disabled') { ?>
                        <div class="btn-user-nav <?php echo (isset($box['class'])) ? $box['class'] : ''; ?>" id="<?php echo (array_key_exists("id", $box) && $box['id'] != "") ? $box['id'] : "box-" . $id; ?>" title="<?php echo $box['label']; ?>" href="<?php echo $box['link']; ?>">
                            <span>
                                <i class="<?php echo $box['icon_class']; ?>"></i>
                            </span>
                            <h5>
                                <?php echo $box['label']; ?>
                            </h5>
                        </div>
                    <?php } else { ?>
                        <a class="btn-user-nav <?php echo (isset($box['class'])) ? $box['class'] : ''; ?>" id="<?php echo (array_key_exists("id", $box) && $box['id'] != "") ? $box['id'] : "box-" . $id; ?>" title="<?php echo $box['label']; ?>" href="<?php echo $box['link']; ?>">
                            <span>
                                <i class="<?php echo $box['icon_class']; ?>"></i>
                            </span>
                            <h5>
                                <?php echo $box['label']; ?>
                            </h5>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="col-12 col-title">
                <?php echo ($this->viewagent) ? '<h3>enotecaves</h3>' : '<h3>enotecaves</h3>'; ?>
            </div>
            <?php
            $account_boxes = array();
            if ($this->viewagent) {
                $account_boxes['advisor'] = array(
                    "label" => "Abilita presentatore",
                    "link" => $this->path->getUrlFor('Account', 'address/list'),
                    "icon_class" => "fas fa-store",
                    "id" => "openadvisor"
                );
                $account_boxes['advisorlist'] = array(
                    "label" => "Riepilogo presentatori",
                    "link" => $this->path->getUrlFor('Account', 'list/advisor'),
                    "icon_class" => "fas fa-user-tie"
                );
                $cashback_link = (isset($this->customer) && $this->customer != 0) ? $this->path->getUrlFor('Order', 'order/cashbackagent') . '?customer=' . $this->customer : $this->path->getUrlFor('Order', 'order/cashbackagent');
                $account_boxes['cashbackadvisor'] = array(
                    "label" => "Cashback Presentatori",
                    "link" => $cashback_link,
                    "icon_class" => "fas fa-money-bill-wave",
                    "id" => "opencashback"
                );
                $cashback_link = (isset($this->user->id) && $this->user->id != 0) ? $this->path->getUrlFor('Order', 'order/cashback') . '?customer=' . $this->user->id : $this->path->getUrlFor('Order', 'order/cashback');
                $account_boxes['cashback'] = array(
                    "label" => "Provvigioni privati",
                    "link" => $cashback_link,
                    "icon_class" => "fas fa-file-invoice-dollar",
                    "id" => "cashback"
                );
            } else {
                if (in_array(1, $this->user->groups)) {
                    if ($this->advisor == 1) {
                        $account_boxes['invitation'] = array(
                            "label" => "Invita un cliente",
                            "link" => $this->path->getUrlFor('Account', 'registration/invitation'),
                            "icon_class" => "fas fa-paper-plane",
                        );
                    } else {
                        $account_boxes['invitation'] = array(
                            "label" => "Invita un cliente",
                            "link" => $this->path->getUrlFor('Account', 'user/index'),
                            "icon_class" => "fas fa-paper-plane",
                            "class" => "disabled"
                        );
                    }
                } else if (in_array(42, $this->user->groups)) {
                    if ($this->advisor == 1) {
                        $account_boxes['invitation'] = array(
                            "label" => "Invita un amico",
                            "link" => $this->path->getUrlFor('Account', 'registration/invitation'),
                            "icon_class" => "fas fa-paper-plane",
                        );
                    } else {
                        $account_boxes['invitation'] = array(
                            "label" => "Invita un amico",
                            "link" => $this->path->getUrlFor('Account', 'user/index'),
                            "icon_class" => "fas fa-paper-plane",
                            "class" => "disabled"
                        );
                    }
                }
                if ($this->advisor == 1) {
                    $account_boxes['invitationlist'] = array(
                        "label" => "Riepilogo inviti",
                        "link" => $this->path->getUrlFor('Account', 'list/invitation'),
                        "icon_class" => "fas fa-user-friends"
                    );
                } else {
                    $account_boxes['invitationlist'] = array(
                        "label" => "Riepilogo inviti",
                        "link" => $this->path->getUrlFor('Account', 'user/index'),
                        "icon_class" => "fas fa-user-friends",
                        "class" => "disabled"
                    );
                }
                if (in_array(1, $this->user->groups) && $this->advisor == 0) {
                    $account_boxes['reactivatedadvisor'] = array(
                        "label" => "Riattivati come presentatore",
                        "link" => $this->path->getUrlFor('Account', 'user/index') . '?user=' . $this->user->id . '&advisor=2',
                        "icon_class" => "fas fa-user-tie",
                    );
                }
                $cashback_link = (isset($this->customer) && $this->customer != 0) ? $this->path->getUrlFor('Order', 'order/cashback') . '?customer=' . $this->customer : $this->path->getUrlFor('Order', 'order/cashback');
                $account_boxes['cashback'] = array(
                    "label" => "Cashback",
                    "link" => $cashback_link,
                    "icon_class" => "fas fa-money-bill-wave",
                    "id" => "cashback"
                );
            }
            ?>
            <?php foreach ($account_boxes as $id => $box) { ?>
                <div class="col-6 col-sm-4 mb-5 text-center single-box">
                    <?php if (isset($box['class']) && $box['class'] == 'disabled') { ?>
                        <div class="btn-user-nav <?php echo (isset($box['class'])) ? $box['class'] : ''; ?>" id="<?php echo (array_key_exists("id", $box) && $box['id'] != "") ? $box['id'] : "box-" . $id; ?>" title="<?php echo $box['label']; ?>" href="<?php echo $box['link']; ?>">
                            <span>
                                <i class="<?php echo $box['icon_class']; ?>"></i>
                            </span>
                            <h5>
                                <?php echo $box['label']; ?>
                            </h5>
                        </div>
                    <?php } else { ?>
                        <a class="btn-user-nav <?php echo (isset($box['class'])) ? $box['class'] : ''; ?>" id="<?php echo (array_key_exists("id", $box) && $box['id'] != "") ? $box['id'] : "box-" . $id; ?>" title="<?php echo $box['label']; ?>" href="<?php echo $box['link']; ?>">
                            <span>
                                <i class="<?php echo $box['icon_class']; ?>"></i>
                            </span>
                            <h5>
                                <?php echo $box['label']; ?>
                            </h5>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>

    </section>
    <?php
    if (isset($_COOKIE['lescaves_check_access'])) {
    } else {
        setcookie("lescaves_check_access", "OK", time() + 86400);
        if (!empty((new \EOS\Components\Content\Widgets\SectionWidget($this, 'news-modal'))->data)) {
    ?>
            <div id="newsBanner" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <?php if ((new \EOS\Components\Content\Widgets\SectionWidget($this, 'news-modal'))->data['options']['showtitle'] == 1) { ?>
                                <h5 class="modal-title"><?php echo (new \EOS\Components\Content\Widgets\SectionWidget($this, 'news-modal'))->data['title']; ?></h5>
                            <?php } ?>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="overflow-auto news">
                                <?php if (in_array(2, $this->user->groups)) {
                                    echo (new \EOS\Components\Content\Widgets\SectionWidget($this, 'news-modal'))->data['content'];
                                } else if (in_array(1, $this->user->groups)) {
                                    echo (new \EOS\Components\Content\Widgets\SectionWidget($this, 'news-modal-b2b'))->data['content'];
                                } else {
                                    echo (new \EOS\Components\Content\Widgets\SectionWidget($this, 'news-modal-b2c'))->data['content'];
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->startCaptureScript(); ?>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#newsBanner').modal('show');
                });
            </script>
    <?php $this->endCaptureScript();
        }
    } ?>
</div>