<!--  pannello agente-->
<div id="alertDebit" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Attenzione!
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Prima di visualizzare lo scadenziario e necessario selezionare il cliente.
            </div>
        </div>
    </div>
</div>

<div id="alertAddress" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Attenzione!
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Prima di visualizzare l'elenco degli indirizzi e necessario selezionare il cliente.
            </div>
        </div>
    </div>
</div>

<div id="alertDebit2" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Attenzione!
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if (in_array(2, $this->user->groups)) { ?>
                    Il cliente ha degli scaduti - Effettuare una verifica o contattare l'amministrazione
                <?php } else { ?>
                    Sono presenti degli scaduti - Effettuare una verifica o contattare l'amministrazione
                <?php }  ?>
            </div>
        </div>
    </div>
</div>

<div id="alertAdvisor" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Attenzione!
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                È necessario selezionare il cliente per procedere all'abilitazione come presentatore.
            </div>
        </div>
    </div>
</div>

<div id="alertCashback" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Attenzione!
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Prima di visualizzare l'elenco dei cashback e necessario selezionare il cliente.
            </div>
        </div>
    </div>
</div>

<div id="alertPassword" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Attenzione!
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Hai appena richiesto una password provvisoria. Se sei stato tu ti ricordiamo di variarla, altrimenti segnalalo a shop@lescaves.it.
            </div>
            <div class="modal-footer">
                <a href="<?php echo $this->path->getUrlFor('Account', 'user/change-pass'); ?>" class="btn btn-primary">Cambio password</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="advisorModal" tabindex="-1" role="dialog" aria-labelledby="advisorModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="advisorModalLabel">Condizioni contrattuali</h5>
            </div>
            <div class="modal-body">
                <div class="overflow-auto contract">
                    <?php if (in_array(2, $this->user->groups)) {
                        echo (new \EOS\Components\Content\Widgets\SectionWidget($this, 'condition-modal'))->data['content'];
                    } else if (in_array(1, $this->user->groups)) {
                        echo (new \EOS\Components\Content\Widgets\SectionWidget($this, 'condition-modal-b2b'))->data['content'];
                    } else {
                        echo (new \EOS\Components\Content\Widgets\SectionWidget($this, 'condition-modal-b2c'))->data['content'];
                    }
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Non accetto</button>
                <button type="button" class="btn btn-primary">Accetto</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="CSCheckData" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Controllo dati</h5>
            </div>
            <div class="modal-body">
                <p id="CSdataCompany">Azienda</p>
                <p id="CSdataAddress">Indirizzo</p>
            </div>
            <div class="modal-footer">
                <button id="CSClear" type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                <button id="CSConfirm" type="button" class="btn btn-primary">Conferma</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="CSRating" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Monitoraggio</h5>
            </div>
            <div class="modal-body">
                <p id="CScommonValue">Score: <span id="CScommonValueV"></span></p>
                <!-- <p id="CScreditLimit">Limite Credito: <span id="CScreditLimitV"></span> €</p> -->
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>