<form id="account-register" method="POST" autocomplete="off">
    <div id="account-register-container" class="container">
        <div class="row">
            <div class="col-md-offset-6 col-md-6">
                <p>Inserisci una passoword con minimo 8 caratteri, composta da lettere, numeri e caratteri speciali (es. #@!.;$).</p>
                <p>Non puoi inserire la password uguale al tuo nome utente.</p>
                <p>Non condividere la password con nessuno.</p>
            </div>
            <div class="col-md-offset-6 col-md-6 mt-4 mt-md-0">
                <div class="form-group">
                    <input id="account-change-pwd-old" name="pwdold" type="password" class="form-control" placeholder="Password attuale">
                </div>
                <div class="form-group">
                    <input id="account-change-pwd1" name="pwd1" type="password" class="form-control" placeholder="Nuova password">
                </div>
                <div class="form-group">
                    <input id="account-change-pwd2" name="pwd2" type="password" class="form-control" placeholder="Ripeti password">
                </div>
                <div class="form-group ">
                    <button id="btn-pwd-change" class="btn btn-primary">Cambia</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="offset-4 col-4 submit-box">
                <p><a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Torna Alla pagina</a> | <a href="<?php echo $this->path->urlFor('account', ['user', 'index']); ?>">Account</a></p>
            </div>
        </div>
    </div>
    <?php
    $this->util->writeAsyncTokenHtml();
    $this->util->writeAsyncRequestUrl();
    ?>
    <input type="hidden" name="id" value="<?php echo $this->user->id; ?>">
</form>
<?php
$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
