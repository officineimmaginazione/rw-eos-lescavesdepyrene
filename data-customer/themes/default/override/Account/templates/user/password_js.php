<script>
    $(function() {
        $('#btn-pwd-change').click(function(event) {
            event.preventDefault();
            var data = $('#account-register').serializeFormJSON();
            $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '<?php echo $this->path->urlFor('account', ['user', 'ajaxPassword']); ?>',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                })
                .done(function(json) {
                    if (json.result == true) {
                        bootbox.alert(json.message, function() {
                            location.href = '<?php echo $this->path->urlFor('account', ['user', 'index']); ?>';
                        });
                    } else {
                        bootbox.alert(json.message);
                    }
                })
                .fail(function(jqxhr, textStatus, error) {
                    var err = textStatus + ", " + error;
                    bootbox.alert(err);
                });
        });
    });
</script>