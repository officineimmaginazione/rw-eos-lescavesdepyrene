<?php

return
    [
        'cart.details.title' => 'Dettagli carrello',
        'cart.details.products' => 'Riepilogo',
        'cart.details.id' => 'Codice',
        'cart.details.product' => 'Nome',
        'cart.details.quantity' => 'Quantità',
        'cart.details.price' => 'Prezzo',
        'cart.details.pricediscount' => 'Prezzo Scontato',
        'cart.details.totrow' => 'Tot. Scontato',
];

