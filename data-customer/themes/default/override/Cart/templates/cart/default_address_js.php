<?php
$this->startCaptureScript();
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
?>
<script>
    function runAjaxAddress(data, resultEvent) {

        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Account', 'address/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    function runAjaxUser(data, resultEvent) {

        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Account', 'user/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    $(function() {

        <?php
        if ($this->filterBy == 1) {
            $url = $this->path->getUrlFor('account', 'user/getdatasource/user/typeahead1/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue());
        } elseif ($this->filterBy == 2) {
            $url = $this->path->getUrlFor('account', 'user/getdatasource/user/typeahead2/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue());
        } else {
            $url = $this->path->getUrlFor('account', 'user/getdatasource/user/typeahead0/' . $this->session->getTokenName() . '/' . $this->session->getTokenValue());
        }
        ?>
        $.get("<?php echo $url; ?>", function(data) {
            var customerEdit = $('#customer');
            var customerID = $('input[name=\'idcustomer\']');
            var addressID = $('input[name=\'idaddressdelivery\']');
            customerEdit.typeahead({
                source: data,
                delay: 100,
                minLength: 2,
                autoSelect: true,
                items: 30
            });

            customerEdit.change(function() {
                var current = customerEdit.typeahead("getActive");
                if (current) {
                    if (current.name == customerEdit.val()) {
                        var data = {
                            'command': "customerdata",
                            'idcustomer': current.id,
                            'status': false,
                            '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                        };
                        runAjaxUser(data, function(json) {
                            setDiscPay(json, true);
                            if (json.address['id-payment'] != null) {
                                //$("#payment option[value=" + json.address['id-payment'] + "]").removeAttr('disabled');
                                $("#payment").val(json.address['id-payment']);
                                var headpayment = json.address['id-payment'];
                            } else {
                                $("#payment").val(6);
                                var headpayment = 6;
                            }
                            var headnotes = $(".head-note").val();
                            var headdiscount1 = $("#discount1").val();
                            var headdiscount2 = $("#discount2").val();
                            var headdiscount3 = $("#discount3").val();
                            var orderresid = $("#orderresid").val();
                            //var headpayment = $("#payment").val();
                            var headpaymentdiscount = $('input[name=\'paymentdiscount\']').val();
                            var headparameters = [headdiscount1, headdiscount2, headdiscount3, headnotes, headpayment, headpaymentdiscount, orderresid];
                            var data = {
                                id: <?php echo $this->cartid; ?>,
                                idcustomer: current.id,
                                idaddress: 0,
                                idagent: <?php echo $this->agentid; ?>,
                                headparameters: headparameters,
                                command: 'updatecart',
                                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                            };
                            addressID.val(0);
                            customerID.val(current.id);
                            updateCart(data);
                        });
                        customerID.val(current.id);
                    } else {
                        customerID.val('');
                    }
                } else {
                    // Nothing is active so it is a new value (or maybe empty value)
                }
            });


        }, 'json');
    });

    function dataCustomer() {
        <?php if (isset($this->head['id_address_delivery']) && $this->head['id_address_delivery'] != 0) { ?>
            var data = {
                'command': "customerdata",
                'id-address': <?php echo $this->head['id_address_delivery']; ?>,
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            runAjaxAddress(data, function(json) {
                setDiscPay(json, false);
                var headnotes = $(".head-note").val();
                var headparameters = ["", headnotes];
                $("#payment option[value=" + json.address['id-payment'] + "]").removeAttr('disabled');
                $('input[name=\'idcustomer\']').val(json.address['id-customer']);
                //$("#payment").val(json.address['id-payment']).change();

            });
            $('input[name=\'idaddressdelivery\']').val(<?php echo $this->head['id_address_delivery']; ?>);
        <?php } ?>
    }

    function setDiscPay(json, change) {
        var paymentdiscount = json.address['payment-discount'];
        if (paymentdiscount === undefined || paymentdiscount === null) {
            paymentdiscount = '';
        }
        $('input[name=\'paymentdiscount\']').val(paymentdiscount);

        if (change == true) {
            if (json.address['discount-count'] == 0) {
                $('input[name=\'discount1\']').val(0);
                $('input[name=\'discount2\']').val(0);
                $('input[name=\'discount3\']').val(0);
            } else {
                for (index = 0; index < json.address['discount-count']; ++index) {
                    var payment1 = json.address['discount-' + index + '-reduction1'];
                    if (payment1 === undefined || payment1 === null) {
                        payment1 = 0;
                    }
                    var payment2 = json.address['discount-' + index + '-reduction2'];
                    if (payment2 === undefined || payment2 === null) {
                        payment2 = '';
                    }
                    var payment3 = json.address['discount-' + index + '-reduction3'];
                    if (payment3 === undefined || payment3 === null) {
                        payment3 = '';
                    }
                    $('input[name=\'discount1\']').val(payment1);
                    $('input[name=\'discount2\']').val(payment2);
                    $('input[name=\'discount3\']').val(payment3);
                }
            }
        } else {

        }
    }

    function displayInfoCustomer(json, change) {
        $("#customer-data").empty();
        var company = json.address['company'];
        if (company === undefined || company === null) {
            company = '';
        }
        var name = json.address['name'];
        if (name === undefined || name === null) {
            name = '';
        }
        var surname = json.address['surname'];
        if (surname === undefined || surname === null) {
            surname = '';
        }
        var address1 = json.address['address1'];
        if (address1 === undefined || address1 === null) {
            address1 = '';
        }
        var city = json.address['city'];
        if (city === undefined || city === null) {
            city = '';
        }
        var phone = json.address['phone'];
        if (phone === undefined || phone === null) {
            phone = '';
        }
        var piva = json.address['vat-number'];
        if (piva === undefined || piva === null) {
            piva = '';
        }
        var payment = json.address['payment'];
        if (payment === undefined || payment === null) {
            payment = '';
        }
        var payment = json.address['payment'];
        if (payment === undefined || payment === null) {
            payment = '';
        }
        var paymentdiscount = json.address['payment-discount'];
        if (paymentdiscount === undefined || paymentdiscount === null) {
            paymentdiscount = '';
        }


        var customerdata = '<div class="row mt-3"><div class="col-12 col-sm-5 offset-sm-1 single-address"><div class="content"><h6>Indirizzo di consegna</h6>';
        if (name != "" || surname != "")
            customerdata = customerdata + '<p class="name">' + name + ' ' + surname + '</p>';
        if (company != "")
            customerdata = customerdata + '<p class="company">' + company + '</p>';
        if (address1 != "")
            customerdata = customerdata + '<p class="address">' + address1 + '</p>';
        if (city != "")
            customerdata = customerdata + '<p class="city">' + city + '</p>';
        if (phone != "")
            customerdata = customerdata + '<p class="telephone">Tel: ' + phone + '</p>';
        customerdata = customerdata + '</div></div><div class="col-12 col-sm-5 single-address"><div class="content"><h6>Sconti e pagamento</h6>';
        if (payment != "")
            customerdata = customerdata + '<p class="payment">Pagamento cliente: ' + payment + '</p>';

        $('input[name=\'paymentdiscount\']').val(paymentdiscount);

        if (change == true) {
            if (json.address['discount-count'] == 0) {
                $('input[name=\'discount1\']').val(0);
                $('input[name=\'discount2\']').val(0);
                $('input[name=\'discount3\']').val(0);
            } else {
                for (index = 0; index < json.address['discount-count']; ++index) {
                    customerdata = customerdata + '<p class="discount">' + json.address['discount-' + index + '-name'] + '</p>';
                    var payment1 = json.address['discount-' + index + '-reduction1'];
                    if (payment1 === undefined || payment1 === null) {
                        payment1 = 0;
                    }
                    var payment2 = json.address['discount-' + index + '-reduction2'];
                    if (payment2 === undefined || payment2 === null) {
                        payment2 = '';
                    }
                    var payment3 = json.address['discount-' + index + '-reduction3'];
                    if (payment3 === undefined || payment3 === null) {
                        payment3 = '';
                    }
                    $('input[name=\'discount1\']').val(payment1);
                    $('input[name=\'discount2\']').val(payment2);
                    $('input[name=\'discount3\']').val(payment3);
                }
            }
        } else {
            for (index = 0; index < json.address['discount-count']; ++index) {
                customerdata = customerdata + '<p class="discount">' + json.address['discount-' + index + '-name'] + '</p>';
            }
        }
        customerdata = customerdata + '</div></div></div>';
        $("#customer-data").html(customerdata);
    }

    $('document').ready(function() {
        //dataCustomer();
    });
</script>
<?php
$this->endCaptureScript();
