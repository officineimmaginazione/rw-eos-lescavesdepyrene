<?php
$this->startCaptureScript();
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
?>
<script src="/data-customer/themes/default/js/jquery.print.min.js" type="text/javascript"></script>
<script>
    function runAjaxUrl(data, resultEvent, ajaxUrl) {
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: ajaxUrl,
                data: JSON.stringify(data),
                contentType: "application/json"
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                bootbox.alert(err);
            });
    }

    $("#printcart").click(function(event) {
        event.preventDefault();
        //$(".cart-section").print(/*options*/);
        window.print();
    });

    function runAjax(data, resultEvent) {
        runAjaxUrl(data, resultEvent, '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>');
    }

    $("#cart-confirm").click(function(event) {
        event.preventDefault();
        saveData();
    });

    function saveData() {
        var message = '';
        var check = true;
        if ($('input[name=\'idcustomer\']').val() === '0') {
            message = 'Inserisci il cliente';
            check = false;
        }
        if (check === true) {
            var data = $('#main-cart-list').serializeFormJSON();
            runAjax(data, function(json) {
                location.href = json.redirect;
            });
        } else {
            bootbox.alert(message);
        }
    }

    $(function() {
        var data;
        $('.add').on('click', function() {
            $id = jQuery(this).attr("id");
            $id = $id.replace("add", "");
            $qty = $('#' + $id).find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
            }
            var data = {
                id: $id,
                qty: $qty.val(),
                command: 'updaterow',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            updateRow(data);
        });
        $('.minus').on('click', function() {
            $id = jQuery(this).attr("id");
            $id = $id.replace("minus", "");
            $qty = $('#' + $id).find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
            }
            var data = {
                id: $id,
                qty: $qty.val(),
                command: 'updaterow',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            updateRow(data);
        });
        $('.delete').on('click', function(e) {
            e.preventDefault();
            $id = jQuery(this).attr("id");
            $id = $id.replace("delete", "");
            var data = {
                id: $id,
                command: 'deleterow',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            deleteRow(data);
        });
        $(".pro-quantity input").on("change", function() {
            var quantity = $(this).val();
            if (quantity >= 1) {
                var id = this.getAttribute('data-rowId');
                var idproduct = this.getAttribute('data-rowIdProduct');
                id = id.replace("qta", "");
                //            if ($("#chk" + id).parent().hasClass("checked"))
                //            {
                //                if (quantity != 1)
                //                {
                //                    $('#errorModal-qta').modal('toggle');
                //                    $(this).val(1);
                //                }
                //            } else
                //            {
                var discount = $("#discount" + id).val();
                if (discount == "" || discount == "null") {
                    discount = 0;
                }
                var issample = 0;
                if ($("#chk" + id).parent().hasClass("checked")) {
                    issample = 1;
                }
                var array = [discount, issample];
                var data = {
                    id: id,
                    idproduct: idproduct,
                    qty: quantity,
                    options: array,
                    command: 'updaterow',
                    '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                };
                updateRow(data);
                //            }
            } else {
                $('#errorModal-qtamin').modal('toggle');
                $(this).val(1);
            }
        });
        $(".pro-discount input").on("change", function() {
            var id = $(this).attr('id');
            //id = id.replace("discount", "");
            var id = this.getAttribute('data-rowId');
            var idproduct = this.getAttribute('data-rowIdProduct');
            var discount = $(this).val();
            var quantity = $("#qta" + id).val();

            if (discount > 25 || discount < 0) {
                $('#errorModal').modal('toggle');
                $(this).val(0);
                $(".head-discount").val(0);
            } else {
                var issample = 0;
                if ($("#chk" + id).parent().hasClass("checked")) {
                    issample = 1;
                }
                var array = [discount, issample];
                var data = {
                    id: id,
                    idproduct: idproduct,
                    qty: quantity,
                    options: array,
                    command: 'updaterow',
                    '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                };
                updateRow(data);
            }
        });

        $(".discount-head").on("change", function() {
            var headdiscount1 = $("#discount1").val();
            var headdiscount2 = $("#discount2").val();
            var headdiscount3 = $("#discount3").val();
            var orderresid = $("#orderresid").val();
            var headnotes = $(".head-note").val();
            var headpayment = $("#payment").val();
            var headpaymentdiscount = $("#paymentdiscount").val();
            var headparameters = [headdiscount1, headdiscount2, headdiscount3, headnotes, headpayment, headpaymentdiscount, orderresid];
            var customer = "<?php echo ($this->head['id_customer']) ? $this->head['id_customer'] : "0"; ?>";
            var address = "<?php echo ($this->head['id_address_delivery']) ? $this->head['id_address_delivery'] : "0"; ?>";
            var data = {
                id: <?php echo $this->cartid; ?>,
                idcustomer: parseInt(customer),
                idaddress: parseInt(address),
                idagent: <?php echo $this->agentid; ?>,
                headparameters: headparameters,
                command: 'updatecart',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            updateCart(data);
        });

        $("#payment").on("change", function() {
            $("#paymentdiscount").val($("#payment").find(':selected').attr('data-payment-discount'));
            var headdiscount1 = $("#discount1").val();
            var headdiscount2 = $("#discount2").val();
            var headdiscount3 = $("#discount3").val();
            var orderresid = $("#orderresid").val();
            var headnotes = $(".head-note").val();
            var headpayment = $("#payment").val();
            var headpaymentdiscount = $("#payment").find(':selected').attr('data-payment-discount');
            var headparameters = [headdiscount1, headdiscount2, headdiscount3, headnotes, headpayment, headpaymentdiscount, orderresid];
            var customer = "<?php echo ($this->head['id_customer']) ? $this->head['id_customer'] : "0"; ?>";
            var address = "<?php echo ($this->head['id_address_delivery']) ? $this->head['id_address_delivery'] : "0"; ?>";
            var data = {
                id: <?php echo $this->cartid; ?>,
                idcustomer: parseInt(customer),
                idaddress: parseInt(address),
                idagent: <?php echo $this->agentid; ?>,
                headparameters: headparameters,
                command: 'updatecart',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            updateCart(data);
        });

        $(".head-discount").on("change", function() {
            var headdiscount = $(this).val();
            if (headdiscount > 0 && headdiscount < 25 && headdiscount) {
                //                $(".cart-list .single-product").each(function (i, el)
                //                {
                //                    var el = $(el);
                //                    if (!el.find(".pro-discount").hasClass("is_sample"))
                //                    {
                //                        el.find(".additional-discount").val(headdiscount);
                //                        el.find(".additional-discount").trigger("change");
                //                        updateCart(data);
                //                    }
                //                });
                var data = {
                    headdiscount: headdiscount,
                    command: 'updateheaddiscount',
                    '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                };
                updateCart(data);
            } else {
                $(this).val(0);
                $('#errorModal').modal('toggle');
            }
        });


        $(".head-note").on("focusout", function() {
            var headnotes = $(this).val();
            var headdiscount1 = $("#discount1").val();
            var headdiscount2 = $("#discount2").val();
            var headdiscount3 = $("#discount3").val();
            var orderresid = $("#orderresid").val();
            var headpayment = $("#payment").val();
            var headpaymentdiscount = $("#paymentdiscount").val();
            var headparameters = [headdiscount1, headdiscount2, headdiscount3, headnotes, headpayment, headpaymentdiscount, orderresid];
            var customer = "<?php echo ($this->head['id_customer']) ? $this->head['id_customer'] : "0"; ?>";
            var address = "<?php echo ($this->head['id_address_delivery']) ? $this->head['id_address_delivery'] : "0"; ?>";
            var data = {
                id: <?php echo $this->cartid; ?>,
                idcustomer: parseInt(customer),
                idaddress: parseInt(address),
                idagent: <?php echo $this->agentid; ?>,
                headparameters: headparameters,
                command: 'updatecart',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            updateCart(data);
        });

        $(".pro-check .form-check").click(function() {
            var idstr = $(this).find("input").attr('id');
            var chkele = document.getElementById(idstr);
            //var id = idstr.replace("chk", "");
            var id = chkele.getAttribute('data-rowId');
            var idproduct = chkele.getAttribute('data-rowIdProduct');
            var discount = $("#discount" + id).val();
            var quantity = $("#qta" + id).val();

            if ($(this).hasClass("checked")) {
                var array = [discount, 1];
                var data = {
                    id: id,
                    idproduct: idproduct,
                    qty: quantity,
                    options: array,
                    command: 'updaterow',
                    '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                };
            } else {
                var array = [discount, 0];
                var data = {
                    id: id,
                    idproduct: idproduct,
                    qty: quantity,
                    options: array,
                    command: 'updaterow',
                    '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                };
            }
            updateRow(data);
        });

        //validate current step fields and move forward
        $(".btn-next-step").click(function(e) {
            const currStep = $(this).parents(".single-step");
            currStep.removeClass("collapse-open");
            if (!currStep.next().hasClass("active")) {
                currStep.next().addClass("active");
                currStep.next().addClass("collapse-open");
            }
            currStep.next().find(".section-content").collapse('show');
        });

    });

    $(".btn-disable-address").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("disable-address-", "");
        updateaddress($id, 0);
    });

    $(".btn-uses-address").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("uses-address-", "");
        $('input[name=\'idaddressdelivery\']').val($id);
        var headdiscount1 = $("#discount1").val();
        var headdiscount2 = $("#discount2").val();
        var headdiscount3 = $("#discount3").val();
        var orderresid = $("#orderresid").val();
        var headnotes = $(".head-note").val();
        var headpayment = $("#payment").val();
        var headpaymentdiscount = $("input[name=\'paymentdiscount\']").val();
        var headparameters = [headdiscount1, headdiscount2, headdiscount3, headnotes, headpayment, headpaymentdiscount, orderresid];
        var customer = "<?php echo ($this->head['id_customer']) ? $this->head['id_customer'] : "0"; ?>";
        var address = $('input[name=\'idaddressdelivery\']').val();
        var data = {
            id: <?php echo $this->cartid; ?>,
            idcustomer: parseInt(customer),
            idaddress: parseInt(address),
            idagent: <?php echo $this->agentid; ?>,
            headparameters: headparameters,
            command: 'updatecart',
            '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
        };
        updateCart(data);
    });

    function updateaddress($id, $status) {
        var data = {};
        data.id = $id;
        data.status = $status;
        data.command = 'updatestatus';
        data.<?php echo $this->session->getTokenName() ?> = '<?php echo $this->session->getTokenValue() ?>';
        runAjaxAddress(data, function(json) {
            location.reload();
        });
    }

    $("#deletecart").on("click", function(event) {
        event.preventDefault();
        var data = {};
        data.command = 'emptycart';
        data.<?php echo $this->session->getTokenName() ?> = '<?php echo $this->session->getTokenValue() ?>';
        runAjax(data, function(json) {
            location.reload();
        });
    });

    $("#resetcart").on("click", function(event) {
        event.preventDefault();
        var data = {};
        data.command = 'resetcart';
        data.<?php echo $this->session->getTokenName() ?> = '<?php echo $this->session->getTokenValue() ?>';
        runAjax(data, function(json) {
            location.reload();
        });
    });

    function updateCart(data) {
        runAjax(data, function() {
            window.location.reload(false);
        });
    }

    function updateRow(data) {
        runAjax(data, function() {
            window.location.reload(false);
        });
    }

    function deleteRow(data) {
        runAjax(data, function() {
            window.location.reload(false);
        });
    }

    $(".bottom-info .form-check .form-check-label").click(function() {
        if ($(this).parent().hasClass("checked")) {
            $(this).parent().removeClass("checked");
        } else {
            $(this).parent().addClass("checked");
        }
    });
    $(".choose-address .form-check .form-check-label").click(function() {
        if ($(this).parent().hasClass("checked")) {
            $(this).parent().removeClass("checked");
        } else {
            $(this).parent().addClass("checked");
        }
    });

    $(function() {
        $('#btn-calcfreerows').on('click', function(ev) {
            ev.preventDefault();
            if ($('input[name=\'idcustomer\']').val() === '0') {
                message = 'Inserisci il cliente';
                bootbox.alert(message);
            } else {
                var data = {
                    command: 'calcfreerows',
                    '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                };
                runAjaxUrl(data, function(json) {
                    location.href = json.redirect;
                }, '<?php echo $this->path->getUrlFor('LCDPComp', 'cartext/ajaxcommand'); ?>');
            }
        });

        $('.btn-updatefreerow').on('click', function(ev) {
            ev.preventDefault();
            var $row = $(this).closest('.single-product');
            var idRow = $row.attr('data-id');
            var idProduct = $row.find('.updatefreerow').val();
            if (idProduct === '') {
                bootbox.alert('Selezionare un prodotto!');
            } else {
                var data = {
                    idrow: idRow,
                    idproduct: idProduct,
                    command: 'updatefreerow',
                    '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
                };
                runAjaxUrl(data, function(json) {
                    window.location.reload(false);
                }, '<?php echo $this->path->getUrlFor('LCDPComp', 'cartext/ajaxcommand'); ?>');
            }
        });
    });

    $('input').on('keydown', function(event) {
        var x = event.which;
        if (x === 13) {
            event.preventDefault();
        }
    });

    $(document).on('click', 'input[name="filterBy"]', function() {
        location.href = '<?php echo $this->path->getUrlFor('Cart', 'cart/index'); ?>' + '?filterby=' + $(this).val();
    });

    $(".btn-scroll").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#sec-" + this.dataset.scrollto).offset().top
        }, 1000);
    });

    $("#btn-cashback").click(function(event) {
        event.preventDefault();
        $('#cashbackModal').modal('show');
    });

    $("#btn-cashback2").click(function(event) {
        event.preventDefault();
        $('#cashbackModal').modal('show');
    });

    $(".btn-add-cashback").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        var data = $('#add-cashback').serializeFormJSON();
        runAjax(data, function(json) {
            setTimeout(function() {
                window.location.reload(false)
            }, 200);
        });
    });

    $(function() {
        $("#cashback").change(function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max) {
                $(this).val(max);
            } else if ($(this).val() < min) {
                $(this).val(min);
            }
        });
    });
</script>
<?php
$this->endCaptureScript();
