<?php
$option = \EOS\System\Util\ArrayHelper::fromJSON($this->head['options']);
$shippingData = \EOS\System\Util\ArrayHelper::getArray(\EOS\System\Util\ArrayHelper::fromJSON($this->head['options']), 'shipping');
$paymentdiscount = 0;
if (isset($option['head_payment_discount']) && $option['head_payment_discount'] != 0) {
    $paymentdiscount = $option['head_payment_discount'];
}
/* if (isset($this->user->groups) && in_array(42, $this->user->groups)) { */
$cart_steps = array(
    array(
        "title" => "customer",
        "label" => "Cliente",
        "label2" => "Prima di procedere completa il passaggio precedente",
        "is_active" => true
    ),
    array(
        "title" => "cart",
        "label" => "Carrello",
        "label2" => "Prima di procedere seleziona il cliente",
        "is_active" => true,
    ),
    array(
        "title" => "address",
        "label" => "Indirizzo spedizione",
        "label2" => 'Prima di procedere completa clicca su "Calcola omaggi"',
        "is_active" => ($this->controller->session->get('cart.set.calcfree.' . $this->cartid, 0) == 0 && !in_array(42, $this->user->groups)) ? false : true,
    ),
    array(
        "title" => "payment",
        "label" => "Pagamento",
        "label2" => "Prima di procedere seleziona l'indirizzo di spedizione",
        "is_active" => (isset($this->head['id_address_delivery']) && $this->head['id_address_delivery'] != 0) ? true : false,
    ),
    array(
        "title" => "shipping",
        "label" => "Spedizione",
        "label2" => "Prima di procedere seleziona il pagamento",
        "is_active" => (isset($this->head['id_address_delivery']) && $this->head['id_address_delivery'] != 0) ? true : false,
    ),
    array(
        "title" => "summary",
        "label" => "Riepilogo",
        "label2" => "Prima di procedere seleziona il metodo di spedizione",
        "class" => "last",
        "is_active" => ($this->controller->session->get('cart.set.shipping.' . $this->cartid, 0) == 0 && EOS\System\Util\ArrayHelper::getInt($shippingData, 'count', 1) == 2 && !in_array(42, $this->user->groups)) ? false : true,
    ),
);
/* } else {
    $cart_steps = array(
        array(
            "title" => "customer",
            "label" => "Cliente",
            "is_active" => true
        ),
        array(
            "title" => "address",
            "label" => "Indirizzo",
            "is_active" => (isset($this->customerinfo['name']) && $this->customerinfo['name'] != '') ? true : false,
        ),
        array(
            "title" => "payment",
            "label" => "Pagamento",
            "is_active" => (isset($this->head['id_address_delivery']) && $this->head['id_address_delivery'] != 0) ? true : false,
        ),
        array(
            "title" => "shipping",
            "label" => "Spedizione",
            "is_active" => (isset($option['head_payment']) && $option['head_payment'] != '') ? true : false,
        ),
        array(
            "title" => "cart",
            "label" => "Carrello",
            "is_active" => ($this->controller->session->get('cart.set.shipping.' . $this->cartid, 0) == 0 && EOS\System\Util\ArrayHelper::getInt($shippingData, 'count', 1) == 2) ? false : true,
        ),
        array(
            "title" => "summary",
            "label" => "Riepilogo",
            "class" => "last",
            "is_active" => ($this->controller->session->get('cart.set.calcfree.' . $this->cartid, 0) == 0 && $this->viewagent) ? false : true,
        ),
    );
} */
?>
<div class="container">
    <form class="main-cart-list cart-section" id="main-cart-list" method="POST">
        <input type="hidden" name="idcustomer" value="<?php echo $this->head['id_customer']; ?>">
        <input type="hidden" name="idaddressdelivery" value="<?php echo $this->head['id_address_delivery']; ?>">
        <input name="orderresid" id="orderresid" type="hidden" value="<?php echo ($this->orderresid); ?>">
        <input name="paymentdiscount" type="hidden" value="<?php echo $paymentdiscount; ?>">
        <input type="hidden" name="idagent" value="<?php echo $this->agentid; ?>">
        <?php $prevStatus = true; ?>
        <?php foreach ($cart_steps as $step) { ?>
            <div id="step-<?php echo $step['title']; ?>" class="single-step <?php echo ($step['is_active'] && $prevStatus) ? "active collapse-open" : ""; ?> <?php echo (isset($step['class'])) ? $step['class'] : ""; ?>">
                <div class=" section-title-box text-center">
                    <?php echo (!$step['is_active']) ? "<h6>" . $step['label2'] . "</h6>" : ""; ?>
                    <h3 class="section-title default-title step-title">
                        <?php echo $step['label']; ?>
                    </h3>
                </div>
                <div id="step-<?php echo $step['title']; ?>-collapse" class="collapse section-content <?php echo ($step['is_active'] && $prevStatus) ? "show" : ""; ?>">
                    <div class="step-content">
                        <?php $this->includePartial('step_' . $step['title']); ?>
                    </div>
                </div>
            </div>
            <?php
            if ($prevStatus != false) {
                $prevStatus = $step['is_active'];
            }
            ?>
        <?php } ?>
        <?php $this->writeTokenHtml(); ?>
        <input type="hidden" name="viewagent" value="<?php echo (isset($this->viewagent)) ? $this->viewagent : false; ?>">
        <input type="hidden" name="payoncheckout" value="<?php echo (isset($this->viewagent) && $this->viewagent == true) ? 0 : 1; ?>">
        <input type="hidden" value="savedata" name="command">
        <input type="hidden" name="cashback" value="<?php echo $this->cartcashback; ?>">
        <input type="hidden" value="1" name="status">
        <input type="hidden" value="<?php echo $this->cartid; ?>" name="cartid">
        <input type="hidden" name="consent1" value="true">
        <input type="hidden" value="<?php echo number_format($this->totalDisplay, 4, '.', ''); ?>" name="totalTaxable">
        <input type="hidden" value="<?php echo number_format($this->totalWithTax, 4, '.', ''); ?>" name="total">
    </form>
</div>