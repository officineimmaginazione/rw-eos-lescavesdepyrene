<div class="modal fade" id="shipping-alert-modal" tabindex="-1" role="dialog" aria-labelledby="shipping-alert-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Attenzione</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Non tutti prodotti ordinati sono al momento disponibili.
            </div>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="errorModal-qtamin" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Attenzione!<br />Non puoi inserire una quantità minore di 1!<br>Se eliminare il prodotto clicca su "rimuovi articolo"</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Attenzione!<br />Lo sconto massimo consentito è del 25%!</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="errorModal-qta" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Attenzione!<br />Massima quantità per vini in campionatura è 1!</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal" id="cashbackModal" tabindex="-1" role="dialog" aria-labelledby="cashbackModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cashback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-container add-to-cart" id="add-cashback" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <?php if ($this->viewagent) { ?>
                            <label for="total">In questo momento il cliente ha un totale di <b><?php echo $this->cashback; ?> &euro;</b> e ne può utilizzare <b><?php echo $this->max; ?> &euro;</b>. Inserisci il cashback che il cliente intende utilizzare</label>
                        <?php } else { ?>
                            <label for="total">In questo momento hai un totale di <b><?php echo $this->cashback; ?> &euro;</b> e ne puoi utilizzare <b><?php echo $this->max; ?> &euro;</b>. Inserisci il cashback che vuoi utilizzare</label>
                        <?php } ?>
                        <input type="number" class="form-control" id="cashback" name="cashback" aria-describedby="cashback" min="0" max="<?php echo $this->max; ?>" value="<?php echo $this->max; ?>">
                    </div>
                    <?php $this->writeTokenHtml(); ?>
                    <input type="hidden" value="addcashback" name="command">
                    <input type="hidden" value="<?php echo $this->cartid; ?>" name="cartid">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annulla</button>
                    <button type="button" class="btn btn-add-cashback btn-primary" data-product="1" id="add-1">Utilizza</button>
                </div>
            </form>
        </div>
    </div>
</div>