<div class="addresses-list cart-addresses shipping">
    <div class="row">
        <?php if (!empty($this->listaddresses)) { ?>
            <?php foreach ($this->listaddresses as $address) { ?>
                <?php $options = json_decode($address['options'], true); ?>
                <div class="col-12 col-sm-4 mb-3 single-address <?php echo ($address["id"] == $this->head['id_address_delivery']) ? 'selected' : ''; ?>">
                    <div class="d-flex flex-column content">
                        <?php echo ($address["id"] == $this->head['id_address_delivery']) ? '<h6>Indirizzo selezionato per la spedizione</h6>' : ''; ?>
                        <h5><?php echo $address['alias'] ?></h5>
                        <!-- <p class="name"><?php echo $address['name'] . ' ' . $address['surname'] ?></p> -->
                        <p class="address"><?php echo $address['address1'] ?></p>
                        <p class="city"><?php echo $address['cityname'] ?><?php echo ($address['address2'] != '') ? ' - ' . $address['address2'] : ''; ?> (<?php echo $address['state_code'] ?>) <?php echo $address['zip_code'] ?></p>
                        <p class="telephone"><?php echo $address['phone'] ?></p>
                        <p class="note"><?php echo $options['note'] ?></p>
                        <div class="btn-box mt-auto">
                            <button class="btn btn-info btn-sm btn-disable-address" id="disable-address-<?php echo $address["id"]; ?>" <?php echo ($address["id"] == $this->head['id_address_delivery']) ? 'disabled' : ''; ?>><i class="fa fa-map-marker-alt"></i> Disabilita indirizzo</button>
                            <button class="btn btn-info btn-sm btn-uses-address" id="uses-address-<?php echo $address["id"]; ?>" <?php echo ($address["id"] == $this->head['id_address_delivery']) ? 'disabled' : ''; ?>><i class="fa fa-map-marker"></i> Utilizza questo indirizzo</button>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="row mt-5">
        <div class="col-12 offset-md-3 col-md-6 offset-lg-4 col-lg-4">
            <a href="<?php echo $this->path->getUrlFor('Account', 'address/insert/'); ?>?fp=cart&customer=<?php echo $this->head['id_customer']; ?>" class="btn btn-primary"><i class="far fa-address-book"></i> Inserisci nuovo indirizzo</a>
        </div>
    </div>
</div>