<?php
$option = \EOS\System\Util\ArrayHelper::fromJSON($this->head['options']);
$headnotes = \EOS\System\Util\ArrayHelper::getStr($option, 'head_note');
$discount1 = 0;
$discount2 = 0;
$discount3 = 0;
if (isset($option['head_discount1']) && $option['head_discount1'] != 0) {
    $discount1 = $option['head_discount1'];
}
if (isset($option['head_discount2']) && $option['head_discount2'] != 0) {
    $discount2 = $option['head_discount2'];
}
if (isset($option['head_discount3']) && $option['head_discount3'] != 0) {
    $discount3 = $option['head_discount3'];
}
$paymentdiscount = 0;
if (isset($option['head_payment_discount']) && $option['head_payment_discount'] != 0) {
    $paymentdiscount = $option['head_payment_discount'];
}
$shippingData = \EOS\System\Util\ArrayHelper::getArray(\EOS\System\Util\ArrayHelper::fromJSON($this->head['options']), 'shipping');
?>
<div class="container cart-header">
    <div class="container">
        <?php if ($this->viewagent) { ?>
            <div class="row row-discount">
                <div class="col-12 col-md-6 col-discount text-center">
                    <h5>Sconto da applicare a tutte le righe</h5>
                    <input class="head-discount" min="0" value="<?php echo (!isset($headdiscount) || $headdiscount == "") ? 0 : $headdiscount; ?>" name="head_additional_account" type="number" />
                </div>
                <div class="col-12 col-md-6 text-center col-discount">
                    <h5>Sconto da anagrafica<br>(modificabile per il singolo ordine)</h5>
                    <div class="row">
                        <div class="col-6 col-md-4 offset-md-2 text-center">
                            <input class="discount-head" min="0" value="<?php echo ($discount1 != 0) ? $discount1 : 0; ?>" name="discount1" id="discount1" type="number" />
                        </div>
                        <div class="col-6 col-md-4 text-center">
                            <input class="discount-head" min="0" value="<?php echo ($discount2 != 0) ? $discount2 : 0; ?>" name="discount2" id="discount2" type="number" />
                        </div>
                        <input class="discount-head" min="0" value="<?php echo ($discount3 != 0) ? $discount3 : 0; ?>" name="discount3" id="discount3" type="hidden" />
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <input value="<?php echo ($discount1 != 0) ? $discount1 : 0; ?>" id="discount1" name="discount1" type="hidden" />
            <input value="<?php echo ($discount2 != 0) ? $discount2 : 0; ?>" id="discount2" name="discount2" type="hidden" />
            <input value="<?php echo ($discount3 != 0) ? $discount3 : 0; ?>" id="discount3" name="discount3" type="hidden" />
        <?php } ?>
        <div class="row mt-3">
            <div class="col-12 col-md-6 text-center">
                <?php if (!in_array(42, $this->user->groups)) { ?>
                    <h5>Note carrello</h5>
                <?php } else { ?>
                    <h5>Note ordine</h5>
                <?php } ?>
                <textarea class="head-note" name="head-note" style="width:75%" maxlength="200"><?php $this->printHtml($headnotes); ?></textarea>
            </div>
        </div>
    </div>
</div>
<div class="cart-section">
    <div class="container content-block">
        <div class="content-head">
            <div class="row">
                <div class="col-4 head-col">Prodotto</div>
                <div class="col-2 head-col">Prezzo<br>unitario</div>
                <div class="col-2 head-col">Prezzo<br>scontato</div>
                <?php echo ($this->viewagent) ? '<div class="col-1 head-col">Sconto</div>' : ''; ?>
                <div class="<?php echo ($this->viewagent) ? 'col-1' : 'col-2'; ?> head-col">Q.tà</div>
                <div class="col-2 head-col">Totale<br><?php echo ($this->viewagent) ? 'Tot. scontato' : ''; ?></div>
            </div>
        </div>
        <div class="cart-list">
            <?php
            if (isset($this->rows) && !empty($this->rows)) {
                for ($i = 0; $i < 3; $i++) {
                    $this->shippingIndex = $i;
                    $this->includePartial('table');
                }
            } else {
            ?>
                <h3 class="empty-cart">Il tuo carrello è vuoto!</h3>
            <?php } ?>
        </div>
        <div class="cart-footer" id="sec-freesample">
            <div class="row align-items-end col-right">
                <?php if (!in_array(42, $this->user->groups)) { ?>
                    <div class="col-6 col-md-3 final price">
                        N° PROD. SPED. 1
                        <p>
                            <span><?php $this->printHtml($this->numprodshipping1); ?></span>
                        </p>
                    </div>
                    <div class="col-6 col-md-3 final price">
                        N° PROD. SPED. 2
                        <p>
                            <span><?php $this->printHtml($this->numprodshipping2); ?></span>
                        </p>
                    </div>
                <?php } ?>
                <div class="col-6 <?php echo (!in_array(42, $this->user->groups)) ? 'col-md-3' : 'offset-md-6 col-md-3'; ?> final price">
                    NUMERO PRODOTTI
                    <p>
                        <span><?php $this->printHtml($this->numberproducts); ?></span>
                    </p>
                </div>
                <div class="col-6 col-md-3 final price">
                    TOTALE PRODOTTI
                    <p>
                        <span><?php (isset($this->subTotal) && $this->subTotal != '') ? $this->printHtml($this->currency->format($this->subTotal)) : 0; ?></span>
                    </p>
                </div>
            </div>
            <div class="row mt-3 btn-list-cart">
                <?php if (in_array(1, $this->user->groups)) { ?>
                    <div class="col-6 col-md-4 col-xl-2 cart-btn">
                        <a href="<?php echo $this->path->getUrlFor('Product', ''); ?>search/index/?category=0&price=0-10000&manufacturer=0&vintage=0&region=0&vineyards=0&country=0&typecondition=0" class="btn btn-primary">Continua gli acquisti</a>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="printcart" class="btn btn-primary print-btn">Stampa carrello</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="btn-calcfreerows" class="btn btn-primary">Calcola omaggi/sconti</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="btn-cashback" class="btn btn-primary">Utilizza cashback</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="deletecart" class="btn btn-primary delete-btn">Svuota carrello</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="resetcart" class="btn btn-primary reset-btn">Abbandona carrello</button>
                    </div>
                <?php } else if (in_array(2, $this->user->groups)) { ?>
                    <div class="col-6 col-md-4 col-xl-2 cart-btn">
                        <a href="<?php echo $this->path->getUrlFor('Product', ''); ?>search/index/?category=0&price=0-10000&manufacturer=0&vintage=0&region=0&vineyards=0&country=0&typecondition=0" class="btn btn-primary">Continua gli acquisti</a>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="printcart" class="btn btn-primary print-btn">Stampa carrello</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="btn-calcfreerows" class="btn btn-primary">Calcola omaggi/sconti</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="btn-cashback" class="btn btn-primary">Utilizza cashback</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="deletecart" class="btn btn-primary delete-btn">Svuota carrello</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="resetcart" class="btn btn-primary reset-btn">Abbandona carrello</button>
                    </div>
                <?php } else { ?>
                    <div class="col-6 col-md-4 col-xl-2 cart-btn">
                        <a href="<?php echo $this->path->getUrlFor('Product', ''); ?>search/index/?category=0&price=0-10000&manufacturer=0&vintage=0&region=0&vineyards=0&country=0&typecondition=0" class="btn btn-primary">Continua gli acquisti</a>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2 offset-xl-1">
                        <button id="printcart" class="btn btn-primary print-btn">Stampa carrello</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="btn-cashback" class="btn btn-primary">Utilizza cashback</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2 offset-xl-1">
                        <button id="deletecart" class="btn btn-primary delete-btn">Svuota carrello</button>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <button id="resetcart" class="btn btn-primary reset-btn">Abbandona carrello</button>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>