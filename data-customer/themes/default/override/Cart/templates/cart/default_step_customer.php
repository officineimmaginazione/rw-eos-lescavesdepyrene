<?php $cartDescription = (new \EOS\Components\Content\Widgets\SectionWidget($this, 'cart-description'))->data; ?>
<div class="container cart-addresses">
    <?php if ($this->viewagent) { ?>
        <div class="row">
            <div class="d-flex align-items-center col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4 user-box">
                <img class="user-img" src="data-customer/themes/default/img/user.png" alt="user" />
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="SELEZIONA CLIENTE" id="customer" aria-label="Cerca" name="customer_holder" aria-describedby="customer">
                    <input type="hidden" name="idagent" value="<?php echo $this->agentid; ?>">
                    <div class="input-group-prepend">
                        <button type="submit" class="btn btn-search search-submit"><img src="data-customer/themes/default/img/lent.png" alt="search" /></button>
                    </div>
                    <div class="input-group radioFilterBy">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="filterBy" id="filterBy1" value="0" <?php echo ($this->filterBy == 0) ? 'checked' : ''; ?>>
                            <label class="form-check-label" for="filterBy1">Rag. Soc.</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="filterBy" id="filterBy2" value="1" <?php echo ($this->filterBy == 1) ? 'checked' : ''; ?>>
                            <label class="form-check-label" for="filterBy2">P. Iva</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="filterBy" id="filterBy3" value="2" <?php echo ($this->filterBy == 2) ? 'checked' : ''; ?>>
                            <label class="form-check-label" for="filterBy3">Sede</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (isset($this->customerinfo['name']) && $this->customerinfo['name'] != '') { ?>
        <div class="row mt-5">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3  single-address">
                <div class="content">
                    <h5><?php echo $this->customerinfo['name'] ?></h5>
                    <h6>Dati amministrativi</h6>
                    <?php if ($this->customeroptions['account_type'] == 0) {
                        echo '<p><b></b>Cliente nuovo / S.r.l.s.</b></p>';
                    } else {
                        echo '<p><b>Cliente verificato</b></p>';
                    }
                    ?>
                    <p><?php echo (isset($this->customerinfo['vat']) && $this->customerinfo['vat'] != '') ? 'P. Iva: <b>' . $this->customerinfo['vat'] . '</b><br>' : ''; ?>C.F.: <b><?php echo $this->customerinfo['cif']; ?></b></p>
                    <h6>Dati commerciali</h6>
                    <?php
                    for ($i = 0; $i < $this->customerinfo['discount-count']; $i++) {
                        echo '<p class="discount">Sconto: <b>' . $this->customerinfo['discount-' . $i . '-name'] . '</b></p>';
                    }
                    ?>
                    <p class="payment">Pagamento: <b><?php echo $this->customerinfo['payment']; ?></b></p>
                    <?php if (isset($this->customerinfo['cscommonvalue']) && $this->customerinfo['cscommonvalue'] != '') {
                        if ($this->customerinfo['cscommonvalue'] <= '44') {
                            $class = 'danger';
                        } else if ($this->customerinfo['cscommonvalue'] > '45' && $this->customerinfo['cscommonvalue'] <= '60') {
                            $class = 'alert';
                        } else {
                            $class = 'success';
                        }
                        echo '<p class="csinfo">Rischio: <span class="' . $class . '">' . $this->customerinfo['cscommondescription'] . '</span> - Score:  <span class="' . $class . '">' . $this->customerinfo['cscommonvalue'] . '</span></p>';
                    }  ?>
                    <h6>Altre informazioni</h6>
                    <p class="email"><b><?php echo $this->customerinfo['email-alternative']; ?></b></p>
                    <p class="cashback">Cashback: <b><?php echo $this->cashback; ?> &euro;</b></p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<?php /*
<div class="btn-box text-center pb-5">
    <a class="btn btn-primary btn-big btn-next-step <?php echo (isset($this->customerinfo['name']) && $this->customerinfo['name'] != '') ? "" : "disabled"; ?>">
        Continua
    </a>
</div>
*/ ?>