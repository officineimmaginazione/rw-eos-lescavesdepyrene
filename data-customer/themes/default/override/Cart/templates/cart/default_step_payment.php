<?php
$option = \EOS\System\Util\ArrayHelper::fromJSON($this->head['options']);
$accounttype = \EOS\System\Util\ArrayHelper::getInt($this->customeroptions, 'account_type');
?>
<div class="container cart-pay-method">
    <div class="row pay-method-list">
        <div class="col-12 col-md-4 offset-md-4">
            <select id="payment" name="payment" class="form-control custom-select">
                <?php if (isset($this->discount['id-payment']) || isset($this->head['id_address_delivery'])) { ?>
                    <?php foreach ($this->payments as $payment) : ?>
                        <?php if (isset($option['head_payment']) && $option['head_payment'] != '' && $payment['id'] == $option['head_payment']) { ?>
                            <option value="<?php echo $payment['id']; ?>" data-payment-discount="<?php echo $payment['PASCONTO']; ?>" selected="selected"><?php echo $payment['PADESCRI']; ?></option>
                        <?php } else if ((!isset($option['head_payment']) || $option['head_payment'] == '') && $payment['id'] == $this->discount['id-payment']) { ?>
                            <option value="<?php echo $payment['id']; ?>" data-payment-discount="<?php echo $payment['PASCONTO']; ?>" selected="selected"><?php echo $payment['PADESCRI']; ?></option>
                        <?php } else if (isset($option['head_payment']) && isset($this->discount['id-payment']) && $payment['id'] == $this->discount['id-payment']) { ?>
                            <option value="<?php echo $payment['id']; ?>" data-payment-discount="<?php echo $payment['PASCONTO']; ?>"><?php echo $payment['PADESCRI']; ?></option>
                        <?php } else if ($accounttype == 0 && !in_array(42, $this->user->groups) && ((($payment['status'] == 1 && in_array(2, $this->user->groups))) || (($payment['status_b2b'] == 1 && in_array(1, $this->user->groups))))) { ?>
                            <option value="<?php echo $payment['id']; ?>" data-payment-discount="<?php echo $payment['PASCONTO']; ?>" disabled="disable"><?php echo $payment['PADESCRI']; ?></option>
                        <?php } else if (isset($this->user->groups) && (($payment['status'] == 1 && in_array(2, $this->user->groups))) || (($payment['status_b2b'] == 1 && in_array(1, $this->user->groups))) || (($payment['status_b2c'] == 1 && in_array(42, $this->user->groups)))) { ?>
                            <option value="<?php echo $payment['id']; ?>" data-payment-discount="<?php echo $payment['PASCONTO']; ?>"><?php echo $payment['PADESCRI']; ?></option>
                        <?php } else { ?>

                        <?php } ?>
                    <?php endforeach; ?>
                <?php } ?>
            </select>
        </div>
    </div>
</div>