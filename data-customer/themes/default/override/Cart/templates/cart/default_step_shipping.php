<div class="container cart-shipping" id="sec-shipping" name="sec-shipping">
    <?php
    $shippingData = \EOS\System\Util\ArrayHelper::getArray(\EOS\System\Util\ArrayHelper::fromJSON($this->head['options']), 'shipping');
    $shippingList = [];
    if (!in_array(42, $this->user->groups)) {
        $shippingList[0] = [
            'title' => 'Les Caves Express',
            'text' => 'La consegna avverrà tra le 24 e le 72 ore a partire dalla spedizione. Per le isole maggiori, entro le 96 ore. Per ordini superiori ai 350 € + iva la spedizione è gratuita. Per ordini inferiori ai 350 €, il costo di spedizione è 25 € + iva.',
            'price' => $this->controller->container->get('database')->setting->getStr('lcdp', 'shipping.price.standard', 15),
            'img' => 'data-customer/themes/default/img/delivery-fast.png'
        ];
        /*
        if ($this->showShppingExpress) {
            $shippingList[1] = [
                'title' => 'SPEDIZIONE EXPRESS',
                'text' => 'Tempi di consegna: 24/72 ore',
                'price' => $this->controller->container->get('database')->setting->getStr('lcdp', 'shipping.price.express', 35),
                'img' => 'data-customer/themes/default/img/fastdelivery.png'
            ];
        }
        */
    } else {
        $shippingList[0] = [
            'title' => 'SPEDIZIONE STANDARD',
            'text' => 'Tempi di consegna: 5 giorni lavorativi',
            'price' => $this->controller->container->get('database')->setting->getStr('lcdp', 'shipping.price.standardb2c', 9.5),
            'img' => 'data-customer/themes/default/img/delivery.png'
        ];
    }
    $shippingType = $this->showShppingExpress ? \EOS\System\Util\ArrayHelper::getInt($shippingData, 'type') : 0;
    $shipping = $shippingList[$shippingType];
    $shippingDate = \EOS\System\Util\ArrayHelper::getStr($shippingData, 'date');
    if (empty($shippingDate)) {
        $shippingDateLang = $this->lang->dateTimeToLangDate(\EOS\System\Util\DateTimeHelper::date());
    } else {
        $shippingDateLang = $this->lang->dbDateToLangDate($shippingDate);
    }
    $shippingMethod = \EOS\System\Util\ArrayHelper::getInt($shippingData, 'method', 0);
    ?>
    <input id="shipping-date" type="hidden" value="<?php echo $shippingDateLang ?>">
    <input id="shipping-type" type="hidden" value="<?php echo $shippingType; ?>">
    <input id="shipping-method" type="hidden" value="<?php echo $shippingMethod; ?>">
    <div class="container shipping">
        <div class="row mt-3">
            <div class="col-md-12 mb-4">
                <h6>
                    Spedizione il
                    <span id="shipping-text-date"></span>
                </h6>
                <h6>
                    <span id="shipping-text-method"></span>
                </h6>
                <?php if ($this->shippingCount > 1) { ?>
                    <h6>
                        <span id="shipping-text-count">(Previste <?php $this->printHtml($this->shippingCount); ?> spedizioni)</span>
                    </h6>
                <?php } ?>
            </div>
            <div class="col-12 mb-4">
                <div class="row single-shipping shipping-active">
                    <div class="d-none">
                        <input type="hidden" value="<?php $this->printHtml($shipping['price']); ?>">
                    </div>
                    <div class="d-flex align-items-center col-12 col-sm-3 col-lg-2 delivery-img">
                        <img class="img-fluid" src="<?php $this->printHtml($shipping['img']); ?>" alt="Shipping" />
                    </div>
                    <div class="d-flex justify-content-center flex-column col-12 col-sm-8 offset-lg-1 text">
                        <h6><?php $this->printHtml($shipping['title']); ?></h6>
                        <p><?php $this->printHtml($shipping['text']); ?></p>
                    </div>
                    <!--
                    <div class="d-flex align-items-center text-right col-12 col-sm-4 offset-lg-1 col-lg-3 shipping-price">
                        <p><?php $this->printHtml($this->currency->format($shipping['price']));
                            if (!in_array(42, $this->user->groups)) {
                                echo ($shippingMethod == 0) ? ' (porto franco ' . $this->controller->container->get('database')->setting->getStr('lcdp', 'shipping.freeport', 250) . ' €)' : '';
                            } else {
                                echo ($shippingMethod == 0) ? ' (gratuita a partire da ' . $this->controller->container->get('database')->setting->getStr('lcdp', 'shipping.freeportb2c', 90) . ' €)' : '';
                            }
                            ?>
                        </p>
                    </div>
                        -->
                </div>
            </div>
            <?php if (!in_array(42, $this->user->groups)) { ?>
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#shipping-modal" data-backdrop="static" data-keyboard="false">
                        <i class="far fa-edit"></i> Modifica
                    </button>
                </div>
            <?php } else { ?>
            <?php } ?>
        </div>
    </div>

    <div class="modal fade" id="shipping-modal" tabindex="-1" role="dialog" aria-labelledby="shipping-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Spedizione</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body">
                    <?php if ($this->viewagent) { ?>
                        <div class="form-group">
                            <label>Data di spedizione</label>
                            <div class="input-group">
                                <input type="text" name="date" id="shipping-modal-date" class="form-control" maxlength="10">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon"><img src="data-customer/themes/default/img/calendar.png" alt="date-request" /></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tipologia consegna</label>
                            <div class="input-group">
                                <select id="shipping-modal-method" class="form-control custom-select">
                                    <option value="0">Consegna subito la merce disponibile e attendi i prodotti non disponibili.</option>
                                    <?php if ($this->numprodshipping2 > 0 || ($this->numprodshipping2 > 0 && $this->numprodshipping1 > 0)) { ?>
                                        <option value="1">Consegna l'ordine all'arrivo dell'ultimo prodotto</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    <?php } else { ?>
                        <input hidden id="shipping-modal-method" value="0" />
                        <input hidden name="date" id="shipping-modal-date" value="<?php echo date('d') . '/' . date('m') . '/' . date('Y'); ?>" />
                    <?php } ?>
                    <div class="shipping-radio-list">
                        <?php foreach ($shippingList as $k => $shipping) { ?>
                            <?php $checked = $shippingType === $k ? ' checked' : ''; ?>
                            <?php /*
                            <div class="row single-shipping single-shipping-item">
                                <div class="d-flex align-items-center col-sm-1">
                                    <input class="form-check-input" type="radio" name="deliveryRadios" value="<?php $this->printHtml($shipping['price']); ?>" <?php echo $checked; ?> data-id="<?php echo $k; ?>">
                                </div>
                                <div class="d-flex align-items-center offset-1 col-1 delivery-img">
                                    <img src="<?php $this->printHtml($shipping['img']); ?>" alt="" />
                                </div>
                                <div class="d-flex justify-content-center flex-column offset-sm-1 col-sm-5 text">
                                    <h6><?php $this->printHtml($shipping['title']); ?></h6>
                                    <p><?php $this->printHtml($shipping['text']); ?></p>
                                </div>
                                <div class="d-flex align-items-center offset-1 col-2">
                                    <p><?php $this->printHtml($this->currency->format($shipping['price'])); ?></p>
                                </div>
                            </div>
                            */ ?>
                            <label class="row single-shipping single-shipping-item">
                                <div class="radio-box">
                                    <input class="form-check-input" type="radio" name="deliveryRadios" value="<?php $this->printHtml($shipping['price']); ?>" <?php echo $checked; ?> data-id="<?php echo $k; ?>">
                                </div>
                                <div class="info-box">
                                    <div class="delivery-img">
                                        <img class="img-fluid" alt="<?php echo $this->printHtml($shipping['title']); ?>" src="<?php $this->printHtml($shipping['img']); ?>" />
                                    </div>
                                    <div class="text">
                                        <h6>
                                            <?php $this->printHtml($shipping['title']); ?>
                                        </h6>
                                        <p>
                                            <?php $this->printHtml($shipping['text']); ?>
                                        </p>
                                    </div>
                                    <div class="delivery-price">
                                        <p>
                                            <?php $this->printHtml($this->currency->format($shipping['price'])); ?>
                                        </p>
                                    </div>
                                </div>
                            </label>
                        <?php } ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-light" data-dismiss="modal">Annulla</button> -->
                    <button type="button" class="btn btn-primary">Salva</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->startCaptureScript(); ?>
<script>
    $(function() {
        $('#shipping-modal .btn-primary').on('click', function() {
            var type = $("#shipping-modal .single-shipping-item input:checked").attr('data-id');
            var date = $("#shipping-modal-date").val();
            var method = $('#shipping-modal-method').val();
            var data = {
                'command': "updateshipping",
                'id': <?php echo $this->cartid; ?>,
                'params': {
                    'type': type,
                    'date': date,
                    'method': method
                },
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            runAjax(data, function() {
                window.location.reload(false);
            });
        });

        $('#shipping-modal-date').val('<?php echo $shippingDateLang; ?>');
        var optionsDate = {
            language: "<?php echo $this->lang->getCurrentISO(); ?>",
            format: "<?php echo $this->lang->getCurrentDatePickerFormat(); ?>",
            todayHighlight: true,
            autoclose: true,
            startDate: new Date('<?php echo \EOS\System\Util\DateTimeHelper::formatATOM(\EOS\System\Util\DateTimeHelper::date()); ?>'),
            endDate: new Date('<?php echo \EOS\System\Util\DateTimeHelper::formatATOM(EOS\System\Util\DateTimeHelper::incDay(\EOS\System\Util\DateTimeHelper::date(), 60)); ?>')
        };
        $("#shipping-modal-date").datepicker(optionsDate);

        $('#shipping-text-date').text($("#shipping-modal-date").val());
        $('#shipping-modal-method').val($('#shipping-method').val());
        $('#shipping-text-method').html($("#shipping-modal-method option:selected").html());

        var loadModal = function() {

        };

        $('#shipping-modal').on('show.bs.modal', function() {
            $('#shipping-modal-date').val($('#shipping-date').val());
            $('#shipping-modal-method').val($('#shipping-method').val());
            $('#shipping-moda .single-shipping-item').each(function() {
                if ($(this).val() === $('#shipping-method').val()) {
                    $(this).trigger('click');
                }
            });
        });
    });

    <?php if ($this->shippingCount > 1) { ?>
        $(window).on('load', function() {
            $('#shipping-alert-modal').modal('show');
        });
    <?php } ?>
</script>
<?php $this->endCaptureScript();  ?>