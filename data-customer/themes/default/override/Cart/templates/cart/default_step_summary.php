<?php $shippingData = \EOS\System\Util\ArrayHelper::getArray(\EOS\System\Util\ArrayHelper::fromJSON($this->head['options']), 'shipping'); ?>
<div class="container cart-final">
    <?php if (!in_array(42, $this->user->groups)) { ?>
        <div class="row ship-resume-total">
            <div class="col-12 col-md-6 col-left">
                <?php if ($this->numprodshipping1 > 0) { ?>
                    <h4>Spedizione</h4>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">Totale prodotti</div>
                        <div class="col-6 price cart-noiva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->prodtotalshipping1)); ?>
                            </span>
                        </div>
                    </div>
                    <?php if ($this->discounttotal1 != 0) { ?>
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-end col-6 title">Sconto Q.tà (<?php echo $this->discountpercshipping1; ?> &percnt;)</div>
                            <div class="col-6 price shipping-total">
                                <span>
                                    <?php $this->printHtml($this->currency->format((float) $this->discounttotal1)); ?>
                                </span>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($this->discountpayment1 != 0) { ?>
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-end col-6 title">Sconto Pagamento</div>
                            <div class="col-6 price shipping-total">
                                <span>
                                    <?php $this->printHtml($this->currency->format((float) $this->discountpayment1)); ?>
                                </span>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">Costo di spedizione</div>
                        <div class="col-6 price cart-noiva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->shippingPrices[0])); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">Totale imponibile</div>
                        <div class="col-6 price cart-noiva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->totalDisplay1)); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">IVA</div>
                        <div class="col-6 price cart-iva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->totalTax1)); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">TOTALE</div>
                        <div class="col-6 final price">
                            <p>
                                <span>
                                    <?php $this->printHtml($this->currency->format((float) $this->totalWithTax1)); ?>
                                </span>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-12 col-md-6 col-right">
                <?php if ($this->numprodshipping2 > 0) { ?>
                    <h4>Spedizione 2</h4>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">Totale prodotti</div>
                        <div class="col-6 price cart-noiva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->prodtotalshipping2)); ?>
                            </span>
                        </div>
                    </div>
                    <?php if ($this->discounttotal2 != 0) { ?>
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-end col-6 title">Sconto Q.tà (<?php echo $this->discountpercshipping2; ?> &percnt;)</div>
                            <div class="col-6 price shipping-total">
                                <span>
                                    <?php $this->printHtml($this->currency->format((float) $this->discounttotal2)); ?>
                                </span>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($this->discountpayment2 != 0) { ?>
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-end col-6 title">Sconto Pagamento</div>
                            <div class="col-6 price shipping-total">
                                <span>
                                    <?php $this->printHtml($this->currency->format((float) $this->discountpayment2)); ?>
                                </span>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">Costo di spedizione</div>
                        <div class="col-6 price cart-noiva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->shippingPrices[1])); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">Totale imponibile</div>
                        <div class="col-6 price cart-noiva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->totalDisplay2)); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">IVA</div>
                        <div class="col-6 price cart-iva">
                            <span>
                                <?php $this->printHtml($this->currency->format((float) $this->totalTax2)); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-flex align-items-center justify-content-end col-6 title">TOTALE</div>
                        <div class="col-6 final price">
                            <p>
                                <span>
                                    <?php $this->printHtml($this->currency->format((float) $this->totalWithTax2)); ?>
                                </span>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <h4 class="title-resume">Totale ordine</h4>
        <div class="col-12 col-md-6 col-left">

        </div>
        <div class="col-12 col-md-6 col-right">
            <div class="row">
                <div class="d-flex align-items-center justify-content-end col-6 title">Totale prodotti</div>
                <div class="col-6 price cart-noiva">
                    <span>
                        <?php $this->printHtml($this->currency->format((float) $this->productstotal)); ?>
                    </span>
                </div>
            </div>
            <?php if (!in_array(42, $this->user->groups)) { ?>
                <div class="row">
                    <div class="d-flex align-items-center justify-content-end col-6 title">Sconto Pagamento</div>
                    <div class="col-6 price shipping-total">
                        <span>
                            <?php $this->printHtml($this->currency->format((float) $this->discountpayment)); ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
            <?php if (!in_array(42, $this->user->groups)) { ?>
                <!-- <div class="row">
                    <div class="d-flex align-items-center justify-content-end col-6 title">Sconto Totale</div>
                    <div class="col-6 price shipping-total">
                        <span>
                            <?php $this->printHtml($this->currency->format((float) $this->discounttotal)); ?>
                        </span>
                    </div>
                </div> -->

            <?php } ?>
            <?php if ($this->discountqty != 0) { ?>
                <div class="row">
                    <div class="d-flex align-items-center justify-content-end col-6 title">Sconto Q.ta</div>
                    <div class="col-6 price shipping-total">
                        <span>
                            <?php $this->printHtml($this->currency->format((float) $this->discountqty)); ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->cartcashback != 0) { ?>
                <div class="row">
                    <div class="d-flex align-items-center justify-content-end col-6 title">Cashback</div>
                    <div class="col-6 price shipping-total">
                        <span>
                            <?php $this->printHtml($this->currency->format((float) $this->cartcashback)); ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="d-flex align-items-center justify-content-end col-6 title">Costo di spedizione</div>
                <div class="col-6 price shipping-total">
                    <span>
                        <?php $this->printHtml($this->currency->format((float) $this->shipping)); ?>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="d-flex align-items-center justify-content-end col-6 title">Totale imponibile</div>
                <div class="col-6 price cart-noiva">
                    <span>
                        <?php $this->printHtml($this->currency->format((float) $this->totalDisplay)); ?>
                    </span>
                </div>
            </div>
            <?php if (!in_array(42, $this->user->groups)) { ?>
                <div class="row">
                    <div class="d-flex align-items-center justify-content-end col-6 title">IVA</div>
                    <div class="col-6 price cart-iva">
                        <span>
                            <?php $this->printHtml($this->currency->format((float) $this->totalTax)); ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="d-flex align-items-center justify-content-end col-6 title">TOTALE</div>
                <div class="col-6 final price">
                    <p>
                        <span>
                            <?php $this->printHtml($this->currency->format((float) $this->totalWithTax)); ?>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="sec-buy">
        <div class="col-12 col-lg-6 order-2 order-lg-1 pt-5 pt-lg-0 d-flex align-items-end col-left">
            <?php echo (!in_array(42, $this->user->groups)) ? '<button class="btn btn-primary btn-scroll" data-scrollto="freesample">Calcola omaggi/sconti</button>' : ''; ?>
        </div>
        <div class="col-12 col-lg-6 order-1 order-lg-2  col-right">
            <div class="btn-buy">
                <?php
                $agentoptions = json_decode($this->accountinfo['options'], true);
                if ($this->lockConfirm) { ?>
                    <label class="text-danger">Acquisto bloccato per problemi di disponibilità</label>
                <?php } elseif (isset($agentoptions['budget']) && $agentoptions['budget'] < $this->productsissampletotal && $this->viewagent) { ?>
                    <label class="text-danger">Acquisto bloccato - La merce in campionatura supera il budget</label>
                <?php } elseif ($this->controller->session->get('cart.set.shipping.' . $this->cartid, 0) == 0 && EOS\System\Util\ArrayHelper::getInt($shippingData, 'count', 1) == 2) { ?>
                    <label class="text-danger">Acquisto bloccato - Convalida la modalità di spedizione</label>
                <?php } elseif ($this->controller->session->get('cart.set.calcfree.' . $this->cartid, 0) == 0 && $this->viewagent) { ?>
                    <label class="text-danger">Acquisto bloccato - Clicca su "Calcola Omaggi" per proseguire</label>
                <?php } elseif ($this->controller->session->get('cart.set.address.' . $this->cartid, 0) == 0) { ?>
                    <label class="text-danger">Acquisto bloccato - Seleziona l'indirizzo a cui spedire la merce</label>
                <?php } elseif ($this->numberproducts == 0) { ?>
                    <label class="text-danger">Acquisto bloccato - Non sono presenti articolo nel carrello</label>
                <?php } else { ?>
                    <button id="cart-confirm">ACQUISTA</button>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <a href="<?php echo $this->path->getUrlFor('Product', ''); ?>search/index/?category=0&price=0-10000&manufacturer=0&vintage=0&region=0&vineyards=0&country=0&typecondition=0" class="btn btn-primary" id="return-shipping"><i class="far fa-arrow-alt-circle-left"></i> Continua gli acquisti</a>
        </div>
    </div>
</div>