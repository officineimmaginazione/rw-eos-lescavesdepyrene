<?php
$firstround = true;
foreach ($this->rows as &$row) {
    //print_r($row);
    $option = EOS\System\Util\ArrayHelper::fromJSON($row['options']);
    $isFreeRow = EOS\System\Util\ArrayHelper::getMdBool($option, ['data', 'isfree']);
    $isSample = EOS\System\Util\ArrayHelper::getMdBool($option, ['data', 'issample']);
    $changeProduct = EOS\System\Util\ArrayHelper::getMdBool($option, ['data', 'change-product']);
    $classError = $row['quantity-error'] ? 'text-danger' : '';
    $addCssClass = '';
    if ($isFreeRow) {
        $catalog = new \EOS\Components\Product\Models\CatalogModel($this->controller->container);
        $productsbyprice = $catalog->getProductsChangeFreeByPrice($row['price-prod']);
        $addCssClass .= ' free-product';
    }
    $stockStatus = $row['stock-status'];

    $visible = false;
    if ($this->shippingIndex == 0 && $stockStatus['quantity1'] > 0 && $isFreeRow != 1) {
        $visible = true;
        //} elseif ($this->shippingIndex == 1 && ($stockStatus['quantity2'] > 0 || $stockStatus['quantity3'] > 0) && $stockStatus['quantity1'] == 0 && $isFreeRow != 1) {
    } elseif ($this->shippingIndex == 1 && $stockStatus['quantity1'] == 0 && $isFreeRow != 1) {
        $visible = true;
    } elseif ($this->shippingIndex == 2 && !isset($row['__printed'])) {
        $visible = true;
    }

    if ($this->shippingIndex == 0 && $visible == true && $firstround == true && $isFreeRow != 1 && !in_array(42, $this->user->groups)) {
        echo '<h4>Spedizione 1</h4>';
        $firstround = false;
    }
    if ($this->shippingIndex == 1 && $visible == true && $firstround == true && $isFreeRow != 1 && !in_array(42, $this->user->groups)) {
        echo '<h4>Spedizione 2</h4>';
        $firstround = false;
    }

    if ($this->shippingIndex == 2 && $visible == true && $firstround == true && !in_array(42, $this->user->groups)) {
        echo '<h4>Eventuali sconti/omaggi</h4>';
        $firstround = false;
    }

    $isSampleDiscount = false;
    if ($row['type_condition'] == 1) {
        $isSampleDiscount = true;
    }

    if ($visible) {
        $row['__printed'] = true;
        $freediscountquantity = 0;
        if (isset($option['data']['free-row-discount'][0]) && $option['data']['free-row-discount'][0] > 0) {
            $freediscountquantity = $option['data']['free-row-discount'][0];
        }
?>
        <div class="row single-product<?php echo $addCssClass; ?>" data-id="<?php echo $row['id']; ?>">
            <?php
            ?>
            <div class="d-flex justify-content-center flex-column col-12 product-inner">
                <div class="row top-info">
                    <div class="col-12 col-lg-4 pro-info">
                        <a href="/it/product/catalog/item/<?php echo $row['id_product']; ?>/" class="product-link">
                            <h5 class="pro-name"><?php echo $row['name']; ?></h5>
                        </a>
                        <h6><?php echo (isset($row['manufacturer']) && $row['manufacturer'] != '') ? $row['manufacturer'] : ""; ?></h6>
                        <h6><?php echo (isset($row['vintage']) && $row['vintage'] != '') ? $row['vintage'] : ""; ?></h6>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-2 d-flex align-items-center pro-single-price">
                        <?php
                        if (!$isFreeRow) {
                        ?>
                            <p><span class="d-lg-none">Prezzo unitario</span> <?php $this->printHtml($this->currency->format($row['price'])); ?></p>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-2 d-flex align-items-center pro-discounted-price">
                        <?php
                        if (!$isFreeRow) {
                        ?>
                            <p><span class="d-lg-none">Prezzo scontato</span> &euro; <?php echo number_format($row['price-disc'], 2, ',', '.'); ?></p>
                        <?php
                        } else {
                        ?>
                            <p>OMAGGIO</p>
                        <?php
                        }
                        ?>
                    </div>
                    <?php if ($this->viewagent) { ?>
                        <div class="col-12 col-sm-6 col-lg-1 d-flex align-items-center pro-discount <?php echo ($isSample == 1) ? "is_sample" : ''; ?>">
                            <?php
                            if (!$isFreeRow) { ?>
                                <h6 class="d-lg-none">Sconto</h6>
                                <input id="discount<?php echo $row['id']; ?>" data-rowId="<?php echo $row['id']; ?>" data-rowIdProduct="<?php echo $row['id_product']; ?>" class="additional-discount<?php echo $isSampleDiscount ? ' disable' : '' ?>" min="0" max="25" name="additional-discount" type="number" value="<?php echo $row['discount']; ?>" <?php echo $isSampleDiscount ? 'readonly="readonly"' : '' ?> />
                            <?php } ?>
                            <?php if ($freediscountquantity > 0) { ?>
                                <div class="input-group-append">
                                    <span class="input-group-text">- <?php echo $freediscountquantity ?></span>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <input hidden id="discount<?php echo $row['id']; ?>" data-rowId="<?php echo $row['id']; ?>" data-rowIdProduct="<?php echo $row['id_product']; ?>" class="additional-discount<?php echo $isSampleDiscount ? ' disable' : '' ?>" min="0" max="25" name="additional-discount" type="number" value="0" />
                    <?php } ?>

                    <div class="col-12 col-sm-6 <?php echo ($this->viewagent) ? 'col-lg-1' : 'col-lg-2'; ?> d-flex align-items-center pro-quantity">
                        <h6 class="d-lg-none">Quantità</h6>
                        <input id="qta<?php echo $row['id']; ?>" data-rowId="<?php echo $row['id']; ?>" data-rowIdProduct="<?php echo $row['id_product']; ?>" min="1" type="number" value="<?php echo $row['quantity']; ?>" <?php echo $isFreeRow ? 'readonly' : '' ?> class="<?php echo $classError; ?>">
                    </div>
                    <div class="col-12 col-lg-2 d-flex align-items-center pro-total">
                        <div class="row">
                            <?php echo $row['show-disc'] == false ? '' : '<div class="col-12"><p><span class="d-lg-none">Totale scontato</span> <span style="text-decoration: line-through;">' . $this->currency->format((float) $row['tot-price']) . '</span></p></div>'; ?>
                            <div class="col-12">
                                <p><span class="d-lg-none">Totale</span><span id="def-price"> <?php $this->printHtml($this->currency->format((float) $row['tot-price-disc'])); ?></span></p>
                            </div>
                        </div>

                    </div>

                    <?php if ($isFreeRow && $changeProduct && ($row['quantity'] < 6)) {
                    ?>
                        <div class="col-12">
                            <p class="chk-free-row-msg">Prima di confermare l'ordine verificare il numero di bottiglie in omaggio.</p>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="row stock-row">
                    <div class="col-6 stock-number">
                        <div class="stock"><span><?php $this->printHtml($row['stock']['stock-quantity-str']); ?></span></div>
                        <div class="incoming"> <span><?php $this->printHtml($row['stock']['stock-incoming-str']); ?></span> </div>
                    </div>
                    <div class="col-6 stock-status">
                        <?php
                        foreach ($row['stock-status']['quantity-str-list'] as $str) {
                        ?>
                            <div><span><?php $this->printHtml($str); ?></span></div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="row bottom-info">
                    <?php
                    if (array_key_exists("issample", $option['data'])) {
                        $issample = $option['data']['issample'];
                    } else {
                        $issample = 0;
                    }
                    if (!$isFreeRow) {
                    ?>
                        <div class="col-12 col-sm-6 pro-check">
                            <div class="form-check <?php echo ($issample != 0) ? "checked" : ""; ?>">
                                <?php
                                $agentoptions = json_decode($this->accountinfo['options'], true);
                                if ($agentoptions['budget'] > $row['price']) { ?>
                                    <input id="chk<?php echo $row['id']; ?>" data-rowId="<?php echo $row['id']; ?>" data-rowIdProduct="<?php echo $row['id_product']; ?>" type="checkbox" class="form-check-input">
                                    <label class="form-check-label">di cui n.1 prodotto in campionatura</label>
                                <?php } else { ?>
                                    <input type="hidden" id="chk<?php echo $row['id']; ?>" data-rowId="<?php echo $row['id']; ?>" data-rowIdProduct="<?php echo $row['id_product']; ?>" value="false">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 mt-4 mt-sm-0 text-center text-sm-right pro-remove">
                            <button class="delete" id="delete<?php echo $row['id']; ?>">rimuovi articolo</button>
                        </div>
                        <?php
                    } else {
                        if ($changeProduct) {
                        ?>
                            <div class="d-flex bottom-info-content">
                                <h6>Prodotti alternativi</h6>
                                <select class="custom-select updatefreerow">
                                    <option value="">(Seleziona un prodotto)</>
                                        <?php
                                        foreach ($productsbyprice as $prod) {
                                        ?>
                                    <option value="<?php echo $prod['id']; ?>"><?php $this->printHtml($prod['name']); ?> - <?php $this->printHtml($prod['manufacturer']); ?> - <?php $this->printHtml($prod['vintage']); ?></option>
                                <?php } ?>
                                </select>
                            </div>
                            <button class="btn btn-primary btn-updatefreerow">cambia</button>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
<?php }
}
unset($row);
