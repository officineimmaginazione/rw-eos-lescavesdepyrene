<section class="cart-details">
    <div class="overflow"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Carrello n° <?php echo $this->data['id']; ?></h2>
                <h4><?php echo $this->data['company']; ?></h4>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-sm-6">
                <h5>Riepilogo cliente</h5>
                <p>Riferimento: <?php echo $this->data['name']; ?> <?php echo $this->data['surname']; ?></p>
                <p>Indirizzo: <?php echo $this->data['address']; ?> - <?php echo $this->data['city']; ?> (<?php echo $this->data['state']; ?>) - <?php echo $this->data['country']; ?></p>
                <p>Tel.: <?php echo $this->data['phone']; ?></p>
                <p>Email: <?php echo $this->data['email']; ?></p>
                <p>P. Iva: <?php echo $this->data['vat']; ?> - C.F.: <?php echo $this->data['cif']; ?></p>
                <p>Note: <?php echo $this->data['note']; ?></p>
                <p>Agente: <?php echo $this->data['agent-name']; ?></p>
            </div>
            <div class="col-12 col-sm-6">
                <h5>Riepilogo carrello</h5>
                <p>Sconto cliente: <?php echo ($this->data['head_discount1'] > 0) ? number_format($this->data['head_discount1'], 0) : ''; ?><?php echo ($this->data['head_discount2'] > 0) ? ' + ' . number_format($this->data['head_discount2'], 0) : ''; ?><?php echo ($this->data['head_discount3'] > 0) ? ' + ' . $this->data['head_discount3'] : ''; ?></p>
                <p>Sconto pagamento: <?php echo ($this->data['head_payment_discount'] > 0) ? number_format($this->data['head_payment_discount'], 0) : ''; ?></p>
                <p>Note: <?php echo $this->data['head_note']; ?></p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-sm-6">
                <a class="btn btn-primary btn-print" onclick="event.preventDefault(); window.print();" id="download-attachment" title="Stampa">Stampa</a>
            </div>
        </div>
        <?php
        echo '<div class="row mt-3">';
        echo '<div class="col-12">';
        echo '<div class="box">';
        echo '<div class="box-header with-border">';
        echo '<h3 class="box-title">' . $this->transE('cart.details.products') . '</h3>';
        echo '<div class="pull-right box-tools">';
        echo '</div>';
        echo '</div>';
        echo '<div class="box-body no-padding">';
        echo '<table class="table table-striped">';
        echo '<tr>';
        echo '<th style="width: 10px">#</th>';
        echo '<th>' . $this->transE('cart.details.id') . '</th>';
        echo '<th>' . $this->transE('cart.details.product') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.price') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.pricediscount') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.quantity') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.totrow') . '</th>';
        echo '</tr>';
        $idgroup = 0;
        $total = 0;
        $totqty = 0;
        $taxtotalproduct = 0;
        if (count($this->datarow)) {
            foreach ($this->datarow as $row) {
                $options = json_decode($row['options'], true);
                $adddiscount = 0;
                $price = 0;
                $idgroup = $row['idgroup'];
                if (isset($options['data']['additional-discount']) && $options['data']['additional-discount'] > 0) {
                    $adddiscount = $options['data']['additional-discount'];
                }
                if ($row['idgroup'] == 42) {
                    $price = $row['price'] / (1 + ($row['rate'] / 100));
                    //$pricedisc = round(($row['price'] * ((1 - $this->data['head_discount1'] / 100) * (1 - $this->data['head_discount2'] / 100) * (1 - $this->data['head_discount3'] / 100))) * (1 - $adddiscount / 100), 4);
                    $taxrow = (($row['quantity']) * $price) * ($row['rate'] / 100);
                    $taxtotalproduct += $taxrow;
                    $total = $total + ($row['quantity'] * $price);
                    $totqty = $totqty + $row['quantity'];
                    echo '<tr>';
                    echo '<td>' . $row['id_product'] . '</td>';
                    echo '<td>' . $row['reference'] . '</td>';
                    echo '<td>' . $row['name'] . '</td>';
                    echo '<td class="text-right">' . number_format($row['price'], 2, ',', ' ') . ' &euro;</td>';
                    echo '<td class="text-right">' . number_format($row['price'], 2, ',', ' ') . ' &euro;</td>';
                    echo '<td class="text-right">' . $row['quantity'] . '</td>';
                    echo '<td class="text-right">' . number_format(($row['quantity'] * $row['price']), 2, ',', ' ') . ' &euro;</td>';
                    echo '<tr>';
                } else {
                    $price = $row['price'];
                    $pricedisc = round(($price * ((1 - $this->data['head_discount1'] / 100) * (1 - $this->data['head_discount2'] / 100) * (1 - $this->data['head_discount3'] / 100))) * (1 - $adddiscount / 100), 4);
                    $taxrow = (($row['quantity']) * $pricedisc) * ($row['rate'] / 100);
                    $taxtotalproduct += $taxrow;
                    $total = $total + ($row['quantity'] * $pricedisc);
                    $totqty = $totqty + $row['quantity'];
                    echo '<tr>';
                    echo '<td>' . $row['id_product'] . '</td>';
                    echo '<td>' . $row['reference'] . '</td>';
                    echo '<td>' . $row['name'] . '</td>';
                    echo '<td class="text-right">' . number_format($price, 2, ',', ' ') . ' &euro;</td>';
                    echo '<td class="text-right">' . number_format($pricedisc, 2, ',', ' ') . ' &euro;</td>';
                    echo '<td class="text-right">' . $row['quantity'] . '</td>';
                    echo '<td class="text-right">' . number_format(($row['quantity'] * $pricedisc), 2, ',', ' ') . ' &euro;</td>';
                    echo '<tr>';
                }
            }
        }

        if (!$this->viewagent && isset($this->data['cashback']) && $this->data['cashback'] != 0) {
            $total = $total + ($this->data['cashback']);
        } else {
            $total = $total - $this->data['discount-shipping1'];
        }

        if ($idgroup == 42) {
            $shipping = $this->data['shipping_payment'] / (1 + ($row['rate'] / 100));
            $taxshipping = $this->data['shipping_payment'] - $shipping;
        } else {
            $shipping = $this->data['shipping_payment'];
            $taxshipping = ($this->data['shipping_payment'] * 1.22) - $this->data['shipping_payment'];
        }

        $headpaymentdiscount = 0;
        if (isset($this->data['head_payment_discount']) && $this->data['head_payment_discount'] > 0) {
            $headpaymentdiscount = $this->data['head_payment_discount'];
        }

        $totalDisplay = round(($total * (1 - $headpaymentdiscount / 100)), 4) + $shipping;
        $paymentDiscount = round($total - ($total * (1 - $headpaymentdiscount / 100)), 4);

        $totalTax = round(($taxtotalproduct + $taxshipping), 4);
        $totalWithTax = round(($total * (1 - $headpaymentdiscount / 100)) + $shipping + $totalTax, 4);
        if (!$this->viewagent && isset($this->data['cashback']) && $this->data['cashback'] != 0) {
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Cashback:</td><td class="text-right"><b>' . number_format(($this->data['cashback']), 2, ',', ' ') . ' &euro;</b></td></tr>';
        }
        if ($idgroup == 42) {
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Spedizione:</td><td class="text-right"><b>' . number_format(($this->data['shipping_payment']), 2, ',', ' ') . ' &euro;</b></td></tr>';
        } else {
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Tot. Prod.:<b>' . number_format($totqty, 0, ',', ' ') . '</b></td><td class="text-right"><b>' . number_format(($total), 2, ',', ' ') . ' &euro;</b></td></tr>';
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Sconto Qta:</td><td class="text-right"><b>' . number_format(($this->data['discount-shipping1']), 2, ',', ' ') . ' &euro;</b></td></tr>';
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Sconto pagamento:</td><td class="text-right"><b>' . number_format(($paymentDiscount), 2, ',', ' ') . ' &euro;</b></td></tr>';
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Spedizione:</td><td class="text-right"><b>' . number_format(($shipping), 2, ',', ' ') . ' &euro;</b></td></tr>';
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Tot. imponibile:</td><td class="text-right"><b>' . number_format(($totalDisplay), 2, ',', ' ') . ' &euro;</b></td></tr>';
            echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">IVA:</td><td class="text-right"><b>' . number_format(($totalTax), 2, ',', ' ') . ' &euro;</b></td></tr>';
        }
        echo '<tr class="table-active"><td colspan="4"></td><td class="text-right" colspan="2">Tot.:</td><td class="text-right"><b class="total">' . number_format(($totalWithTax), 2, ',', ' ') . ' &euro;</b></td></tr>';
        echo '</table>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        ?>
    </div>
    </div>
</section>