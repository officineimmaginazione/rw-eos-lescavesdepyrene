<section class="container preorder-page">
    <h1 class="section-title">Ordine<br />non completato</h1>
    <h2>Durante il pagamento qualcosa non è andato a buon fine.</h2>
    <img class="img-fluid" src="data-customer/themes/default/img/automobile.jpg" alt="macchina" />
</section>


<?php
$this->startCaptureScript();
?>
<script>
    setTimeout(function() {
        window.location.href = '<?php echo $this->path->getUrlFor('Cart', 'cart/index'); ?>';
    }, 8000);
</script>
<?php
$this->endCaptureScript();
