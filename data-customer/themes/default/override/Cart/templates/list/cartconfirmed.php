<div class="admin-agent">
    <section class="container admin-main-body">
        <div id="cart-managed" class="order-section col-12">
            <div class="row section-header">
                <div class="col-12 offset-md-3 col-md-6 col-center">
                    <?php echo ($this->viewagent) ? '<h2>carrelli inviati in ufficio</h2>' : '<h2>carrelli confermati</h2>'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table id="cart-confirmed-managed-table" class="table table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>ordine</th>
                                <th>status</th>
                                <th>cliente</th>
                                <th>prodotti</th>
                                <th>data</th>
                                <th>totale</th>
                                <th>riordina</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->listcartconfirmed as $row) : ?>
                                <tr>
                                    <td><a href="<?php echo $this->path->getUrlFor('Cart', 'cart/details'); ?><?php echo $row["id"]; ?>" title="Vai al riepilogo"><?php echo $row["id"]; ?></a></td>
                                    <?php if ($row["status"] == 2) { ?>
                                        <td>Confermato<br>Da inviare</td>
                                    <?php } else { ?>
                                        <td>Confermato<br>In gestione</td>
                                    <?php } ?>
                                    <td><?php echo (isset($row['company']) && $row['company'] != '') ? $row['company'] . ' - ' . $row['alias'] : $row['name'] . ' ' . $row['surname'] . ' - ' . $row['alias']; ?></td>
                                    <td><?php echo $row['products']; ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($row['ins_date'])); ?></td>
                                    <td>&euro; <span><?php echo number_format($row['total'], 2, ',', '.'); ?></td>
                                    <td><button class="btn  btn-info btn-sm btn-restore-cart" id="restore-cart-<?php echo $row["id"]; ?>"><i class="fa fa-cart-arrow-down"></i></button></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <a href="<?php echo $this->path->getUrlFor('Account', 'user/index'); ?><?php echo (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : ''; ?>" class="btn btn-primary" id="return-shipping"><i class="far fa-arrow-alt-circle-left"></i> Ritorna al menù</a>
                </div>
            </div>
        </div>
    </section>
</div>

<?php
$this->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#cart-confirmed-managed-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [{
                "targets": [3],
                "visible": false
            }]
        });
    });

    function runAjax(data, resultEvent) {
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    $(".btn-restore-cart").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("restore-cart-", "");
        restoreCart($id);
    });

    function restoreCart($id) {
        var data = {};
        data.id = $id;
        data.command = 'restorecart';
        data.<?php echo $this->session->getTokenName() ?> = '<?php echo $this->session->getTokenValue() ?>';
        runAjax(data, function(json) {
            location.href = json.redirect;
        });
    }
</script>
<?php
$this->endCaptureScript();
