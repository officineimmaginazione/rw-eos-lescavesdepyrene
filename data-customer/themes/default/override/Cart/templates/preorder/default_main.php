<section class="container preorder-page">
    <h1 class="section-title">Ordine<br />completato</h1>
    <h2>Lo staff prenderà in carico l'ordine quanto prima.</h2>
    <img class="img-fluid" src="data-customer/themes/default/img/automobile.jpg" alt="macchina"/>
</section>


<?php
$this->startCaptureScript();
?>
<script>
setTimeout(function () {
   window.location.href= '<?php echo $this->path->getUrlFor('Account', 'user/index'); ?>';
},5000);
</script>
<?php
$this->endCaptureScript();