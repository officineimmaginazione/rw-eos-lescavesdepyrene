<div id="cart-left" class="order-section col-12">
    <div class="row section-header">
        <div class="col-12 offset-md-3 col-md-6 col-center">
            <h2>carrelli abbandonati</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table id="cart-left-table" class="table table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>ordine</th>
                        <th>status</th>
                        <th>cliente</th>
                        <th>prodotti</th>
                        <th>data</th>
                        <th>totale</th>
                        <th>riordina</th>
                        <th>cancella</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $first = true; ?>
                    <?php foreach ($this->listcart as $row) : ?>
                        <tr>
                            <td><a href="it/cart/cart/details/<?php echo $row["id"]; ?>" title="Vai al riepilogo"><?php echo $row["id"]; ?></a></td>
                            <?php if ($first) {
                                $first = false; ?>
                                <td>Attivo</td>
                            <?php } else { ?>
                                <td>Abbandonato</td>
                            <?php } ?>
                            <td><?php echo $row['company']; ?> - <?php echo $row['alias']; ?></td>
                            <td><?php echo $row['products']; ?></td>
                            <td><?php echo date('d/m/Y', strtotime($row['ins_date'])); ?></td>
                            <td>&euro; <span><?php echo number_format($row['total'], 2, ',', '.'); ?></td>
                            <td class="text-center"><button class="btn  btn-info btn-sm btn-restore-cart" id="restore-cart-<?php echo $row["id"]; ?>"><i class="fa fa-cart-arrow-down"></i></button></td>
                            <td class="text-center"><button class="btn  btn-info btn-sm btn-delete-cart" id="delete-cart-<?php echo $row["id"]; ?>"><i class="fa fa-trash-alt"></i></button></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="alertRestoreCart" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Il carrello è stato messo in riordine
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                I prodotti disponibili sono stati reinseiti. Vai nel carrello per concluderlo inserendo cliente e indirizzo.
            </div>
        </div>
    </div>
</div>

<?php
$this->view->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#cart-left-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [{
                "targets": [3],
                "visible": false
            }]
        });
    });

    function runAjax(data, resultEvent) {
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->view->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json",
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    $(".btn-restore-cart").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("restore-cart-", "");
        restoreCart($id);
    });

    $(".btn-delete-cart").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("delete-cart-", "");
        deleteCart($id);
    });

    function restoreCart($id) {
        var data = {};
        data.id = $id;
        data.command = 'restorecart';
        data.<?php echo $this->view->session->getTokenName() ?> = '<?php echo $this->view->session->getTokenValue() ?>';
        runAjax(data, function (json){
			//location.href = json.redirect;
            $('#alertRestoreCart').modal('show');
		});
    }

    function deleteCart($id) {
        var data = {};
        data.id = $id;
        data.command = 'deletecart';
        data.<?php echo $this->view->session->getTokenName() ?> = '<?php echo $this->view->session->getTokenValue() ?>';
        runAjax(data, function(json) {
            location.reload();
        });
    }
</script>
<?php
$this->view->endCaptureScript();
?>