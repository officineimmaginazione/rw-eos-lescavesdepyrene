<div id="cart-managed" class="order-section col-12">
    <div class="row section-header">
        <div class="col-12 offset-md-3 col-md-6 col-center">
            <?php echo ($this->viewagent) ? '<h2>carrelli inviati in ufficio</h2>' : '<h2>ordini confermati</h2>'; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table id="cart-confirmed-managed-table" class="table table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>ordine</th>
                        <th>status</th>
                        <th>cliente</th>
                        <th>prodotti</th>
                        <th>data</th>
                        <th>totale</th>
                        <th>riordina</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->listcartconfirmed as $row) : ?>
                        <tr>
                            <td><a href="it/cart/cart/details/<?php echo $row["id"]; ?>" title="Vai al riepilogo"><?php echo $row["id"]; ?></a></td>
                            <?php if ($row["status"] == 2) { ?>
                                <td>Confermato<br>Da inviare</td>
                            <?php } else { ?>
                                <td>Confermato<br>In gestione</td>
                            <?php } ?>
                            <td><?php echo $row['company']; ?> - <?php echo $row['alias']; ?></td>
                            <td><?php echo $row['products']; ?></td>
                            <td><?php echo date('d/m/Y', strtotime($row['ins_date'])); ?></td>
                            <td>&euro; <span><?php echo number_format($row['total'], 2, ',', '.'); ?></td>
                            <td><button class="btn  btn-info btn-sm btn-restore-cart" id="restore-cart-<?php echo $row["id"]; ?>"><i class="fa fa-cart-arrow-down"></i></button></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
$this->view->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#cart-confirmed-managed-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [{
                "targets": [3],
                "visible": false
            }]
        });
    });
</script>
<?php
$this->view->endCaptureScript();
