<?php

return [
	'content.article.projects'  => 'Progetti',
	'content.article.news'      => 'News',
	'content.article.more'      => 'Scopri di più',
	'content.article.project'	=> 'Progetto',
	'content.article.category'  => 'Categoria',
	'content.article.client'	=> 'Cliente',
	'content.article.date'		=> 'Data',
	'content.article.author'	=> 'Autore',
	'content.article.by'		=> 'di',
	'content.article.last'		=> 'Ultimi articoli',
	'content.article.prev'		=> 'Precedente',
	'content.article.next'		=> 'Successivo',
	'content.article.share'		=> 'Condividi',
	'content.article.related'  => 'Articoli correlati',
	'content.article.category.label' => 'Categorie',
	'content.article.category.all' => 'Tutte le categorie',
	'content.article.banner.label' => 'In evidenza',
];
