<?php
return
[
	'content.formcontacts.title' => 'Contattaci',
	'content.formcontacts.description' => 'Hai bisogno di un preventivo o vuoi presentarci il tuo progetto.<br>Inserisci i tuoi dati e verrai contattato a breve!',
	'content.formcontacts.name' => 'Nome e Cognome *',
	'content.formcontacts.name.example' => 'es: Mario Rossi',
	'content.formcontacts.email' => 'eMail *',
	'content.formcontacts.email.example' => 'es: m.rossi@example.it',
	'content.formcontacts.phone' => 'Telefono',
	'content.formcontacts.message' => 'Messaggio *',
	'content.formcontacts.message.example' => 'Cosa vuoi chiederci?',
	'content.formcontacts.privacy.message' => 'Ho letto l\'informativa privacy e do il mio consenso al trattamento dei miei dati in merito alla mia richiesta commerciale.',
	'content.formcontacts.authorise' => 'Accetta',
	'content.formcontacts.send' => 'Invia',
	'content.formcontacts.error.general' => 'Errore imprevisto, ricaricare la pagina o cambiare browser!',
     'content.formcontacts.error.product' => 'Selezionare almeno un prodotto!',
    'content.formcontacts.error.name' => 'Nome e cognome non validi!',
    'content.formcontacts.error.email' => 'Email non valida!',
    'content.formcontacts.error.phone' => 'Numero di telefono del richiedente non valido!',
    'content.formcontacts.error.message' => 'È necessario inserire un messaggio!',
    'content.formcontacts.error.privacy' => 'Acconsentire al trattamento dei dati!',
    'content.formcontacts.thankyou' => 'Grazie per il Vostro interesse, sarà nostra cura ricontattarvi al più presto!',
    'content.formcontacts.newsletter' => 'Sì, voglio ricevere consigli di viaggio e le offerte'
 
];

