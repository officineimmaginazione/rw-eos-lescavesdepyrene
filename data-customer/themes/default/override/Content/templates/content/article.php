<?php if (empty(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode'))) : ?>
	<?php
	$m = new \EOS\Components\Content\Models\ContentModel($this->controller->container);
	?>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8">

				<article class="post-single <?php echo (isset($this->data["parent"]["options"]["css"])) ? $this->data["parent"]["options"]["css"] : ''; ?>" id="single-<?php echo $this->data['id']; ?>">
					<section class="post-header">

						<?php /* post image */ ?>
						<?php if ($this->data["options"]["extra"][0]["image"][0]) : ?>
							<div class="post-header-img">
								<img class="img-fluid" src="<?php echo $this->data["options"]["extra"][0]["image"][0]; ?>" alt="<?php echo strip_tags($this->data["title"]); ?>" />
							</div>
						<?php endif; ?>

						<?php /* post breadcrumbs */ ?>
						<div class="breadcrumb-container">
							<?php
							$breadcrumbs = array();
							$breadcrumb = array(
								"id" => $this->data['id'],
								"title" => EOS\System\Util\StringHelper::htmlPreview($this->data['title'], 30),
								"link" => "",
							);
							array_push($breadcrumbs, $breadcrumb);
							$parent_id = $this->data['parent']['id'];
							if ($parent_id != 0) {
								$exit = true;
								while ($exit == true) {
									$curcat = $m->getItem($parent_id, $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Category);
									$breadcrumb = array(
										"id" => $curcat['id'],
										"title" => $curcat['title'],
										"link" => $this->path->getUrlFor('content', 'category/' . $curcat['id']),
									);
									array_unshift($breadcrumbs, $breadcrumb);
									if ($curcat['parent'] != 0) {
										$parent_id = $curcat['parent'];
									} else {
										$exit = false;
									}
								}
							}
							?>
							<ul class="d-flex flex-wrap align-items-center">
								<?php foreach ($breadcrumbs as $breadcrumb) {
									if ($breadcrumb['link'] != "") {
										echo "<li><a href=" . $breadcrumb['link'] . ">" . $breadcrumb['title'] . "</a></li>";
									} else {
										echo "<li>" . $breadcrumb['title'] . "</li>";
									}
								} ?>
							</ul>
						</div>

						<?php /* post title */ ?>
						<?php if ($this->data["title"] && $this->data['options']['showtitle']) : ?>
							<h1 class="post-title"><?php echo $this->data["title"]; ?></h1>
						<?php endif; ?>

						<?php /* post info */ ?>
						<div class="post-info">
							<?php if (isset($this->data['subtitle']) && $this->data['subtitle'] != "") { ?>
								<div class="single-info post-author">
									<i class="fas fa-pencil-alt"></i>
									<span>
										<?php echo $this->data['subtitle']; ?>
									</span>
								</div>
							<?php } ?>
							<div class="single-info post-publish-date">
								<i class="fas fa-calendar-alt"></i>
								<span>
									<?php echo $this->data['publish_date']; ?>
								</span>
							</div>
						</div>

					</section>

					<?php /* post body */ ?>
					<section class="post-body">
						<?php echo $this->data['content']; ?>
					</section>

					<?php /* post share */ ?>
					<div class="post-share">
						<h4 class="mb-3">
							<?php $this->transP('content.article.share'); ?>
						</h4>
						<div id="post-share-icons"></div>
					</div>

				</article>

				<?php /* post related */ ?>
				<?php if (!empty($this->relatedPosts)) { ?>
					<div class="mt-5 related-posts">
						<h4 class="mb-3">
							<?php $this->transP('content.article.related'); ?>
						</h4>
						<div class="blog-body related-posts-list">
							<div class="row">
								<?php foreach ($this->relatedPosts as $post) { ?>
									<?php
									$post_title = $post["title"];
									$post_image = $post["options"]["extra"][0]["image"][0];
									$post_category = $this->encodeHtml($m->getItem($post["parent"], $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Category)['title']);
									$post_publish_date = $post["publish_date"];
									$post_content = EOS\System\Util\StringHelper::htmlPreview($post['content'], $this->data['preview-length']);
									$post_link = $this->path->getUrlFor('content', 'article/' . $post['id']);
									?>
									<article class="col-12 col-lg-6 post">
										<header class="post-header">
											<?php if ($post_image) : ?>
												<div class="post-img">
													<a title="<?php echo strip_tags($post_title); ?>" href="<?php echo $post_link; ?>">
														<img loading="lazy" src="<?php echo $post_image; ?>" alt="<?php echo $post["title"]; ?>" />
													</a>
												</div>
											<?php endif; ?>
											<div class="post-info">
												<span><?php echo $post_category; ?></span> | <span><?php echo $post_publish_date; ?></span>
											</div>
										</header>
										<section class="post-body">
											<?php if (($post['options']['showtitle']) && (!empty($post_title))) : ?>
												<a title="<?php echo strip_tags($post_title); ?>" href="<?php echo $post_link; ?>">
													<h3 class="post-title">
														<?php echo $post_title; ?>
													</h3>
												</a>
											<?php endif; ?>
											<div class="post-content">
												<?php echo $post_content; ?>
											</div>
										</section>
										<footer class="post-footer">
											<a class="btn btn-primary" href="<?php echo $post_link; ?>">
												<?php $this->transP('content.article.more'); ?>
											</a>
										</footer>
									</article>
								<?php } ?>
							</div>
						</div>
					</div>
				<?php } ?>


			</div>
			<div class="col-12 col-md-4">
				<?php $sidebar_template = "article"; ?>
				<?php include 'sidebar.php'; ?>
			</div>
		</div>
	</div>


	<?php $this->startCaptureScript(); ?>
	<?php $this->addScriptLink("https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"); ?>
	<script>
		$(() => {
			setShare();

			function setShare() {
				$("#post-share-icons").jsSocials({
					showCount: false,
					showLabel: false,
					shares: [{
							share: "facebook",
							logo: "fab fa-facebook-f",
						},
						{
							share: "twitter",
							logo: "fab fa-twitter",
						},
						{
							share: "linkedin",
							logo: "fab fa-linkedin-in",
						},
						{
							share: "whatsapp",
							logo: "fab fa-whatsapp",
						},
						{
							share: "email",
							logo: "fas fa-envelope",
						},
					],
				});
			}
		});
	</script>
	<?php $this->endCaptureScript(); ?>

<?php
else :
	include($this->path->getThemeCustomPath() . \EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode') . '.php');
endif;
