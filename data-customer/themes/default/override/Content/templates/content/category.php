<div class="container blog-body">
	<?php if (empty(\EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode'))) : ?>
		<?php $m = new \EOS\Components\Content\Models\ContentModel($this->controller->container); ?>
		<div class="row">
			<div class="col-12 col-md-8">
				<div class="category-main">
					<div class="category-heading">
						<?php if (($this->data['options']['showtitle']) && (!empty($this->data['title']))) : ?>
							<h1 class="section-title category-title"><?php echo $this->encodeHtml($this->data['title']); ?></h1>
						<?php endif; ?>
						<?php if (($this->data['options']['showsubtitle']) && (!empty($this->data['subtitle']))) : ?>
							<h2 class="section-subtitle category-subtitle"><?php echo $this->encodeHtml($this->data['subtitle']); ?></h2>
						<?php endif; ?>
						<?php if (!empty($this->data['content'])) : ?>
							<div class="category-content">;
								<?php echo $this->data['content']; ?>
							</div>;
						<?php endif; ?>
					</div>
					<div class="category-posts">
						<div class="row">

							<?php foreach ($this->data['items'] as $post) : ?>
								<?php
								$post_title = $post["title"];
								$post_image = $post["options"]["extra"][0]["image"][0];
								$post_category = $this->encodeHtml($m->getItem($post["parent"], $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Category)['title']);
								$post_publish_date = $post["publish_date"];
								$post_content = EOS\System\Util\StringHelper::htmlPreview($post['content'], $this->data['preview-length']);
								$post_link = $this->path->getUrlFor('content', 'article/' . $post['id']);
								?>
								<article class="col-12 col-lg-6 post">
									<header class="post-header">
										<?php if ($post_image) : ?>
											<div class="post-img">
												<a title="<?php echo strip_tags($post_title); ?>" href="<?php echo $post_link; ?>">
													<img loading="lazy" src="<?php echo $post_image; ?>" alt="<?php echo $post["title"]; ?>" />
												</a>
											</div>
										<?php endif; ?>
										<div class="post-info">
											<span><?php echo $post_category; ?></span> | <span><?php echo $post_publish_date; ?></span>
										</div>
									</header>
									<section class="post-body">
										<?php if (($post['options']['showtitle']) && (!empty($post_title))) : ?>
											<a title="<?php echo strip_tags($post_title); ?>" href="<?php echo $post_link; ?>">
												<h3 class="post-title">
													<?php echo $post_title; ?>
												</h3>
											</a>
										<?php endif; ?>
										<div class="post-content">
											<?php echo $post_content; ?>
										</div>
									</section>
									<footer class="post-footer">
										<a class="btn btn-primary" href="<?php echo $post_link; ?>">
											<?php $this->transP('content.article.more'); ?>
										</a>
									</footer>
								</article>
							<?php endforeach; ?>

						</div>
					</div>
					<div class="blog-pagination">
						<?php
						$url = $this->path->getUrlFor('content', 'category/' . $this->data['id']);
						?>
						<?php if ($this->data['url-prev'] != "") { ?>
							<a class="prev" href="<?php echo $this->data['url-prev']; ?>" title="<?php $this->transP('content.article.prev'); ?>">
								<?php $this->transP('content.article.prev'); ?>
							</a>
						<?php } ?>
						<?php if ($this->data['page-count'] != 1) { ?>
							<ul class="d-flex align-items-center">
								<?php
								$pagination = $m->getPagination($this->data['page-count'], $this->data['page'], $url);
								foreach ($pagination as $page) {
									$el  = "<li>";
									if ($page['label'] != "...") {
										$el .= "<a class='" . $page['active'] . "' href='" . $page['pageLink'] . "'>" . $page['label'] . "</a>";
									} else {
										$el .= "...";
									}
									$el .= "</li>";
									echo $el;
								}
								?>
							</ul>
						<?php } ?>
						<?php if ($this->data['url-next'] != "") { ?>
							<a class="next" href="<?php echo $this->data['url-next']; ?>" title="<?php $this->transP('content.article.next'); ?>">
								<?php $this->transP('content.article.next'); ?>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>

			<div class="col-12 col-md-4">
				<?php $sidebar_template = "category"; ?>
				<?php include 'sidebar.php'; ?>
			</div>

		</div>

	<?php else : ?>
		<?php include($this->path->getThemeCustomPath() . \EOS\System\Util\ArrayHelper::getStr($this->data['options'], 'customcode') . '.php'); ?>
	<?php endif; ?>
</div>