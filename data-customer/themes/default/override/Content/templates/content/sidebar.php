<?php
$m = new \EOS\Components\Content\Models\ContentModel($this->controller->container);
?>

<div id="sidebar-container">
    <div id="blog-sidebar">

        <?php
        $sidebar_banner = (new \EOS\Components\Content\Widgets\SectionWidget($this, "sidebar-banner"))->data["options"];
        ?>
        <?php if ($sidebar_banner) { ?>
            <div class="sidebar-block side-banner">
                <?php (new \EOS\Components\Content\Widgets\SectionWidget($this, 'sidebar-banner'))->write(); ?>
            </div>
        <?php } ?>

        <div class="sidebar-block last-posts">
            <h5><?php $this->transP('content.article.last'); ?></h5>
            <?php
            if ($sidebar_template == "category") {
                $idContent = $this->data['id'];
            } elseif ($sidebar_template == "article") {
                $idContent = $this->data['parent']['id'];
            }
            $m->getChildList($idContent, \EOS\Components\Content\Classes\ContentType::Category, $allCats);
            if (is_array($allCats)) {
                array_push($allCats, $idContent);
            } else {
                $allCats = $idContent;
            }
            $items = $m->getItems($allCats, $m->lang->getCurrent()->id, \EOS\Components\Content\Classes\ContentType::Article, 1, 5);
            ?>
            <ul>
                <?php foreach ($items['data'] as $item) { ?>
                    <?php
                    $title = EOS\System\Util\StringHelper::htmlPreview(strip_tags($item['title']), 30);
                    $link = $this->path->getUrlFor('content', 'article/' . $item['id']);
                    ?>
                    <li>
                        <a title="<?php echo $title; ?>" href="<?php echo $link; ?>">
                            <?php echo $title; ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <div class="sidebar-block all-categories">
            <h5><?php $this->transP('content.article.category.label'); ?></h5>
            <?php
            $items = array();

            //if in category
            if ($sidebar_template == "category") {
                if ($this->data['parent'] == 0) {
                    $currCat['id'] = $this->data['id'];
                    $currCat['content'] = $this->data['content'];
                    $currCat['parent'] = $this->data['parent'];
                    $item = array(
                        "id" => $currCat['id'],
                        "title" => $currCat['content'],
                        "parent" => $currCat['parent'],
                    );
                } else {
                    $currCat = $m->getItem($this->data['parent'], $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Category);
                    $item = array(
                        "id" => $currCat['id'],
                        "title" => $currCat['content'],
                        "parent" => $currCat['parent'],
                    );
                }
                array_push($items, $item);
                if (isset($this->data['parent']['id'])) {
                    $m->getChildList($currCat['parent'], \EOS\Components\Content\Classes\ContentType::Category, $childCat);
                } else {
                    $m->getChildList($currCat['id'], \EOS\Components\Content\Classes\ContentType::Category, $childCat);
                }

                //if in article
            } elseif ($sidebar_template == "article") {
                $parent = $m->getLastItemByParent(0, $m->lang->getCurrent()->id, \EOS\Components\Content\Classes\ContentType::Category);
                $items[] = array(
                    "id" => $parent['id'],
                    "title" => $parent['content'],
                    "parent" => $parent['parent'],
                );
                $m->getChildList($parent['id'], \EOS\Components\Content\Classes\ContentType::Category, $childCat);
            }



            foreach ($childCat as $subcat) {
                $subcatData = $m->getItem($subcat, $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Category);
                $item = array(
                    "id" => $subcatData['id'],
                    "title" => $subcatData['title'],
                    "parent" => $subcatData['parent'],
                );
                array_push($items, $item);
            }
            ?>
            <ul>
                <?php foreach ($items as $item) { ?>
                    <li>
                        <a <?php echo ($this->data['id'] == $item['id']) ? "class='active'" : ""; ?> href="<?php echo $this->path->getUrlFor('content', 'category/' . $item['id']); ?>">
                            <?php if ($item['parent'] == 0) { ?>
                                <?php $this->transP('content.article.category.all'); ?>
                            <?php } else { ?>
                                <?php echo $item['title']; ?>
                            <?php } ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>

    </div>
</div>

<?php $this->startCaptureScript(); ?>
<?php $this->addScriptLink($this->path->getThemeUrl() . "js/jquery.sticky-sidebar.min.js"); ?>
<?php $this->addScriptLink($this->path->getThemeUrl() . "js/resize-sensor.js"); ?>
<script>
    $(() => {
        setTimeout(setStickySidebar, 500);

        function setStickySidebar() {
            if ($("#sidebar-container")[0] && $(window).width() >= 768) {
                $("#sidebar-container").stickySidebar({
                    topSpacing: 30,
                });
            }

        }
    });
</script>
<?php $this->endCaptureScript(); ?>