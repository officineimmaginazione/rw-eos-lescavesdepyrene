<?php $m = new \EOS\Components\Content\Models\ContentModel($this->view->controller->container); ?>

<section class="homepage-section section-blog">
	<?php
	$m->getChildList(62, \EOS\Components\Content\Classes\ContentType::Category, $childProjectCat);
	$blog = $m->getItem(62, $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Category);
	$posts = $m->getItems($childProjectCat, $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Article, 1, 3);
	?>
	<div class="container">
		<?php if(isset($posts) && count($posts)) : ?>
			<?php if (($blog['options']['showtitle']) && (!empty($blog['title']))) : ?>
				<h2 class="section-title blog-title"><?php echo $this->view->encodeHtml($blog['title']); ?></h2>
			<?php endif; ?>
			<div class="row">
				<?php foreach($posts["data"] as $post) : ?>
					<div class="col-12 col-md-4 blog-post">
						<?php $post_image = $post["options"]["extra"][0]["image"][0]; ?>
						<?php if($post_image) : ?>
							<div class="post-img">
								<img src="<?php echo $post_image; ?>" alt="<?php echo $post["title"]; ?>" />
							</div>
						<?php endif; ?>
						<div class="post-info">
							<span><?php echo $this->view->encodeHtml($m->getItem($post["parent"], $m->lang->getCurrent()->id, EOS\Components\Content\Classes\ContentType::Category)['title']); ?></span> | <span><?php echo $post["publish_date"]; ?></span>
						</div>
						<h3 class="post-title"><?php echo $post["title"]; ?></h3>
						<a class="btn btn-primary" href="<?php echo $this->view->path->getUrlFor('content', 'article/' . $post['id']) ?>"><?php $this->view->transP('content.article.more'); ?></a>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</section>