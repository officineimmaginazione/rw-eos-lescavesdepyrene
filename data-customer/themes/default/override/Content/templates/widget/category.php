<?php
if (!empty($this->data))
{
    echo '<div class="category-widget">' . "\n";
    if (($this->data['options']['showtitle']) && (!empty($this->data['title'])))
    {
        echo '<h1 class="category-title">' . $this->view->encodeHtml($this->data['title']) . '</h1>' . "\n";
    }
    echo '<div class="category-widget-items">' . "\n";
    foreach ($this->data['items'] as $item)
    {
        echo '<article class="category-widget-item">' . "\n";
        echo '<header>' . "\n";
        if (($item['options']['showtitle']) && (!empty($item['title'])))
        {
            $link = $this->view->path->getUrlFor('content', 'article/' . $item['id']);
            printf('<h4 class="category-item-title"><a href="%s">%s</a></h4>' . "\n", $link, $this->view->encodeHtml($item['title']));
        }
        if (($item['options']['showdate']) && (!empty($item['publish_date'])))
        {
            printf('<p class="category-widget-item-date">%s</p>' . "\n", $item['publish_date']);
        }
        echo '</header>' . "\n";
        echo '<div class="category-widget-item-content">';
        echo EOS\System\Util\StringHelper::htmlPreview($item['content'], $this->data['preview-length']);
        echo '</div>' . "\n";
        echo '</article>' . "\n";
    }
    echo '</div>' . "\n";
    echo '</div>' . "\n";
} 
