<?php
$totalnow = 0;
$totalalltime = 0;
$totalused = 0;
?>

<div class="admin-agent">
    <section class="container admin-main-body">
        <div id="order-list" class="order-section col-12">
            <div class="row section-header">
                <div class="col-12 offset-md-3 col-md-6 col-center">
                    <?php echo ($this->viewagent) ? '<h2>provvigioni privati</h2>' : '<h2>riepilogo cashback</h2>'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table id="order-list-table" class="table table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Data</th>
                                <th>Carrello</th>
                                <th>Cashback</th>
                                <th>Utente</th>
                                <th>Livello</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->cashbackList as $row) :
                                if ($row['status'] == 0) {
                                    $status = 'Non accreditato';
                                } elseif ($row['status'] == 1) {
                                    $status = 'Accreditato';
                                } elseif ($row['status'] == 2) {
                                    $status = 'Stornato parzialmente';
                                } elseif ($row['status'] == 3) {
                                    $status = 'Stornato totalmente';
                                } elseif ($row['status'] == 4) {
                                    $status = 'Fee scaricate';
                                }
                                if ((float) $row["cashback"] > 0) {
                                    $totalalltime += $row["cashback"];
                                } else {
                                    $totalused += ($row["cashback"]);
                                }
                            ?>
                                <tr>
                                    <td><?php echo $row["id"]; ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($row["ins_date"])); ?></td>
                                    <td><?php echo $row["id_cart"]; ?></td>
                                    <td>&euro; <?php echo number_format($row["cashback"], 2, ',', '.'); ?></td>
                                    <td><?php echo preg_replace('/.\K./', '*', $row['name']); ?></td>
                                    <td><?php echo $row["level"]; ?></td>
                                    <td><?php echo $status; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-12 offset-md-3 col-md-6 resume">
                    <h5>Riepilogo cashback</h5>
                    <h6>Attuale: <?php echo (in_array(2, $this->user->groups)) ? number_format($this->fee, 2, ',', '.') : number_format($this->cashback, 2, ',', '.'); ?> &euro;</h6>
                    <h6>Totale (dal momento dell'iscrizione): <?php echo number_format($totalalltime, 2, ',', '.'); ?> &euro;</h6>
                    <h6>Utilizzato: <?php echo number_format($totalused, 2, ',', '.'); ?> &euro;</h6>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 text-center">
                    <a href="<?php echo $this->path->getUrlFor('Account', 'user/index'); ?><?php echo (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : ''; ?>" class="btn btn-primary" id="return-shipping"><i class="far fa-arrow-alt-circle-left"></i> Ritorna al menù</a>
                    <?php if (isset($this->user->groups) && in_array(42, $this->user->groups)) { ?>
                        <a href="https://www.lescaves.it/it/come-acquistare/#sec3" class="btn btn-primary" id="return-shipping" target="_blank"><i class="far fa-question-circle"></i> Come funziona il cashback</a>
                    <?php  } else { ?>
                        <a href="#" onclick="event.preventDefault(); $('#cashbackB2BModal').modal('toggle');" class="btn btn-primary" id="return-shipping" target="_blank"><i class="far fa-question-circle"></i> Come funziona il cashback</a>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
</div>


<div id="cashbackB2BModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Cashback per clienti Horeca
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Grazie agli acquisti del cliente finale, voi ristoranti ed enoteche – nel ruolo di “Presentatori” - riceverete un Cashback, somma da spendere in acquisti futuri. Lo stesso vale per i vostri clienti: presentando un nuovo amico, anch’essi guadagneranno un credito per futuri acquisti (e anche voi ne beneficerete).</p>
                <p>Lo schema sotto traduce in cifre il funzionamento del Cashback: tutto inizia con il 16% di Cashback sull’importo dell’ordine del primo cliente presentato. Se anch’egli presenterà un cliente, il Cashback relativo a quell’ordine sarà del 10% rispetto all’importo mentre al primo cliente verrà corrisposto un Cashback del 5%. </p>
                <p>La tabella sotto sviluppa tutte le percentuali per ogni livello successivo.</p>
                <img src="data-customer/uploads/img/les-caves-cashback-schema-b2b.png" class="img-fluid" alt="close" />
            </div>
        </div>
    </div>
</div>

<div id="cashbackB2BModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Cashback per clienti Horeca
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Grazie agli acquisti del cliente finale, voi ristoranti ed enoteche – nel ruolo di “Presentatori” - riceverete un Cashback, somma da spendere in acquisti futuri. Lo stesso vale per i vostri clienti: presentando un nuovo amico, anch’essi guadagneranno un credito per futuri acquisti (e anche voi ne beneficerete).</p>
                <p>Lo schema sotto traduce in cifre il funzionamento del Cashback: tutto inizia con il 16% di Cashback sull’importo dell’ordine del primo cliente presentato. Se anch’egli presenterà un cliente, il Cashback relativo a quell’ordine sarà del 10% rispetto all’importo mentre al primo cliente verrà corrisposto un Cashback del 5%. </p>
                <p>La tabella sotto sviluppa tutte le percentuali per ogni livello successivo.</p>
                <img src="data-customer/uploads/img/les-caves-cashback-schema1.jpg.png" alt="close" />
            </div>
        </div>
    </div>
</div>

<?php
$this->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#order-list-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
<?php
$this->endCaptureScript();
