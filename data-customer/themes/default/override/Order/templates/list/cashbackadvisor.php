<div class="admin-agent">
    <section class="container admin-main-body">
        <div id="order-list" class="order-section col-12">
            <div class="row section-header">
                <div class="col-12 offset-md-3 col-md-6 col-center">
                    <h2>provvigioni presentatori</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table id="order-list-table" class="table table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Utente</th>
                                <th>eMail</th>
                                <th>Cashback</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->cashbackList as $row) : ?>
                                <tr>
                                    <td><?php echo $row["id"]; ?></td>
                                    <td><?php echo $row['name']; ?></td>
                                    <td><?php echo $row['email']; ?></td>
                                    <td>&euro; <?php echo number_format($row["cashback"], 2, ',', '.'); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <a href="<?php echo $this->path->getUrlFor('Account', 'user/index'); ?><?php echo (isset($this->customer) && $this->customer != 0) ? '?customer=' . $this->customer : ''; ?>" class="btn btn-primary" id="return-shipping"><i class="far fa-arrow-alt-circle-left"></i> Ritorna al menù</a>
                    <?php if (isset($this->user->groups) && in_array(42, $this->user->groups)) { ?>
                        <a href="https://www.lescaves.it/it/come-acquistare/#sec3" class="btn btn-primary" id="return-shipping" target="_blank"><i class="far fa-question-circle"></i> Come funziona il cashback</a>
                    <?php  } else { ?>
                        <a href="#" onclick="event.preventDefault(); $('#cashbackB2BModal').modal('toggle');" class="btn btn-primary" id="return-shipping" target="_blank"><i class="far fa-question-circle"></i> Come funziona il cashback</a>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
</div>

<div id="cashbackB2BModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Cashback per clienti Horeca
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Grazie agli acquisti del cliente finale, voi ristoranti ed enoteche – nel ruolo di “Presentatori” - riceverete un Cashback, somma da spendere in acquisti futuri. Lo stesso vale per i vostri clienti: presentando un nuovo amico, anch’essi guadagneranno un credito per futuri acquisti (e anche voi ne beneficerete).</p>
                <p>Lo schema sotto traduce in cifre il funzionamento del Cashback: tutto inizia con il 16% di Cashback sull’importo dell’ordine del primo cliente presentato. Se anch’egli presenterà un cliente, il Cashback relativo a quell’ordine sarà del 10% rispetto all’importo mentre al primo cliente verrà corrisposto un Cashback del 5%. </p>
                <p>La tabella sotto sviluppa tutte le percentuali per ogni livello successivo.</p>
                <img src="data-customer/uploads/img/les-caves-cashback-schema1.jpg.png" alt="close" />
            </div>
        </div>
    </div>
</div>

<?php
$this->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#order-list-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
<?php
$this->endCaptureScript();
