<?php
$this->startCaptureScript();
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
?>
<script>
    function runAjax(data, resultEvent)
    {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Order', 'order/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json",
        })
            .done(function (json)
            {
                if (json.result == true)
                {
                    resultEvent(json);
                }
                else
                {
                    bootbox.alert(json.message);
                }
            })
            .fail(function (jqxhr, textStatus, error)
            {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    $("#order-send").click(function (event)
    {
        event.preventDefault();
        saveData();
    });

    function saveData()
    {
        var data = $('#order').serializeFormJSON();
        runAjax(data, function (json)
        {
            location.href = json.redirect;
        });
    }

</script>
<?php
$this->endCaptureScript();