<section class="order-details">
    <div class="overflow"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Ordine n° <?php echo $this->data['reference']; ?> del <?php echo date("d/m/Y", strtotime($this->data['ins_date'])); ?></h2>
                <h4><?php echo $this->data['company']; ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6">
                <h5>Riepilogo cliente</h5>
                <p>Indirizzo: <?php echo $this->data['address']; ?> - <?php echo $this->data['city']; ?> (<?php echo $this->data['state']; ?>) - <?php echo $this->data['country']; ?></p>
                <p>Tel.: <?php echo $this->data['phone']; ?></p>
                <p>email: <?php echo $this->data['email']; ?></p>
                <p>P. Iva: <?php echo $this->data['vat']; ?> - C.F.: <?php echo $this->data['cif']; ?></p>
            </div>
            <div class="col-12 col-sm-6">
                <h5>Dati</h5>
                <p>Note: <?php echo $this->data['notecustomer']; ?></p>
                <p>Agente: <?php echo $this->data['agent-name']; ?></p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-sm-6">
                <?php if (isset($this->data['notes-transaction']) && $this->data['notes-transaction'] != '') { ?>
                    <?php echo '<a href="https://www.lescaves.it/data-customer/uploads/adhoc/orderattachment/' . strtolower($this->data["notes-transaction"]) . '" target="_blank" class="btn btn-primary" id="download-attachment" title="Scarica il documento">Scarica il documento</a>' ?>
                <?php } else { ?>
                    <a class="btn btn-primary btn-print" onclick="event.preventDefault(); window.print();" id="download-attachment" title="Stampa">Stampa</a>
                <?php } ?>
            </div>
        </div>
        <?php
        echo '<div class="row mt-3 pb-4">';
        echo '<div class="col-12">';
        echo '<div class="box">';
        echo '<div class="box-header with-border">';
        echo '<h3 class="box-title">' . $this->transE('order.order.products') . '</h3>';
        echo '<div class="pull-right box-tools">';
        echo '</div>';
        echo '</div>';
        echo '<div class="box-body no-padding">';
        echo '<table class="table table-striped">';
        echo '<tr>';
        echo '<th style="width: 10px">#</th>';
        echo '<th>' . $this->transE('cart.details.id') . '</th>';
        echo '<th>' . $this->transE('order.order.product') . '</th>';
        echo '<th>' . $this->transE('order.order.manufacturer') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.price') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.pricediscount') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.quantity') . '</th>';
        echo '<th class="text-right">' . $this->transE('cart.details.totrow') . '</th>';
        echo '</tr>';
        $taxtotalproduct = 0;
        $total = 0;
        $totqty = 0;
        if (count($this->datarow)) {
            foreach ($this->datarow as $row) {
                $options = json_decode($row['options'], true);
                if (isset($options['data']['additional-discount']) && $options['data']['additional-discount'] > 0) {
                    $adddiscount = $options['data']['additional-discount'];
                }
                $pricedisc = round($row['price'], 4);
                $taxrow = (($row['quantity']) * $pricedisc) * (22 / 100);
                $taxtotalproduct += $taxrow;
                $total = $total + ($row['quantity'] * $pricedisc);
                $totqty = $totqty + $row['quantity'];
                $reference = (isset($row['reference'])) ? $row['reference'] : '';
                $manufacturer = (isset($row['manufacturer'])) ? $row['manufacturer'] : '';
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo '<td>' . $reference . '</td>';
                echo "<td>" . $row['product_name'] . "</td>";
                echo "<td>" . $manufacturer . "</td>";
                echo '<td class="text-right">' . number_format($row['price'], 2, ',', ' ') . ' &euro;</td>';
                echo '<td class="text-right">' . number_format($pricedisc, 2, ',', ' ') . ' &euro;</td>';
                echo '<td class="text-right">' . $row['quantity'] . '</td>';
                echo '<td class="text-right">' . number_format(($row['quantity'] * $pricedisc), 2, ',', ' ') . ' &euro;</td>';
                echo "<tr>";
            }
        }
        $totalDisplay = round(($total), 4);
        $totalTax = round((($taxtotalproduct)), 4);
        $totalWithTax = round(($total) + $totalTax, 4);
        echo '<tr class="table-active"><td colspan="5"></td><td class="text-right" colspan="2">Tot. Prod.:<b>' . number_format($totqty, 0, ',', ' ') . '</b></td><td class="text-right"><b>' . number_format(($total), 2, ',', ' ') . ' &euro;</b></td></tr>';
        echo '<tr class="table-active"><td colspan="5"></td><td class="text-right" colspan="2">IVA Prod.:</td><td class="text-right"><b>' . number_format(($totalTax), 2, ',', ' ') . ' &euro;</b></td></tr>';
        echo '<tr class="table-active"><td colspan="5"></td><td class="text-right" colspan="2">Tot. Prod.:</td><td class="text-right"><b class="total">' . number_format(($totalWithTax), 2, ',', ' ') . ' &euro;</b></td></tr>';
        echo '</table>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        ?>
    </div>
</section>