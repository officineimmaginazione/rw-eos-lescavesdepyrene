<?php 
if(isset($this->displaydebit) &&  $this->displaydebit == true) { 
$debit = 0;        
?>
<div id="customer-opendebit" class="order-section col-12">
    <div class="row section-header">
        <div class="offset-3 col-6 col-center">
            <h2>scadenziario</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table id="customer-opendebit-table" class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>N° Doc.</th>
                        <th>Data Doc.</th>
                        <th>Scad. Doc.</th>
                        <th>Dare</th>
                        <th>Avere</th>
                        <th>Ultima Reg.</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->openDebit as $row) : ?>
                    <?php if($row["Avere"] == 0.0000 || $row["Dare"] == 0.0000) { 
                    if (new DateTime() > new DateTime($row["ScadenzaDocumento"])) {
                        $debit = 1;
                    } ?>
                    <tr>  
                        <td><?php echo $row["PTNUMPAR"]; ?></td>
                        <td><?php echo date('d/m/Y', strtotime($row["DataDocumento"])); ?></td>
                        <td><?php echo date('d/m/Y', strtotime($row["ScadenzaDocumento"])); ?></td>
                        <td>&euro; <?php echo number_format($row["Dare"], 2, ',', ''); ?></td>
                        <?php if($row["Avere"] == 0.0000) { ?>
                        <td style='color:#FF0000'>&euro; <b><?php echo number_format($row["Avere"], 2, ',', ''); ?></b></td>
                        <?php } else { ?>
                        <td>&euro; <?php echo number_format($row["Avere"], 2, ',', ''); ?></td>
                        <?php } ?>
                        <td><?php echo date('d/m/Y', strtotime($row["UltimaRegistrazione"])); ?></td>
                    </tr>
                    <?php } ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="opendebitmodal" tabindex="-1" role="dialog" aria-labelledby="opendebitmodaltitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="opendebitmodaltitlelong">Attenzione</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Il cliente ha degli scaduti - Effettuare una verifica o contattare l'amministrazione
            </div>
        </div>
    </div>
</div>

<?php
$this->view->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#customer-opendebit-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [[ 0, "desc" ]]
        } );
    } );
    
<?php if($debit == 1) { ?>
    $(window).on('load',function(){
        $('#opendebitmodal').modal('show');
    });
<?php } ?>
    
</script>
<?php
$this->view->endCaptureScript();
}
