<div id="order-list" class="order-section col-12">
    <div class="row section-header">
        <div class="col-12 offset-md-3 col-md-6 col-center">
            <?php echo ($this->viewagent) ? '<h2>ordini spediti</h2>' : '<h2>ordini spediti</h2>'; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table id="order-confirmed-list-table" class="table table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>Numero</th>
                        <th>Data Doc.</th>
                        <th>Cliente</th>
                        <th>Totale</th>
                        <th>Copia fattura<br>(uso interno)</th>
                        <th>prodotti</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->orderListConfirmed as $row) : ?>
                        <tr>
                            <td><a class="" href="<?php echo $this->view->path->getUrlFor('Order', 'order/details');
                                                    echo $row["id"]; ?>" title="Vai ai dettegli"><?php echo $row["reference"]; ?></a></td>
                            <td><?php echo date('d/m/Y', strtotime($row["ins_date"])); ?></td>
                            <td><?php echo $row["company"]; ?></td>
                            <td>&euro; <?php echo number_format($row["amount"], 2, ',', '.'); ?></td>
                            <?php
                            $attach = 'Non disponibile';
                            if (isset($row["notes_transaction"]) && $row["notes_transaction"] != '') {
                                $attach = '<a href="https://www.lescaves.it/data-customer/uploads/adhoc/orderattachment/' . strtolower($row["notes_transaction"]) . '" target="_blank" class="btn btn-info btn-sm btn-download-invoice" id=""><i class="fas fa-file-alt"></i></a>';
                            }
                            ?>
                            <td><?php echo $attach; ?></td>
                            <td><?php echo $row['products']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
$this->view->startCaptureScript();
?>
<script>
    $(document).ready(function() {
        $('#order-confirmed-list-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [{
                "targets": [5],
                "visible": false
            }]
        });
    });
</script>
<?php
$this->view->endCaptureScript();
