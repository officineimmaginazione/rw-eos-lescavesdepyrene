<?php

return
    [
        'product.catalog.error.invalidcommand'		=> 'Invalid command!',
        'product.catalog.error.product'				=> 'Invalud product!',
        'product.catalog.title'						=> 'Showcase',
        'product.catalog.subtitle'					=> 'Showcase',
        'product.catalog.header.title'				=> 'Order them here',
		'product.catalog.header.desc'				=> '',
		'product.catalog.quantity.title'			=> 'Quantity',
		'product.catalog.product.book'				=> 'Order',
		'product.catalog.product.continue'			=> '',
		'product.catalog.product.or'				=> 'or',
        'product.catalog.product.title'				=> '',
        'product.catalog.product.send.title'		=> '',
        'product.catalog.product.quantity'			=> 'How many people?',
        'product.catalog.product.people'			=> 'People',
        'product.catalog.product.yes'				=> 'Yes',
        'product.catalog.product.no'				=> 'No',
        'product.catalog.product.message.answer'	=> '',
        'product.catalog.product.message'			=> '',
        'product.catalog.product.addcart'			=> '',
        'product.catalog.product.error.mail'		=> 'Invalid email!',
        'product.catalog.product.select'			=> 'Select',
        'product.catalog.product.box1.title1'		=> '',
        'product.catalog.product.perperson'			=> '',
        'product.catalog.product.contactus'			=> 'Contact us',
        'product.catalog.product.previewgift'		=> ''
];
