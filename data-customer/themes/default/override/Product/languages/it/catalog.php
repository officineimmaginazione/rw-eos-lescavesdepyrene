<?php

return
    [
        'product.catalog.error.invalidcommand'		=> 'Comando non valido!',
        'product.catalog.error.product'				=> 'Prodotto non valido!',
        'product.catalog.title'						=> 'Catalogo',
        'product.catalog.title.search'              => 'Risultati della ricerca',
		'product.catalog.header.title'				=> 'Prenota qui'
];
