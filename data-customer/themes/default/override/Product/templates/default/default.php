<?php
$this->includePartial('cart');
$this->includePartial('header'); // Testata
$this->includePartial('products');
$this->includePartial('modal');
//$this->includePartial('pagination');   Inserito all'interno del template della pagina

$this->addScriptLink($this->path->getThemeUrl() . "js/eos.util.js");

$this->startCaptureScript();
$this->includePartial('js');
$this->endCaptureScript();
