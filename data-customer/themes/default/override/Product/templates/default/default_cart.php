<?php $cartBase = (new \EOS\Components\Cart\Classes\CartBase($this->controller->container)); ?>
<section class="collapse cart-collapse" id="cart-collapse">
    <h3 class="h1 cart-collapse-title"><?php $this->transPE('cart.cart.title'); ?></h3>
    <button class="btn cart-collapse-close" type="button" data-toggle="collapse" data-target="#cart-collapse" aria-expanded="true" aria-controls="cart-collapse"><i class="fas fa-times"></i></button>
    <div class="cart-collapse-details" id="cart-collapse-details">
        <?php
        $cart = $cartBase->id;
        $cartRows = $cartBase->getRowsByCart($cart["id"]);
        ?>
        <?php if (array_filter($cartRows)) : ?>
            <form class="cart-collapse-list" id="cart-collapse-list" method="POST">
                <div class="row">
                    <?php foreach ($cartRows as $row) : ?>
                        <div id="product-<?php echo $row['id']; ?>" class="col-12 col-md-6 col-lg-4 col-xl-3 collapse-list-product">
                            <div class="product-img">
                                <?php if (isset($row['url']) && $row['url'] != '') : ?>
                                    <img class="img-fluid" src="<?php echo $row['url']; ?>" alt="<?php echo $row['name']; ?>" />
                                <?php else : ?>
                                    <img class="img-fluid" src="data-customer/themes/default/img/segnaposto.jpg" alt="<?php echo $row['name']; ?>" />
                                <?php endif; ?>
                            </div>
                            <?php if ($row['name']) : ?>
                                <h4 class="product-name"><?php echo $row['name']; ?></h4>
                            <?php endif; ?>
                            <div class="product-quantity">
                                <input class="form-control form-control-sm" id="quantity-<?php echo $row["id"]; ?>" type="number" name="quantity-<?php echo $row["id"]; ?>" step="1" min="1" value="<?php echo $row['quantity']; ?>" />
                            </div>
                            <div class="product-price">
                                <span><?php echo number_format($row["price"], 2); ?> &euro;</span>
                            </div>
                            <button class="btn delete" id="delete-<?php echo $row['id']; ?>"><i class="fas fa-times"></i></button>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php $this->writeTokenHtml(); ?>
                <input type="hidden" value="savedata" name="command">
                <input type="hidden" value="1" name="status">
                <input type="hidden" value="<?php echo $row['id']; ?>" name="id">
                <input type="hidden" value="<?php echo $cart["id"]; ?>" name="cartid">
                <input type="hidden" value="<?php echo $cart["total"]; ?>" name="total">
                <input type="hidden" value="<?php echo $row["id"]; ?>" name="id_product">
                <input type="hidden" value="<?php echo $row["price"]; ?>" name="p_price">
                <input type="hidden" name="consent1" value="true">
            </form>
            <div class="d-flex justify-content-between align-items-center main-cart-total" id="main-cart-total">
                <h5 class="cart-total-text">
                    <span><?php $this->transPE('cart.cart.total'); ?></span>
                    <strong><?php echo number_format($cart["total"], 2); ?> &euro;</strong>
                </h5>
                <a class="btn btn-primary" href="<?php echo $this->path->getUrlFor('cart', 'cart/index'); ?>">Vai al carrello</a>
            </div>
        <?php endif; ?>
    </div>
</section>