<div class="col-12 col-sm-12 col-md-4 col-lg-3 product-filters-container">
    <button class="btn btn-primary w-100 mobile-filter-trigger">
        Filtri
        <i class="fas fa-sliders-h"></i>
    </button>
    <div class="product-filters">
        <div class="close-filter">
            <span></span>
        </div>
        <form id="search2" method="POST" class="search-form">
            <h3 class="section-title">ricerca e filtri</h3>
            <div class="filter-field clear-filter">
                <button class="btn btn-search" id="clean-filter"><?php $this->transPE('product.search.clean'); ?></button>
            </div>
            <div class="filter-field filter-cat mt-4">
                <h5>Ricerca libera</h5>
                <?php
                if (isset($this->filters)) {
                    $selValue = \EOS\System\Util\ArrayHelper::getStr($this->filters, 'search');
                } else {
                    $selValue = '';
                } ?>
                <input type="text" class="form-control" placeholder="Cerca" aria-label="Cerca" name="search" aria-describedby="search" value="<?php echo $selValue; ?>">
            </div>
            <div class="filter-field clear-filter">
                <button class="btn btn-search" id="submit-search2"><?php $this->transPE('product.search.search'); ?><i class="fas fa-search"></i></button>
            </div>
            <div class="filter-field filter-cat mt-4">
                <h5>Categoria</h5>
                <select class="custom-select filter-select" name="category">
                    <option value="0">Seleziona Categoria</option>
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'category');
                    } else {
                        $selValue = 0;
                    }
                    if (!empty($this->categories)) {
                        foreach ($this->categories as $category) {
                            $selected = $selValue == $category['id'] ? ' selected' : '';
                    ?>
                            <option value="<?php echo $category['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($category['name']); ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="filter-field filter-price">
                <h5>Prezzo</h5>
                <select class="custom-select filter-select" name="price">
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getStr($this->filters, 'price');
                    } else {
                        $selValue = '0-1000';
                    }
                    if (isset($this->user->groups) && !in_array(42, $this->user->groups)) { ?>
                        <option value="0-10000" <?php echo ($selValue == '0-10000') ? 'selected' : ''; ?>>Seleziona fascia di prezzo</option>
                        <option value="0-7" <?php echo ($selValue == '0-7') ? 'selected' : ''; ?>>0 - 7€</option>
                        <option value="7-10" <?php echo ($selValue == '7-10') ? 'selected' : ''; ?>>7 - 10€</option>
                        <option value="10-15" <?php echo ($selValue == '10-15') ? 'selected' : ''; ?>>10 - 15€</option>
                        <option value="15-20" <?php echo ($selValue == '15-20') ? 'selected' : ''; ?>>15 - 20€</option>
                        <option value="20-30" <?php echo ($selValue == '20-30') ? 'selected' : ''; ?>>20 - 30€</option>
                        <option value="30-50" <?php echo ($selValue == '30-50') ? 'selected' : ''; ?>>30 - 50€</option>
                        <option value="50-10000" <?php echo ($selValue == '50-1000') ? 'selected' : ''; ?>>> 50€</option>
                    <?php } else { ?>
                        <option value="0-10000" <?php echo ($selValue == '0-10000') ? 'selected' : ''; ?>>Seleziona fascia di prezzo</option>
                        <option value="0-15" <?php echo ($selValue == '0-15') ? 'selected' : ''; ?>>0 - 15€</option>
                        <option value="15-25" <?php echo ($selValue == '15-25') ? 'selected' : ''; ?>>15 - 25€</option>
                        <option value="25-35" <?php echo ($selValue == '25-35') ? 'selected' : ''; ?>>25 - 35€</option>
                        <option value="35-50" <?php echo ($selValue == '35-50') ? 'selected' : ''; ?>>35 - 50€</option>
                        <option value="50-10000" <?php echo ($selValue == '50-1000') ? 'selected' : ''; ?>>> 50€</option>
                    <?php } ?>
                </select>
            </div>
            <div class="filter-field filter-producer">
                <h5>Produttore</h5>
                <select class="custom-select filter-select" name="manufacturer">
                    <option value="0">Seleziona Produttore</option>
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'manufacturer');
                    } else {
                        $selValue = 0;
                    }
                    if (!empty($this->manufacturer)) {
                        foreach ($this->manufacturer as $manufacturer) {
                            $selected = $selValue == $manufacturer['id'] ? ' selected' : '';
                    ?>
                            <option value="<?php echo $manufacturer['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($manufacturer['name']); ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="filter-field filter-producer">
                <h5>Annata</h5>
                <select class="custom-select filter-select" name="vintage">
                    <option value="0">Seleziona Annata</option>
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'vintage');
                    } else {
                        $selValue = 0;
                    }
                    if (!empty($this->manufacturer)) {
                        foreach ($this->vintage as $vintage) {
                            $selected = $selValue == $vintage['id'] ? ' selected' : '';
                    ?>
                            <option value="<?php echo $vintage['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($vintage['name']); ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="filter-field filter-producer">
                <h5>Nazione</h5>
                <select class="custom-select filter-select" name="country">
                    <option value="0">Seleziona Nazione</option>
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'country');
                    } else {
                        $selValue = 0;
                    }
                    if (!empty($this->manufacturer)) {
                        foreach ($this->country as $country) {
                            $selected = $selValue == $country['id'] ? ' selected' : '';
                    ?>
                            <option value="<?php echo $country['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($country['name']); ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="filter-field filter-producer">
                <h5>Regione</h5>
                <select class="custom-select filter-select" name="region">
                    <option value="0">Seleziona Regione</option>
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'region');
                    } else {
                        $selValue = 0;
                    }
                    if (!empty($this->region)) {
                        foreach ($this->region as $region) {
                            $selected = $selValue == $region['id'] ? ' selected' : '';
                    ?>
                            <option value="<?php echo $region['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($region['name']); ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="filter-field filter-producer">
                <h5>Vitigno</h5>
                <select class="custom-select filter-select" name="vineyards">
                    <option value="0">Seleziona Vitigno</option>
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'vineyards');
                    } else {
                        $selValue = 0;
                    }
                    if (!empty($this->vineyards)) {
                        foreach ($this->vineyards as $vineyards) {
                            $selected = $selValue == $vineyards['id'] ? ' selected' : '';
                    ?>
                            <option value="<?php echo $vineyards['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($vineyards['name']); ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="filter-field filter-producer">
                <h5>Formato</h5>
                <select class="custom-select filter-select" name="size">
                    <option value="0">Seleziona formato</option>
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'size');
                    } else {
                        $selValue = 0;
                    }
                    if (!empty($this->size)) {
                        foreach ($this->size as $size) {
                            $selected = $selValue == $size['id'] ? ' selected' : '';
                    ?>
                            <option value="<?php echo $size['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($size['name']); ?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="filter-field filter-price">
                <h5>Tipologia prodotto</h5>
                <select class="custom-select filter-select" name="typecondition">
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getStr($this->filters, 'typecondition');
                    } else {
                        $selValue = '0';
                    }
                    ?>
                    <option value="0" <?php echo ($selValue == '0') ? 'selected' : ''; ?>>Standard</option>
                    <?php if (isset($this->user->groups) && !in_array(42, $this->user->groups)) { ?>
                        <option value="1" <?php echo ($selValue == '1') ? 'selected' : ''; ?>>Campione</option>
                        <option value="2" <?php echo ($selValue == '2') ? 'selected' : ''; ?>>Annata esaurimento</option>
                    <?php } ?>
                </select>
            </div>
            <?php if (isset($this->user->groups) && !in_array(42, $this->user->groups)) { ?>
                <div class="filter-field filter-producer">
                    <h5>Cheap&Chic</h5>
                    <select class="custom-select filter-select" name="selection">
                        <option value="0">Seleziona</option>
                        <?php
                        if (isset($this->filters)) {
                            $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'selection');
                        } else {
                            $selValue = 0;
                        }
                        if (!empty($this->selection)) {
                            foreach ($this->selection as $selection) {
                                $selected = $selValue == $selection['id'] ? ' selected' : '';
                        ?>
                                <option value="<?php echo $selection['id']; ?>" <?php echo $selected; ?>><?php $this->printHtml($selection['name']); ?></option>
                        <?php }
                        }
                        ?>
                    </select>
                </div>
            <?php } ?>
            <div class="filter-field filter-price">
                <h5>Novità/Promo/Focus</h5>
                <select class="custom-select filter-select" name="extrainfo">
                    <?php
                    if (isset($this->filters)) {
                        $selValue = \EOS\System\Util\ArrayHelper::getStr($this->filters, 'extrainfo');
                    } else {
                        $selValue = '0';
                    }
                    ?>
                    <option value="0" <?php echo ($selValue == '0') ? 'selected' : ''; ?>>Seleziona</option>
                    <option value="1" <?php echo ($selValue == '1') ? 'selected' : ''; ?>>Novità</option>
                    <?php if (isset($this->user->groups) && !in_array(42, $this->user->groups)) { ?>
                        <option value="2" <?php echo ($selValue == '2') ? 'selected' : ''; ?>>Focus</option>
                        <option value="3" <?php echo ($selValue == '3') ? 'selected' : ''; ?>>Promo</option>
                    <?php } ?>
                </select>
            </div>
        </form>
    </div>
</div>