<?php
$page_url = $this->path->getPageUrl();
$queryParams = $this->request->getQueryParams();
if (isset($queryParams['order'])) {
    unset($queryParams['order']);
}
if (isset($queryParams['pagesize'])) {
    unset($queryParams['pagesize']);
}

$page_url .= empty($queryParams) ? '?page=1' : '?' . http_build_query($queryParams);
?>
<script>
    $(document).ready(function() {
        $(".featured-carousel").owlCarousel({
            nav: false,
            dots: true,
            loop: true,
            responsive: {
                0: {
                    items: 2
                },
                576: {
                    items: 2
                },
                992: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        });
        winResize();
        $(window).resize(winResize);

        $(".mobile-filter-trigger").click(function(e) {
            e.preventDefault();
            if (!$(".product-filters").hasClass("open")) {
                $(".product-filters").addClass("open");
            }
        });

        $(".close-filter").click(function(e) {
            e.preventDefault();
            if ($(".product-filters").hasClass("open")) {
                $(".product-filters").removeClass("open");
            }
        });

        function winResize() {
            setTimeout(positionOverflow, 200);
        }

        function positionOverflow() {
            $(".product-list .overflow").css("height", $(".product-list .product-filters").height() + 130);
        }

        <?php if (!isset($this->user)) { ?>
            name = 'confirmadult';
            var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
            if (match[2] != 1) {
                $('#adultModal').modal('show');
            }
        <?php } ?>
    });

    $(function() {
        var jSelect = $('.single-product .product-select');
        jSelect.on('change', function() {
            var jOption = $(this).find('option:selected');
            var jRoot = $(this).closest('.single-product');
            if (jOption.length === 1 && jRoot.length === 1) {
                var dpStr = jOption.attr('data-product');
                var pInfo = JSON.parse(dpStr);
                jRoot.find('.product-image img').attr('src', pInfo['url-0']).attr('alt', pInfo['name']);
                jRoot.find('.product-image').attr('href', pInfo['url']);
                jRoot.find('.product-image').removeClass("img-new").removeClass("img-sale").removeClass("img-promo").removeClass("img-sample");
                <?php if (isset($this->user->groups) && !in_array(42, $this->user->groups)) { ?>
                    if (pInfo['extra-display-info'] == 1) {
                        jRoot.find('.product-image').addClass("img-new");
                    } else if (pInfo['extra-display-info'] == 2) {
                        jRoot.find('.product-image').addClass("img-sale");
                    } else if (pInfo['extra-display-info'] == 3) {
                        jRoot.find('.product-image').addClass("img-promo");
                    }
                <?php } ?>
                if (pInfo['type-condition'] == 1) {
                    jRoot.find('.product-image').addClass("img-sample");
                }
                jRoot.find('.product-manufacturer').text(pInfo['manufacturer-name']);
                jRoot.find('.product-url').attr('href', pInfo['url']);
                jRoot.find('.product-name').text(pInfo['name']);
                jRoot.find('.product-price').text(pInfo['price-formatted']);
                var jAdd = jRoot.find('.btn-add-to-cart');
                var jAddSample = jRoot.find('.add-sample-to-cart');
                //jAdd.removeClass('disable-buy');
                //jAddSample.removeClass('disable-buy');
                var budget = <?php echo number_format((float)$this->budget, 2, '.', ''); ?>;
                if (pInfo['disabled'] || budget < pInfo['price-formatted']) {
                    jAdd.addClass('disable-buy');
                    jAddSample.addClass('disable-buy');
                }
                var min = 0;
                var value = 1;
                if (pInfo["min-quantity"] > 1) {
                    min = pInfo["min-quantity"];
                    value = pInfo["min-quantity"];
                } else if ((pInfo["stock-quantity"] + pInfo["stock-incoming"]) == 0) {
                    min = 0;
                    value = 0;
                }
                jRoot.find('.quantity').val(value);
                jRoot.find('.quantity').attr({
                    "min": min
                });
                jRoot.find('input[name=id_product]').val(pInfo['id']);
                jRoot.find('input[name=maxqty]').val(pInfo['stock-quantity'] + pInfo['stock-incoming']);
                jRoot.find('input[name=p_price]').val(pInfo['price']);
                jRoot.find('.product-stock .stock span').text(pInfo['stock-quantity-str']);
                jRoot.find('.product-stock .incoming span').text(pInfo['stock-incoming-str']);
                //console.log(pInfo['stock-incoming-str']);
            }
        });
        jSelect.trigger('change');
    });

    $(function() {
        var data;
        $('.add').on('click', function() {
            $id = jQuery(this).attr("id");
            $id = $id.replace("add", "");
            $qty = $('#' + $id).find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
            }
            var data = {
                id: $id,
                qty: $qty.val(),
                command: 'updaterow',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            updateRow(data);
        });
        $('.minus').on('click', function() {
            $id = jQuery(this).attr("id");
            $id = $id.replace("minus", "");
            $qty = $('#' + $id).find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
            }
            var data = {
                id: $id,
                qty: $qty.val(),
                command: 'updaterow',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            updateRow(data);
        });
        $('.delete').on('click', function() {
            $id = jQuery(this).attr("id");
            $id = $id.replace("delete", "");
            var data = {
                id: $id,
                command: 'deleterow',
                '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
            };
            deleteRow(data);
        });

    });

    function changeview() {
        var optPage = $(".custom-select-pagesize");
        var optPageValue = optPage.val();
        var optView = $(".custom-select-view");
        var optViewValue = optView.val();
        var url = '<?php echo $page_url; ?>' + '&order=' + optViewValue + '&pagesize=' + optPageValue;
        location.href = url;
    }

    function updateRow(data) {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        });
        setTimeout(function() {
            window.location.reload(false);
        }, 200);
    }

    function deleteRow(data) {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        });
        runAjax(data, function(json) {
            $('#cart-collapse-details').load(document.URL + ' #cart-collapse-details');
        });
    }

    function saveData($form_id, $field_id) {
        var data = $($form_id).serializeFormJSON();
        if ($form_id)
            data.quantity = $($form_id).find(".quantity").val();
        data.total = parseFloat(data.total) + (parseInt(data.quantity) * parseFloat(data.p_price));
        $('input[name="total"]').val(data.total);
        runAjax(data, function(json) {
            $('#cart-collapse-details').load(document.URL + ' #cart-collapse-details');
            $('#successModal').modal('toggle');
        });
    }

    function saveDataSample($form_id, $field_id) {
        var data = $($form_id).serializeFormJSON();
        if ($form_id)
            data.quantity = 1;
        data.total = parseFloat(data.total) + (parseInt(data.quantity) * parseFloat(data.p_price));
        data.issample = 1;
        $('input[name="total"]').val(data.total);
        runAjax(data, function(json) {
            $('#successModal').modal('toggle');
        });
    }

    function runAjax(data, resultEvent) {
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json"
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    function search() {
        location.href = '<?php echo $this->path->getUrlFor('Product', 'search/index'); ?>?' + $('#search').serialize();
    }

    function search2() {
        location.href = '<?php echo $this->path->getUrlFor('Product', 'search/index'); ?>?' + $('#search2').serialize();
    }

    function checkQty($id) {
        var check = true;
        $($id).find('.quantity').attr("value", $($id).find('.quantity').val());
        var maxqty = parseInt($($id).find("input[name=maxqty]").val());
        var minqty = parseInt($($id).find("input[name=minqty]").val());
        var outofstock = parseInt($($id).find("input[name=outofstock]").val());
        var qty = parseInt($($id).find('.quantity').val());
        if (outofstock != 1) {
            if (maxqty < qty || maxqty == 0) {
                $('#qtyModal').modal('show');
                $($id).find('.quantity').val(maxqty);
                check = false;
            }
            if (minqty > 1 && (minqty > qty || !Number.isInteger(qty / minqty))) {
                $('#qtyminModal').modal('show');
                $($id).find('.quantity').val(minqty);
                check = false;
            }
        }
        return check;
    }

    $("#submit-search").on("click", function(event) {
        event.preventDefault();
        search();
    });

    $("#submit-search2").on("click", function(event) {
        event.preventDefault();
        search2();
    });

    $(".filter-select").change(function() {
        event.preventDefault();
        search2();
    });

    $(".btn-add-to-cart").on("click", function(event) {
        event.preventDefault();
        var shop = <?php echo $this->shop; ?>;
        if (shop == 1) {
            $id = jQuery(this).attr("id");
            var parentId = $(this).parent().attr("id");
            if (parentId.indexOf("evidence") != -1) {
                $id = $id.replace("add-", "#add-to-cart-evidence-");
            } else {
                $id = $id.replace("add-", "#add-to-cart-");
            }
            if (checkQty($id)) {
                saveData($id);
                $("#cart-collapse").collapse('show');
            }
        } else {
            $('#shopModal').modal('show');
        }
    });

    $(".add-sample-to-cart").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        var parentId = $(this).parent().attr("id");
        if (parentId.indexOf("pro") != -1) {
            $id = $id.replace("add-sample-cart", "#add-sample");
        } else {
            $id = $id.replace("add-sample-cart", "#add-sample-pro");
        }
        saveDataSample($id);
    });

    $(".delete").on("click", function(event) {
        event.preventDefault();
        $id = jQuery(this).attr("id");
        $id = $id.replace("delete-", "");
        var data = {
            id: $id,
            command: 'deleterow',
            '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
        };
        deleteRow(data);
    });

    $("#clean-filter").click(function(event) {
        event.preventDefault();
        $("#search2 select").val(0);
        $("#search2 .filter-price select").val(0 - 10000);
        $("#submit-search2").trigger("click");
    });

    $(".products-layout .layout-grid").click(function() {
        if ($(".layout").hasClass("row-layout")) {
            $(".layout").removeClass("row-layout");
            var cookieValue = $.cookie("product-layout");
            if (cookieValue == "row") {
                $.removeCookie('product-layout', {
                    path: '/'
                });
            }
        }
        if (!$(".products-layout .layout-grid").hasClass("active")) {
            $(".products-layout .layout-grid").addClass("active");
            $(".products-layout .layout-row").removeClass("active");
        }
    });

    $("#adult").on("click", function(event) {
        event.preventDefault();
        var date = new Date();
        date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
        document.cookie = "confirmadult=1" + expires + "; path=/";
        setTimeout(function() {
            window.location.reload(false);
        }, 200);
    });

    $("#cart-collapse-list .product-quantity input").on("change", function(event) {
        event.preventDefault();
        saveData('#cart-collapse-list', $(this).attr("id"));
        //$("#cart-collapse").collapse('show');
    });

    $(".product-quantity input").on("change", function() {
        $(this).attr("value", $(this).val());
        var maxqty = parseInt($(this).parent().parent().find("input[name=maxqty]").val());
        var minqty = parseInt($(this).parent().parent().find("input[name=minqty]").val());
        var outofstock = parseInt($(this).parent().parent().find("input[name=outofstock]").val());
        var qty = parseInt($(this).val());
        if (outofstock != 1) {
            if (maxqty < qty || maxqty == 0) {
                $('#qtyModal').modal('show');
                $(this).val(maxqty);
            }
            if (minqty > 1 && (minqty > qty || !Number.isInteger(qty / minqty))) {
                $('#qtyminModal').modal('show');
                $(this).val(minqty);
            }
        }
    });




    $(".products-layout .layout-row").click(function() {
        if (!$(".layout").hasClass("row-layout")) {
            $(".layout").addClass("row-layout");
            var cookieValue = $.cookie("product-layout");
            if (cookieValue != "row") {
                $.cookie('product-layout', 'row', {
                    expires: 7,
                    path: '/'
                });
            }
        }
        if (!$(".products-layout .layout-row").hasClass("active")) {
            $(".products-layout .layout-row").addClass("active");
            $(".products-layout .layout-grid").removeClass("active");
        }
    });

    $('.custom-select-view').on('change', function(event) {
        //var optionSelected = $(".custom-select-view", this);
        //var valueSelected = this.value;
        //select = jQuery(this).select();
        //url = '<?php echo $page_url; ?>' + valueSelected;
        //location.href = url;
        changeview();
    });

    $('.custom-select-pagesize').on('change', function(event) {
        //var optionSelected = $(".custom-select-pagesize", this);
        //var valueSelected = this.value;
        //select = jQuery(this).select();
        //url = '<?php echo $page_url; ?>' + valueSelected;
        //location.href = url;
        changeview();
    });

    if ($.cookie("product-layout") == "row") {
        $(".layout").addClass("row-layout");
        $(".layout-grid").removeClass("active");
        $(".layout-row").addClass("active");
    }
</script>