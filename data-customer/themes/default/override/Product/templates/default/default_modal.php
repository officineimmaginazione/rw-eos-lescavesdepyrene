<!-- Modal -->
<div class="modal fade alert-modal" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Il prodotto è stato aggiungo al carrello</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<!-- No prodotto in carrelo se quantità maggiore di disp attuale -->
<div class="modal fade alert-modal" id="qtyModal" tabindex="-1" role="dialog" aria-labelledby="qtyModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Attenzione! La quantità inserita è maggiore della disponibilità.<br>Inseriscene il massimo numero in stock o verifica se disponibile una nuova annata.</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="qtyminModal" tabindex="-1" role="dialog" aria-labelledby="qtyminModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Attenzione! La quantità inserita è minore della quantità minima o non è un multiplo di essa.</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="shopModal" tabindex="-1" role="dialog" aria-labelledby="shopModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Per procedere all'acquisto è necessario effettuare l'accesso</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
            <div class="btn-action">
                <a href="https://www.lescaves.it/it/come-acquistare/" class="btn btn-primary">Come acquistare</a>
                <a href="<?php echo $this->path->urlFor('account', ['access', 'login']); ?>" class="btn btn-primary">Come accedere</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="adultModal" tabindex="-1" role="dialog" aria-labelledby="adultModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Devi essere maggiorenne per poter accedere a questa sezione del sito</div>
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button> -->
            <div class="btn-action">
                <a href="#" id="adult" class="btn btn-primary">Sì, sono maggiorenne</a>
                <a href="https://www.google.com/search?q=giocattoli" class="btn btn-primary">Non sono maggiorenne</a>
            </div>
        </div>
    </div>
</div>