
<?php

if ($this->pageCount > 1)
{
//    $page_url = $_SERVER['REQUEST_URI'];
//    if  (strpos($page_url, '?page=') != false) {
//        $url = preg_replace('/?page=.*/', '', $page_url);
//        $page_url_add = 'page=';
//    } else
//    {
//        $url = preg_replace('/&page=.*/', '', $page_url);
//        $page_url_add = '&page=';
//    }
//    
//    
//    $explodedUrl = explode('?', $page_url);
//    if(!isset($explodedUrl[1])){
//        $explodedUrl[1] = "/";
//    }
//    list($file, $parameters) = $explodedUrl;
//    parse_str($parameters, $output);
//    unset($output['page']); // remove the make parameter
//    $page_url = $file . '?' . http_build_query($output).$page_url_add;
    $page_url = $this->path->getPageUrl();
    $queryParams = $this->request->getQueryParams();
    if (isset($queryParams['page']))
    {
      unset($queryParams['page']);   
    }
    $page_url .=  empty($queryParams) ? '?page=' : '?' . http_build_query($queryParams).'&page=';
    
    $adjacents = "2";

    if (isset($this->pageIndex))
        $page = $this->pageIndex;

    $page = ($page == 0 ? 1 : $this->pageIndex);
    //$start = ($page - 1) * $per_page;

    $prev = $page - 1;
    $next = $page + 1;
    $setLastpage = $this->pageCount;
    $lpm1 = $setLastpage - 1;

    $paging = "";
    if ($setLastpage > 1)
    {
        $paging .= '<nav aria-label = "Page navigation">';
        $paging .= '<ul id = "product-catalog-pagination" class = "pagination">';
        if ($setLastpage < 5 + ($adjacents * 2))
        {
            for ($counter = 1; $counter <= $setLastpage; $counter++)
            {
                if ($counter == $page)
                    $paging .= "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                else
                    $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$counter'>$counter</a></li>";
            }
        }
        elseif ($setLastpage > 3 + ($adjacents * 2))
        {
            if ($page < 1 + ($adjacents * 2))
            {
                for ($counter = 1; $counter < 2 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $paging .= "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                    else
                        $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$counter'>$counter</a></li>";
                }
                $paging .= "<li class='page-item dot'><span>...</span></li>";
                $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$lpm1'>$lpm1</a></li>";
                $paging .= "<li class='page-item'><a class='page-link' ref='{$page_url}$setLastpage'>$setLastpage</a></li>";
            }
            elseif ($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}1'>1</a></li>";
                $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}2'>2</a></li>";
                $paging .= "<li class='page-item dot'><span>...</span></li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $paging .= "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                    else
                        $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$counter'>$counter</a></li>";
                }
                $paging .= "<li class='page-item dot'><span>...</span></li>";
                $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$lpm1'>$lpm1</a></li>";
                $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$setLastpage'>$setLastpage</a></li>";
            }
            else
            {
                $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}1'>1</a></li>";
                $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}2'>2</a></li>";
                $paging .= "<li class='page-item dot'><span>...</span></li>";
                for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++)
                {
                    if ($counter == $page)
                        $paging .= "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                    else
                        $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$counter'>$counter</a></li>";
                }
            }
        }
/*
        if ($page < $counter - 1)
        {
            $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$next'><i class='fas fa-step-forward'></i></a></li>";
            $paging .= "<li class='page-item'><a class='page-link' href='{$page_url}$setLastpage'><i class='fas fa-fast-forward '></i></a></li>";
        }
*/
        $paging .= "</ul>\n";
        $paging .= "</nav>\n";
    }

    echo $paging;
}