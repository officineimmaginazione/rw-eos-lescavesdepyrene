<section class="product-list">
    <div class="overflow"></div>
    <div class="container">
        <div class="row">
            <?php $this->includePartial('filter'); ?>
            <div class="col-12 col-sm-12 col-md-8 col-lg-9 product-category-container">
                <div class="row">
                    <div class="col-12 products-options">
                        <h5>Tutti i prodotti</h5>
                        <div class="row">
                            <div class="col-6 col-sm-5 products-number">
                                <p>Visualizza</p>
                                <select class="custom-select custom-select-pagesize">
                                    <option value="12" <?php echo ($this->pagesize == '12') ? 'selected' : ''; ?>>12</option>
                                    <option value="24" <?php echo ($this->pagesize == '24') ? 'selected' : ''; ?>>24</option>
                                    <option value="36" <?php echo ($this->pagesize == '36') ? 'selected' : ''; ?>>36</option>
                                </select>
                                <p>per pagina</p>
                            </div>
                            <div class="col-12 col-sm-2 order-12 order-sm-0 products-layout">
                                <div class="layout-row"></div>
                                <div class="layout-grid active"></div>
                            </div>
                            <div class="col-6 col-sm-5 products-order">
                                <p>Ordina per </p>
                                <select class="custom-select custom-select-view">
                                    <option value="name-asc" <?php echo ($this->orderby == 'name-asc') ? 'selected' : ''; ?>>Nome (A - Z)</option>
                                    <option value="name-desc" <?php echo ($this->orderby == 'name-desc') ? 'selected' : ''; ?>>Nome (Z - A)</option>
                                    <option value="price-asc" <?php echo ($this->orderby == 'expecteddate-desc') ? 'selected' : ''; ?>>Prezzo (basso - alto)</option>
                                    <option value="price-desc" <?php echo ($this->orderby == 'expecteddate-asc') ? 'selected' : ''; ?>>Prezzo (alto - basso)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row layout">
                    <?php
                    if (count($this->products) != 0) {
                        foreach ($this->products as $product) {
                            $this->singleProduct = $product;
                            $this->includePartial('singleproduct');
                        }
                    } else {
                        echo '<h4 class="no-prod">Non sono presenti prodotti<br>con i paramentri inseriti<br>Cerca meglio che qualcosa lo trovi</h4>';
                    }
                    ?>
                </div>
                <div class="pagination">
                    <?php $this->includePartial('pagination'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (isset($this->viewagent) && $this->viewagent) { ?>
    <section class="evidence">
        <div class="container">
            <h2 class="section-title">in evidenza</h2>
            <div class="owl-carousel featured-carousel">
                <?php
                $this->cont = 0;
                foreach ($this->productsfeatured as $product) {
                    $this->singleProductFeatured = $product;
                    $this->includePartial('singleproductfeatured');
                }
                ?>

            </div>
        </div>
    </section>
<?php } ?>