<?php
$product = $this->singleProductFeatured;
$defImg = $this->path->getThemeUrl() . 'img/wine-default.jpg';
$class = "";
if (isset($this->user->groups) && !in_array(42, $this->user->groups)) {
    if (\EOS\System\Util\ArrayHelper::getValue($product, 'type-condition') == 1) {
        $class = " img-sample";
    } elseif (\EOS\System\Util\ArrayHelper::getValue($product, 'extra-display-info') == 1) {
        $class = " img-new";
    } elseif (\EOS\System\Util\ArrayHelper::getValue($product, 'extra-display-info') == 2) {
        $class = " img-sale";
    } elseif (\EOS\System\Util\ArrayHelper::getValue($product, 'extra-display-info') == 3) {
        $class = " img-promo";
    }
}
if ($this->cont < 12) {
?>
    <div class="single-product">
        <div class="featured-product-container">
            <div class="featured-image product-image align-items-center <?php echo $class; ?>">
                <img class="img-fluid" src="<?php $this->printHtml(\EOS\System\Util\ArrayHelper::getStr($product, 'url-0', $defImg)); ?>" alt="<?php echo $product['name']; ?>" />
            </div>
            <div class="featured-info-box">
                <h5 class="product-manufacturer mt-auto"><?php $this->printHtml($product['manufacturer-name']); ?></h5>
                <a class="product-url" href="<?php $this->printHtml($product['url']); ?>">
                    <h4 class="product-name"><?php $this->printHtml($product['name']); ?></h4>
                </a>
                <h6 class="product-price"><?php $this->printHtml($product['price-formatted']); ?></h6>
                <div class="row no-gutters mb-0">
                    <div class="col-12 col-sm-6 attr-year">
                        <div class="d-flex align-items-center product-quantity">
                            <div>
                                <?php
                                $vItems = $product['group-items'];
                                if (count($vItems) === 1) {
                                    $this->printHtml($product['attribute-name-1']);
                                } else {
                                ?>
                                    <select class="product-select custom-select">
                                        <?php
                                        foreach ($vItems as $item) {
                                            $item['url-0'] = \EOS\System\Util\ArrayHelper::getStr($item, 'url-0', $defImg);
                                            $sel = (int) $item['id'] === (int) $product['id'] ? ' selected' : '';
                                            printf(
                                                '<option value="%s"%s data-product="%s">%s</option>' . "\n",
                                                $item['id'],
                                                $sel,
                                                $this->encodeHtml(\EOS\System\Util\ArrayHelper::toJSON($item)),
                                                $this->encodeHtml($item['attribute-name-1'])
                                            );
                                        }
                                        ?>
                                    </select>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 qta">
                        <form class="form-container add-to-cart" id="add-to-cart-evidence-<?php echo $product["id"]; ?>" method="POST">
                            <?php $cart = (new \EOS\Components\Cart\Classes\CartBase($this->controller->container))->getCart(); ?>
                            <div class="product-quantity">
                                <input class="quantity" min="0" type="number" value="1" />
                            </div>
                            <button class="btn btn-primary btn-circle btn-add-to-cart <?php echo ($product["disabled"]) ? "disable-buy" : ''; ?>" id="add-<?php echo $product["id"]; ?>"><i class="fas fa-shopping-cart"></i></button>
                            <?php $this->writeTokenHtml(); ?>
                            <input type="hidden" value="saveadddata" name="command">
                            <input type="hidden" value="1" name="status">
                            <input type="hidden" value="<?php echo ($product["stock-quantity"] + $product["stock-incoming"]); ?>" name="maxqty">
                            <input type="hidden" value="<?php echo $product["min-quantity"]; ?>" name="minqty">
                            <input type="hidden" value="<?php echo $cart["id"]; ?>" name="cartid">
                            <input type="hidden" value="<?php echo $product["out-of-stock"]; ?>" name="outofstock">
                            <input type="hidden" value="<?php echo $cart["total"]; ?>" name="total">
                            <input type="hidden" value="<?php echo $product["id"]; ?>" name="id_product">
                            <input type="hidden" value="<?php echo $product["price"]; ?>" name="p_price">
                            <input type="hidden" value="<?php echo ($this->viewagent) ? 1 : 0; ?>" name="viewagent">
                            <input type="hidden" name="consent1" value="true">
                        </form>
                    </div>
                </div>
            </div>
            <?php if ($this->viewagent) { ?>
                <form class="form-container add-sample" id="add-sample-<?php echo $product["id"]; ?>">
                    <?php
                    $disable = "";
                    if ($product["disabled"] || $this->budget < $product["price"]) {
                        $disable = "disable-buy";
                    }
                    ?>
                    <button class="add-sample-to-cart <?php echo $disable; ?>" id="add-sample-cart-<?php echo $product["id"]; ?>">Aggiungi a campionatura</button>
                    <?php $this->writeTokenHtml(); ?>
                    <input type="hidden" value="saveadddata" name="command">
                    <input type="hidden" value="1" name="status">
                    <input type="hidden" value="<?php echo $cart["id"]; ?>" name="cartid">
                    <input type="hidden" value="<?php echo $cart["total"]; ?>" name="total">
                    <input type="hidden" value="<?php echo $product["id"]; ?>" name="id_product">
                    <input type="hidden" value="<?php echo $product["price"]; ?>" name="p_price">
                    <input type="hidden" value="<?php echo ($this->viewagent) ? 1 : 0; ?>" name="viewagent">
                    <input type="hidden" name="consent1" value="true">
                </form>
            <?php } ?>
            <div class="row no-gutters product-stock featured-stock">
                <!-- <div class="stock">in stock | <span><?php echo $product['stock-quantity']; ?></span></div>
                                <div class="incoming">in arrivo | <span><?php echo $product['stock-incoming']; ?></span></div> -->
                <div class="col-12 stock"><?php echo $product['stock-quantity-str']; ?></div>
                <div class="col-12 incoming"><?php echo (!in_array(42, $this->user->groups) && isset($product['stock-incoming-str']) && $product['stock-incoming-str'] != '') ? $product['stock-incoming-str'] : '&nbsp;'; ?></div>
            </div>
        </div>
    </div>
<?php
    $this->cont++;
}
