<?php
$this->includePartial('cart');
//$this->includePartial('header');
$this->includePartial('main');
$this->includePartial('footer');
$this->addScriptLink($this->path->getThemeUrl() . "js/eos.util.js");

$this->startCaptureScript();
$this->includePartial('js');
$tokenJson = sprintf('"%s":"%s",', $this->session->getTokenName(), $this->session->getTokenValue());
$this->endCaptureScript();
