<section class="container product-advice">
    <h3 class="advice-title">Ti potrebbero anche interessare</h3>
    <div class="row product-advice-list">
        <?php foreach ($this->relatedproduct as $product) : ?>
        <div class="col-6 col-sm-6 col-lg-3 related-product">
            <div class="advice-img-box">
                <img class="img-fluid" src="<?php echo (isset($product['url-0']) && $product['url-0'] != '') ? $product['url-0'] : "data-customer/themes/default/img/wine-default.jpg"; ?>" alt="" />
            </div>
            <div class="advice-info-box">
                <h5><?php $this->printHtml($product["manufacturer"]); ?></h5>
                <a class="product-url" href="<?php echo "/it/product/catalog/item/" . $product['id']; ?>"><h4><?php echo $product["name"]; ?></h4></a>
                <h6 class="product-price"><?php echo number_format((float)$product['price'], 2, ',', ''); ?> &euro;</h6>
            </div>
            <div class="row no-gutters advice-stock">
                <div class="stock">in stock | <span><?php echo $product['stock-quantity']; ?></span></div>
                <div class="incoming">in arrivo | <span><?php echo $product['stock-incoming']; ?></span></div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</section>

