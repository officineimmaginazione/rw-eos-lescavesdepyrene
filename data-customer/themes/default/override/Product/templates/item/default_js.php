<script>
    function runAjax(data, resultEvent) {
        $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
                data: JSON.stringify(data),
                contentType: "application/json"
            })
            .done(function(json) {
                if (json.result == true) {
                    resultEvent(json);
                } else {
                    bootbox.alert(json.message);
                }
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert(err);
            });
    }

    function saveData($form_id, $field_id) {
        var data = $($form_id).serializeFormJSON();
        if ($form_id)
            data.quantity = $($form_id).find(".quantity").val();

        data.total = parseFloat(data.total) + (parseInt(data.quantity) * parseFloat(data.p_price));
        $('input[name="total"]').val(data.total);
        runAjax(data, function(json) {
            $('#cart-collapse-details').load(document.URL + ' #cart-collapse-details');
            //location.href = json.redirect;
            $('#successModal').modal('toggle');
        });
    }

    function updateRow(data) {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        });
        setTimeout(function() {
            window.location.reload(false);
        }, 200);
    }

    function deleteRow(data) {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '<?php echo $this->path->getUrlFor('Cart', 'cart/ajaxcommand'); ?>',
            data: JSON.stringify(data),
            contentType: "application/json"
        });
        runAjax(data, function(json) {
            $('#cart-collapse-details').load(document.URL + ' #cart-collapse-details');
            //location.href = json.redirect;
        });
    }

    function searchBook() {
        location.href = '<?php echo $this->path->getUrlFor('Product', 'search/index'); ?>?' + $('#search').serialize();
    }

    function checkQty($id) {
        var check = true;
        $($id).find('.quantity').attr("value", $($id).find('.quantity').val());
        var maxqty = parseInt($($id).find("input[name=maxqty]").val());
        var minqty = parseInt($($id).find("input[name=minqty]").val());
        var outofstock = parseInt($($id).find("input[name=outofstock]").val());
        var qty = parseInt($($id).find('.quantity').val());
        if (outofstock != 1) {
            if (maxqty < qty || maxqty == 0) {
                $('#qtyModal').modal('show');
                $($id).find('.quantity').val(maxqty);
                check = false;
            }
            if (minqty > 1 && (minqty > qty || !Number.isInteger(qty / minqty))) {
                $('#qtyminModal').modal('show');
                $($id).find('.quantity').val(minqty);
                check = false;
            }
        }
        return check;
    }

    $(function() {
        var jSelect = $('.single-product .product-select');
        jSelect.on('change', function() {
            var jOption = $(this).find('option:selected');
            var jRoot = $(this).closest('.single-product');
            if (jOption.length === 1 && jRoot.length === 1) {
                var dpStr = jOption.attr('data-product');
                var pInfo = JSON.parse(dpStr);
                jRoot.find('.product-main-image').attr('src', pInfo['url-0']).attr('alt', pInfo['name']);
                jRoot.find('.product-image').removeClass("img-new").removeClass("img-sale").removeClass("img-promo").removeClass("img-sample");
                if (pInfo['extra-display-info'] == 1) {
                    jRoot.find('.product-image').addClass("img-new");
                } else if (pInfo['extra-display-info'] == 2) {
                    jRoot.find('.product-image').addClass("img-sale");
                } else if (pInfo['extra-display-info'] == 3) {
                    jRoot.find('.product-image').addClass("img-promo");
                }
                if (pInfo['type-condition'] == 1) {
                    jRoot.find('.product-image').addClass("img-sample");
                }
                jRoot.find('.product-manufacturer').text(pInfo['manufacturer-name']);
                jRoot.find('.product-name').text(pInfo['name']);
                var jPrice = jRoot.find('.product-price');
                jPrice.text(pInfo['price-formatted']);
                var descrshort = pInfo['descr-short-1'] !== '' ? pInfo['descr-short-1'] : pInfo['descr-short-1'];
                var descr = pInfo['descr-1'] !== '' ? pInfo['descr-1'] : pInfo['descr-1'];
                jRoot.find('.product-short-desc').html(descrshort);
                jRoot.find('.product-long-desc').html(descr);
                var jAdd = jRoot.find('.btn-add-to-cart');
                jAdd.removeClass('disable-buy');
                if (pInfo['disabled']) {
                    jAdd.addClass('disable-buy');
                    jPrice.hide();
                } else {
                    jPrice.show();
                }
                var min = 0;
                var value = 1;
                if (pInfo["min-quantity"] > 1) {
                    min = pInfo["min-quantity"];
                    value = pInfo["min-quantity"];
                } else if ((pInfo["stock-quantity"] + pInfo["stock-incoming"]) == 0) {
                    min = 0;
                    value = 0;
                }
                jRoot.find('.quantity').val(value);
                jRoot.find('.quantity').attr({
                    "min": min
                });
                jRoot.find('input[name=id_product]').val(pInfo['id']);
                jRoot.find('input[name=p_price]').val(pInfo['price']);
                var jAttribute = jRoot.find('.attributes-list');
                jAttribute.html('');
                for (var i = 1; i <= 8; i++) {
                    if (pInfo.hasOwnProperty('attribute-name-' + i)) {
                        var jR = $('<div class="row no-gutters single-attribute"> ' +
                            '<div class="title"></div><div class="content"></div></div>');
                        jR.find('.title').text(pInfo['attribute-group-' + i]);
                        jR.find('.content').text(pInfo['attribute-name-' + i]);
                        jAttribute.append(jR);
                    }
                }
                jRoot.find('.product-stock .stock span').text(pInfo['stock-quantity']);
                jRoot.find('.product-stock .incoming span').text(pInfo['stock-incoming']);
            }
        });
        jSelect.trigger('change');
    });

    $(".btn-add-to-cart").on("click", function(event) {
        event.preventDefault();
        var shop = <?php echo $this->shop; ?>;
        if (shop == 1) {
            var id = $(this).attr("id");
            id = id.replace("add-", "");
            if (checkQty('#add-to-cart-' + id)) {
                saveData('#add-to-cart-' + id);
                $("#cart-collapse").collapse('show');
            }

        } else {
            $('#shopModal').modal('show');
        }
    });

    $(".delete").on("click", function(event) {
        event.preventDefault();
        //alert('test');
        $id = jQuery(this).attr("id");
        $id = $id.replace("delete-", "");
        var data = {
            id: $id,
            command: 'deleterow',
            '<?php echo $this->session->getTokenName() ?>': '<?php echo $this->session->getTokenValue() ?>'
        };
        deleteRow(data);
        //saveData('#add-to-cart');
        //$("#cart-collapse").collapse('show');
    });

    $("#clean-filter").click(function(event) {
        event.preventDefault();
        $("#search")[0].reset();
    });

    $("#cart-collapse-list .product-quantity input").on("change", function(event) {
        event.preventDefault();
        saveData('#cart-collapse-list', $(this).attr("id"));
        //$("#cart-collapse").collapse('show');
    });

    $("#submit-search").on("click", function(event) {
        event.preventDefault();
        searchBook();
    });

    $(".product-quantity input").on("change", function() {
        $(this).attr("value", $(this).val());

        var maxqty = parseInt($(this).parent().parent().find("input[name=maxqty]").val());
        var minqty = parseInt($(this).parent().parent().find("input[name=minqty]").val());
        var qty = parseInt($(this).val());
        if (maxqty < qty) {
            $('#qtyModal').modal('show');
        }
        if (minqty > 1 && (minqty > qty || !Number.isInteger(qty / minqty))) {
            $('#qtyminModal').modal('show');
            $(this).val(minqty);
        }
    });
</script>