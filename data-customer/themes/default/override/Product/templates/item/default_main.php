<section class="container main-product" id="main-product">
    <div class="row single-product">
        <div class="d-flex align-items-center justify-content-center col-12 col-md-4 col-lg-5 mb-5 mb-md-0 text-center">
            <img class="img-fluid product-main-image" src="<?php $this->printHtml(\EOS\System\Util\ArrayHelper::getStr($this->data, 'url-0', $this->path->getThemeUrl() . 'img/wine-default.jpg')); ?>" alt="" />
        </div>
        <div class="col-12 col-md-8 col-lg-7">
            <?php //print_r($this->data); 
            ?>
            <div class="row no-gutters">
                <div class="d-flex align-items-center col-12 col-sm-6 product-category"><?php echo $this->data['category']; ?></div>
                <div class="col-12 col-sm-6 back-to-category"><a class="btn btn-primary" href="<?php echo $this->path->getUrlFor('Product', ''); ?>catalog/category/<?php echo $this->data["id_category"] ?>">TORNA ALLA CATEGORIA</a></div>
            </div>
            <div class="product-info">

                <h2 class="product-manufacturer"><?php $this->printHtml(\EOS\System\Util\ArrayHelper::getStr($this->data, 'manufacturer-name')); ?></h2>

                <h1 class="product-name"><?php echo $this->data["name-1"]; ?></h1>

                <div class="product-desc">
                    <?php if (isset($this->user) && !in_array(42, $this->user->groups)) { ?>
                        <p class="product-short-desc"><?php echo ($this->data["descr-short-1"] != "") ? $this->data["descr-short-1"] : ''; ?></p>
                    <?php } ?>
                    <p class="product-long-desc"><?php echo ($this->data["descr-1"] != "") ? $this->data["descr-1"] : ''; ?></p>
                </div>

                <?php $manufacturer_desc = \EOS\System\Util\ArrayHelper::getStr($this->data, "manufacturer-description"); ?>
                <?php if ($manufacturer_desc != "") { ?>
                    <div class="product-manufacturer-desc">
                        <a class="btn-collapse collapsed" data-toggle="collapse" href="#manufacturer" role="button" aria-expanded="false" aria-controls="manufacturer">
                            <h6><i class="fa" aria-hidden="true"></i> Produttore</h6>
                        </a>
                        <div class="collapse" id="manufacturer">
                            <div class="card card-body">
                                <?php echo $manufacturer_desc; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="attributes-list">
                    <?php for ($i = 1; $i < 8; $i++) { ?>
                        <?php if (array_key_exists("attribute-name-" . $i, $this->data)) { ?>
                            <div class="row no-gutters single-attribute">
                                <div class="title"><?php $this->printHtml($this->data["attribute-group-" . $i]); ?></div>
                                <div class="content"><?php $this->printHtml($this->data["attribute-name-" . $i]); ?></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="row no-gutters">
                    <div class="attr-year">
                        <div class="d-flex align-items-center product-quantity">
                            <div>
                                <?php
                                $vItems = $this->data['group-items'];
                                if (count($vItems) === 1) {
                                    $this->printHtml(\EOS\System\Util\ArrayHelper::getStr($this->data, 'attribute-name-1'));
                                } else {
                                ?>
                                    <select class="product-select custom-select">
                                        <?php
                                        foreach ($vItems as $item) {
                                            $item['url-0'] = \EOS\System\Util\ArrayHelper::getStr($item, 'url-0', $this->path->getThemeUrl() . 'img/wine-default.jpg');
                                            $sel = (int)$item['id'] === (int)$this->data['id'] ? ' selected' : '';
                                            if (isset($this->user->groups) && !in_array(42, $this->user->groups)) {
                                                if ($this->typecondition == 1) {
                                                    if (strpos($item['attribute-name-1'], 'C') !== false) {
                                                        printf(
                                                            '<option value="%s"%s data-product="%s">%s</option>' . "\n",
                                                            $item['id'],
                                                            $sel,
                                                            $this->encodeHtml(\EOS\System\Util\ArrayHelper::toJSON($item)),
                                                            $this->encodeHtml($item['attribute-name-1'])
                                                        );
                                                    }
                                                } else {
                                                    printf(
                                                        '<option value="%s"%s data-product="%s">%s</option>' . "\n",
                                                        $item['id'],
                                                        $sel,
                                                        $this->encodeHtml(\EOS\System\Util\ArrayHelper::toJSON($item)),
                                                        $this->encodeHtml($item['attribute-name-1'])
                                                    );
                                                }
                                            } else {
                                                if (strpos($item['attribute-name-1'], 'C') === false) {
                                                    printf(
                                                        '<option value="%s"%s data-product="%s">%s</option>' . "\n",
                                                        $item['id'],
                                                        $sel,
                                                        $this->encodeHtml(\EOS\System\Util\ArrayHelper::toJSON($item)),
                                                        $this->encodeHtml($item['attribute-name-1'])
                                                    );
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="qta">
                        <form class="form-container add-to-cart" id="add-to-cart-<?php echo $this->data["id"]; ?>" method="POST">
                            <?php $cart = (new \EOS\Components\Cart\Classes\CartBase($this->controller->container))->getCart(); ?>
                            <?php
                            $min = 0;
                            $value = 1;
                            if ($this->data["min-quantity"] > 1) {
                                $min = $this->data["min-quantity"];
                                $value = $this->data["min-quantity"];
                            }
                            ?>
                            <div class="product-quantity">
                                <input class="quantity" min="<?php echo $min; ?>" type="number" value="<?php echo $value; ?>" />
                            </div>
                            <button class="btn btn-primary btn-circle btn-add-to-cart" id="add-<?php echo $this->data["id"]; ?>"><i class="fas fa-shopping-cart"></i></button>
                            <?php $this->writeTokenHtml();
                            ?>
                            <input type="hidden" value="saveadddata" name="command">
                            <input type="hidden" value="1" name="status">
                            <input type="hidden" value="<?php echo $cart["id"]; ?>" name="cartid">
                            <input type="hidden" value="<?php echo $cart["total"]; ?>" name="total">
                            <input type="hidden" value="<?php echo $this->data["id"]; ?>" name="id_product">
                            <input type="hidden" value="<?php echo $this->data["price"]; ?>" name="p_price">
                            <input type="hidden" value="<?php echo ($this->data["stock-quantity"] + $this->data["stock-incoming"]); ?>" name="maxqty">
                            <input type="hidden" value="<?php echo $this->data["min-quantity"]; ?>" name="minqty">
                            <input type="hidden" value="<?php echo ($this->viewagent) ? 1 : 0; ?>" name="viewagent">
                            <input type="hidden" name="consent1" value="true">
                        </form>
                    </div>
                </div>
                <h5 class="product-price" style="<?php echo $this->data['disabled'] ? 'display:none' : ''; ?>"><?php echo $this->encodeHtml($this->data['price-formatted']); ?></h5>
                <div class="row no-gutters product-stock">
                    <div class="stock">in stock | <span><?php echo  $this->data['stock-quantity']; ?></span></div>
                    <?php echo (isset($this->user) && !in_array(42, $this->user->groups)) ? '<div class="incoming">in arrivo | <span>' . $this->data['stock-incoming'] . '</span></div>' : ''; ?>
                </div>
            </div>
            <!-- <a class="btn btn-primary data-sheet"  href="#">SCARICA SCHEDA VINO</a> -->
        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade alert-modal" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Il prodotto è stato aggiungo al carrello</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="qtyModal" tabindex="-1" role="dialog" aria-labelledby="qtyModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Attenzione! La quantità inserita è maggiore della disponibilità attuale.<br>Verifica altre se disponibili altre annate.</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="qtyminModal" tabindex="-1" role="dialog" aria-labelledby="qtyminModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Attenzione! La quantità inserita è minore della quantità minima o non è un multiplo di essa.</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
        </div>
    </div>
</div>

<div class="modal fade alert-modal" id="shopModal" tabindex="-1" role="dialog" aria-labelledby="shopModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">Per procedere all'acquisto è necessario effettuare l'accesso</div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><img src="data-customer/themes/default/img/close-black.png" alt="close" /></button>
            <div class="btn-action">
                <a href="https://www.lescaves.it/it/come-acquistare/" class="btn btn-primary">Come acquistare</a>
                <a href="<?php echo $this->path->urlFor('account', ['access', 'login']); ?>" class="btn btn-primary">Accedi</a>
            </div>
        </div>
    </div>
</div>