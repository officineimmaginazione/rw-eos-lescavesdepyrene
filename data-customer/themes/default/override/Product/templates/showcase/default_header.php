<!-- sostituire con una section -->
<section class="admin-main-header">
    <div class="d-flex flex-row align-items-center align-content-center">
        <h1 class="main-header-title"><?php $this->transPE('product.catalog.title'); ?></h1>
        <div class="input-group mr-3 search-form">

            </div>
        <a class="btn btn-primary btn-filter" data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="collapseExample">
            Filtri avanzati
        </a>
        <a  href="/it/cart/cart/index/" id="btn-new" class="btn  btn-primary btn-circle"><i class="fas fa-shopping-cart"></i></a>
        <div class="dropdown ml-2">
            <button class="btn btn-default btn-circle btn-outline dropdown-toggle" type="button" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user"></i>
                <span class="sr-only"><?php $this->printHTML($this->userlabel); ?></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUser">
                <li class="dropdown-item"><a href="<?php echo $this->path->getUrlFor('Account', 'user/edit'); ?>">Profilo</a></li>
                <li class="dropdown-item"><a href="<?php echo $this->path->getUrlFor('Account', 'user/logout'); ?>">Log out</a></li>
            </ul>
        </div>					
    </div>
    <div class="collapse" id="filter">
        <form id="search" method="POST" class="search-form">
            <div class="row">
                <div class="form-group col-12 col-sm-6">
                    <label class="control-label" for="search"><?php $this->transPE('product.search.title'); ?></label>
                    <input type="text" class="form-control" placeholder="Cerca" aria-label="Cerca" name="search" aria-describedby="search">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12 col-sm-6">
                    <label class="control-label" for="category"><?php $this->transPE('product.search.category'); ?></label>
                    <select class="form-control custom-select" name="category">
                        <option value="0"> - <?php $this->transPE('product.search.select.category'); ?> - </option>
                        <?php
                        if(isset($this->filters)) {
                            $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'category');
                        }
                        else {
                            $selValue = 0;
                        }
                        foreach ($this->categories as $category)
                        {
                            $selected = $selValue == $category['id'] ? ' selected' : '';
                            ?>
                            <option value="<?php echo $category['id']; ?>"<?php echo $selected; ?>><?php $this->printHtml($category['name']); ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label class="control-label" for="manufacturer"><?php $this->transPE('product.search.manufacturer'); ?></label>
                    <select class="form-control custom-select" name="manufacturer">
                        <option value="0"> - <?php $this->transPE('product.search.select.manufacturer'); ?> - </option>
                        <?php
                        if(isset($this->filters)) {
                            $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'manufacturer');
                        }
                        else {
                            $selValue = 0;
                        }
                        foreach ($this->manufacturer as $manufacturer)
                        {
                            $selected = $selValue == $manufacturer['id'] ? ' selected' : '';
                            ?>
                            <option value="<?php echo $manufacturer['id']; ?>"<?php echo $selected; ?>><?php $this->printHtml($manufacturer['name']); ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12 col-sm-6">
                    <label class="control-label" for="author"><?php $this->transPE('product.search.author'); ?></label>
                    <select class="form-control custom-select" name="author">
                        <option value="0"> - <?php $this->transPE('product.search.select.author'); ?> - </option>
                        <?php
                        if(isset($this->filters)) {
                            $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'author');
                        }
                        else {
                            $selValue = 0;
                        }
                        foreach ($this->author as $author)
                        {
                            $selected = $selValue == $author['id'] ? ' selected' : '';
                            ?>
                            <option value="<?php echo $author['id']; ?>"<?php echo $selected; ?>><?php $this->printHtml($author['name']); ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label class="control-label" for="cedola"><?php $this->transPE('product.search.cedola'); ?></label>
                    <select class="form-control custom-select" name="cedola">
                        <option value="0"> - <?php $this->transPE('product.search.select.cedola'); ?> - </option>
                        <?php
                        if(isset($this->filters)) {
                            $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'cedola');
                        }
                        else {
                            $selValue = 0;
                        }
                        foreach ($this->cedola as $cedola)
                        {
                            $selected = $selValue == $cedola['id'] ? ' selected' : '';
                            ?>
                            <option value="<?php echo $cedola['id']; ?>"<?php echo $selected; ?>><?php $this->printHtml($cedola['name']); ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12 col-sm-6">
                    <label class="control-label" for="series"><?php $this->transPE('product.search.series'); ?></label>
                    <select class="form-control custom-select" name="series">
                        <option value="0"> - <?php $this->transPE('product.search.select.series'); ?> - </option>
                        <?php
                        if(isset($this->filters)) {
                            $selValue = \EOS\System\Util\ArrayHelper::getInt($this->filters, 'series');
                        }
                        else {
                            $selValue = 0;
                        }
                        foreach ($this->series as $serie)
                        {
                            $selected = $selValue == $serie['id'] ? ' selected' : '';
                            ?>
                            <option value="<?php echo $serie['id']; ?>"<?php echo $selected; ?>><?php $this->printHtml($serie['name']); ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label class="control-label" for="type"><?php $this->transPE('product.search.expecteddate'); ?></label>
                    <div class="input-group date" id="from-date-box">
                        <span class="input-group-addon"><i class="fa fa-calendar-check-o"></i></span>
                        <input type="text" name="expecteddate" id="expecteddate" class="form-control" maxlength="10" value="<?php echo (isset($this->filters)) ? $this->printHtml(EOS\System\Util\ArrayHelper::getStr($this->filters, 'date')) : ''; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12 text-right">
                    <button class="btn btn-search" id="submit-search"><?php $this->transPE('product.search.search'); ?></button>
                </div>
            </div>
        </form>
    </div>
</section>