<script type="text/javascript" async>
	jQuery(document).ready(function ($) {
		$('.carousel-1').owlCarousel({
				loop: true,
				margin: 30,
				nav: true,
				navContainer: '.owl-nav-1',
				dotsContainer: '.dots-nav',
				navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
				stagePadding: 30,
				responsive:{
					0: {items:1},
					480: {items:2},
					768: {items:3},
					992: {items:4},
					1200: {items:5}
				}
		});
        
        $('.carousel-2').owlCarousel({
				loop: true,
				margin: 30,
				nav: true,
				navContainer: '.owl-nav-2',
				dotsContainer: '.dots-nav',
				navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
				stagePadding: 30,
				responsive:{
					0: {items:1},
					480: {items:2},
					768: {items:3},
					992: {items:4},
					1200: {items:5}
				}
		});
        
        $('.carousel-3').owlCarousel({
				loop: true,
				margin: 30,
				nav: true,
				navContainer: '.owl-nav-3',
				dotsContainer: '.dots-nav',
				navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
				stagePadding: 30,
				responsive:{
					0: {items:1},
					480: {items:2},
					768: {items:3},
					992: {items:4},
					1200: {items:5}
				}
		});
	});
</script>