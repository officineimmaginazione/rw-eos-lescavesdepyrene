<section class="main-product-list">
    <h3 class="product-carousel-title"><a href="/it/product/search/index/?search=&category=0&manufacturer=0&author=0&cedola=3215&series=0&expecteddate=">Novità</a></h3>
	<div class="product-carousel-container">
		<div class="custom-nav owl-nav-1"></div>
		<div class="owl-carousel carousel-1 product-carousel">		
			<?php foreach ($this->productsnew as $product) : ?>
				<a class="d-block item" href="<?php echo "/it/product/catalog/item/" . $product['id']; ?>">
					<div class="item-image">
                        <img class="img-fluid" src="<?php echo (isset($product['url-0']) && $product['url-0'] != '') ? $product['url-0'] : 'data-customer/themes/default/img/segnaposto.jpg'; ?>" alt="<?php echo $product['name']; ?>" />
					</div>
					<h5 class="item-author"><?php echo (isset($product["attribute-name-1"]) && $product["attribute-name-1"] != '') ? $product["attribute-name-1"] : ''; ?></h5>
					<h4 class="item-name"><?php echo $product['name']; ?></h4>
				</a>
			<?php endforeach; ?>
		</div>
	</div>
    <h3 class="product-carousel-title"><a href="/it/product/search/index/?search=&isbn=&category=0&manufacturer=0&author=0&cedola=4174&series=0&expecteddate=">Codici Hoepli</a></h3>
	<div class="product-carousel-container">
		<div class="custom-nav owl-nav-2"></div>
		<div class="owl-carousel carousel-2 product-carousel">		
			<?php foreach ($this->productscode as $product) : ?>
				<a class="d-block item" href="<?php echo "/it/product/catalog/item/" . $product['id']; ?>">
					<div class="item-image">
                        <img class="img-fluid" src="<?php echo (isset($product['url-0']) && $product['url-0'] != '') ? $product['url-0'] : 'data-customer/themes/default/img/segnaposto.jpg'; ?>" alt="<?php echo $product['name']; ?>" />
					</div>
					<h5 class="item-author"><?php echo (isset($product["attribute-name-1"]) && $product["attribute-name-1"] != '') ? $product["attribute-name-1"] : ''; ?></h5>
					<h4 class="item-name"><?php echo (isset($product["name"]) && $product["name"] != '') ? $product["name"] : ''; ?></h4>
				</a>
			<?php endforeach; ?>
		</div>
	</div>
    <h3 class="product-carousel-title"><a href="/it/product/catalog/category/0/">Catalogo</a></h3>
	<div class="product-carousel-container">
		<div class="custom-nav owl-nav-3"></div>
		<div class="owl-carousel carousel-3 product-carousel">		
			<?php foreach ($this->products as $product) : ?>
				<a class="d-block item" href="<?php echo "/it/product/catalog/item/" . $product['id']; ?>">
					<div class="item-image">
                        <img class="img-fluid" src="<?php echo (isset($product['url-0']) && $product['url-0'] != '') ? $product['url-0'] : 'data-customer/themes/default/img/segnaposto.jpg'; ?>" alt="<?php echo $product['name']; ?>" />
					</div>
					<h5 class="item-author"><?php echo (isset($product["attribute-name-1"]) && $product["attribute-name-1"] != '') ? $product["attribute-name-1"] : ''; ?></h5>
					<h4 class="item-name"><?php echo (isset($product["name"]) && $product["name"] != '') ? $product["name"] : ''; ?></h4>
				</a>
			<?php endforeach; ?>
		</div>
	</div>
</section>
