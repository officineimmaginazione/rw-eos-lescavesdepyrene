<?php
$this->renderList = function ($list, $level, $currentPath, $langPrefix) {
	if (!empty($list)) {
		$cl = $this->cssClass;
		$ex = '';
		$id = $this->cssId;
		
		if(strpos($this->cssClass, "onside") !== false) {
			$cl .= 'nav flex-column nav-pills';
			$ex = 'role="tablist" aria-orientation="vertical"';
			printf('<div class="%s" id="menu-depth-%s" %s>', $cl, $level, $ex) . "\n";

			foreach ($list as $i => $a) {

				$class = 'nav-link';
				$extra = '';

				$path = $a['path'];
				if ($a['custom']) {
					if (empty($langPrefix)) {
						if (EOS\System\Routing\PathHelper::removeSlash($path) == $currentPath) {
							$class .= ' active';
						}
					}
					else {
						// Verifico che se un url interno lo collego al menu
						if (!\EOS\System\Util\StringHelper::startsWith($path, 'http')) {
							$paus = EOS\System\Routing\PathHelper::removeSlash(EOS\System\Routing\PathHelper::removeExtension($path));
							if (\EOS\System\Util\StringHelper::startsWith($paus, $langPrefix)) {
								$paus = substr($paus, strlen($langPrefix));
							}
							$paus = EOS\System\Routing\PathHelper::removeSlash($paus);
							if ($paus == EOS\System\Routing\PathHelper::removeSlash($currentPath)) {
								$class .= ' active';
							}
						}
					}
				}
				else {
					if ($path == $currentPath) {
						$class .= ' active';
					}
					if (!empty($path)) {
						$pa = \EOS\System\Routing\PathHelper::explodeSlash($path);
						$cp = $pa[0];
						array_shift($pa);
						$path = $this->view->path->urlFor($cp, $pa);
					}
				}
				if (!empty($class)) {
					$class = ' class="' . $class . '"';
				}

				if(!empty($a["children"])) {
					$extra = sprintf('id="#pills-%s-tab" data-toggle="pill" role="tab" aria-controls="pills-%s"', $i, $i, $i);
					printf('<a %s href="#pills-%s" %s><i class="%s"></i><span>%s</span></a>', $class, $i, $extra, $icons[$i], $this->view->encodeHtml($a['title']));
				}
				else {
					printf('<a %s href="%s" %s><i class="%s"></i><span>%s</span></a>', $class, $this->view->encodeHtml($path), $extra, $icons[$i], $this->view->encodeHtml($a['title']));
				}
			}

			echo "</div>\n";

			$level += 1;

			echo '<div class="tab-content"  id="menu-depth-' . $level . '">';

			foreach ($list as $i => $a) {

				if(!empty($a["children"])) {

					$class = 'tab-pane fade';
					$extra = sprintf('id="pills-%s" role="tabpanel" aria-labelledby="pills-%s-tab"', $i, $i);

					$path = $a['path'];
					if ($a['custom']) {
						if (empty($langPrefix)) {
							if (EOS\System\Routing\PathHelper::removeSlash($path) == $currentPath) {
								$class .= ' show active';
							}
						}
						else {
							// Verifico che se un url interno lo collego al menu
							if (!\EOS\System\Util\StringHelper::startsWith($path, 'http')) {
								$paus = EOS\System\Routing\PathHelper::removeSlash(EOS\System\Routing\PathHelper::removeExtension($path));
								if (\EOS\System\Util\StringHelper::startsWith($paus, $langPrefix)) {
									$paus = substr($paus, strlen($langPrefix));
								}
								$paus = EOS\System\Routing\PathHelper::removeSlash($paus);
								if ($paus == EOS\System\Routing\PathHelper::removeSlash($currentPath)) {
									$class .= ' active';
								}
							}
						}
					}
					else {
						if ($path == $currentPath) {
							$class .= ' show active';
						}
						if (!empty($path)) {
							$pa = \EOS\System\Routing\PathHelper::explodeSlash($path);
							$cp = $pa[0];
							array_shift($pa);
							$path = $this->view->path->urlFor($cp, $pa);
						}
					}

					printf('<div class="%s" %s>', $class, $extra);

					foreach ($a["children"] as $child) {
						$path = $child['path'];
						printf('<a href="%s">%s</a>', $this->view->encodeHtml($path), $this->view->encodeHtml($child['title']));

					}

					echo "</div>";
				}

			}
			
			echo "</div>";
			
		}
		else {
        if ($level == 0)
        {
            $cl = $this->cssClass;
        } else
        {
            if ($this->dropdown)
            {
                $cl = 'dropdown-menu';
            }
        }
        if($id==='')
        {
            printf('<ul class="%s">', $cl) . "\n";
        }
        else   
        {
            printf('<ul id="%s" class="%s">', $id, $cl) . "\n";
        }
        
        foreach ($list as $i => $li)
        {
            $classLI = 'nav-item';
            $extraA = '';
            if (!empty($li['children']))
            {
                $classLI .= 'dropdown';
                $extraA = sprintf(' aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle"');
            }
            $path = $li['path'];
            if ($li['custom'])
            {
                if (empty($langPrefix))
                {
                    if (EOS\System\Routing\PathHelper::removeSlash($path) == $currentPath)
                    {
                        $classLI .= ' active';
                    }
                } else
                {
                    // Verifico che se un url interno lo collego al menu
                    if (!\EOS\System\Util\StringHelper::startsWith($path, 'http'))
                    {
                        $paus = EOS\System\Routing\PathHelper::removeSlash(EOS\System\Routing\PathHelper::removeExtension($path));
                        if (\EOS\System\Util\StringHelper::startsWith($paus, $langPrefix))
                        {
                            $paus = substr($paus, strlen($langPrefix));
                        }
                        $paus = EOS\System\Routing\PathHelper::removeSlash($paus);
                        if ($paus == EOS\System\Routing\PathHelper::removeSlash($currentPath))
                        {
                            $classLI .= ' active';
                        }
                    }
                }
            } else
            {
                if ($path == $currentPath)
                {
                    $classLI .= ' active';
                }
                if (!empty($path))
                {
                    $pa = \EOS\System\Routing\PathHelper::explodeSlash($path);
                    $cp = $pa[0];
                    array_shift($pa);
                    $path = $this->view->path->urlFor($cp, $pa);
                }
            }
            if (!empty($classLI))
            {
                $classLI = ' class="' . $classLI . '"';
            }
            printf("<li%s>", $classLI);
						
						if(strpos($this->cssClass, "onside") === false) {
							printf('<a class="nav-link" href="%s"%s><span>%s</span></a>', $this->view->encodeHtml($path), $extraA, $this->view->encodeHtml($li['title']));
						}
						else {
							printf('<a class="nav-link" href="%s"%s>%s</a>', $this->view->encodeHtml($path), $extraA, $this->view->encodeHtml($li['title']));
						}

            if (!empty($li['children']))
            {
                $method = $this->renderList;
                $method($li['children'], $level + 1, $this->currentPath, $this->langPrefix);
            }
            echo "</li>";
        }
        echo "</ul>\n";
    }
		
		

	}
};
if (!empty($this->data) && (isset($this->data['menu']))) {
	$this->currentPath = \EOS\System\Routing\PathHelper::removeSlash($this->view->request->getUri()->getPath());
	$this->langPrefix = '';
	$icons = null;
	if ($this->view->controller->container->get('currentParams')['multiLanguage']) {
		$this->langPrefix = $this->view->controller->container->get('language')->getCurrentISO();
	}
	if(strpos($this->cssClass, "noicons") === false) {
		if(strpos($this->cssClass, "onside") !== false) {
			$icons = array("logo-icon", "fas fa-th-large", "fas fa-id-card-alt", "fas fa-book", "fas fa-file-import", "fas fa-file-export", "fas fa-cog");
		}
		else {
			$icons = array("icon-chi-siamo", "icon-clienti", "icon-agenti", "icon-editori", "icon-contatti", "icon-login");
		}
	}
	$method = $this->renderList;
	$method($this->data['menu'], 0, $this->currentPath, $this->langPrefix, $icons);
}