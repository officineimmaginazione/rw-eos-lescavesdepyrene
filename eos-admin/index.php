<?php

define('_EOS_PATH_FOLDER_', '');
define('_EOS_PATH_APP_', __DIR__ . DIRECTORY_SEPARATOR);
define('_EOS_PATH_APP_OVERRIDE_', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data-customer' . DIRECTORY_SEPARATOR . 'override' . DIRECTORY_SEPARATOR . 'eos-admin' . DIRECTORY_SEPARATOR);
require_once '..' . DIRECTORY_SEPARATOR . 'eos-config' . DIRECTORY_SEPARATOR . 'eos.defines.php';

if (_EOS_PROFILER_)
{
    $profiler = new \EOS\System\Util\Profiler();
    $profiler->start();
} else
{
    $profiler = null;
}

$app = new \EOS\System\Application(
    ['dbRouter' => false,
    'multiLanguage' => true,
    'languageType' => 'admin',
    'pageExtension' => '/',
    'profiler' => $profiler,
    'sessionTimeOut' => 20 * 60]);
$app->add(new \EOS\Components\System\Classes\AuthMiddleware($app->getContainer()));
$app->run();
if (_EOS_PROFILER_)
{
    unset($app);
    $profiler->stop('Global');
    $profiler->render();
    unset($profiler);
}