<?php
/**

EcoSoft e Officine Immaginazione Copyright (c) 2015
by Alessandro Savoiardo
www.ecosoft.it - www.officineimmaginazione.com

**/

// Imposto la visualizzazione dei dati in italiano
setlocale(LC_ALL, 'it_IT'); 
date_default_timezone_set('Europe/Rome');

//define('_EOS_PROFILER_', false);
//define('_EOS_DEBUG_', false);

define('_EOS_PROFILER_', false);
define('_EOS_DEBUG_', true);

define('_EOS_PATH_ROOT_', dirname(__DIR__).DIRECTORY_SEPARATOR);
define('_EOS_PATH_SYSTEM_', _EOS_PATH_ROOT_.'eos-system'.DIRECTORY_SEPARATOR);

// Parametri
define('_EOS_PATH_DATA_', _EOS_PATH_ROOT_.'data-customer'.DIRECTORY_SEPARATOR);
define('_EOS_PATH_UPLOADS_', _EOS_PATH_DATA_.'uploads'.DIRECTORY_SEPARATOR);
define('_EOS_PATH_THEMES_', _EOS_PATH_DATA_.'themes'.DIRECTORY_SEPARATOR);
define('_EOS_PATH_CACHE_', _EOS_PATH_DATA_.'--cache'.DIRECTORY_SEPARATOR);

// url
define('_EOS_URL_LIBRARY_', 'library');

if (_EOS_DEBUG_ === true)
{
  error_reporting(E_ALL);
  ini_set("display_errors", 1);
}

require_once('eos.settings.php');

require_once(_EOS_PATH_SYSTEM_.'eos.loader.php');
